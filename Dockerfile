FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine
ENV TZ=America/Sao_Paulo
ENV CORECLR_ENABLE_PROFILING=1
ENV CORECLR_PROFILER={cf0d821e-299b-5307-a3d8-b283c03916dd}
ENV CORECLR_PROFILER_PATH=/app/instana_tracing/CoreProfiler.so
ENV DOTNET_STARTUP_HOOKS=/app/Instana.Tracing.Core.dll
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT false
RUN apk update
RUN apk add --no-cache icu-libs
RUN apk add --no-cache tzdata
ENV LC_ALL pt_BR.UTF-8
ENV LANG pt_BR.UTF-8
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /app
COPY /app .
EXPOSE 80
ENTRYPOINT ["dotnet", "Easynvest.Alcatraz.Api.dll"]