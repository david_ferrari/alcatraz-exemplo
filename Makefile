SHELL := /bin/sh

# runners
# #################################################################################################################

# dotnet
# #################################################################################################################

build:
	dotnet publish -c Release -o app --configfile Nuget.Config 

test-ci:
	dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:Exclude="[*.CrossCutting*]*%2c[*.IoC*]*%2c[*.Data*]*%2c[*xunit*]*"

static analysis:
	dotnet sonarscanner begin /k:"Easynvest.Alcatraz" /d:sonar.cs.opencover.reportsPaths="test/*/coverage.opencover.xml" /d:sonar.coverage.exclusions="**/*.CrossCutting*/**/*.*,***/*.Infra.IoC*/**/*.*,*/*.Infra.Data*/**/*.*,**Test*.cs" && dotnet build --configfile Nuget.Config && dotnet sonarscanner end

deploy:
	kubectl config use-context ${KUBE_CONTEXT} && \
    helm upgrade --install --namespace=${KUBE_NAMESPACE} ${HELM_RELEASE_NAME} --values=${VALUES_YAML} \
    --set-string image.tag=${BUILD_ID}.${GIT_COMMIT} --set environment=${ENVIRONMENT} \
    ${PROGET} --force