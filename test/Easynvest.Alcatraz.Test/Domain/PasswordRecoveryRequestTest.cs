﻿using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Test.Common;
using System;
using Xunit;

namespace Easynvest.Alcatraz.Test.Domain
{
    public class PasswordRecoveryRequestTest
    {
        private PasswordRecoveryRequest _passwordRecoveryRequest;
        private readonly string _email = "email@email.com";
        private string _cpf => "71409232727";
        private Customer _customer;
        private ExpirationDate _expirationDate;
        private readonly int _days = 1;
        private readonly ITimeServer _timerServer = new TimeServerStub(DateTime.Now);

        public PasswordRecoveryRequestTest()
        {
            _customer = new Customer(_cpf, Name.Create(string.Empty), new BirthDate(DateTime.Now), "", "A");
            _expirationDate = new ExpirationDate(_timerServer, _days);
            _passwordRecoveryRequest = PasswordRecoveryRequest.Create(_email, _customer, _expirationDate, _timerServer);
        }

        [Fact]
        public void Should_CreationDateBeEqualsDateTimeNowDate()
        {
            Assert.True(_passwordRecoveryRequest.CreationDate == _timerServer.Current);
        }

        [Fact]
        public void Should_CreateInstanceWithPropertyFillAndIdNull()
        {
            Assert.NotNull(_passwordRecoveryRequest.Hash);
            Assert.NotNull(_passwordRecoveryRequest.Customer.CustomerId);
            Assert.NotNull(_passwordRecoveryRequest.Email);
            Assert.Null(_passwordRecoveryRequest.Id);
        }

        [Fact]
        public void Should_ExpirationDateMatchWithCreationDatePlusDaysAmount()
        {
            var expirationDate = _passwordRecoveryRequest.ExpirationDate.Value;

            Assert.True(_timerServer.Current.AddDays(_days) == expirationDate);
        }
    }
}
