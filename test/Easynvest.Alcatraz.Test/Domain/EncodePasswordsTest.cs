﻿using Easynvest.Alcatraz.Domain.Services;
using Xunit;

namespace Easynvest.Alcatraz.Test.Domain
{
    public class EncodePasswordsTest
    {
        [Fact]
        public void Encode()
        {
            var salt1 = SaltGenerator.Generate(8);
            var output1 = EncodePasswords.Encode("usuario", "senha", salt1.Salt);
            var output2 = EncodePasswords.Encode("usuario", "senha", SaltGenerator.Generate().Salt);
            var output3 = EncodePasswords.Encode("usuario", "senha", salt1.Salt);

            Assert.NotNull(output1);
            Assert.NotNull(output2);
            Assert.NotEqual(output1, output2);
            Assert.Equal(output1, output3);
        }
    }
}
