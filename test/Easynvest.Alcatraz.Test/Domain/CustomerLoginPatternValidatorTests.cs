﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Services;
using Xunit;

namespace Easynvest.Alcatraz.Test.Domain
{
    public class CustomerLoginPatternValidatorTests
    {
        [Theory]
        [InlineData("36635893822@email.com", "EMAIL")]
        [InlineData("366.358.938-22@email.com", "EMAIL")]
        [InlineData("36635893822", "CPF")]
        [InlineData("366.358.938-22", "CPF")]
        [InlineData("123456", "ACCOUNTNUMBER")]
        [InlineData("1234567", "ACCOUNTNUMBER")]
        public void Should_Match_Type(string username, string type)
            => Assert.Equal(CustomerLoginType.GetByValue(type), CustomerLoginPatternValidator.Match(username).Value);

        [Theory]
        [InlineData("12")]
        [InlineData("123456789")]
        [InlineData("12345ABC")]
        [InlineData("email@email")]
        [InlineData("")]
        public void Should_NotMatch_Type(string username) => Assert.Null(CustomerLoginPatternValidator.Match(username).Value);
    }
}
