﻿using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Xunit;

namespace Easynvest.Alcatraz.Test.CrossCutting
{
    public class NameTest
    {
        private const string firstName = "Almah";
        private const string lastName = "Dyring";
        private string fullName = $"{firstName} {lastName}";

        [Fact]
        public void Should_ReturnFirstName_When_NameIsComposed()
        {
            var name = Name.Create($"{firstName} {lastName}");

            Assert.Equal(firstName, name.FirstName);
            Assert.Equal(lastName, name.LastName);
            Assert.Equal(fullName, name.FullName);
        }

        [Fact]
        public void Should_ReturnTheSameString_WhenNameIsSingle()
        {
            var name = Name.Create(firstName);

            Assert.Equal(firstName, name.FirstName);
            Assert.Equal(string.Empty, name.LastName);
            Assert.Equal(firstName, name.FullName);
        }
    }
}
