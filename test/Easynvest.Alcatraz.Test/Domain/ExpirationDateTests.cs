﻿using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Test.Common;
using Xunit;
using System;

namespace Easynvest.Alcatraz.Test.Domain
{
    public class ExpirationDateTests
    {
        [Theory]
        [InlineData(1, false)]
        [InlineData(-1, true)]
        public void Should_ValidateExpirationDate(int milliseconds, bool expected)
        {
            var now = DateTime.Now;
            var expirationDate = now.AddMilliseconds(milliseconds);

            var sut = new ExpirationDate(new TimeServerStub(now), expirationDate);
            var actual = sut.IsExpired;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0, false)]
        [InlineData(-1, false)]
        [InlineData(1, true)]
        public void Should_Validate_When_ConstructorIsCalled(int days, bool expected)
        {
            var sut = new ExpirationDate(new TimeServerStub(DateTime.Now), days);

            var actual = sut.IsValid;

            Assert.Equal(expected, actual);
        }
    }
}