﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Easynvest.Alcatraz.Test.Domain
{
    public class PasswordTests
    {
        private const string NON_ENCODED_PASSWORD = "1Senh@10";
        private const string NON_ENCODED_ELETRONIC_SIGNATURE = "@10senhA";
        private const string NON_ENCODED_PREVIOUS_PASSWORD = "Ant1g@";
        private const string CPF = "74428779585";
        private const string NAME = "Usuário De Teste";
        private Customer _customer;
        private Password _sut;
        private PasswordDetails _details;

        public PasswordTests()
        {
            var currentPassword = EncodePasswords.EncodeString(NON_ENCODED_PASSWORD);
            var currentEletronicSignature = EncodePasswords.EncodeString(NON_ENCODED_ELETRONIC_SIGNATURE);
            var previousPassword = EncodePasswords.EncodeString(NON_ENCODED_PREVIOUS_PASSWORD);
            var passwordsHistory = new List<PasswordHistory>(2)
            {
                new PasswordHistory(previousPassword,null, SecretType.Password),
                new PasswordHistory(currentPassword, null, SecretType.Password),
            };

            _details = new PasswordDetails(currentPassword, currentEletronicSignature, 0, 0, null, null);
            _customer = new Customer(CPF, Name.Create(NAME), new BirthDate(new DateTime(2000, 1, 1)), "", "A");
            _sut = new Password(_customer, passwordsHistory, new List<PasswordHistory>(), _details);
        }

        [Theory]
        [InlineData("ab345", "Senha deve possuir no mínimo 6 caracteres.")]
        [InlineData("123456", "Senha deve possuir letras e números.")]
        [InlineData("123456", "Senha deve possuir no máximo 2 números em sequência.")]
        [InlineData("654xab", "Senha deve possuir no máximo 2 números em sequência.")]
        [InlineData("456xab", "Senha deve possuir no máximo 2 números em sequência.")]
        [InlineData("abcdef", "Senha deve possuir letras e números.")]
        [InlineData("Usuário", "Nome não pode ser usado como senha.")]
        [InlineData("UsuArIo", "Nome não pode ser usado como senha.")]
        [InlineData(NON_ENCODED_PASSWORD, "Não podem ser utilizadas as últimas 6 senhas.")]
        [InlineData(NON_ENCODED_ELETRONIC_SIGNATURE, "A senha de acesso ao portal e assinatura eletrônica não podem ser iguais.")]
        public void Should_RejectWeakPassword(string weakPassword, string expectedMessage)
        {
            var notifications = _sut.TryChangeSecret(weakPassword, SecretType.Password, SecretChangeType.ChangeSecret);

            Assert.NotEqual(0, notifications.Count);
            Assert.NotNull(notifications.FirstOrDefault(n => n.Message.Equals(expectedMessage)));
        }

        [Theory]
        [InlineData("01#02x2000")]
        [InlineData("ab12c3")]
        [InlineData("Usuári0")]
        [InlineData("áíóú10")]
        [InlineData("abc135")]
        public void Should_AcceptStrongPassword(string strongPassword)
        {
            var notifications = _sut.TryChangeSecret(strongPassword, SecretType.Password, SecretChangeType.ChangeSecret);

            Assert.Equal(0, notifications.Count);
            var hash = EncodePasswords.Encode(_sut.Customer.CustomerId, strongPassword, _sut.PasswordDetails.PasswordSalt.Salt);
            Assert.Equal(hash, _sut.PasswordDetails.EncodedPassword);
        }

        [Fact]
        public void Should_AcceptStrongPassword_And_Update_Version()
        {
            var notifications = _sut.TryChangeSecret("01#02x2000", SecretType.Password, SecretChangeType.ChangeSecret);

            Assert.Equal(PasswordVersion.New, _sut.PasswordDetails.PasswordVersion);
            Assert.NotNull(_sut.PasswordDetails.PasswordSalt);
            Assert.Equal(0, notifications.Count);
        }

        [Fact]
        public void Should_Update_Salt_When_Password_Changes()
        {
            var salt = SaltGenerator.Generate();
            _sut.PasswordDetails.PasswordSalt = salt;
            var notifications = _sut.TryChangeSecret("01#02x2000", SecretType.Password, SecretChangeType.ChangeSecret);

            Assert.Equal(PasswordVersion.New, _sut.PasswordDetails.PasswordVersion);
            Assert.NotEqual(salt, _sut.PasswordDetails.PasswordSalt);
            Assert.Equal(0, notifications.Count);
        }

        [Fact]
        public void Should_RejectPassword_When_ItContainsTheBirthDate()
        {
            var notifications = _sut.TryChangeSecret("ab" + _customer.BirthDate.Value?.ToString("ddMMyyyy") + "!", SecretType.Password, SecretChangeType.ChangeSecret);

            Assert.NotEqual(0, notifications.Count);
            Assert.NotNull(notifications.FirstOrDefault(n => n.Message.Equals("Data de aniversário não pode ser usada como senha.")));
        }

        [Fact]
        public void Should_RejectPassword_When_PasswordIsNotDefined()
        {
            var notifications = _sut.TryChangeSecret(string.Empty, SecretType.Password, SecretChangeType.ChangeSecret);

            Assert.NotEqual(0, notifications.Count);
            Assert.NotNull(notifications.FirstOrDefault(n => n.Message.Equals("Senha deve possuir letras e números.")));
        }

        [Fact]
        public void Should_AcceptPassword_After_SixthReset()
        {
            var newPassword = NON_ENCODED_PREVIOUS_PASSWORD + "new";
            _sut.TryChangeSecret("senha3", SecretType.Password, SecretChangeType.ChangeSecret);
            _sut.TryChangeSecret("senha4", SecretType.Password, SecretChangeType.ChangeSecret);
            _sut.TryChangeSecret("senha5", SecretType.Password, SecretChangeType.ChangeSecret);
            _sut.TryChangeSecret("senha6", SecretType.Password, SecretChangeType.ChangeSecret);
            _sut.TryChangeSecret("senha7", SecretType.Password, SecretChangeType.ChangeSecret);

            var notifications = _sut.TryChangeSecret(newPassword, SecretType.Password, SecretChangeType.ChangeSecret);

            Assert.Equal(0, notifications.Count);
        }

        [Theory]
        [InlineData("ab345", "Senha deve possuir no mínimo 6 caracteres.")]
        [InlineData("123456", "Senha deve possuir letras e números.")]
        [InlineData("123456", "Senha deve possuir no máximo 2 números em sequência.")]
        [InlineData("654xab", "Senha deve possuir no máximo 2 números em sequência.")]
        [InlineData("456xab", "Senha deve possuir no máximo 2 números em sequência.")]
        [InlineData("abcdef", "Senha deve possuir letras e números.")]
        [InlineData("Usuário", "Nome não pode ser usado como senha.")]
        [InlineData("UsuArIo", "Nome não pode ser usado como senha.")]
        [InlineData(NON_ENCODED_PASSWORD, "Não podem ser utilizadas as últimas 6 senhas.")]
        [InlineData(NON_ENCODED_PASSWORD, "A senha de acesso ao portal e assinatura eletrônica não podem ser iguais.")]
        public void Should_RejectWeakElectronicSignature(string weakElectronicSignature, string expectedMessage)
        {
            _sut.LastSixElectronicSignatures.Add(new PasswordHistory(EncodePasswords.EncodeString(NON_ENCODED_PASSWORD), null, SecretType.EletronicSignature));
            var notifications = _sut.TryChangeSecret(weakElectronicSignature, SecretType.EletronicSignature, SecretChangeType.ChangeSecret);

            Assert.NotEqual(0, notifications.Count);
            Assert.NotNull(notifications.FirstOrDefault(n => n.Message.Equals(expectedMessage)));
        }

        [Theory]
        [InlineData(NON_ENCODED_ELETRONIC_SIGNATURE + "test", NON_ENCODED_ELETRONIC_SIGNATURE + "wrong", "Assinatura Eletrônica inválida.")]
        public void Should_RejectChange_WhenIncorretElectronicSignature(string newElectronicSignature, string oldElectronicSignature, string expectedMessage)
        {
            _sut.LastSixElectronicSignatures.Add(new PasswordHistory(EncodePasswords.EncodeString(NON_ENCODED_PASSWORD), null, SecretType.EletronicSignature));
            var notifications = _sut.TryChangeSecret(newElectronicSignature, oldElectronicSignature, SecretType.EletronicSignature, SecretChangeType.ChangeSecret);

            Assert.NotEqual(0, notifications.Count);
            Assert.NotNull(notifications.FirstOrDefault(n => n.Message.Equals(expectedMessage)));
        }

        [Theory]
        [InlineData("01#02x2000")]
        [InlineData("ab12c3")]
        [InlineData("Usuári0")]
        [InlineData("áíóú10")]
        [InlineData("abc135")]
        public void Should_AcceptStrongElectronicSignature(string strongPassword)
        {
            var notifications = _sut.TryChangeSecret(strongPassword, SecretType.EletronicSignature, SecretChangeType.ChangeSecret);

            Assert.Equal(0, notifications.Count);
        }

        [Fact]
        public void Should_RejectElectronicSignature_When_ItContainsTheBirthDate()
        {
            var signature = "ab" + _customer.BirthDate.Value?.ToString("ddMMyyyy") + "!";
            var notifications = _sut.TryChangeSecret(signature, SecretType.EletronicSignature, SecretChangeType.ChangeSecret);

            Assert.NotEqual(0, notifications.Count);
            Assert.NotNull(notifications.FirstOrDefault(n => n.Message.Equals("Data de aniversário não pode ser usada como senha.")));
        }

        [Theory]
        [InlineData(SecretType.EletronicSignature)]
        [InlineData(SecretType.Password)]
        public void Should_Reject_Password_On_Blacklist(SecretType secretType)
        {
            _sut.Blacklist = new PasswordBlacklist(new List<string> { "P@assword" });
            var notifications = _sut.TryChangeSecret("P@assword", secretType, SecretChangeType.ChangeSecret);

            Assert.NotEqual(0, notifications.Count);
            Assert.NotNull(notifications.FirstOrDefault(n => n.Message.Equals("A senha informada não atende os requisitos mínimos de segurança.")));
        }
    }
}
