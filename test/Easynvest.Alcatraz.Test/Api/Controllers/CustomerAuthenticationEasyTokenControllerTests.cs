﻿using System.Net;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Api.Controllers;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Test.FakeDatas;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using Xunit;

namespace Easynvest.Alcatraz.Test.Api
{
    public class CustomerAuthenticationEasyTokenControllerTests
    {
        private readonly IMediator _mediator;
        private readonly CustomerAuthenticationEasyTokenController _controller;


        public CustomerAuthenticationEasyTokenControllerTests()
        {
            _mediator = Substitute.For<IMediator>();

            _controller = new CustomerAuthenticationEasyTokenController(_mediator);
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = Substitute.For<HttpContext>()
            };
            _controller.HttpContext.Connection.RemoteIpAddress = Substitute.For<IPAddress>(16885952);
        }

        [Fact]
        public async Task Should_Return_BadRequest_When_Handler_StatusCode_Response_Is_Zero()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerTokenQuery>())
                       .Returns(FakeData.FailValidateAuthenticatedCustomerTokenResponseWithStatusZero());

            var result = await _controller.Authenticate(FakeData.ValidateAuthenticatedCustomerTokenRequest) as ObjectResult;

            result.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async Task Should_Return_InternalServerError_When_Handler_StatusCode_Response_Is_500()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerTokenQuery>())
                       .Returns(FakeData.FailValidateAuthenticatedCustomerTokenResponseWithStatus500());

            var result = await _controller.Authenticate(FakeData.ValidateAuthenticatedCustomerTokenRequest) as ObjectResult;

            result.StatusCode.Should().Be(StatusCodes.Status500InternalServerError);
        }

        [Fact]
        public async Task Should_Return_Ok_When_Handler_IsSuccess()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerTokenQuery>())
                       .Returns(FakeData.SucceedValidateAuthenticatedCustomerTokenResponse());

            var result = await _controller.Authenticate(FakeData.ValidateAuthenticatedCustomerTokenRequest) as ObjectResult;

            result.StatusCode.Should().Be(StatusCodes.Status200OK);
        }
    }
}
