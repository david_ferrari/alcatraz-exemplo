﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Query.Handlers;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Test.FakeDatas;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ValidateAuthenticationCodeHandlerTest
    {
        private ILogger<ValidateAuthenticationCodeHandler> _logger;
        private IMediator _mediator;
        private IAuthCodeRepository _codeRepository;
        private IValidateTimeTokenRepository _validateTimeTokenRepository;
        private ValidateAuthenticationCodeHandler _handler;

        public ValidateAuthenticationCodeHandlerTest()
        {
            _logger = Substitute.For<ILogger<ValidateAuthenticationCodeHandler>>();
            _mediator = Substitute.For<IMediator>();
            _codeRepository = Substitute.For<IAuthCodeRepository>();
            _validateTimeTokenRepository = Substitute.For<IValidateTimeTokenRepository>();

            _handler = new ValidateAuthenticationCodeHandler(_mediator, _codeRepository, _validateTimeTokenRepository, _logger);
        }

        [Fact]
        public async Task Should_Fail_When_Auth_Fails()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), CancellationToken.None)
                .Returns(new ValidateAuthenticatedCustomerResponse
                {
                    AuthenticatedMessages = new Dictionary<string, string> { { "Erro", "Erro" } }
                });

            var response = await _handler.Handle(new ValidateAuthenticationCodeQuery("usuario", "senha", "codigo", null, null, "deviceUid"),
                CancellationToken.None);

            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async Task Should_Fail_When_Auth_Is_Exception()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), CancellationToken.None)
                .Returns(_ => Task.FromResult(FakeData.SucceedValidateAuthenticatedCustomerResponse()));

            _codeRepository.When(x => x.GetLatestCode(Arg.Any<string>()))
                .Do(_ => throw new Exception());

            var response = await _handler.Handle(new ValidateAuthenticationCodeQuery("usuario", "senha", "codigo", null, null, "deviceUid"),
                CancellationToken.None);

            response.IsFailure.Should().BeTrue();
            response.IsSuccess.Should().BeFalse();
            response.ErrorResponse.Error.StatusCode.Should().Be(StatusCodes.Status500InternalServerError);
        }

        [Fact]
        public async Task Should_Fail_When_Code_Does_Not_Exists()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), CancellationToken.None)
                .Returns(new ValidateAuthenticatedCustomerResponse
                {
                    CustomerUser = new Alcatraz.Domain.Dtos.CustomerUserDto
                    {
                        CustomerId = "10"
                    }
                });

            var response = await _handler.Handle(new ValidateAuthenticationCodeQuery("usuario", "senha", "codigo", null, null, "deviceUid"),
                CancellationToken.None);

            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be("NOTFOUNDCODE");
            response.ErrorResponse.Error.Message.Should().Be("Código inválido");
        }

        [Fact]
        public async Task Should_Fail_When_Code_Is_Invalid()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), CancellationToken.None)
                .Returns(new ValidateAuthenticatedCustomerResponse
                {
                    CustomerUser = new Alcatraz.Domain.Dtos.CustomerUserDto
                    {
                        CustomerId = "10"
                    }
                });

            _codeRepository.GetLatestCode("10").Returns(new AuthCode("10", "codigoDiferente", DateTime.Now, DateTime.Now.AddYears(1)));
            var response = await _handler.Handle(new ValidateAuthenticationCodeQuery("usuario", "senha", "codigo", null, null, "deviceUid"),
                CancellationToken.None);

            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be("INVALIDCODE");
            response.ErrorResponse.Error.Message.Should().Be("Código inválido");
        }

        [Fact]
        public async Task Should_Fail_When_Code_Is_Expired()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), CancellationToken.None)
                .Returns(new ValidateAuthenticatedCustomerResponse
                {
                    CustomerUser = new Alcatraz.Domain.Dtos.CustomerUserDto
                    {
                        CustomerId = "10"
                    }
                });

            _codeRepository.GetLatestCode("10").Returns(new AuthCode("10", "codigo", DateTime.Now, DateTime.Now.AddYears(-1)));
            var response = await _handler.Handle(new ValidateAuthenticationCodeQuery("usuario", "senha", "codigo", null, null, "deviceUid"),
                CancellationToken.None);

            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be("EXPIREDCODE");
            response.ErrorResponse.Error.Message.Should().Be("O código é válido por 10 minutos. Solicite um novo envio e tente novamente.");
        }

        [Fact]
        public async Task Should_Validate_Code_And_User()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), CancellationToken.None)
                .Returns(new ValidateAuthenticatedCustomerResponse
                {
                    CustomerUser = new Alcatraz.Domain.Dtos.CustomerUserDto
                    {
                        CustomerId = "10"
                    }
                });

            _codeRepository.GetLatestCode("10").Returns(new AuthCode("10", "codigo", DateTime.Now, DateTime.Now.AddYears(1)));
            var result = await _handler.Handle(new ValidateAuthenticationCodeQuery("usuario", "senha", "codigo", null, null, "deviceUid"),
                CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
        }
    }
}