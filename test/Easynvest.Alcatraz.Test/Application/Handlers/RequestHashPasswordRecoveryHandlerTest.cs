﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Test.Common;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.Configuration;

namespace Easynvest.Alcatraz.Test.Application
{
    public class RequestHashPasswordRecoveryHandlerTest : BaseRequestHashPasswordHandler
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ILogger<RequestHashPasswordRecoveryHandler> _logger;
        private readonly RequestHashPasswordRecoveryCommand _command;
        private readonly RequestHashPasswordRecoveryHandler _handler;

        public RequestHashPasswordRecoveryHandlerTest()
        {
            _customerRepository = Substitute.For<ICustomerRepository>();
            _logger = Substitute.For<ILogger<RequestHashPasswordRecoveryHandler>>();
            _command = new RequestHashPasswordRecoveryCommand(_email, _cpf);
            _handler = new RequestHashPasswordRecoveryHandler(_configuration, _repository, _customerRepository, _mediator, _logger);
        }

        [Fact]
        public void Should_CreateInstance_Without_Exceptions()
        {
            new RequestHashPasswordRecoveryHandler(_configuration, _repository, _customerRepository, _mediator, _logger);
        }

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_CommandIsOk()
        {
            ConfigurationIsValid();
            _customerRepository.GetCustomerInfoByCustomerId(Arg.Any<string>()).Returns(Task.FromResult(new Customer(_cpf, Name.Create("Teste"), new BirthDate(DateTime.Now), _email, _customerStatus)));
            _repository.When(x => x.Create(Arg.Any<PasswordRecoveryRequest>())).Do(x => { });
            _mediator.Send(Arg.Any<SendEmailCommand>()).Returns(Task.FromResult(Response.Ok()));

            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            Assert.True(!response.IsFailure && !response.Messages.Any());
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_RequestPasswordRecoveryCommandEmailIsNullOrEmpty()
        {
            var command = new RequestHashPasswordRecoveryCommand(string.Empty, _cpf);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_RequestPasswordRecoveryCommandCpfIsEmpty()
        {
            var command = new RequestHashPasswordRecoveryCommand(_email, string.Empty);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_RequestPasswordRecoveryCommandCpfIsNull()
        {
            var command = new RequestHashPasswordRecoveryCommand(_email, null);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_ExpirationTimeInDayNotFound()
        {
            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_CreatePasswordRecoveryFail()
        {
            var repository = Substitute.For<IHashPasswordRepository>();
            repository.Create(Arg.Any<PasswordRecoveryRequest>()).Returns(Task.FromException(new Exception()));
            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_CpfLengthIsInvalid()
        {
            var command = new RequestHashPasswordRecoveryCommand(_email, $"{_cpf}00000");
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_CustomerId_Not_Equals()
        {
            _customerRepository.GetCustomerInfoByCustomerId(Arg.Any<string>())
                .Returns(Task.FromResult(new Customer("22896545584", Name.Create("Teste"), new BirthDate(DateTime.Now), _email, _customerStatus)));

            var command = new RequestHashPasswordRecoveryCommand(_email, _cpf);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ThrowsException_When_Customer_IsNull()
        {
            await Task.Run(() =>
            {
                _customerRepository
                    .When(x => x.GetCustomerInfoByCustomerId(Arg.Any<string>()))
                    .Do(x => { throw new Exception(); });

                var command = new RequestHashPasswordRecoveryCommand(_email, _cpf);
                Assert.ThrowsAsync<Exception>(async () =>
                    await _handler.Handle(command, new System.Threading.CancellationToken()));
            });
        }

        [Fact]
        public async Task Should_ThrowsException_When_SendMail_Error()
        {
            await Task.Run(() =>
            {
                ConfigurationIsValid();
                _mediator.When(x => x.Send(Arg.Any<SendEmailCommand>())).Do(x => { throw new Exception(); });

                _customerRepository.GetCustomerInfoByCustomerId(Arg.Any<string>())
                    .Returns(Task.FromResult(new Customer(_cpf, Name.Create("Teste"), new BirthDate(DateTime.Now), _email, _customerStatus)));

                var command = new RequestHashPasswordRecoveryCommand(_email, _cpf);

                Assert.ThrowsAsync<Exception>(async () =>
                    await _handler.Handle(command, new System.Threading.CancellationToken()));
            });
        }
    }
}