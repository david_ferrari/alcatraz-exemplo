﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Alcatraz.Test.FakeDatas;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class SendAuthenticationCodeHandlerTest
    {
        private const string INVALID_USERNAME = "1234567855";
        private const string VALID_USERNAME = "03078621782";
        private const string EMAILUSER = "diego.silva@easynvest.com.br";
        private readonly IAuthCodeRepository _codeRepository;
        private readonly IAuthenticateCustomerRepository _authenticateCustomerRepository;
        private readonly IOptions<AuthCodeOptions> _codeOptions;
        private readonly IOptions<LoginConfigurationOptions> _loginConfigurationOptions;
        private readonly IMediator _mediator;
        private readonly ILogger<SendAuthenticationCodeHandler> _logger;
        private readonly SendAuthenticationCodeHandler _handler;

        public SendAuthenticationCodeHandlerTest()
        {
            _authenticateCustomerRepository = Substitute.For<IAuthenticateCustomerRepository>();
            _codeRepository = Substitute.For<IAuthCodeRepository>();
            _codeOptions = Substitute.For<IOptions<AuthCodeOptions>>();
            _codeOptions.Value.Returns(new AuthCodeOptions
            {
                EmailId = 123,
                ExpirationInMinutes = 10
            });

            _loginConfigurationOptions = Substitute.For<IOptions<LoginConfigurationOptions>>();
            _loginConfigurationOptions.Value.Returns(new LoginConfigurationOptions
            {
                EmailLoginDisabled = true
            });

            _mediator = Substitute.For<IMediator>();
            _logger = Substitute.For<ILogger<SendAuthenticationCodeHandler>>();

            _handler = new SendAuthenticationCodeHandler(_mediator, _codeRepository, _authenticateCustomerRepository, _codeOptions, _logger, _loginConfigurationOptions);
        }

        [Fact]
        public async Task Should_Return_Fail_When_Username_Is_Empty()
        {
            var response = await _handler.Handle(CreateCommand(string.Empty), CancellationToken.None);

            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be(Errors.ErrorsToken.InvalidUsername().Code);
        }

        [Fact]
        public async Task Should_Return_Fail_When_ValidateAuthenticatedCustomer_Is_Invalid_Username()
        {
            var response = await _handler.Handle(CreateCommand(INVALID_USERNAME), CancellationToken.None);

            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be(ValidateAuthenticateType.InvalidCredential.Value);
        }

        [Fact]
        public async Task Should_Return_Fail_When_EmailLogin_Is_True()
        {
            _loginConfigurationOptions.Value.Returns(new LoginConfigurationOptions
            {
                EmailLoginDisabled = false
            });

            var response = await _handler.Handle(CreateCommand(EMAILUSER), CancellationToken.None);

            response.IsSuccess.Should().BeFalse();
            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be(ValidateAuthenticateType.InvalidCredential.Value);
        }

        [Fact]
        public async Task Should_Return_Fail_When_GetAuthenticatedCustomer_Is_Exception()
        {
            _authenticateCustomerRepository.When(x => x.GetAuthenticatedCustomer(Arg.Any<string>(), Arg.Any<CustomerLoginType>()))
                .Do(_ => throw new Exception());

            var response = await _handler.Handle(CreateCommand(VALID_USERNAME), CancellationToken.None);

            response.IsSuccess.Should().BeFalse();
            response.IsFailure.Should().BeTrue();
        }

        [Fact]
        public async Task Should_Return_Success_When_GetAuthenticatedCustomer_Exists()
        {
            _authenticateCustomerRepository.GetAuthenticatedCustomer(Arg.Any<string>(), Arg.Any<CustomerLoginType>())
                .Returns(_ => FakeData.GetAuthenticatedCustomers);

            var response = await _handler.Handle(CreateCommand(VALID_USERNAME), CancellationToken.None);

            await _codeRepository.Received(1).InsertCode(Arg.Is<AuthCode>(c => c.CustomerId.Equals(VALID_USERNAME)));

            response.IsSuccess.Should().BeTrue();
            response.IsFailure.Should().BeFalse();
        }

        [Fact]
        public async Task Should_Return_Fail_When_GetAuthenticatedCustomer_Is_Multiply()
        {
            _authenticateCustomerRepository.GetAuthenticatedCustomer(Arg.Any<string>(), Arg.Any<CustomerLoginType>())
                .Returns(_ => FakeData.GetMultiplyCustomers);

            var response = await _handler.Handle(CreateCommand(VALID_USERNAME), CancellationToken.None);

            response.IsSuccess.Should().BeFalse();
            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be(ValidateAuthenticateType.InvalidCredential.Value);
        }

        [Fact]
        public async Task Should_Return_Fail_When_Handle_Is_Exception()
        {
            _authenticateCustomerRepository.GetAuthenticatedCustomer(Arg.Any<string>(), Arg.Any<CustomerLoginType>())
                .Returns(_ => FakeData.GetAuthenticatedCustomers);

            _codeRepository.When(x => x.InsertCode(Arg.Any<AuthCode>()))
                .Do(_ => throw new Exception());

            var response = await _handler.Handle(CreateCommand(VALID_USERNAME), CancellationToken.None);

            response.IsSuccess.Should().BeFalse();
            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.StatusCode.Should().Be(StatusCodes.Status500InternalServerError);
        }

        private static SendAuthenticationCodeCommand CreateCommand(string username)
            => new SendAuthenticationCodeCommand(username, "1234", "6ee27d56-6cc6-473d-954b-6fd3bef26573", "1.1.1.1");
    }
}