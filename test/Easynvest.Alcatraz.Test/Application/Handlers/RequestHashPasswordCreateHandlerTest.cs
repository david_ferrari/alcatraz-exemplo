﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Test.Common;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Test.Application
{
    public class RequestHashPasswordCreateHandlerTest : BaseRequestHashPasswordHandler
    {
        private RequestHashPasswordCreateCommand _command;
        private RequestHashPasswordCreateHandler _handler;
        private ILogger<RequestHashPasswordCreateHandler> _logger;

        public RequestHashPasswordCreateHandlerTest()
        {
            _logger = Substitute.For<ILogger<RequestHashPasswordCreateHandler>>();
            _command = new RequestHashPasswordCreateCommand(_name, _email, _cpf);
            _handler = new RequestHashPasswordCreateHandler(_configuration, _repository, _mediator, _logger);
        }

        [Fact]
        public void Should_CreateInstance_Without_Exceptions()
        {
            new RequestHashPasswordCreateHandler(_configuration, _repository, _mediator, _logger);
        }

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_CommandIsOk()
        {
            ConfigurationIsValid();
            var templateEmail = TemplateEmail.Create(string.Empty, string.Empty, string.Empty, string.Empty);
            _repository.When(x => x.Create(Arg.Any<PasswordRecoveryRequest>())).Do(x => { });
            _mediator.Send(Arg.Any<SendEmailCommand>()).Returns(Response.Ok());

            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            Assert.True(!response.IsFailure && !response.Messages.Any());
        }

        [Fact]
        public async Task Should_ThrowsException_When_SendMail_Error()
        {
            await Task.Run(() =>
            {
                ConfigurationIsValid();
                _mediator.When(x => x.Send(Arg.Any<SendEmailCommand>())).Do(x => { throw new Exception(); });
                var command = new RequestHashPasswordCreateCommand(_name, _email, _cpf);

                Assert.ThrowsAsync<Exception>(async () =>
                    await _handler.Handle(command, new System.Threading.CancellationToken()));
            });
        }

        [Fact]
        public async Task Should_ReturnErrorMessage_When_appsettings_Email_Is_Null()
        {
            ConfigurationIsInvalid();
            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            Assert.True(response.IsFailure);
        }

        [Fact]
        public async Task Should_ReturnErrorMessage_When_AddressMail_IsInvalid()
        {
            ConfigurationIsValid();
            var command = new RequestHashPasswordCreateCommand(_name, "easynvest.com.br", _cpf);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }
    }
}
