﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Query.Handlers;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Test.FakeDatas;
using FluentAssertions;
using MediatR;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ValidateAuthenticatedCustomerTokenHandlerTests
    {
        private const string USERNAME = "12860210814";
        private const string PASSWORD = "123654";
        private const string FCMTOKEN = "123token456";
        private const string IPADDRESS = "192.168.0.1";
        private const string DEVICE_UID = "C7A3BE50-286C-4655-A11D-5FFAFC94093C";
        private const string OTP_CODE = "123654";
        private const int STATUSCODE_BADREQUEST = 400;
        private readonly IMediator _mediator;
        private readonly ILogger<ValidateAuthenticatedCustomerTokenHandler> _logger;
        private readonly IBopeRepository _bopeRepository;
        private readonly IValidateTimeTokenRepository _validateTimeTokenRepository;
        private readonly ValidateAuthenticatedCustomerTokenHandler _sut;

        public ValidateAuthenticatedCustomerTokenHandlerTests()
        {
            _mediator = Substitute.For<IMediator>();
            _logger = Substitute.For<ILogger<ValidateAuthenticatedCustomerTokenHandler>>();
            _bopeRepository = Substitute.For<IBopeRepository>();
            _validateTimeTokenRepository = Substitute.For<IValidateTimeTokenRepository>();

            _sut = new ValidateAuthenticatedCustomerTokenHandler(_mediator, _logger, _bopeRepository, _validateTimeTokenRepository);
        }

        [Fact]
        public async Task Should_Return_ValidateAuthenticatedCustomerResponse_Fail()
        {
            var validateAuthenticatedCustomerResponse = new ValidateAuthenticatedCustomerResponse();
            validateAuthenticatedCustomerResponse.AddAuthenticatedMessages("INVALIDUSER", "Usuário Inválido");
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => validateAuthenticatedCustomerResponse);

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            response.Authenticated.Should().BeFalse();
            response.ErrorResponse.Error.StatusCode.Should().Be(STATUSCODE_BADREQUEST);
        }

        [Fact]
        public async Task Should_Return_TokenIsActive_Exception()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SucceedValidateAuthenticatedCustomerResponse());

            _bopeRepository.When(x => x.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>()))
                .Do(_ => throw new Exception());

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            response.IsSuccess.Should().BeFalse();
            response.IsFailure.Should().BeTrue();
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task Should_Return_TokenIsActive_IsNull(bool hasFeature)
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SucceedValidateAuthenticatedCustomerResponse());

            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => (DeviceRegisteredDto)null);

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            response.IsSuccess.Should().BeTrue();
            response.IsFailure.Should().BeFalse();
        }

        [Fact]
        public async Task Should_Return_TokenIsActive_IsNotNull()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SucceedValidateAuthenticatedCustomerResponse());

            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.DeviceRegistered);

            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.ValidateTimeTokenValid);

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            response.IsSuccess.Should().BeTrue();
            response.IsFailure.Should().BeFalse();
        }

        [Fact]
        public async Task Should_Return_TokenIsActive_IsNull_And_OtpCode_IsEmpty()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SucceedValidateAuthenticatedCustomerResponse());

            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.DeviceRegistered);

            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.ValidateTimeTokenNull);

            var response = await _sut.Handle(CreateQuery(string.Empty, true), CancellationToken.None).ConfigureAwait(false);

            response.IsSuccess.Should().BeFalse();
            response.IsFailure.Should().BeTrue();
            response.ErrorResponse.Error.Code.Should().Be("NEEDTOKEN");
        }

        [Fact]
        public async Task Should_Return_ValidateAuthenticatedCustomerResponse_Success()
        {
            var validateAuthenticatedCustomerResponse = new ValidateAuthenticatedCustomerResponse();
            validateAuthenticatedCustomerResponse.CustomerUser = FakeData.GetAuthenticatedCustomer;
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => validateAuthenticatedCustomerResponse);

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            response.Authenticated.Should().BeTrue();
            response.CustomerUser.Should().NotBeNull();
        }

        [Fact]
        public async Task Should_Return_AuthenticateTransaction_Fail()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SucceedValidateAuthenticatedCustomerResponse());

            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.DeviceRegistered);

            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.ValidateTimeTokenNull);

            _mediator.Send(Arg.Any<AuthenticateTransactionCommand>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.FailAuthenticateTransactionResponse());

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            response.IsSuccess.Should().BeFalse();
            response.IsFailure.Should().BeTrue();
            response.Authenticated.Should().BeFalse();
            response.ErrorResponse.Error.Code.Should().Be("AuthenticateTransaction");
            response.ErrorResponse.Error.InnerError.Should().NotBeNull();
        }

        [Fact]
        public async Task Should_Return_AuthenticateTransaction_Success_And_RegisterTimeToken()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SucceedValidateAuthenticatedCustomerResponse());

            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.DeviceRegistered);

            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.ValidateTimeTokenNull);

            _mediator.Send(Arg.Any<AuthenticateTransactionCommand>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SuccessAuthenticateTransactionResponse());

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            await _validateTimeTokenRepository.Received().RegisterTimeToken(Arg.Any<ValidateTimeToken>());

            response.IsSuccess.Should().BeTrue();
            response.IsFailure.Should().BeFalse();
            response.Authenticated.Should().BeTrue();
        }

        [Fact]
        public async Task Should_Return_AuthenticateTransaction_Success_And_UpdateTimeToken()
        {
            _mediator.Send(Arg.Any<ValidateAuthenticatedCustomerQuery>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SucceedValidateAuthenticatedCustomerResponse());

            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.DeviceRegistered);

            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>())
                .Returns(_ => FakeData.ValidateTimeTokenInvalid);

            _mediator.Send(Arg.Any<AuthenticateTransactionCommand>(), Arg.Any<CancellationToken>())
                .Returns(_ => FakeData.SuccessAuthenticateTransactionResponse());

            var response = await _sut.Handle(CreateQuery(OTP_CODE, true), CancellationToken.None).ConfigureAwait(false);

            await _validateTimeTokenRepository.Received().UpdateTimeToken(Arg.Any<ValidateTimeToken>());

            response.IsSuccess.Should().BeTrue();
            response.IsFailure.Should().BeFalse();
            response.Authenticated.Should().BeTrue();
        }

        private static ValidateAuthenticatedCustomerTokenQuery CreateQuery(string otpCode, bool trustedDevice)
            => new ValidateAuthenticatedCustomerTokenQuery(USERNAME, PASSWORD, FCMTOKEN, DEVICE_UID, otpCode, IPADDRESS, trustedDevice);
    }
}