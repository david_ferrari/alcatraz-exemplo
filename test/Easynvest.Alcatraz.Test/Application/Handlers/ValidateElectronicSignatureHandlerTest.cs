﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.Domain.Entities;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ValidateElectronicSignatureHandlerTest
    {
        private const string CUSTOMER_ID = "12345678900";
        private const string ELECTRONIC_SIGNATURE = "qx3123";
        private const string INVALID_ELECTRONICSIGNATURE = "qx3123_wrong";
        private readonly IMediator _mediator;
        private readonly ICustomerSecretRepository _customerSecretRepository;
        private readonly IPasswordRepository _passwordRepository;
        private readonly ILogger<ValidateElectronicSignatureHandler> _logger;
        private readonly IOptions<CustomerSecretOption> _options;

        private ValidateElectronicSignatureHandler _handler;

        public ValidateElectronicSignatureHandlerTest()
        {
            _mediator = Substitute.For<IMediator>();
            _customerSecretRepository = Substitute.For<ICustomerSecretRepository>();
            _passwordRepository = Substitute.For<IPasswordRepository>();
            _logger = Substitute.For<ILogger<ValidateElectronicSignatureHandler>>();
            _options = Substitute.For<IOptions<CustomerSecretOption>>();
            _options.Value.Returns(x => { return new CustomerSecretOption { MaxInvalidLoginAttempts = 3 }; });

            _handler = new ValidateElectronicSignatureHandler(_mediator, _customerSecretRepository, _passwordRepository, _logger, _options);
        }

        [Fact]
        public async Task Should_ReturnOk_When_ElectronicSingatureIsCorrect()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                    .Returns(p => CreateCustomer(ELECTRONIC_SIGNATURE));

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            Assert.True(response.IsSuccess);
        }

        [Fact]
        public async Task Should_ReturnBadRequest_When_CustomerSecretNotFound()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                .Returns(p => Task.FromResult(default(CustomerSecret)));

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            Assert.True(response.IsFailure);
            Assert.Equal("Cliente não localizado.", response.Messages.ToList()[0]);
        }

        [Fact]
        public async Task Should_ReturnBadRequest_When_ElectronicSingatureIsBlocked()
        {
            _customerSecretRepository
                .Get(Arg.Any<string>(), Arg.Any<SecretType>())
                .Returns(p => CreateCustomer(ELECTRONIC_SIGNATURE, _options.Value.MaxInvalidLoginAttempts));

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            Assert.True(response.IsFailure);
            Assert.Equal("Assinatura Eletrônica bloqueada.", response.Messages.ToList()[0]);
        }

        [Fact]
        public async Task Should_ReturnBadRequest_When_ElectronicSingatureIsIncorrect()
        {
            var loginAttempts = 1;

            var customerSecret = CreateCustomer(INVALID_ELECTRONICSIGNATURE, loginAttempts);

            _customerSecretRepository
                .Get(Arg.Any<string>(), Arg.Any<SecretType>())
                .Returns(p => customerSecret);

            var password = CreatePassword
                    (
                        CreatePasswordHistory("AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJ7890="),
                        CreatePasswordDetails("AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJ7890=", "AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJ8888=")
                    );

            _passwordRepository.GetByCustomerId(CUSTOMER_ID)
                .Returns(p => password);

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            Assert.True(response.IsFailure);
            Assert.Equal(loginAttempts + 1, customerSecret.LoginAttempts);
            Assert.Equal("Assinatura Eletrônica inválida.", response.Messages.ToList()[0]);
        }

        [Fact]
        public async Task Should_ReturnBadRequest_When_OldElectronicSingatureIsIncorrect()
        {
            var customer = CreateCustomer(INVALID_ELECTRONICSIGNATURE);
            customer.Version = PasswordVersion.Old;
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                    .Returns(p => customer);

            var password = CreatePassword
        (
            CreatePasswordHistory("AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJmwYE="),
            CreatePasswordDetails("AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJ7890=", "AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJ8888=")
        );


            _passwordRepository.GetByCustomerId(CUSTOMER_ID)
                .Returns(p => password);

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            Assert.True(response.IsFailure);
            Assert.Equal("Você está utilizando uma Assinatura Eletrônica antiga, utilize a mais atual.", response.Messages.ToList()[0]);
        }

        [Fact]
        public async Task Should_ReturnBadRequest_When_PasswordInsteadElectronicSingatureIsIncorrect()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                    .Returns(p => CreateCustomer(INVALID_ELECTRONICSIGNATURE));

            var password = CreatePassword
                    (
                        CreatePasswordHistory("AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJmwXX"),
                        CreatePasswordDetails("AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJmwYE=", "AEqR+UaFvq0z52V45iV6q1kA2UjXCgqYKGVbst2tFaKhUKJ8888=")
                    );

            _passwordRepository.GetByCustomerId(CUSTOMER_ID)
                .Returns(p => password);

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            var response = await _handler.Handle(cmd, CreateCancellationToken());

            Assert.True(response.IsFailure);
            Assert.Equal("Você está utilizando a sua Senha ao invés da sua Assinatura Eletrônica.", response.Messages.ToList()[0]);
        }

        [Fact]
        public async Task Should_Not_Call_UpdateCustomerSecret_When_Consecutive_Successfull_LoginAttempts()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                    .Returns(p => CreateCustomer(ELECTRONIC_SIGNATURE));

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            await _customerSecretRepository.DidNotReceiveWithAnyArgs().UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.EletronicSignature);
        }

        [Fact]
        public async Task Should_Call_UpdateCustomerSecret_When_Failed_After_Success_LoginAttempt()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                    .Returns(p => CreateCustomer(ELECTRONIC_SIGNATURE));

            var cmd = new ValidateElectronicSignatureCommand(INVALID_ELECTRONICSIGNATURE, CUSTOMER_ID);

            await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            await _customerSecretRepository.ReceivedWithAnyArgs(1).UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.EletronicSignature);
        }

        [Fact]
        public async Task Should_Call_UpdateCustomerSecret_When_Consecutive_Failed_LoginAttempt()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                                     .Returns(p => CreateCustomer(ELECTRONIC_SIGNATURE, 1));

            var cmd = new ValidateElectronicSignatureCommand(INVALID_ELECTRONICSIGNATURE, CUSTOMER_ID);

            await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            await _customerSecretRepository.ReceivedWithAnyArgs(1).UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.EletronicSignature);
        }

        [Fact]
        public async Task Should_Not_Call_UpdateCustomerSecret_When_Blocked_User()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                                     .Returns(p => CreateCustomer(ELECTRONIC_SIGNATURE, 3));

            var cmd = new ValidateElectronicSignatureCommand(INVALID_ELECTRONICSIGNATURE, CUSTOMER_ID);

            await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            await _customerSecretRepository.DidNotReceiveWithAnyArgs().UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.EletronicSignature);
        }

        [Fact]
        public async Task Should_Call_UpdateCustomerSecret_When_Success_After_Failed_LoginAttempt()
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>())
                    .Returns(p => CreateCustomer(ELECTRONIC_SIGNATURE, 1));

            var cmd = new ValidateElectronicSignatureCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID);

            await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            await _customerSecretRepository.ReceivedWithAnyArgs(1).UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.EletronicSignature);
        }

        private static CancellationToken CreateCancellationToken() => new CancellationToken();

        private static CustomerSecret CreateCustomer(string criptoPassword, int loginAttempts = 0)
            => new CustomerSecret(CUSTOMER_ID, criptoPassword, SecretType.EletronicSignature, loginAttempts);

        private static PasswordHistory CreatePasswordHistory(string encodedPassword) =>
            new PasswordHistory(encodedPassword, null, SecretType.EletronicSignature);

        private static PasswordDetails CreatePasswordDetails
            (
                string encodedPassword,
                string encodedEletronicSignature
            ) =>
            new PasswordDetails(encodedPassword, encodedEletronicSignature, 0, 0, null, null)
            {
                PasswordVersion = PasswordVersion.Old,
                EletronicSignatureVersion = PasswordVersion.Old
            };

        private static Password CreatePassword(PasswordHistory passwordHistory, PasswordDetails passwordDetails)
            => new Password
            (
                default(Customer),
                new List<PasswordHistory>(),
                new List<PasswordHistory>()
                {
                    passwordHistory
                },
                passwordDetails
            );
    }
}