﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Messages.Events.Security.User;
using FluentAssertions;
using MassTransit;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class BlockUserHandlerTests
    {
        private readonly ILogger<BlockUserHandler> _logger;
        private readonly IUserBlockRepository _repository;
        private readonly IBusControl _busControl;
        private readonly IAuthenticateCustomerRepository _customerRepository;

        public BlockUserHandlerTests()
        {
            _logger = Substitute.For<ILogger<BlockUserHandler>>();
            _repository = Substitute.For<IUserBlockRepository>();
            _busControl = Substitute.For<IBusControl>();
            _customerRepository = Substitute.For<IAuthenticateCustomerRepository>();
        }

        [Fact]
        public async Task Should_Block_User()
        {
            _customerRepository.GetAuthenticatedCustomer("123", CustomerLoginType.CustomerLoginDocument).Returns(new[] { new CustomerUserDto() });
            var handler = new BlockUserHandler(_repository, _customerRepository, _busControl, _logger);
            var result = await handler.Handle(new BlockUserCommand("123", BlockType.Security), CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
            await _repository.Received(1).Add(Arg.Is<UserBlock>(u => u.CustomerId == "123"));
            await _busControl.Received(1).Publish<UserBlocked>(Arg.Any<object>());
        }

        [Fact]
        public async Task Should_Fail_On_Error()
        {
            _customerRepository.GetAuthenticatedCustomer("123", CustomerLoginType.CustomerLoginDocument).Returns(new[] { new CustomerUserDto() });
            _repository.Add(Arg.Is<UserBlock>(u => u.CustomerId == "123")).Throws(new Exception("erro"));
            var handler = new BlockUserHandler(_repository, _customerRepository, _busControl, _logger);
            var result = await handler.Handle(new BlockUserCommand("123", BlockType.Security), CancellationToken.None);
            result.IsFailure.Should().BeTrue();
        }

        [Fact]
        public async Task Should_Fail_When_User_Is_Not_Found()
        {
            var handler = new BlockUserHandler(_repository, _customerRepository, _busControl, _logger);
            var result = await handler.Handle(new BlockUserCommand("123", BlockType.Security), CancellationToken.None);
            result.IsFailure.Should().BeTrue();
            result.Message.Should().Be(Errors.General.NotFound("Usuário").Message);
        }
    }
}
