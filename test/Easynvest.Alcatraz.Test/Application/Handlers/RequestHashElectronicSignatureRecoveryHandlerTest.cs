﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Test.Common;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application
{
    public class RequestHashElectronicSignatureRecoveryHandlerTest : BaseRequestHashPasswordHandler
    {
        private ICustomerRepository _customerRepository;
        private ILogger<RequestHashElectronicSignatureRecoveryHandler> _logger;
        private RequestHashElectronicSignatureRecoveryCommand _command;
        private RequestHashElectronicSignatureRecoveryHandler _handler;

        public RequestHashElectronicSignatureRecoveryHandlerTest()
        {
            _customerRepository = Substitute.For<ICustomerRepository>();
            _logger = Substitute.For<ILogger<RequestHashElectronicSignatureRecoveryHandler>>();
            _command = new RequestHashElectronicSignatureRecoveryCommand(_email, _cpf);
            _handler = new RequestHashElectronicSignatureRecoveryHandler(_repository, _logger, _configuration, _mediator, _customerRepository);
        }

        [Fact]
        public void Should_CreateInstance_Without_Exceptions()
        {
            new RequestHashElectronicSignatureRecoveryHandler(_repository, _logger, _configuration, _mediator, _customerRepository);
        }

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_CommandIsOk()
        {
            ConfigurationIsValid();
            _customerRepository.GetCustomerInfoByCustomerId(Arg.Any<string>()).Returns(Task.FromResult(new Customer(_cpf, Name.Create("Teste"), new BirthDate(DateTime.Now), _email, _customerStatus)));
            _repository.When(x => x.Create(Arg.Any<PasswordRecoveryRequest>())).Do(x => { });
            _mediator.Send(Arg.Any<SendEmailCommand>()).Returns(Response.Ok());

            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            Assert.True(!response.IsFailure && !response.Messages.Any());
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_RequestPasswordRecoveryCommandEmailIsNullOrEmpty()
        {
            var command = new RequestHashElectronicSignatureRecoveryCommand(string.Empty, _cpf);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_RequestPasswordRecoveryCommandCpfIsEmpty()
        {
            var command = new RequestHashElectronicSignatureRecoveryCommand(_email, string.Empty);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_RequestPasswordRecoveryCommandCpfIsNull()
        {
            var command = new RequestHashElectronicSignatureRecoveryCommand(_email, null);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_ExpirationTimeInDayNotFound()
        {
            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_CreatePasswordRecoveryFail()
        {
            var repository = Substitute.For<IHashPasswordRepository>();
            repository.Create(Arg.Any<PasswordRecoveryRequest>()).Returns(Task.FromException(new Exception()));
            var response = await _handler.Handle(_command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_CpfLengthIsInvalid()
        {
            var command = new RequestHashElectronicSignatureRecoveryCommand(_email, $"{_cpf}00000");
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_CustomerId_Not_Equals()
        {
            _customerRepository.GetCustomerInfoByCustomerId(Arg.Any<string>())
                .Returns(Task.FromResult(new Customer("22896545584", Name.Create("Teste"), new BirthDate(DateTime.Now), _email, _customerStatus)));

            var command = new RequestHashElectronicSignatureRecoveryCommand(_email, _cpf);
            var response = await _handler.Handle(command, new System.Threading.CancellationToken());
            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_ThrowsException_When_Customer_IsNull()
        {
            await Task.Run(() =>
             {
                 _customerRepository
                     .When(x => x.GetCustomerInfoByCustomerId(Arg.Any<string>()))
                     .Do(x => { throw new Exception(); });

                 var command = new RequestHashElectronicSignatureRecoveryCommand(_email, _cpf);
                 Assert.ThrowsAsync<Exception>(async () =>
                     await _handler.Handle(command, new System.Threading.CancellationToken()));
             });
        }

        [Fact]
        public async Task Should_ThrowsException_When_SendMail_Error()
        {
            await Task.Run(() =>
            {
                ConfigurationIsValid();
                _mediator.When(x => x.Send(Arg.Any<SendEmailCommand>())).Do(x => { throw new Exception(); });

                _customerRepository.GetCustomerInfoByCustomerId(Arg.Any<string>())
                    .Returns(Task.FromResult(new Customer(_cpf, Name.Create("Teste"), new BirthDate(DateTime.Now), _email, _customerStatus)));

                var command = new RequestHashElectronicSignatureRecoveryCommand(_email, _cpf);

                Assert.ThrowsAsync<Exception>(async () =>
                    await _handler.Handle(command, new System.Threading.CancellationToken()));
            });
        }
    }
}
