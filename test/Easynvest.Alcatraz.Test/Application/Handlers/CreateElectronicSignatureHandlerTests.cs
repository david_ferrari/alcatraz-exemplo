﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Application.Queries;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class CreateElectronicSignatureHandlerTests
    {
        private const string CUSTOMERID = "12345678900";
        private const string CUSTOMEREMAIL = "EMAIL@EMAIL.COM";
        private const string CUSTOMERNAME = "NOME CLIENTE";
        private const string COMMAND_HASH = "HASH";
        private const string CUSTOMER_STATUS = "A";
        private const string COMMAND_PASSWORD = "PassW0rd";
        private readonly DateTime BIRTHDATE = new DateTime(1999, 1, 1);

        private IPasswordRepository _passwordRepository;
        private IHashPasswordRepository _passwordRecoveryRepository;
        private IMediator _mediator;
        private CreateElectronicSignatureCommand _command;
        private ILogger<CreateElectronicSignatureHandler> _logger;
        private IOptions<EmailTemplateOption> _templateOption;
        private const string IP_ADDRESS = "127.0.0.1";

        public CreateElectronicSignatureHandlerTests()
        {
            _passwordRepository = Substitute.For<IPasswordRepository>();
            _passwordRecoveryRepository = Substitute.For<IHashPasswordRepository>();
            _mediator = Substitute.For<IMediator>();
            _command = Substitute.For<CreateElectronicSignatureCommand>(COMMAND_HASH, COMMAND_PASSWORD, IP_ADDRESS);
            _logger = Substitute.For<ILogger<CreateElectronicSignatureHandler>>();
            _templateOption = Options.Create(new EmailTemplateOption()
            {
                ChangedLoggedElectronicSignature = 136,
                ChangedLoggedPassword = 137
            });
        }

        [Fact]
        public async Task Should_CreateNewElectronicSignature_When_ReceiveValidData()
        {
            _passwordRepository
                .GetByCustomerId(Arg.Any<string>())
                .Returns(x => { return CreatePasswordEntity(); });

            _passwordRecoveryRepository
                .GetByHash(Arg.Any<string>())
                .Returns(x => { return CreatePasswordRecoveryEntity(); });

            _mediator
                .Send(Arg.Any<ValidHashQuery>())
                .Returns(x => { return CreateHashResponse(); });

            _mediator
                .Send(Arg.Any<SendEmailCommand>())
                .Returns(x => Response.Ok());

            _command
                .Response
                .Returns(x => { return new Response<CreateElectronicSignatureResponse>(); });

            var sut = new CreateElectronicSignatureHandler(_mediator, _passwordRepository, _passwordRecoveryRepository, _templateOption, _logger);

            var result = await sut.Handle(_command, CancellationToken.None);

            Assert.NotNull(result);
            Assert.False(result.IsFailure);
            await _passwordRepository.Received().UpdateSecret(Arg.Any<Password>());
        }

        private PasswordRecoveryRequest CreatePasswordRecoveryEntity()
        {
            return PasswordRecoveryRequest.Create(CreateCustomer(), new ExpirationDate(2), new DateTime(), COMMAND_HASH);
        }

        private Customer CreateCustomer()
        {
            return new Customer(CUSTOMERID, Name.Create(CUSTOMERNAME), new BirthDate(BIRTHDATE), CUSTOMEREMAIL, CUSTOMER_STATUS);
        }

        private static Response<ValidHashQueryResponse> CreateHashResponse()
        {
            return Response<ValidHashQueryResponse>.Ok(new ValidHashQueryResponse
            {
                CustomerId = CUSTOMERID
            });
        }

        private Password CreatePasswordEntity()
        {
            return new Password
                (
                    CreateCustomer(),
                    new List<PasswordHistory>(),
                    new List<PasswordHistory>(),
                    new PasswordDetails("", "", 0, 0, SaltGenerator.Generate(), SaltGenerator.Generate())
                );
        }
    }
}
