﻿using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;
using FluentAssertions;
using Easynvest.Alcatraz.Domain.Dtos;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class AuthenticateWithChangedPasswordHandlerTests
    {
        private ILogger<AuthenticateWithChangedPasswordHandler> _logger;
        private IAuthenticationRepository _repository;

        public AuthenticateWithChangedPasswordHandlerTests()
        {
            _logger = Substitute.For<ILogger<AuthenticateWithChangedPasswordHandler>>();
            _repository = Substitute.For<IAuthenticationRepository>();
        }

        [Fact]
        public async Task Should_ReturnAuthenticatedUser_WhenSendedValidUser()
        {
            var authenticationData = new AuthenticationDto();
            _repository
                .Authenticate(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
                .Returns(await Task.FromResult(authenticationData));

            var command = new AuthenticateWithChangedPasswordCommand("username", "password", "clientId");

            var authHandler = new AuthenticateWithChangedPasswordHandler(_logger, _repository);

            var result = await authHandler.Handle(command, CancellationToken.None);

            result.IsFailure.Should().BeFalse();
            await _repository.Received(1).Authenticate(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>());
        }

        [Fact]
        public async Task Should_ReturnInvalidUser_WhenSendedInValidUser()
        {
            AuthenticationDto authenticationData = null;
            _repository
                .Authenticate(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
                .Returns(await Task.FromResult(authenticationData));

            var command = new AuthenticateWithChangedPasswordCommand("username", "password", "clientId");

            var authHandler = new AuthenticateWithChangedPasswordHandler(_logger, _repository);

            var result = await authHandler.Handle(command, CancellationToken.None);

            result.IsFailure.Should().BeTrue();
            await _repository.Received(1).Authenticate(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>());
        }

        [Fact]
        public async Task Should_ReturnInvalidUser_WhenSendedCommandWithoutClientId()
        {
            AuthenticationDto authenticationData = null;
            _repository
                .Authenticate(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
                .Returns(await Task.FromResult(authenticationData));

            var command = new AuthenticateWithChangedPasswordCommand("username", "password", string.Empty);

            var authHandler = new AuthenticateWithChangedPasswordHandler(_logger, _repository);

            var result = await authHandler.Handle(command, CancellationToken.None);

            result.IsFailure.Should().BeTrue();
            await _repository.DidNotReceive().Authenticate(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>());
        }
    }
}
