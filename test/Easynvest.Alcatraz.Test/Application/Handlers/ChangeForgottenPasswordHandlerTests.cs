﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Application.Queries;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ChangeForgottenPasswordHandlerTests
    {
        private IPasswordRepository _repo;
        private IHashPasswordRepository _passwordRecoveryRepository;
        private IMediator _mediator;
        private IPushNotificationService _pushNotification;
        private ILogger<ChangeForgottenPasswordHandler> _logger;
        private IOptions<EmailTemplateOption> _templateOption;
        private const string IP_ADDRESS = "127.0.0.1";
        private ChangeForgottenPasswordHandler _sut;

        public ChangeForgottenPasswordHandlerTests()
        {
            _repo = Substitute.For<IPasswordRepository>();
            _passwordRecoveryRepository = Substitute.For<IHashPasswordRepository>();
            _mediator = Substitute.For<IMediator>();
            _pushNotification = Substitute.For<IPushNotificationService>();
            _logger = Substitute.For<ILogger<ChangeForgottenPasswordHandler>>();
            _templateOption = Options.Create(new EmailTemplateOption()
            {
                ChangedLoggedElectronicSignature = 136,
                ChangedLoggedPassword = 137
            });
            _sut = new ChangeForgottenPasswordHandler(_mediator, _repo, _passwordRecoveryRepository, _pushNotification, _templateOption, _logger);
        }

        [Fact]
        public async Task Should_ValidatePassword_Before_TryingToUpdateDatabase()
        {
            var command = new ChangeForgottenPasswordCommand("senha11", "", IP_ADDRESS, Guid.NewGuid().ToString());
            var response = Response<ValidHashQueryResponse>.Ok(new ValidHashQueryResponse());
            _mediator.Send(Arg.Any<ValidHashQuery>()).ReturnsForAnyArgs((callInfo) => Task.FromResult(response));

            var entity = CreatePasswordEntity("senha10", "senha@2");
            _repo.GetByCustomerId(Arg.Any<string>()).Returns((callInfo) => Task.FromResult(entity));

            var actual = await _sut.Handle(command, CancellationToken.None);

            Assert.True(actual.IsFailure);
            await _repo.DidNotReceive().UpdateSecret(Arg.Any<Password>());
            await _passwordRecoveryRepository.DidNotReceive().UpdateExpirationDate(Arg.Any<PasswordRecoveryRequest>());
        }

        [Fact]
        public async Task Should_UpdateDatabase_When_PasswordSuccessfullyChanged()
        {
            var command = new ChangeForgottenPasswordCommand("", "senha11", IP_ADDRESS, Guid.NewGuid().ToString());
            var response = Response<ValidHashQueryResponse>.Ok(new ValidHashQueryResponse());

            _mediator
                .Send(Arg.Any<ValidHashQuery>())
                .ReturnsForAnyArgs((callInfo) => Task.FromResult(response));

            _mediator
                .Send(Arg.Any<SendEmailCommand>())
                .Returns(x => Response.Ok());

            var entity = CreatePasswordEntity("senha10", "senha@2");
            _repo.GetByCustomerId(Arg.Any<string>()).Returns((callInfo) => Task.FromResult(entity));

            var passwordRecovery = CreatePasswordRecoveryEntity(entity.Customer);
            _passwordRecoveryRepository.GetByHash(Arg.Any<string>()).Returns((callInfo) => Task.FromResult(passwordRecovery));

            var actual = await _sut.Handle(command, CancellationToken.None);

            Assert.False(actual.IsFailure);
            await _repo.Received(1).UpdateSecret(Arg.Any<Password>());
        }

        private static Password CreatePasswordEntity(string eletronicSignature, string password)
        {
            var customer = new Customer
                (
                    "",
                    Name.Create("Usuário"),
                    new BirthDate(new DateTime(2000, 01, 01)),
                    "",
                    "A"
                );

            var entity = new Password
                (
                    customer,
                    new List<PasswordHistory>(),
                    new List<PasswordHistory>(),
                    new PasswordDetails(password, eletronicSignature, 3, 3, SaltGenerator.Generate(), SaltGenerator.Generate())
                );

            return entity;
        }

        private static PasswordRecoveryRequest CreatePasswordRecoveryEntity(Customer customer)
        {
            return PasswordRecoveryRequest.Create("", customer, new ExpirationDate(2));
        }
    }
}
