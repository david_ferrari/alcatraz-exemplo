﻿using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class AuthenticateTransactionCommandHandlerTests
    {
        private IBopeRepository _bopeRepository;
        private ILogger<AuthenticateTransactionCommandHandler> _logger;
        private AuthenticateTransactionCommandHandler _handler;
        private const string REQUEST_ID = "D5F4AB1F-7D04-4C1B-9512-6B90826C70DE";
        private const string ACCOUNT_ID = "123456789";
        private const string OTP_CODE = "987654";
        private const string DEVICE_UID = "ANY_DEVICE_UID";
        private const BopeIntegrationType INTEGRATION_BOPE = BopeIntegrationType.UpdateBankAccount;

        public AuthenticateTransactionCommandHandlerTests()
        {
            _bopeRepository = Substitute.For<IBopeRepository>();
            _logger = Substitute.For<ILogger<AuthenticateTransactionCommandHandler>>();
            _handler = new AuthenticateTransactionCommandHandler(_bopeRepository, _logger);
        }

        [Fact]
        public async Task WhenIsNotAuthenticated_ShouldReturnFail()
        {
            // Arrange
            _bopeRepository.Authenticate(REQUEST_ID, ACCOUNT_ID, OTP_CODE, DEVICE_UID, INTEGRATION_BOPE).Returns(false);
            var command = CreateCommand();

            // Act
            var response = await _handler.Handle(command, default(CancellationToken));

            // Assert
            response.IsFailure.Should().BeTrue();
            response.Messages.Should().HaveCount(1);
            await VerifyRepository(1);
        }



        [Fact]
        public async Task WhenIsAuthenticated_ShouldReturnOk()
        {
            // Arrange
            _bopeRepository.Authenticate(REQUEST_ID, ACCOUNT_ID, OTP_CODE, DEVICE_UID, INTEGRATION_BOPE).Returns(true);
            var command = CreateCommand();

            // Act
            var response = await _handler.Handle(command, default(CancellationToken));

            // Assert
            response.IsSuccess.Should().BeTrue();
            response.Messages.Should().BeEmpty();
            response.Value.Authorized.Should().BeTrue();
            await VerifyRepository(1);
        }

        private static AuthenticateTransactionCommand CreateCommand()
            => new AuthenticateTransactionCommand(REQUEST_ID, ACCOUNT_ID, OTP_CODE, DEVICE_UID, INTEGRATION_BOPE);

        private async Task VerifyRepository(int numberOfCalls) => await _bopeRepository
                            .Received(numberOfCalls)
                            .Authenticate(REQUEST_ID, ACCOUNT_ID, OTP_CODE, DEVICE_UID, INTEGRATION_BOPE);
    }
}
