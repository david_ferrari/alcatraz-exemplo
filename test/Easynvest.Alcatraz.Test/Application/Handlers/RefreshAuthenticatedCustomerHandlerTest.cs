﻿using Easynvest.Alcatraz.Application.Query.Handlers;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class RefreshAuthenticatedCustomerHandlerTest
    {
        private readonly ILogger<RefreshAuthenticatedCustomerHandler> _logger = Substitute.For<ILogger<RefreshAuthenticatedCustomerHandler>>();
        private readonly IAuthenticateCustomerRepository _authenticateCustomerRepository = Substitute.For<IAuthenticateCustomerRepository>();
        private RefreshAuthenticatedCustomerHandler _stubHandler;

        public RefreshAuthenticatedCustomerHandlerTest()
        {
            _stubHandler = new RefreshAuthenticatedCustomerHandler(_logger, _authenticateCustomerRepository);
            GetAuthenticatedCustomer();
        }

        private const string USERDOCUMENT = "123token456";

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_Query_IsOk()
        {
            var response = await _stubHandler.Handle(new RefreshAuthenticatedCustomerQuery(USERDOCUMENT), default(CancellationToken)).ConfigureAwait(false);

            AssertUserIsAuthenticated(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_Query_IsInvalid()
        {
            var response = await _stubHandler.Handle(new RefreshAuthenticatedCustomerQuery(""), default(CancellationToken)).ConfigureAwait(false);

            AssertUserIsNotAuthenticated(response, "CustomerId vazio");
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_CustomerIsNotFound()
        {
            _authenticateCustomerRepository.GetAuthenticatedCustomer(Arg.Any<string>(), Arg.Any<CustomerLoginType>()).Returns(p => Task.FromResult(new List<CustomerUserDto>()).GetAwaiter().GetResult());
            var response = await _stubHandler.Handle(new RefreshAuthenticatedCustomerQuery(USERDOCUMENT), default(CancellationToken)).ConfigureAwait(false);

            AssertUserIsNotAuthenticated(response, ValidateAuthenticateType.InvalidCredential.Display);
        }

        private void GetAuthenticatedCustomer()
        {
            _authenticateCustomerRepository.GetAuthenticatedCustomer(Arg.Any<string>(), Arg.Any<CustomerLoginType>()).Returns(x =>
            {
                var customers = new List<CustomerUserDto>
                {
                    new CustomerUserDto
                    {
                        CustomerId = USERDOCUMENT,
                        AccountNumber = 5090016,
                        Email = "gustavo.almeida@easynvest.com.br"
                    }
                };

                return customers;
            });
        }

        private static void AssertUserIsAuthenticated(ValidateAuthenticatedCustomerResponse response)
        {
            Assert.True(response.Authenticated);
            Assert.False(response.AuthenticatedMessages.Any());
        }

        private static void AssertUserIsNotAuthenticated(ValidateAuthenticatedCustomerResponse response, string message)
        {
            Assert.False(response.Authenticated);
            Assert.Contains(response.AuthenticatedMessages, x => x.Value == message);
        }
    }
}
