﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Services;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class UpdateSecretVersionHandlerTest
    {
        private const string CUSTOMERID = "12345678900";
        private const string CUSTOMEREMAIL = "EMAIL@EMAIL.COM";
        private const string CUSTOMERNAME = "NOME CLIENTE";
        private const string CUSTOMER_STATUS = "A";
        private readonly DateTime BIRTHDATE = new DateTime(1999, 1, 1);


        private readonly IPasswordRepository _repository;
        private readonly ILogger<UpdateSecretVersionHandler> _logger;

        public UpdateSecretVersionHandlerTest()
        {
            _repository = Substitute.For<IPasswordRepository>();
            _logger = Substitute.For<ILogger<UpdateSecretVersionHandler>>();
        }

        [Theory]
        [InlineData(SecretType.Password)]
        [InlineData(SecretType.EletronicSignature)]
        public async Task Should_Update_Valid_Secret(SecretType secretType)
        {
            _repository.GetByCustomerId(CUSTOMERID).Returns(new Password
                (
                    CreateCustomer(),
                    new List<PasswordHistory>(),
                    new List<PasswordHistory>(),
                    new PasswordDetails("EncodedPassword", "EncodedEletronicSignature", 0, 0, SaltGenerator.Generate(), SaltGenerator.Generate())
                ));

            var handler = new UpdateSecretVersionHandler(_repository, _logger);
            var result = await handler.Handle(new UpdateSecretVersionCommand(CUSTOMERID, secretType, "Easynvest4857#@$"), CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
            await _repository.Received(1).UpdateSecret(Arg.Is<Password>(p => p.Customer.CustomerId == CUSTOMERID));
        }

        private Customer CreateCustomer()
        {
            return new Customer(CUSTOMERID, Name.Create(CUSTOMERNAME), new BirthDate(BIRTHDATE), CUSTOMEREMAIL, CUSTOMER_STATUS);
        }
    }
}
