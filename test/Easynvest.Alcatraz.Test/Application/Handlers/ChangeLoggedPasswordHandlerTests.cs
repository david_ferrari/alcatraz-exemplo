﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Commands.Request;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Test.Common;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ChangeLoggedPasswordHandlerTests : BaseChangePasswordHandler
    {
        private ChangeLoggedPasswordHandler _handler;
        private ChangeLoggedPasswordRequest _changeLoggedPasswordRequest;
        private readonly string _customerId = "80030";
        private const string _customerEmail = "teste@easynvest.com.br";
        private readonly ILogger<ChangeLoggedPasswordHandler> _logger = Substitute.For<ILogger<ChangeLoggedPasswordHandler>>();
        private static string electronicSignature = "d4sd56f46sdf";

        public ChangeLoggedPasswordHandlerTests()
        {
            _mediator.Send(Arg.Any<SendEmailCommand>(), Arg.Any<CancellationToken>()).Returns(Task.FromResult(Response.Ok()));
            _handler = new ChangeLoggedPasswordHandler(_mediator, _repo, _pushNotification, _emailTemplateOption, _logger);
            _changeLoggedPasswordRequest = new ChangeLoggedPasswordRequest(electronicSignature, "Qx134679*", "Qx134679*");
        }

        [Fact]
        public async Task Should_ReturnSuccess_When_Entity_IsValid()
        {
            var command = new ChangeLoggedPasswordCommand
                (
                    _changeLoggedPasswordRequest,
                    _customerId
                );

            _repo
                .GetByCustomerId(Arg.Any<string>())
                .Returns(x => CreatePasswordEntity(electronicSignature));

            _mediator
                .Send(Arg.Any<ValidateElectronicSignatureCommand>(), Arg.Any<CancellationToken>())
                .Returns
                (
                    Response<ValidateElectronicSignatureResponse>
                        .Ok(default(ValidateElectronicSignatureResponse))
                );

            var response = await _handler
                .Handle(command, CancellationToken.None)
                .ConfigureAwait(false);

            Assert.True(response.IsSuccess);
            Assert.Equal(0, response.Messages.Count);
        }

        [Fact]
        public async Task Should_Validate_When_Command_NotValid()
        {
            var changeLoggedPasswordRequest = new ChangeLoggedPasswordRequest(string.Empty, string.Empty, "Qx134679*");
            _repo.GetByCustomerId(Arg.Any<string>()).Returns(x => CreatePasswordEntity(electronicSignature));
            var command = new ChangeLoggedPasswordCommand(changeLoggedPasswordRequest, _customerId);
            var response = await _handler.Handle(command, CancellationToken.None).ConfigureAwait(false);

            AssertResponseHasErrors(response);
        }

        [Fact]
        public async Task Should_Not_CreateDomain_When_Entity_NotValid()
        {
            var changeLoggedPasswordRequest = new ChangeLoggedPasswordRequest("12465", "12465", "12465");

            var command = new ChangeLoggedPasswordCommand
                (
                    changeLoggedPasswordRequest,
                    _customerId
                );

            _mediator
                .Send(Arg.Any<ValidateElectronicSignatureCommand>(), Arg.Any<CancellationToken>())
                .Returns
                (
                    Response<ValidateElectronicSignatureResponse>
                        .Ok(default(ValidateElectronicSignatureResponse))
                );

            _repo
                .GetByCustomerId(Arg.Any<string>())
                .Returns(x => CreatePasswordEntity(electronicSignature));

            var response = await _handler
                .Handle(command, CancellationToken.None)
                .ConfigureAwait(false);

            AssertResponseHasErrors(response);
        }
    }
}
