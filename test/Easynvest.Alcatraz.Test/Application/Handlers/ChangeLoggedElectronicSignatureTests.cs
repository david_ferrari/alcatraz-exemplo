﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Commands.Request;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Extensions;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Test.Common;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ChangeLoggedElectronicSignatureTests : BaseChangePasswordHandler
    {
        private ChangeLoggedElectronicSignatureHandler _handler;
        private ChangeElectronicSignatureRequest _changeLoggedElectronicSignatureRequest;
        private readonly string _customerId = "80030";
        private const string NEW_ELECTRONIC_SIGNATURE = "Qx134679*";
        private const string ELECTRONIC_SIGNATURE = "teste";
        private readonly ILogger<ChangeLoggedElectronicSignatureHandler> _logger = Substitute.For<ILogger<ChangeLoggedElectronicSignatureHandler>>();

        public ChangeLoggedElectronicSignatureTests()
        {
            _mediator.Send(Arg.Any<SendEmailCommand>(), Arg.Any<CancellationToken>()).Returns(Task.FromResult(Response.Ok()));
            _handler = new ChangeLoggedElectronicSignatureHandler(_mediator, _repo, _emailTemplateOption, _logger, _pushNotification);
            _changeLoggedElectronicSignatureRequest = new ChangeElectronicSignatureRequest(NEW_ELECTRONIC_SIGNATURE, NEW_ELECTRONIC_SIGNATURE, ELECTRONIC_SIGNATURE);
            _repo.GetByCustomerId(Arg.Any<string>()).Returns(x => CreatePasswordEntity(ELECTRONIC_SIGNATURE));
        }

        [Fact]
        public async Task Should_ReturnSuccess_When_Entity_IsValid()
        {
            var command = new ChangeElectronicSignatureCommand
                (
                    request: _changeLoggedElectronicSignatureRequest,
                    customerId: _customerId
                );

            _repo
                .GetByCustomerId(Arg.Any<string>())
                .Returns(x => CreatePasswordEntity(ELECTRONIC_SIGNATURE));

            _mediator
                .Send(Arg.Any<ValidateElectronicSignatureCommand>(), Arg.Any<CancellationToken>())
                .Returns
                (
                    Response<ValidateElectronicSignatureResponse>
                        .Ok(default(ValidateElectronicSignatureResponse))
                );

            var response = await _handler
                .Handle(command, CancellationToken.None)
                .ConfigureAwait(false);

            Assert.True(response.IsSuccess);
            Assert.Equal(0, response.Messages.Count);
        }

        [Fact]
        public async Task Should_Validate_When_Command_NotValid()
        {
            _repo
                .GetByCustomerId(Arg.Any<string>())
                .Returns(x => CreatePasswordEntity("wrongPassword"));

            var command = new ChangeElectronicSignatureCommand
                (
                    _changeLoggedElectronicSignatureRequest,
                    "Qx134679*"
                );

            _mediator
                .Send(Arg.Any<ValidateElectronicSignatureCommand>(), Arg.Any<CancellationToken>())
                .Returns
                (
                    Response<ValidateElectronicSignatureResponse>
                        .Ok(default(ValidateElectronicSignatureResponse))
                );

            var response = await _handler
                .Handle(command, CancellationToken.None)
                .ConfigureAwait(false);

            AssertResponseHasErrors(response);
        }

        [Theory]
        [InlineData("", "abcde12", ELECTRONIC_SIGNATURE, "Requisição inválida.")]
        [InlineData("abcde12", "", ELECTRONIC_SIGNATURE, "Requisição inválida.")]
        [InlineData("", "", ELECTRONIC_SIGNATURE, "Requisição inválida.")]
        [InlineData("", "", "", "Requisição inválida.")]
        [InlineData("abcde12", "abcde1", ELECTRONIC_SIGNATURE, "As senhas informadas não são iguais.")]
        public async Task Should_Not_CreateDomain_When_Request_NotValid(string newElectronicSignature, string confirmElectronicSignature, string oldElectronicSignature, string responseMessage)
        {
            var changeLoggedPasswordRequest = new ChangeElectronicSignatureRequest(newElectronicSignature, confirmElectronicSignature, oldElectronicSignature);
            var command = new ChangeElectronicSignatureCommand(changeLoggedPasswordRequest, _customerId);

            var response = await _handler.Handle(command, CancellationToken.None).ConfigureAwait(false);

            AssertResponseHasErrors(response, responseMessage);
        }

        [Fact]
        public async Task Should_Not_CreateDomain_When_ElectronicSignature_NotValid()
        {
            var request = new ChangeElectronicSignatureRequest
                (
                    NEW_ELECTRONIC_SIGNATURE,
                    NEW_ELECTRONIC_SIGNATURE,
                    ELECTRONIC_SIGNATURE
                );

            var command = new ChangeElectronicSignatureCommand
                (
                    request,
                    _customerId
                );

            _mediator
                .Send(Arg.Any<ValidateElectronicSignatureCommand>(), Arg.Any<CancellationToken>())
                .Returns
                (
                    Response<ValidateElectronicSignatureResponse>.Fail("Assinatura Eletrônica inválida.")
                );

            var response = await _handler
                .Handle(command, CancellationToken.None)
                .ConfigureAwait(false);

            AssertResponseHasErrors(response, "Assinatura Eletrônica inválida.");
        }

        [Fact]
        public async Task Should_ReturnFailure_When_EmailSentWithErrors()
        {
            var command = new ChangeElectronicSignatureCommand
                (
                    request: _changeLoggedElectronicSignatureRequest,
                    customerId: _customerId
                );

            _mediator
                .Send(Arg.Any<ValidateElectronicSignatureCommand>(), Arg.Any<CancellationToken>())
                .Returns
                (
                    Response<ValidateElectronicSignatureResponse>
                        .Ok(default(ValidateElectronicSignatureResponse))
                );

            _mediator
                .Send(Arg.Any<SendEmailCommand>(), Arg.Any<CancellationToken>())
                .Returns(Task.FromResult(Response.Fail("error")));

            var response = await _handler
                .Handle(command, CancellationToken.None)
                .ConfigureAwait(false);

            AssertResponseHasErrors
                (
                    response,
                    string.Format
                        (
                            ErrorMessages.ALTER_SECRET_EMAIL_ERROR,
                            SecretType.EletronicSignature.GetDescription(),
                            "Operação não concluida"
                        )
                );

            _logger
                .ReceivedWithAnyArgs()
                .LogError(string.Empty);
        }

        [Fact]
        public async Task Should_LogError_When_DomainReturnsNotification()
        {
            var command = new ChangeElectronicSignatureCommand
                (
                    request: _changeLoggedElectronicSignatureRequest,
                    customerId: _customerId
                );

            _repo
                .GetByCustomerId(Arg.Any<string>())
                .Returns(x => CreatePasswordEntity("AssinaturaInvalida"));

            _mediator
                .Send(Arg.Any<ValidateElectronicSignatureCommand>(), Arg.Any<CancellationToken>())
                .Returns(Response<ValidateElectronicSignatureResponse>.Ok());

            var response = await _handler
                .Handle(command, CancellationToken.None)
                .ConfigureAwait(false);

            AssertResponseHasErrors
                (
                    response,
                    "Assinatura Eletrônica inválida."
                );

            _logger.ReceivedWithAnyArgs().LogError(string.Empty);
        }
    }
}
