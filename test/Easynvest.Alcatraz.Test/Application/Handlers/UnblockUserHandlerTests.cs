﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Messages.Events.Security.User;
using FluentAssertions;
using MassTransit;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class UnblockUserHandlerTests
    {
        private readonly ILogger<UnblockUserHandler> _logger;
        private readonly IUserBlockRepository _repository;
        private readonly IBusControl _busControl;

        public UnblockUserHandlerTests()
        {
            _logger = Substitute.For<ILogger<UnblockUserHandler>>();
            _repository = Substitute.For<IUserBlockRepository>();
            _busControl = Substitute.For<IBusControl>();
        }

        [Fact]
        public async Task Should_Block_User()
        {
            var handler = new UnblockUserHandler(_repository, _busControl, _logger);
            var result = await handler.Handle(new UnblockUserCommand("123", BlockType.Security), CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
            await _repository.Received(1).Remove(Arg.Is("123"), Arg.Is(BlockType.Security));
            await _busControl.Received(1).Publish<UserUnblocked>(Arg.Any<object>());
        }

        [Fact]
        public async Task Should_Fail_On_Error()
        {
            _repository.Remove(Arg.Is("123"), Arg.Is(BlockType.Security)).Throws(new Exception("erro"));
            var handler = new UnblockUserHandler(_repository, _busControl, _logger);
            var result = await handler.Handle(new UnblockUserCommand("123", BlockType.Security), CancellationToken.None);

            result.IsFailure.Should().BeTrue();
        }

        [Fact]
        public async Task Should_Not_Unblock_When_User_Has_More_Than_One_Block()
        {
            _repository.Get("123").Returns(new List<UserBlock> { new UserBlock("123", BlockType.Registry) });
            var handler = new UnblockUserHandler(_repository, _busControl, _logger);
            var result = await handler.Handle(new UnblockUserCommand("123", BlockType.Security), CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
            await _repository.Received(1).Remove(Arg.Is("123"), Arg.Is(BlockType.Security));
            await _busControl.DidNotReceiveWithAnyArgs().Publish<UserUnblocked>(Arg.Any<object>());
        }
    }
}
