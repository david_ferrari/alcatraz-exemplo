﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.KingsCross.Bus.Contracts;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class SendEmailHandlerTests
    {
        private const int _emailId = 1;
        private const string _label = "DefaultLabel";
        private readonly Dictionary<string, string> _emailParameters = new Dictionary<string, string>() { { "TESTE", "TESTE" } };
        private readonly ILogger<SendEmailHandler> _logger = Substitute.For<ILogger<SendEmailHandler>>();
        private readonly IKingsCrossServiceBus _serviceBus = Substitute.For<IKingsCrossServiceBus>();
        private readonly IOptions<EmailMessaging> _emailMessaging = Substitute.For<IOptions<EmailMessaging>>();
        private SendEmailHandler _handler;

        public SendEmailHandlerTests()
        {
            _emailMessaging.Value.Returns(new EmailMessaging() { Exchange = "Exchange", Queue = "Queue" });
            _handler = new SendEmailHandler(_serviceBus, _emailMessaging, _logger);
        }

        [Fact]
        public async Task Should_ReturnSuccess_When_CommandIsValid()
        {
            var command = CreateValidCommand();

            var mockResponse = await _handler.Handle(command, CancellationToken.None).ConfigureAwait(false);

            Assert.True(mockResponse.IsSuccess);
            Assert.Empty(mockResponse.Messages);
        }


        [Theory]
        [InlineData(0, default(Dictionary<string, string>), "teste", "Id de template de email inválido.")]
        [InlineData(1, default(Dictionary<string, string>), "", "É necessario um destinatário.")]
        [InlineData(1, null, "teste", "Parâmetros de email inválidos.")]
        [InlineData(0, null, null, "Parâmetros de email inválidos.")]
        public async Task Should_ReturnFailure_When_CommandIsInValid(int emailId, Dictionary<string, string> emailParameters, string emailTo, string message)
        {
            var command = new SendEmailCommand(emailId, emailParameters, emailTo, _label);

            var mockResponse = await _handler.Handle(command, CancellationToken.None).ConfigureAwait(false);

            Assert.True(mockResponse.IsFailure);
            Assert.NotEmpty(mockResponse.Messages);
            Assert.Contains(mockResponse.Messages, x => x == message);
        }

        [Fact]
        public async Task Should_ReturnFailure_When_EmailIsNotFound()
        {
            var command = CreateValidCommandInvalid();

            var mockResponse = await _handler.Handle(command, CancellationToken.None).ConfigureAwait(false);

            Assert.True(mockResponse.IsFailure);
            Assert.NotEmpty(mockResponse.Messages);
            Assert.Contains(mockResponse.Messages, x => x == "Id de template de email inválido.");
        }

        private SendEmailCommand CreateValidCommand()
        {
            return new SendEmailCommand(_emailId, _emailParameters, "teste@teste.com.br", "Alteração de assinatura eletrônica");
        }

        private SendEmailCommand CreateValidCommandInvalid()
        {
            return new SendEmailCommand(default(int), _emailParameters, "teste@teste.com.br", "Alteração de assinatura eletrônica");
        }
    }
}
