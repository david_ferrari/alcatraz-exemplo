﻿using Easynvest.Alcatraz.Application.Query.Handlers;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Entities;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Easynvest.Alcatraz.Domain.Models.Entities;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ValidateAuthenticatedCustomerHandlerTest
    {
        private const string PASSWORD = "123654";
        private const string INVALIDPASSWORD = "654321";
        private const string EMAILUSER = "diego.silva@easynvest.com.br";
        private const string FCMTOKEN = "123token456";
        private const string IPADDRESS = "192.168.0.1";
        private const int ACCOUNTNUMBER = 225123;
        private const string CLEANTEXT = "123654";
        private const string CUSTOMERID = "32927484880";
        private IMediator _mediator;
        private ILogger<ValidateAuthenticatedCustomerHandler> _logger;
        private IAuthenticateCustomerRepository _authenticateCustomerRepositor;
        private ICustomerSecretRepository _customerSecretRepository;
        private IOptions<CustomerSecretOption> _options;
        private IOptions<LoginConfigurationOptions> _loginConfigurationOptions;
        private IUserBlockRepository _blockRepository;
        private ValidateAuthenticatedCustomerHandler _sut;

        public ValidateAuthenticatedCustomerHandlerTest()
        {
            _mediator = Substitute.For<IMediator>();
            _logger = Substitute.For<ILogger<ValidateAuthenticatedCustomerHandler>>();
            _authenticateCustomerRepositor = Substitute.For<IAuthenticateCustomerRepository>();
            _customerSecretRepository = Substitute.For<ICustomerSecretRepository>();
            _options = Substitute.For<IOptions<CustomerSecretOption>>();
            _options.Value.Returns(x => { return new CustomerSecretOption { MaxInvalidLoginAttempts = 3 }; });
            _loginConfigurationOptions = Substitute.For<IOptions<LoginConfigurationOptions>>();
            _loginConfigurationOptions.Value.Returns(x => new LoginConfigurationOptions { EmailLoginDisabled = false });
            _loginConfigurationOptions = Options.Create(new LoginConfigurationOptions());
            _blockRepository = Substitute.For<IUserBlockRepository>();

            _sut = new ValidateAuthenticatedCustomerHandler(_mediator, _logger,
                _authenticateCustomerRepositor,
                _blockRepository,
                _customerSecretRepository,
                _options,
                _loginConfigurationOptions);
        }

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_Query_Receive_Email_IdOk()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(0);

            var response = await _sut.Handle(new ValidateAuthenticatedCustomerQuery(EMAILUSER, PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            Assert.True(response.Authenticated);
            Assert.False(response.AuthenticatedMessages.Any());
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_Query_Receive_Email_AndLoginByEmailIsDisabled()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(0);

            _loginConfigurationOptions = Substitute.For<IOptions<LoginConfigurationOptions>>();
            _loginConfigurationOptions.Value.Returns(x => new LoginConfigurationOptions { EmailLoginDisabled = true });

            var handler = new ValidateAuthenticatedCustomerHandler(_mediator, _logger,
             _authenticateCustomerRepositor,
             _blockRepository,
             _customerSecretRepository,
             _options,
             _loginConfigurationOptions);

            var response = await handler.Handle(new ValidateAuthenticatedCustomerQuery(EMAILUSER, PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            Assert.False(response.Authenticated);
            Assert.True(response.AuthenticatedMessages.Any());
        }

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_Query_Receive_CustomerId_IsOk()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(0);

            var response = await _sut.Handle(new ValidateAuthenticatedCustomerQuery(CUSTOMERID, PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);
            AssertUserAuthenticatedTrue(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithoutErrors_When_Query_Receive_AccountNumber_IsOk()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(0);

            var response = await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);
            AssertUserAuthenticatedTrue(response);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_Query_Receive_InvalidCredentials()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(2);

            var response = await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), "PassErro", FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            Assert.Equal(ValidateAuthenticateType.InvalidCredential.Display, response.AuthenticatedMessages.FirstOrDefault().Value);
            Assert.False(response.Authenticated);
        }

        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_LoginAttemptsInvalid()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(3);

            var response = await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            Assert.Equal(ValidateAuthenticateType.BlockedUser.Display, response.AuthenticatedMessages.FirstOrDefault().Value);
            Assert.False(response.Authenticated);
        }
        [Fact]
        public async Task Should_ReturnResponseWithErrors_When_User_Has_Blocks()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(0);

            _blockRepository.Get(CUSTOMERID).Returns(new List<UserBlock> { new UserBlock(CUSTOMERID, BlockType.Security) });

            var response = await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            Assert.Equal(ValidateAuthenticateType.DisabledUser.Display, response.AuthenticatedMessages.FirstOrDefault().Value);
            Assert.False(response.Authenticated);
        }

        [Fact]
        public async Task Should_Not_Call_UpdateCustomerSecret_When_Consecutive_Successfull_LoginAttempts()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(0);
            await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            await _customerSecretRepository.DidNotReceiveWithAnyArgs().UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.Password);
        }

        [Fact]
        public async Task Should_Call_UpdateCustomerSecret_When_Failed_After_Success_LoginAttempt()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(0);
            await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), INVALIDPASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            await _customerSecretRepository.ReceivedWithAnyArgs(1).UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.Password);
        }

        [Fact]
        public async Task Should_Call_UpdateCustomerSecret_When_Consecutive_Failed_LoginAttempt()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(2);
            await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), INVALIDPASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            await _customerSecretRepository.ReceivedWithAnyArgs(1).UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.Password);
        }

        [Fact]
        public async Task Should_Call_UpdateCustomerSecret_When_Blocked_User()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(3);
            _blockRepository.Get(CUSTOMERID).Returns(new List<UserBlock> { new UserBlock(CUSTOMERID, BlockType.Security) });
            await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            await _customerSecretRepository.ReceivedWithAnyArgs(1).UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.Password);
        }

        [Fact]
        public async Task Should_Call_UpdateCustomerSecret_When_Success_After_Failed_LoginAttempt()
        {
            GetAuthenticatedCustomer();
            GetCustomerSecret(1);
            await _sut.Handle(new ValidateAuthenticatedCustomerQuery(ACCOUNTNUMBER.ToString(), PASSWORD, FCMTOKEN, IPADDRESS), default(CancellationToken)).ConfigureAwait(false);

            await _customerSecretRepository.ReceivedWithAnyArgs(1).UpdateCustomerSecret(Arg.Any<CustomerSecret>(), SecretType.Password);
        }

        private void GetAuthenticatedCustomer()
        {
            _authenticateCustomerRepositor.GetAuthenticatedCustomer(Arg.Any<string>(), Arg.Any<CustomerLoginType>()).Returns(x =>
            {
                var customers = new List<CustomerUserDto>
                {
                    new CustomerUserDto
                    {
                        CustomerId = CUSTOMERID,
                        AccountNumber = 5090016,
                        Email = EMAILUSER,
                        PublicId = "c34eeadef8d55b0e5ae7d637d8bbd449b813cb27bb1a223a25398a368ef53aed",
                        Status = "A"
                    }
                };

                return customers;
            });
        }

        private void GetCustomerSecret(int loginAttempts)
        {
            _customerSecretRepository.Get(Arg.Any<string>(), Arg.Any<SecretType>()).Returns(x =>
            {
                return new CustomerSecret(CUSTOMERID, CLEANTEXT, SecretType.Password, loginAttempts);
            });
        }

        private static void AssertUserAuthenticatedTrue(ValidateAuthenticatedCustomerResponse response)
        {
            Assert.True(response.Authenticated);
            Assert.False(response.AuthenticatedMessages.Any());
        }
    }
}