﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class AddTrustedDeviceHandlerTests
    {
        private readonly IValidateTimeTokenRepository _repository;
        private readonly ILogger<AddTrustedDeviceHandler> _logger;

        public AddTrustedDeviceHandlerTests()
        {
            _repository = Substitute.For<IValidateTimeTokenRepository>();
            _logger = Substitute.For<ILogger<AddTrustedDeviceHandler>>();
        }

        [Fact]
        public async Task Should_Update_Token()
        {
            _repository.GetValidateTimeTokenByDeviceUid("111", "222").Returns(new ValidateTimeToken("222", "111", Alcatraz.Domain.Enumerators.ConfirmedTrustedDeviceType.NotConfirmed));
            var handler = new AddTrustedDeviceHandler(_repository, _logger);
            var result = await handler.Handle(new AddTrustedDeviceCommand("111")
            {
                CustomerId = "222"
            }, CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
            await _repository.Received(1).UpdateTimeToken(Arg.Is<ValidateTimeToken>(t => t.ConfirmedTrustedDevice == Alcatraz.Domain.Enumerators.ConfirmedTrustedDeviceType.Trusted &&
               t.ExpiryTime.Date == DateTime.Now.AddDays(30).Date));
        }

        [Fact]
        public async Task Should_Return_Error_When_Token_Is_Not_Found()
        {
            var handler = new AddTrustedDeviceHandler(_repository, _logger);
            var result = await handler.Handle(new AddTrustedDeviceCommand("111")
            {
                CustomerId = "222"
            }, CancellationToken.None);

            result.IsFailure.Should().BeTrue();
            result.ErrorResponse.Error.Message.Should().Be(Errors.ErrorsToken.DeviceNotFound().Message);
        }
    }
}
