﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class ValidateElectronicSignatureAndDeviceHandlerTest
    {
        private const string CUSTOMER_ID = "12345678900";
        private const string ELECTRONIC_SIGNATURE = "qx3123";
        private const string DEVICE_ID = "111111";
        private readonly IMediator _mediator;
        private readonly IBopeRepository _bopeRepository;
        private readonly IValidateTimeTokenRepository _validateTimeTokenRepository;
        private readonly ILogger<ValidateElectronicSignatureAndDeviceHandler> _logger;

        private ValidateElectronicSignatureAndDeviceHandler _handler;

        public ValidateElectronicSignatureAndDeviceHandlerTest()
        {
            _mediator = Substitute.For<IMediator>();
            _bopeRepository = Substitute.For<IBopeRepository>();
            _validateTimeTokenRepository = Substitute.For<IValidateTimeTokenRepository>();
            _logger = Substitute.For<ILogger<ValidateElectronicSignatureAndDeviceHandler>>();

            _handler = new ValidateElectronicSignatureAndDeviceHandler(_mediator, _bopeRepository, _validateTimeTokenRepository, _logger);
        }

        [Fact]
        public async Task Should_ReturnOk_When_ElectronicSingatureIsCorrectAndDeviceIdEmpty()
        {
            //arrange
            var cmd = new ValidateElectronicSignatureAndDeviceCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID, "");
            _mediator.Send(Arg.Any<ValidateElectronicSignatureCommand>()).Returns(new Response<ValidateElectronicSignatureResponse>());

            //act
            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            //assert
            Assert.True(response.IsSuccess);
        }

        [Fact]
        public async Task Should_ReturnOk_When_ElectronicSingatureIsCorrectAndDeviceIsTrusted()
        {
            //arrange
            var cmd = new ValidateElectronicSignatureAndDeviceCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID, DEVICE_ID);
            _mediator.Send(Arg.Any<ValidateElectronicSignatureCommand>()).Returns(new Response<ValidateElectronicSignatureResponse>());
            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>()).Returns(CreateValidateTimeTokenTrusted());

            //act
            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            //assert
            Assert.True(response.IsSuccess);
        }

        [Fact]
        public async Task Should_ReturnOk_When_ElectronicSingatureIsCorrectAndHasUnstrustedDeviceWithoutEasyToken()
        {
            //arrange
            var cmd = new ValidateElectronicSignatureAndDeviceCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID, DEVICE_ID);
            _mediator.Send(Arg.Any<ValidateElectronicSignatureCommand>()).Returns(new Response<ValidateElectronicSignatureResponse>());
            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>()).Returns(CreateValidateTimeToken(ConfirmedTrustedDeviceType.NotConfirmed));

            //act
            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            //assert
            Assert.True(response.IsSuccess);
        }

        [Fact]
        public async Task Should_ReturnBadRequest_When_DeviceIsUnstrustedAndHasEasyToken()
        {
            //arrange
            var cmd = new ValidateElectronicSignatureAndDeviceCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID, DEVICE_ID);
            _mediator.Send(Arg.Any<ValidateElectronicSignatureCommand>()).Returns(new Response<ValidateElectronicSignatureResponse>());
            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>()).Returns(CreateValidateTimeToken(ConfirmedTrustedDeviceType.NotConfirmed));
            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>()).Returns(new DeviceRegisteredDto());

            //act
            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            //assert
            Assert.True(response.IsFailure);
            Assert.Equal("Dispositivo não é confiável.", response.ErrorResponse.Error.Message);
        }

        [Fact]
        public async Task Should_ReturnBadRequest_When_ThereIsNoDeviseAndElectronicSignatureIsWrong()
        {
            //arrange
            var cmd = new ValidateElectronicSignatureAndDeviceCommand(ELECTRONIC_SIGNATURE, CUSTOMER_ID, string.Empty);
            _mediator.Send(Arg.Any<ValidateElectronicSignatureCommand>()).Returns(Response<ValidateElectronicSignatureResponse>.Fail(""));
            _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(Arg.Any<string>(), Arg.Any<string>()).Returns(CreateValidateTimeToken(ConfirmedTrustedDeviceType.NotConfirmed));
            _bopeRepository.GetTokenInfo(Arg.Any<string>(), Arg.Any<string>()).Returns(new DeviceRegisteredDto());

            //act
            var response = await _handler.Handle(cmd, CreateCancellationToken()).ConfigureAwait(false);

            //assert
            Assert.True(response.IsFailure);
        }

        private static CancellationToken CreateCancellationToken() => new CancellationToken();

        private static ValidateTimeToken CreateValidateTimeToken(ConfirmedTrustedDeviceType confirmedTrustedDevice)
            => new ValidateTimeToken(CUSTOMER_ID, DEVICE_ID, confirmedTrustedDevice);

        private static ValidateTimeToken CreateValidateTimeTokenTrusted() => new ValidateTimeToken(CUSTOMER_ID, DEVICE_ID, ConfirmedTrustedDeviceType.Trusted);
    }
}