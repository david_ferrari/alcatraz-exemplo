﻿using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Application.Queries;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Test.Common;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application
{
    public class ValidHashQueryHandlerTests
    {
        [Fact]
        public async Task Should_ReturnError_When_HashIsEmpty()
        {
            var repo = Substitute.For<IHashPasswordRepository>();
            var logger = Substitute.For<ILogger<ValidHashQueryHandler>>();
            var sut = new ValidHashQueryHandler(repo, logger);

            var actual = await sut.Handle(new ValidHashQuery(string.Empty), CancellationToken.None);

            Assert.True(actual.IsFailure);
            Assert.True(actual.Messages.Count == 1);
        }

        [Fact]
        public async Task Should_ReturnError_When_HashIsNotFound()
        {
            var repo = Substitute.For<IHashPasswordRepository>();
            var logger = Substitute.For<ILogger<ValidHashQueryHandler>>();
            repo.GetByHash(Arg.Any<string>()).Returns(Task.FromResult<PasswordRecoveryRequest>(null));
            var sut = new ValidHashQueryHandler(repo, logger);

            var actual = await sut.Handle(new ValidHashQuery("hash"), CancellationToken.None).ConfigureAwait(false);

            Assert.True(actual.IsFailure);
            Assert.True(actual.Messages.Count == 1);
        }

        [Fact]
        public async Task Should_ReturnError_When_RequestIsExpired()
        {
            var repo = Substitute.For<IHashPasswordRepository>();
            var logger = Substitute.For<ILogger<ValidHashQueryHandler>>();
            var expiredRequest = CreatePasswordRecoveryRequest(-1);
            repo.GetByHash(Arg.Any<string>()).Returns(Task.FromResult(expiredRequest));
            var sut = new ValidHashQueryHandler(repo, logger);

            var actual = await sut.Handle(new ValidHashQuery("hash"), CancellationToken.None);

            Assert.True(actual.IsFailure);
            Assert.True(actual.Messages.Count == 1);
        }

        [Fact]
        public async Task Should_ReturnNoError_When_RequestIsValid()
        {
            var repo = Substitute.For<IHashPasswordRepository>();
            var logger = Substitute.For<ILogger<ValidHashQueryHandler>>();
            var validRequest = CreatePasswordRecoveryRequest(1);
            repo.GetByHash(Arg.Any<string>()).Returns(Task.FromResult(validRequest));
            var sut = new ValidHashQueryHandler(repo, logger);

            var actual = await sut.Handle(new ValidHashQuery("hash"), CancellationToken.None);

            Assert.False(actual.IsFailure);
            Assert.True(actual.Messages.Count == 0);
        }

        private static PasswordRecoveryRequest CreatePasswordRecoveryRequest(int days)
            => PasswordRecoveryRequest.Create(
                string.Empty,
                new Customer("", Name.Create(string.Empty), new BirthDate(DateTime.Now), "", "A"),
                new ExpirationDate(new TimeServerStub(DateTime.Now),
                DateTime.Now.AddDays(days)));




    }
}