﻿using Easynvest.Alcatraz.Application.Query.Handlers;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Easynvest.Alcatraz.Test.Application.Handlers
{
    public class GetBlockedUsersHandlerTests
    {
        private readonly IUserBlockRepository _repository;
        private readonly ILogger<GetBlockedUsersHandler> _logger;

        public GetBlockedUsersHandlerTests()
        {
            _repository = Substitute.For<IUserBlockRepository>();
            _logger = Substitute.For<ILogger<GetBlockedUsersHandler>>();
        }

        [Fact]
        public async Task Should_List_Users()
        {
            var list = new List<UserBlock> { new UserBlock("123", BlockType.Security), new UserBlock("456", BlockType.Security) }.AsReadOnly();
            _repository.Get(BlockType.Security).Returns(list);

            var handler = new GetBlockedUsersHandler(_repository, _logger);
            var result = await handler.Handle(new GetBlockedUsersQuery(BlockType.Security), CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
            result.Value.Users.Should().Equal(list, (u1, u2) => u1.CustomerId == u2.CustomerId
            && u1.BlockDate == u2.BlockDate && u1.BlockType == u2.BlockType.GetDescription());
        }

        [Fact]
        public async Task Should_Fail_On_Error()
        {
            _repository.Get(BlockType.Security).Throws(new Exception("erro"));

            var handler = new GetBlockedUsersHandler(_repository, _logger);
            var result = await handler.Handle(new GetBlockedUsersQuery(BlockType.Security), CancellationToken.None);

            result.IsFailure.Should().BeTrue();
            result.Value?.Users.Should().BeNullOrEmpty();
        }
    }
}
