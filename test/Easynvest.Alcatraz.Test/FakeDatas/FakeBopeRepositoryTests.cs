﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Test.FakeDatas
{
    public class FakeBopeRepositoryTests
    {
    }

    public class FakeErrorHttpMessageHandler : DelegatingHandler
    {
        private readonly HttpResponseMessage _fakeResponse;
        private readonly bool _isException;

        public FakeErrorHttpMessageHandler(HttpResponseMessage responseMessage, bool isException)
        {
            _isException = isException;
            _fakeResponse = responseMessage;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (_isException)
            {
                throw new TimeoutException();
            }

            return await Task.FromResult(_fakeResponse);
        }
    }
}
