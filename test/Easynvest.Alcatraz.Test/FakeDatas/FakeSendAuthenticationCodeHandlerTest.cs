﻿using System.Collections.Generic;
using Easynvest.Alcatraz.Domain.Dtos;

namespace Easynvest.Alcatraz.Test.FakeDatas
{
    public static partial class FakeData
    {
        public static IEnumerable<CustomerUserDto> GetAuthenticatedCustomers
        {
            get
            {
                return new List<CustomerUserDto>
                {
                    new CustomerUserDto
                    {
                        CustomerId = "03078621782",
                        AccountNumber = 5090016,
                        Email = "informatica@easynvest.com.br",
                        PublicId = "c34eeadef8d55b0e5ae7d637d8bbd449b813cb27bb1a223a25398a368ef53aed"
                    }
                };
            }
        }

        public static IEnumerable<CustomerUserDto> GetMultiplyCustomers
        {
            get
            {
                return new List<CustomerUserDto>
                {
                    new CustomerUserDto
                    {
                        CustomerId = "03078621782",
                        AccountNumber = 5090016,
                        Email = "informatica@easynvest.com.br",
                        PublicId = "c34eeadef8d55b0e5ae7d637d8bbd449b813cb27bb1a223a25398a368ef53aed"
                    },
                    new CustomerUserDto
                    {
                        CustomerId = "03078621782",
                        AccountNumber = 5090016,
                        Email = "informatica@easynvest.com.br",
                        PublicId = "c34eeadef8d55b0e5ae7d637d8bbd449b813cb27bb1a223a25398a368ef53aed"
                    }
                };
            }
        }
    }
}