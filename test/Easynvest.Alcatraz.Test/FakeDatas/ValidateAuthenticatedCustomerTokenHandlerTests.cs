﻿using System;
using Easynvest.Alcatraz.Application.Query.Requests;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Microsoft.AspNetCore.Http;

namespace Easynvest.Alcatraz.Test.FakeDatas
{
    public static partial class FakeData
    {
        public static CustomerUserDto GetAuthenticatedCustomer
        {
            get
            {
                return new CustomerUserDto
                {
                    CustomerId = "32927484880",
                    AccountNumber = 5090016,
                    Email = "informatica@easynvest.com.br",
                    PublicId = "c34eeadef8d55b0e5ae7d637d8bbd449b813cb27bb1a223a25398a368ef53aed"
                };
            }
        }

        public static ValidateAuthenticatedCustomerResponse SucceedValidateAuthenticatedCustomerResponse()
        {
            var validateAuthenticatedCustomerResponse = new ValidateAuthenticatedCustomerResponse();
            validateAuthenticatedCustomerResponse.CustomerUser = GetAuthenticatedCustomer;
            return validateAuthenticatedCustomerResponse;
        }

        public static ValidateAuthenticatedCustomerTokenResponse SucceedValidateAuthenticatedCustomerTokenResponse()
        {
            var validateAuthenticatedCustomerResponse = new ValidateAuthenticatedCustomerTokenResponse("12332132-3213213213-321321");
            validateAuthenticatedCustomerResponse.CustomerUser = GetAuthenticatedCustomer;
            return validateAuthenticatedCustomerResponse;
        }

        public static ValidateAuthenticatedCustomerTokenResponse FailValidateAuthenticatedCustomerTokenResponseWithStatusZero()
        {
            var validateAuthenticatedCustomerResponse = new ValidateAuthenticatedCustomerTokenResponse("12332132-3213213213-321321");
            validateAuthenticatedCustomerResponse.CustomerUser = GetAuthenticatedCustomer;
            validateAuthenticatedCustomerResponse.AddError(new Ops.Error("NEEDTOKEN", "NEEDTOKEN", 0));
            return validateAuthenticatedCustomerResponse;
        }

        public static ValidateAuthenticatedCustomerTokenResponse FailValidateAuthenticatedCustomerTokenResponseWithStatus500()
        {
            var validateAuthenticatedCustomerResponse = new ValidateAuthenticatedCustomerTokenResponse("12332132-3213213213-321321");
            validateAuthenticatedCustomerResponse.CustomerUser = GetAuthenticatedCustomer;
            validateAuthenticatedCustomerResponse.AddError(new Ops.Error("INTERNALPROCESSERROR", "INTERNALPROCESSERROR", StatusCodes.Status500InternalServerError));
            return validateAuthenticatedCustomerResponse;
        }

        public static Alcatraz.Domain.Responses.Response<AuthenticateTransactionResponse> FailAuthenticateTransactionResponse()
        {
            var authenticateTransactionResponse = new Alcatraz.Domain.Responses.Response<AuthenticateTransactionResponse>();
            authenticateTransactionResponse.AddError("AuthenticateTransaction");
            return authenticateTransactionResponse;
        }

        public static Alcatraz.Domain.Responses.Response<AuthenticateTransactionResponse> SuccessAuthenticateTransactionResponse()
            => new Alcatraz.Domain.Responses.Response<AuthenticateTransactionResponse>();

        public static DeviceRegisteredDto DeviceRegistered
        {
            get
            {
                return new DeviceRegisteredDto
                {
                    ConfirmedDate = DateTime.Now,
                    DeviceId = 10744341,
                    DeviceModel = "Google AOSP on IA Emulator",
                    DeviceNumber = "5511377257000",
                    DeviceSeed = "",
                    DeviceToken = "5b0ea2ae20312e5b7ced",
                    IsActive = true
                };
            }
        }

        public static ValidateAuthenticatedCustomerTokenRequest ValidateAuthenticatedCustomerTokenRequest
        {
            get
            {
                return new ValidateAuthenticatedCustomerTokenRequest("username", "password", "1234343", "23232144323", "34324324233", true);
            }
        }

        public static ValidateTimeToken ValidateTimeTokenValid
        {
            get
            {
                return new ValidateTimeToken("12860210814", "C7A3BE50-286C-4655-A11D-5FFAFC94093C", DateTime.Now.AddDays(1), Alcatraz.Domain.Enumerators.ConfirmedTrustedDeviceType.Trusted); ;
            }
        }

        public static ValidateTimeToken ValidateTimeTokenInvalid
        {
            get
            {
                return new ValidateTimeToken("12860210814", "C7A3BE50-286C-4655-A11D-5FFAFC94093C", DateTime.Now.AddDays(-1), Alcatraz.Domain.Enumerators.ConfirmedTrustedDeviceType.Trusted);
            }
        }

        public static ValidateTimeToken ValidateTimeTokenNull
        {
            get
            {
                return null;
            }
        }
    }
}