﻿using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Options;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Easynvest.Alcatraz.Test.Common
{
    public class BaseChangePasswordHandler
    {
        protected IPasswordRepository _repo;
        protected IHashPasswordRepository _passwordRecoveryRepository;
        protected IMediator _mediator;
        protected IPushNotificationService _pushNotification;
        protected IOptions<EmailTemplateOption> _emailTemplateOption = Substitute.For<IOptions<EmailTemplateOption>>();

        public BaseChangePasswordHandler()
        {
            _repo = Substitute.For<IPasswordRepository>();
            _passwordRecoveryRepository = Substitute.For<IHashPasswordRepository>();
            _mediator = Substitute.For<IMediator>();
            _pushNotification = Substitute.For<IPushNotificationService>();
            _emailTemplateOption.Value.Returns(new EmailTemplateOption() { ChangedLoggedElectronicSignature = 1, ChangedLoggedPassword = 2 });
        }

        protected static void AssertResponseHasErrors<TValue>(Response<TValue> response, string message)
        {
            Assert.True(response.IsFailure && response.Messages.Any(x => x == message));
        }
        protected static void AssertResponseHasErrors<TValue>(Response<TValue> response)
        {
            Assert.True(response.IsFailure && response.Messages.Any());
        }

        protected Password CreatePasswordEntity(string password)
        {
            var passwordHistory = new List<PasswordHistory>
            {
                new PasswordHistory("asda4da54as",null, SecretType.Password),
                new PasswordHistory("asda4da54assd5f46sdf4",null, SecretType.Password)
            };

            var s1 = SaltGenerator.Generate();
            var s2 = SaltGenerator.Generate();

            return new Password
                (
                    new Customer("225123", Name.Create("Tiago Sanches"), new BirthDate(DateTime.Now), "tiago@easynvest.com.br", "A"),
                    passwordHistory,
                    new List<PasswordHistory>(),
                    new PasswordDetails(EncodePasswords.Encode("225123", password, s1.Salt),
                                            EncodePasswords.Encode("225123", password, s2.Salt), 0, 0, s1, s2)
                );
        }
    }
}
