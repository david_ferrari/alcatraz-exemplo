﻿using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Configuration;
using NSubstitute;
using System.Linq;
using Xunit;

namespace Easynvest.Alcatraz.Test.Common
{
    public class BaseRequestHashPasswordHandler
    {
        protected IConfiguration _configuration;
        protected IHashPasswordRepository _repository;
        protected IMediator _mediator;
        protected readonly string _name = "Easynvest Easynvest";
        protected readonly string _email = "email@email.com";
        protected readonly string _cpf = "71409232727";
        protected readonly string _customerStatus = "A";

        public BaseRequestHashPasswordHandler()
        {
            _configuration = Substitute.For<IConfiguration>();
            _repository = Substitute.For<IHashPasswordRepository>();
            _mediator = Substitute.For<IMediator>();
        }

        protected void ConfigurationIsValid()
        {
            _configuration.GetSection("Mail:EmailCreateHashPasswordId").Value.Returns("123");
            _configuration.GetSection("Mail:EmailRecoveryId").Value.Returns("123");
            _configuration.GetSection("Mail:EmailRequestRecoveryEletronicSignatureId").Value.Returns("130");
            _configuration.GetSection("PasswordConfigs")["ExpirationTimeInDay"].Returns("1");
        }

        protected void ConfigurationIsInvalid()
        {
            _configuration.GetSection("Mail:EmailCreateHashPasswordId").Value.Returns(string.Empty);
            _configuration.GetSection("Mail:EmailCreateHashPasswordId").Value.Returns(string.Empty);
            _configuration.GetSection("PasswordConfigs")["ExpirationTimeInDay"].Returns(string.Empty);
        }

        protected static void AssertResponseHasErrors<TValue>(Response<TValue> response)
        {
            Assert.True(response.IsFailure && response.Messages.Any());
        }
    }
}
