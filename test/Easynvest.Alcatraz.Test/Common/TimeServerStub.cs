﻿using Easynvest.Alcatraz.Domain.Interfaces;
using System;

namespace Easynvest.Alcatraz.Test.Common
{
    public class TimeServerStub : ITimeServer
    {

        public TimeServerStub(DateTime date)
        {
            Current = date;
        }

        public DateTime Current { get; }
    }
}
