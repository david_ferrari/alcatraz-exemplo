﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Easynvest.Alcatraz.CrossCutting.Exceptions;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Infra.Data.Repositories;
using Easynvest.Alcatraz.Test.FakeDatas;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NSubstitute;
using Xunit;

namespace Easynvest.Alcatraz.Test.Data
{
    public class BopeRepositoryTests
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly BopeRepository _bopeRepository;
        public BopeRepositoryTests()
        {
            ILogger<BopeRepository> _logger = Substitute.For<ILogger<BopeRepository>>();
            _httpClientFactory = Substitute.For<IHttpClientFactory>();
            _bopeRepository = new BopeRepository(_httpClientFactory, _logger);
        }

        [Fact]
        public async Task Should_Return_Null_When_Bope_Call_Return_500_Level_Status_Code()
        {
            var fakeInternalServerErrorHttpMessageHandler = new FakeErrorHttpMessageHandler(new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.InternalServerError
            }, false);

            var httpClient = new HttpClient(fakeInternalServerErrorHttpMessageHandler);
            httpClient.BaseAddress = new Uri("http://teste.com");

            _httpClientFactory.CreateClient(Arg.Any<string>())
                                .Returns(httpClient);

           await Assert.ThrowsAsync<BopeApiCallException>(async () => await _bopeRepository.GetTokenInfo("1234-5678-9012", "1230987653"));
        }

        [Fact]
        public async Task Should_Return_Null_When_Bope_Call_Fail()
        {
            var fakeInternalServerErrorHttpMessageHandler = new FakeErrorHttpMessageHandler(null, true);

            var httpClient = new HttpClient(fakeInternalServerErrorHttpMessageHandler);
            httpClient.BaseAddress = new Uri("http://teste.com");

            _httpClientFactory.CreateClient(Arg.Any<string>())
                                .Returns(httpClient);

            var result = await _bopeRepository.GetTokenInfo("1234-5678-9012", "1230987653");

            result.Should().BeNull();
        }

        [Fact]
        public async Task Should_Return_Registered_Device_When_Bope_Call_Succeed()
        {
            var resultDeviceDto = new { Value = new DeviceRegisteredDto() };
            var fakeInternalServerErrorHttpMessageHandler = new FakeErrorHttpMessageHandler(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(resultDeviceDto), Encoding.UTF8, "application/json")
            }, false);

            var httpClient = new HttpClient(fakeInternalServerErrorHttpMessageHandler);
            httpClient.BaseAddress = new Uri("http://teste.com");

            _httpClientFactory.CreateClient(Arg.Any<string>())
                                .Returns(httpClient);

            var result = await _bopeRepository.GetTokenInfo("1234-5678-9012", "1230987653");

            result.Should().NotBeNull();
        }
    }
}
