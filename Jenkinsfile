#!groovy
/**
  * Continuous Integration/Delivery script for Jenkins
  * This script runs for every pull request open and will send execution status to Bitbucket
  */
pipeline {
    agent {
        kubernetes {
            yamlFile 'kubernetes/agent-dotnet.yaml'
        }
    }
    options {
        timeout(time: 30, unit: 'MINUTES')
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }
    environment {
        KUBE_NAMESPACE = 'backend'
        HELM_RELEASE_NAME = 'alcatraz'
        DOCKER_IMAGE = 'easynvest.alcatraz'
        PROGET = 'http://proget.easynvest.com.br:81/endpoints/helmcharts/content/default-dotnet-app-0.1.3.tgz'
    }
    // Pipeline steps
    stages {
        // Step 1
        stage("Build app dotnet") {
            steps {
                container('dotnet') {
                sh "make build"
            }
        }
    }
    // Step 2
    stage("Unit tests") {
            steps {
                container('dotnet') {
                sh "make test-ci"
            }
        }
    }
    stage("Static Analysis"){
        steps {
            container('dotnet'){
                withSonarQubeEnv('SonarServer'){
                    sh "make static analysis"
                }
            }
        }
    }
    stage("Quality Gate"){
        steps {
            container('dotnet'){
                sh "sleep 10"
                timeout(time: 5, unit: 'MINUTES'){
                    waitForQualityGate abortPipeline: true
                }
            }
        }
    }
    // Step 5
    stage("Build and Push Docker image") {
       when {
                anyOf {
                    branch 'develop';
                    branch 'master'
                }
        }
        steps {
            container('dotnet') {
                script {
                    docker.withRegistry('https://827461279864.dkr.ecr.sa-east-1.amazonaws.com/easynvest.alcatraz', 'ecr:sa-east-1:svc_alm') {
                        docker.build('${DOCKER_IMAGE}:${BUILD_ID}.${GIT_COMMIT}')
                        docker.image('${DOCKER_IMAGE}:${BUILD_ID}.${GIT_COMMIT}').push()
                    }
                }
            }
        }
    }
    // Step 7
    stage("Deploy HOM") {
        when { branch 'develop'}
        environment {
            ENVIRONMENT = "Homolog"
            KUBE_CONTEXT = "k8s.hom.easynvest.io"
            VALUES_YAML = "kubernetes/hom.yaml"
        }
        steps {
            container('dotnet') {
            sh "make deploy"
            }
        }
    }
    // Step 8
    stage("Deploy PROD") {
        when { branch 'master'}
        environment {
            ENVIRONMENT = "Production"
            KUBE_CONTEXT = "k8s.prod.easynvest.io"
            VALUES_YAML = "kubernetes/prod.yaml"
        }
        steps {
            input (message: 'Prosseguir com o deploy em Produção?', ok: 'Prosseguir')
            container('dotnet') {
				withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'svc_alm', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
					sh "make deploy"
				}
			}
        }
    }
    }
}