﻿namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public enum PasswordVersion
    {
        Old = 0,
        New = 1
    }
}
