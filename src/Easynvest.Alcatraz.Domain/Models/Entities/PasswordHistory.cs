﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Services;
using System;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class PasswordHistory : Entity<string>
    {
        public string EncodedPassword { get; }

        public DateTime CreationDate { get; set; }

        public SecretType Type { get; }

        public string Salt { get; }

        protected PasswordHistory()
        {
        }

        public PasswordHistory(string encodedPassword, string salt, SecretType type)
        {
            EncodedPassword = encodedPassword;
            Salt = salt;
            CreationDate = DateTime.Now;
            Type = type;
        }

        public bool IsEqual(string cleanSecret, string customerId)
        {
            var encoded = string.IsNullOrEmpty(Salt) ? EncodePasswords.EncodeString(cleanSecret)
                : EncodePasswords.Encode(customerId, cleanSecret, Salt);

            return EncodedPassword.Equals(encoded);
        }
    }
}