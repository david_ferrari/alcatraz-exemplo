﻿using System;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class SaltData
    {
        protected SaltData()
        {
        }

        public SaltData(string salt)
        {
            Salt = salt;
            CreationDate = DateTime.Now;
        }

        public string Salt { get; }

        public DateTime CreationDate { get; }
    }
}