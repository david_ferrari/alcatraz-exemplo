﻿using System;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    [Serializable]
    public class TemplateEmail
    {
        protected TemplateEmail(){}

        protected TemplateEmail(string subject, string from, string content, string to)
        {
            From = from;
            To = to;
            Subject = subject;
            Content = content;
        }

        public static TemplateEmail Create(string subject, string from, string content, string to)
        {
            return new TemplateEmail(subject, from, content, to);
        }

        public int Id { get; }
        public string Subject { get; }
        public string From { get; }
        public string To { get; private set; }
        public string Content { get; private set; }

        public void ContentFormat(IDictionary<string, string> values)
        {
            var result = Content;
            foreach (var item in values)
            {
                result = result?.Replace(item.Key, item.Value);
            }

            Content = result;
        }

        public void SetTo(string to)
        {
            To = to;
        }
    }


}
