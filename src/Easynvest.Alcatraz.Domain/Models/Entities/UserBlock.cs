﻿using Easynvest.Alcatraz.Domain.Enumerators;
using System;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class UserBlock
    {
        public UserBlock()
        {
        }

        public UserBlock(string cpf, BlockType blockType)
        {
            CustomerId = cpf;
            BlockType = blockType;
            BlockDate = DateTime.Now;
        }

        public string CustomerId { get; private set; }
        public BlockType BlockType { get; private set; }
        public DateTime BlockDate { get; private set; }
    }
}
