﻿using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using System;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class AccessToken : ValueObject<AccessToken>
    {
        protected AccessToken()
        { }
        public AccessToken(string fcmToken)
        {
            FcmToken = fcmToken;
            LastAccessData = DateTime.Now;
        }

        public string FcmToken { get; }
        public DateTime LastAccessData { get; }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return FcmToken;
        }
    }
}
