﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Services;
using Easynvest.Notificator.Validations;

namespace Easynvest.Alcatraz.Domain.Entities
{
    public class CustomerSecret : Entity<string>
    {
        public string CustomerId { get; }

        public string EncodedPassword { get; private set; }

        public SecretType Type { get; private set; }

        public int LoginAttempts { get; private set; }

        public AccessToken AccessToken { get; set; }

        public PasswordVersion Version { get; set; }

        public SaltData SaltData { get; set; }

        public int CurrentLoginAttempts { get; private set; }

        protected CustomerSecret()
        {

        }

        public CustomerSecret(string username, string cleanText, SecretType passwordType, int loginAttempts)
        {
            AddNotifications(new Contract()
                .Requires()
                .IsNotNullOrEmpty(cleanText, nameof(cleanText), "Não deve ser branco")
            );

            CustomerId = username;
            Version = PasswordVersion.Old;
            EncodedPassword = EncodePasswords.EncodeString(cleanText);
            Type = passwordType;
            LoginAttempts = loginAttempts;
            CurrentLoginAttempts = LoginAttempts;
        }

        public bool ValidateCustomerSecret(string cleanText)
        {
            var encodedPassword = EncodePasswords.Encode(CustomerId, cleanText, Version, SaltData?.Salt);

            var valid = EncodedPassword.Equals(encodedPassword);

            if (valid)
            {
                LoginAttempts = 0;
            }
            else
            {
                LoginAttempts++;
            }

            return valid;
        }

        public bool VerifyLoginAttempts(int maxLoginAttempts)
        {
            var valid = LoginAttempts < maxLoginAttempts;

            if (!valid)
                LoginAttempts++;

            return valid;
        }

        public bool NeedToUpdate()
        {
            return Version == PasswordVersion.Old;
        }

        public bool ShouldUpdateLoginAttempts()
        {
            return CurrentLoginAttempts != LoginAttempts;
        }
    }
}