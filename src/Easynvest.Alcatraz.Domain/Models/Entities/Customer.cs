﻿using Easynvest.Alcatraz.Domain.Models.ValueObjects;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class Customer
    {
        public string CustomerId { get; }
        public Name Name { get; }
        public BirthDate BirthDate { get; }
        public string Email { get; }
        public string FcmToken { get; set; }
        public string Status { get; set; }


        public Customer(string customerId, Name name, BirthDate birthdate, string email, string status)
        {
            CustomerId = customerId;
            Name = name;
            BirthDate = birthdate;
            Email = email;
            Status = status;
        }


        public Customer(string customerId, Name name, string email, string status)
        {
            CustomerId = customerId;
            Name = name;
            Email = email;
            Status = status;
        }
        protected Customer() { }
    }
}
