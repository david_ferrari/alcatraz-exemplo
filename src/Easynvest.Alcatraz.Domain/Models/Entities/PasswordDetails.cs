﻿using Easynvest.Alcatraz.Domain.Enumerators;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class PasswordDetails
    {
        public string EncodedPassword { get; protected set; }

        public PasswordVersion PasswordVersion { get; set; }

        public string EncodedEletronicSignature { get; set; }

        public PasswordVersion EletronicSignatureVersion { get; set; }

        public int InvalidLoginAttempts { get; set; }

        public int InvalidSignatureAttempts { get; set; }

        public SaltData PasswordSalt { get; set; }

        public SaltData EletronicSignatureSalt { get; set; }

        protected PasswordDetails() { }

        public PasswordDetails
            (
                string encodedPassword,
                string encodedEletronicSignature,
                int invalidLoginAttempts,
                int invalidSignatureAttempts,
                SaltData passwordSalt,
                SaltData eletronicSignatureSalt
            )
        {
            EncodedPassword = encodedPassword;
            EncodedEletronicSignature = encodedEletronicSignature;
            InvalidLoginAttempts = invalidLoginAttempts;
            InvalidSignatureAttempts = invalidSignatureAttempts;
            PasswordVersion = passwordSalt == null ? PasswordVersion.Old : PasswordVersion.New;
            EletronicSignatureVersion = eletronicSignatureSalt == null ? PasswordVersion.Old : PasswordVersion.New;
            PasswordSalt = passwordSalt;
            EletronicSignatureSalt = eletronicSignatureSalt;
        }

        public void ChangeEncodedPassword(string newEncodedPassword)
        {
            EncodedPassword = newEncodedPassword;
        }

        public void ChangeEncodedElectronicSignature(string newEncodedSignature)
        {
            EncodedEletronicSignature = newEncodedSignature;
        }

        public void ResetLoginAttempts() => InvalidLoginAttempts = 0;

        public void ResetSignatureAttempts() => InvalidSignatureAttempts = 0;

        public void UpdateVersion(SecretType secretType)
        {
            if (secretType == SecretType.EletronicSignature)
                EletronicSignatureVersion = PasswordVersion.New;
            else
                PasswordVersion = PasswordVersion.New;
        }
    }
}
