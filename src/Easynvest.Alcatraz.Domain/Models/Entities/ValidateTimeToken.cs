﻿using System;
using Easynvest.Alcatraz.Domain.Enumerators;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class ValidateTimeToken
    {
        public ValidateTimeToken(string customerId, string deviceUid, DateTime expiryTime, ConfirmedTrustedDeviceType confirmedTrustedDevice)
        {
            CustomerId = customerId;
            DeviceUid = deviceUid;
            ExpiryTime = expiryTime;
            ConfirmedTrustedDevice = confirmedTrustedDevice;
        }

        public ValidateTimeToken(string customerId, string deviceUid, ConfirmedTrustedDeviceType confirmedTrustedDevice)
        {
            CustomerId = customerId;
            DeviceUid = deviceUid;
            ExpiryTime = confirmedTrustedDevice == ConfirmedTrustedDeviceType.Trusted ? CreateThirtyDays() : CreateTodayDate();
            ConfirmedTrustedDevice = confirmedTrustedDevice;
        }

        public string CustomerId { get; }

        public string DeviceUid { get; }

        public DateTime ExpiryTime { get; private set; }

        public ConfirmedTrustedDeviceType ConfirmedTrustedDevice { get; private set; }

        private DateTime CreateTodayDate() => DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

        private DateTime CreateThirtyDays() => DateTime.Now.Date.AddDays(30).AddHours(23).AddMinutes(59).AddSeconds(59);

        public bool Valid() => ExpiryTime >= DateTime.Now;

        public bool TrustedDevice() => ConfirmedTrustedDevice == ConfirmedTrustedDeviceType.Trusted && Valid();

        public void TrustDevice()
        {
            ConfirmedTrustedDevice = ConfirmedTrustedDeviceType.Trusted;
            ExpiryTime = CreateThirtyDays();
        }
    }
}