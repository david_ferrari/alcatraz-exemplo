﻿using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Notificator.Validations;
using System;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class PasswordRecoveryRequest : Entity<string>
    {
        private const string INVALID_ELECTRONIC_MAIL = "Email inválido";
        private const string INVALID_CPF = "Cpf inválido";
        private const string INVALID_EXPIRATION_DATE = "Expiration Date inválido";
        private readonly ITimeServer _timeServer;

        public DateTime CreationDate { get; }
        public string Hash { get; }
        public string Email { get; }
        public ExpirationDate ExpirationDate { get; private set; }
        public Customer Customer { get; private set; }


        private PasswordRecoveryRequest(ITimeServer timeServer)
        {
            _timeServer = timeServer;
            Hash = Guid.NewGuid().ToString("N");
            CreationDate = _timeServer.Current;
        }
        
        private PasswordRecoveryRequest(ITimeServer timeServer, string email, Customer customer, ExpirationDate expirationDate)
            : this(timeServer)
        {
            AddNotifications(new Contract()
                .IsNotNullOrEmpty(email, nameof(email), INVALID_ELECTRONIC_MAIL)
                .IsNotNullOrEmpty(customer.CustomerId, nameof(customer.CustomerId), INVALID_CPF)
                .IsEmail(email, nameof(email), INVALID_ELECTRONIC_MAIL)
                .IsNotNull(expirationDate, nameof(expirationDate), INVALID_EXPIRATION_DATE));

            Email = email;
            Customer = customer;
            ExpirationDate = expirationDate;
            Validate();
        }

        private PasswordRecoveryRequest(ITimeServer timeServer, string email, Customer customer, ExpirationDate expirationDate, DateTime creationDate, string hash)
            : this(timeServer)
        {
            Email = email;
            Customer = customer;
            ExpirationDate = expirationDate;
            CreationDate = creationDate;
            Hash = hash;
            Validate();
        }

        public void ExpireHash()
        {
            ExpirationDate = new ExpirationDate(_timeServer.Current);
        }

        public static PasswordRecoveryRequest Create(string email, Customer customer, ExpirationDate ExpirationDate)
        {
            return new PasswordRecoveryRequest(new SystemTime(), email, customer, ExpirationDate);
        }

        public static PasswordRecoveryRequest Create(string email, Customer customer, ExpirationDate ExpirationDate, ITimeServer timeServer)
        {
            return new PasswordRecoveryRequest(timeServer, email, customer, ExpirationDate);
        }

        public static PasswordRecoveryRequest Create(Customer customer, ExpirationDate ExpirationDate, DateTime creationDate, string hash)
        {
            return new PasswordRecoveryRequest(new SystemTime(), string.Empty, customer, ExpirationDate, creationDate, hash);
        }

        private bool Validate()
        {
            AddNotifications(ExpirationDate.Notifications);
            return IsValid;
        }
    }
}