﻿using Easynvest.Notificator.Notifications;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public abstract class Entity<TId> : Notifiable
    {
        public TId Id { get; }
    }
}
