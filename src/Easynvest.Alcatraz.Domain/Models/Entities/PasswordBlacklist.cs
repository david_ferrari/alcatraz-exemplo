﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class PasswordBlacklist
    {
        public PasswordBlacklist(IList<string> passwords)
        {
            ForbiddenPasswords = passwords;
        }

        private IList<string> ForbiddenPasswords { get; }

        public bool IsValid(string password)
        {
            if (string.IsNullOrEmpty(password) || ForbiddenPasswords == null || !ForbiddenPasswords.Any())
                return true;

            return ForbiddenPasswords.All(p => !password.Equals(p, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}