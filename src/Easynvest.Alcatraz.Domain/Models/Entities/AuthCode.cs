﻿using System;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class AuthCode
    {
        public AuthCode(string customerId, string code, DateTime creationDate, DateTime expirationDate)
        {
            CustomerId = customerId;
            Code = code;
            CreationDate = creationDate;
            ExpirationDate = expirationDate;
        }

        public AuthCode(string customerId, int expirationInMinutes)
        {
            CustomerId = customerId;
            Code = new Random().Next(100000, 999999).ToString();
            CreationDate = DateTime.Now;
            ExpirationDate = CreationDate.AddMinutes(expirationInMinutes);
        }

        public string CustomerId { get; }

        public string Code { get; }

        public DateTime CreationDate { get; }

        public DateTime ExpirationDate { get; }

        public bool IsExpired()
        {
            return DateTime.Now > ExpirationDate;
        }

        public bool IsValid(string code)
        {
            return Code == code;
        }
    }
}
