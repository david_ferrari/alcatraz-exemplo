﻿using MediatR;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class EmailQueue : IRequest<Unit>
    {
        public int EmailId { get; private set; }

        public IDictionary<string, string> EmailParameters { get; private set; }

        public string To { get; private set; }

        public EmailQueue(int emailId, IDictionary<string, string> emailParameters, string to)
        {
            EmailId = emailId;
            EmailParameters = emailParameters;
            To = to;
        }
    }
}
