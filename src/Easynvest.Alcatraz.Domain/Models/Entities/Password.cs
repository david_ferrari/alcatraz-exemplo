﻿using Easynvest.Alcatraz.Domain.Common;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Factories;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Services;
using Easynvest.Notificator.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Domain.Models.Entities
{
    public class Password : Entity<String>
    {
        private readonly ITimeServer _timeServer;

        public Customer Customer { get; }
        public FixedSizeSet<PasswordHistory> LastSixPasswords { get; private set; }

        public FixedSizeSet<PasswordHistory> LastSixElectronicSignatures { get; private set; }

        public PasswordDetails PasswordDetails { get; set; }
        public PasswordBlacklist Blacklist { get; set; }

        public Password(ITimeServer timeServer, Customer customer, IEnumerable<PasswordHistory> passwordsHistory, IEnumerable<PasswordHistory> lastSixElectronicSignatures, PasswordDetails passwordDetails)
        {
            _timeServer = timeServer;
            Customer = customer;
            PasswordDetails = passwordDetails;
            CreatePasswordHistories(passwordsHistory, lastSixElectronicSignatures);
        }

        public Password(Customer customer, IEnumerable<PasswordHistory> passwordsHistory, IEnumerable<PasswordHistory> electronicSignaturesHistory, PasswordDetails passwordDetails)
            : this(new SystemTime(), customer, passwordsHistory, electronicSignaturesHistory, passwordDetails)
        {
        }

        public virtual IReadOnlyList<Notification> TryChangeSecret(string secretCandidate, SecretType passwordType,
            SecretChangeType secretChangeType, bool ignoreHistory = false, bool validate = true)
        {
            return TryChangeSecret(secretCandidate, string.Empty, passwordType, secretChangeType, ignoreHistory, validate);
        }

        public IReadOnlyList<Notification> TryChangeSecret(string secretCandidate, string rawElectronicSignature, SecretType passwordType,
            SecretChangeType secretChangeType, bool ignoreHistory = false, bool validate = true)
        {
            IReadOnlyList<Notification> notifications = new List<Notification>();
            if (validate)
            {
                var validator = ValidateCustomerSecretFactory.BuildValidateCustomerSecret(this, passwordType, secretChangeType, Blacklist);
                notifications = validator.Verify(secretCandidate, rawElectronicSignature, ignoreHistory);
            }

            if (!notifications.Any())
                UpdatePassword(secretCandidate, passwordType);

            return notifications;
        }

        private void UpdatePassword(string secretCandidate, SecretType passwordType)
        {
            // Remover parametro version quando for liberar para todos os clientes
            if (passwordType == SecretType.EletronicSignature)
            {
                PasswordDetails.EletronicSignatureSalt = SaltGenerator.Generate();
                PasswordDetails.UpdateVersion(passwordType);
                var encodedSecret = EncodePasswords.Encode(Customer.CustomerId, secretCandidate, PasswordDetails.EletronicSignatureSalt.Salt);

                PasswordDetails.ChangeEncodedElectronicSignature(encodedSecret);

                if (LastSixElectronicSignatures == null)
                    LastSixElectronicSignatures = new FixedSizeSet<PasswordHistory>(6, (p1, p2) => p1.CreationDate < p2.CreationDate ? p1 : p2);

                LastSixElectronicSignatures.Add(new PasswordHistory(encodedSecret, PasswordDetails.EletronicSignatureSalt?.Salt, SecretType.EletronicSignature));
            }
            else
            {
                PasswordDetails.PasswordSalt = SaltGenerator.Generate();
                PasswordDetails.UpdateVersion(passwordType);
                var encodedSecret = EncodePasswords.Encode(Customer.CustomerId, secretCandidate, PasswordDetails.PasswordSalt.Salt);

                PasswordDetails.ChangeEncodedPassword(encodedSecret);

                if (LastSixPasswords == null)
                    LastSixPasswords = new FixedSizeSet<PasswordHistory>(6, (p1, p2) => p1.CreationDate < p2.CreationDate ? p1 : p2);

                LastSixPasswords.Add(new PasswordHistory(encodedSecret, PasswordDetails.PasswordSalt?.Salt, SecretType.Password));
            }
        }

        public override string ToString() => $"CustomerId: {Customer.CustomerId}";

        private void CreatePasswordHistories(IEnumerable<PasswordHistory> passwordsHistory, IEnumerable<PasswordHistory> lastSixElectronicSignatures)
        {
            LastSixPasswords = new FixedSizeSet<PasswordHistory>(6, (p1, p2) => p1.CreationDate < p2.CreationDate ? p1 : p2)
            {
                passwordsHistory
            };

            LastSixElectronicSignatures = new FixedSizeSet<PasswordHistory>(6, (p3, p4) => p3.CreationDate < p4.CreationDate ? p3 : p4)
            {
                lastSixElectronicSignatures
            };
        }
    }
}