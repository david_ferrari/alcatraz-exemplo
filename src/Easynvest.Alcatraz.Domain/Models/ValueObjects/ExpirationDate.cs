﻿using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Notificator.Validations;
using System;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Domain.Models.ValueObjects
{
    public class ExpirationDate : ValueObject<ExpirationDate>
    {
        private readonly ITimeServer _timeServer;

        public ExpirationDate(DateTime expirationDate)
            : this(new SystemTime(), expirationDate) { }

        public ExpirationDate(ITimeServer timeServer, DateTime expirationDate)
        {
            _timeServer = timeServer;
            Value = expirationDate;
        }

        public ExpirationDate(int days)
            : this(new SystemTime(), days) { }

        public ExpirationDate(ITimeServer timeServer, int days)
        {
            AddNotifications(new Contract()
               .Requires()
               .IsGreaterThan(days, 0, nameof(days), "A quantidade de dias deve ser maior que zero."));

            if (IsValid)
            {
                _timeServer = timeServer;
                Value = _timeServer.Current.AddDays(days);
            }
        }

        public DateTime Value { get; }

        public bool IsExpired => _timeServer.Current > Value;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}