﻿using Easynvest.Alcatraz.Domain.Interfaces;
using System;

namespace Easynvest.Alcatraz.Domain.Models.ValueObjects
{
    public class SystemTime : ITimeServer
    {
        public DateTime Current => DateTime.Now;
    }
}
