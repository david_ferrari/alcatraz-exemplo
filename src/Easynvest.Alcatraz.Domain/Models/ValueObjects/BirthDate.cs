﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Easynvest.Alcatraz.Domain.Models.ValueObjects
{
    public class BirthDate
    {
        public DateTime? Value { get; private set; }

        public static implicit operator BirthDate(string date)
        {
            return new BirthDate(GetBirthDate(date));
        }

        public BirthDate (DateTime date)
        {
            Value = date;
        }

        private static DateTime GetBirthDate(string date)
        {
            var isSuccess = DateTime.TryParse(date, out DateTime birthDate);

            if (!isSuccess)
            {
                string pattern = @"\b(?<month>\d{1,2})/(?<day>\d{1,2})/(?<year>\d{2,4})\b";
                Regex dateRegex = new Regex(pattern);
                var match = dateRegex.Match(date);
                
                if(match.Success)
                    isSuccess = DateTime.TryParseExact(match.Value, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate);
            }

            if (!isSuccess)
            {
                throw new FormatException("BirthDate cannot be parsed.");
            }

            return birthDate;
        }
    }
}
