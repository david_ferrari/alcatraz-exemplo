﻿namespace Easynvest.Alcatraz.Domain.Models.ValueObjects
{
    public enum BopeIntegrationType
    {
        NewDevice = 0,
        Withdrawal = 1,
        UpdateBankAccount = 2,
        CreateBankAccount = 3,
        DeleteBankAccount = 4,
        Login = 5
    }
}