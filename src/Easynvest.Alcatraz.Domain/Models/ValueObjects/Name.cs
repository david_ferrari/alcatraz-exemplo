﻿using System.Collections.Generic;
using System.Globalization;

namespace Easynvest.Alcatraz.Domain.Models.ValueObjects
{
    public class Name : ValueObject<Name>
    {
        public string FirstName { get; }
        public string LastName { get; }
        public string FullName => $"{FirstName} {LastName}".Trim();

        private Name(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public static implicit operator Name(string name)
        {
            return Create(name);
        }

        public static Name Create(string name)
        {
            return new Name(GetName(NameType.FirstName, name), GetName(NameType.LastName, name));
        }

        private static string GetName(NameType nameType, string source)
        {
            if (!string.IsNullOrWhiteSpace(source) && source.Contains(" "))
            {
                var length = source.IndexOf(' ', 0);
                var text = source.Substring(0, length);
                return nameType == NameType.FirstName 
                    ? CultureInfo.InvariantCulture.TextInfo.ToTitleCase(text.ToLowerInvariant()) 
                    : CultureInfo.InvariantCulture.TextInfo.ToTitleCase(source.Substring(text.Length + 1).ToLowerInvariant());
            }

            return nameType == NameType.FirstName ? source : string.Empty;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return FullName;
        }
    }

    internal enum NameType { FirstName, LastName }
}
