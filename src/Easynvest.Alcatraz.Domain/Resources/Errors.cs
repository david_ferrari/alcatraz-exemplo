﻿using Easynvest.Ops;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Domain.Resources
{
    public static class Errors
    {
        public static class General
        {
            public static Error InternalProcessError()
                => new Error("InternalProcessError", $"Não foi possível processar sua solicitação, por favor tente novamente.", StatusCodes.Status500InternalServerError);

            public static Error NotFound(string entity)
                => new Error("NotFoundError", $"{entity} não encontrado.", StatusCodes.Status404NotFound);
        }

        public static class ErrorsToken
        {
            public static Error NeedToken(InnerErrorAuthenticateNeedToken innerError)
                => new Error("NEEDTOKEN", "Easytoken não informado.", innerError);

            public static List<Error> ErrorsAuthenticate(Dictionary<string, string> validateErrors)
            {
                var errors = new List<Error>();
                foreach (var item in validateErrors)
                {
                    errors.Add(new Error(item.Key, item.Value, StatusCodes.Status400BadRequest));
                }
                return errors;
            }

            public static List<Error> ErrorsAuthenticate(Dictionary<string, string> validateErrors, InnerErrorAuthenticateNeedToken innerError)
            {
                var errors = new List<Error>();
                foreach (var item in validateErrors)
                {
                    errors.Add(new Error(item.Key, item.Value, innerError));
                }
                return errors;
            }

            public static List<Error> ErrorsAuthenticate(string errorCode, List<string> validateErrors)
            {
                var errors = new List<Error>();
                foreach (var item in validateErrors)
                {
                    errors.Add(new Error(errorCode, item, StatusCodes.Status400BadRequest));
                }
                return errors;
            }

            public static List<Error> ErrorsAuthenticate(string errorCode, List<string> validateErrors, InnerErrorAuthenticateNeedToken innerError)
            {
                var errors = new List<Error>();
                foreach (var item in validateErrors)
                {
                    errors.Add(new Error(errorCode, item, innerError));
                }
                return errors;
            }

            public static Error InvalidUsername()
                => new Error("INVALIDUSERNAME", "Código inválido", StatusCodes.Status404NotFound);

            public static Error NotFoundCode()
                => new Error("NOTFOUNDCODE", "Código inválido", StatusCodes.Status404NotFound);

            public static Error InvalidCode()
                => new Error("INVALIDCODE", "Código inválido", StatusCodes.Status400BadRequest);

            public static Error ExpiredCode()
                => new Error("EXPIREDCODE", "O código é válido por 10 minutos. Solicite um novo envio e tente novamente.", StatusCodes.Status400BadRequest);

            public static Error FailRecoverUser()
                => new Error("FAILRECOVERUSER", "Falha ao recuperar o usuário.", StatusCodes.Status400BadRequest);

            public static Error DeviceNotFound()
                => new Error("DEVICENOTFOUND", "Device não encontrado.", StatusCodes.Status400BadRequest);

            public static Error UntrustedDevice()
                => new Error("UNTRUSTED_DEVICE", "Dispositivo não é confiável.", StatusCodes.Status400BadRequest);
        }

        public static class BlockErrors
        {
            public static Error AlreadyBlocked()
               => new Error("AlreadyBlocked", $"O usuário informado já está bloqueado.");
        }
    }
}