﻿using System.Text.RegularExpressions;
using Easynvest.Ops;
using Newtonsoft.Json;

namespace Easynvest.Alcatraz.Domain.Resources
{
    public class InnerErrorAuthenticateNeedToken : IInnerError
    {
        public InnerErrorAuthenticateNeedToken(string customerId, string name, string email, bool? needToken)
        {
            CustomerId = customerId;
            Name = name;
            Email = MaskEmail(email);
            NeedToken = needToken;
        }

        public string CustomerId { get; }

        public string Name { get; }

        public string Email { get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? NeedToken { get; }

        private string MaskEmail(string email)
        {
            const string pattern = @"(?<=[\w]{1})[\w-\._\+%]*(?=[\w]{1}@)";
            return Regex.Replace(email, pattern, m => new string('*', m.Length));
        }
    }
}