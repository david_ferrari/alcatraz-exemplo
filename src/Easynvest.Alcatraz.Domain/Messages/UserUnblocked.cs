﻿using System;

namespace Easynvest.Messages.Events.Security.User
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Msg MassTransit")]
    public interface UserUnblocked
    {
        string CustomerId { get; }

        DateTime Date { get; }
    }
}
