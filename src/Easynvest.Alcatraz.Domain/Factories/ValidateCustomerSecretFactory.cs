﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Validators;

namespace Easynvest.Alcatraz.Domain.Factories
{
    internal static class ValidateCustomerSecretFactory
    {
        public static ValidateCustomerSecret BuildValidateCustomerSecret(Password password, SecretType passwordType,
            SecretChangeType SecretChangeType, PasswordBlacklist blacklist)
        {
            var validateCustomerSecret = default(ValidateCustomerSecret);

            switch (passwordType)
            {
                case SecretType.Password:
                    validateCustomerSecret = new ValidateCustomerPassword(password, SecretChangeType, blacklist);
                    break;
                case SecretType.EletronicSignature:
                    validateCustomerSecret = new ValidateCustomerElectronicSignature(password, SecretChangeType, blacklist);
                    break;
                default:
                    break;
            }

            return validateCustomerSecret;
        }
    }
}