﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Domain.Common
{
    public class FixedSizeSet<T> : IEnumerable<T>
    {
        private readonly HashSet<T> _internalList;
        private readonly Func<T, T, T> _discardItem;
        private readonly int _size;

        public FixedSizeSet(int size, Func<T, T, T> discardItem)
        {
            _internalList = new HashSet<T>();
            _discardItem = discardItem;
            _size = size;
        }

        public void Add(T item)
        {
            if (_internalList.Count >= _size)
            {
                var itemToDiscard = _internalList.Aggregate((ag, cur) => _discardItem(ag, cur));
                _internalList.Remove(itemToDiscard);
            }

            _internalList.Add(item);
        }

        public void Add(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _internalList.GetEnumerator();
        }
    }
}
