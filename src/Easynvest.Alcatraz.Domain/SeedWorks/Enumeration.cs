﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Easynvest.Alcatraz.Domain.SeedWorks
{
    public abstract class Enumeration : IComparable
    {
        public string Value { get; }
        public string Display { get; }

        protected Enumeration()
        {

        }

        protected Enumeration(string value, string display)
        {
            Value = value;
            Display = display;
        }

        public override string ToString() => Display;

        public static IEnumerable<T> GetAll<T>() where T : Enumeration, new()
        {
            var type = typeof(T);
            var fields = type.GetTypeInfo().GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            foreach (var info in fields)
            {
                var instance = new T();
                if (info.GetValue(instance) is T locatedValue)
                {
                    yield return locatedValue;
                }
            }
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration;

            if (otherValue == null)
                return false;

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = Value.ToLowerInvariant().Equals(otherValue.Value.ToLowerInvariant());

            return typeMatches && valueMatches;
        }

        public int CompareTo(object obj) => String.CompareOrdinal(Value, ((Enumeration)obj).Value);

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 17;
                hash = (hash * 23) + Value.GetHashCode();
                return (hash * 23) + Display.GetHashCode();
            }
        }
    }
}
