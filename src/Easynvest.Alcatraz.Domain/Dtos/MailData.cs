﻿using System.Collections.Generic;

namespace Easynvest.Alcatraz.Domain.Dtos
{
    public class MailData
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public List<MailDataFiles> Attachments { get; set; }
    }
}
