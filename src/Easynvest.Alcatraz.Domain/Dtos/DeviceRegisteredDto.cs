﻿using System;

namespace Easynvest.Alcatraz.Domain.Dtos
{
    public class DeviceRegisteredDto
    {
        public string DeviceNumber { get; set; }

        public bool IsActive { get; set; }

        public DateTime? ConfirmedDate { get; set; }

        public int DeviceId { get; set; }

        public string DeviceModel { get; set; }

        public string DeviceToken { get; set; }

        public string DeviceSeed { get; set; }
    }
}