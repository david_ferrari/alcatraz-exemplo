﻿namespace Easynvest.Alcatraz.Domain.Dtos
{
    public class MailDataFiles
    {
        public byte[] Data { get; set; }
        public string FileName { get; set; }
    }
}
