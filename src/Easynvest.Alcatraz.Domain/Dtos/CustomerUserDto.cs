﻿using System;

namespace Easynvest.Alcatraz.Domain.Dtos
{
    public class CustomerUserDto
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string CustomerId { get; set; }
        public string EletronicSignature { get; private set; }
        public string EncodedPassword { get; set; }
        public string Status { get; set; }
        public string AdvisorCode { get; private set; }
        public int InvalidLoginAttempts { get; private set; }
        public long AccountNumber { get; set; }
        public string FcmToken { get; private set; }
        public DateTime LastAccessDate { get; set; }
        public string PublicId { get; set; }
        public string AccessVersion { get; set; }
        public long MainAccountNumber { get; set; }
    }
}
