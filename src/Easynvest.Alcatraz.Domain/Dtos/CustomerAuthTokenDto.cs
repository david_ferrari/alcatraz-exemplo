﻿namespace Easynvest.Alcatraz.Domain.Dtos
{
    public class CustomerAuthTokenDto
    {
        public string Key { get; set; }

        public string DeviceUid { get; set; }
    }
}