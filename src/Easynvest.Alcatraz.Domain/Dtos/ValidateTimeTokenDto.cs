﻿using System;
using Easynvest.Alcatraz.Domain.Enumerators;

namespace Easynvest.Alcatraz.Domain.Dtos
{
    public class ValidateTimeTokenDto
    {
        public string CustomerId { get; set; }
        public string DeviceUid { get; set; }
        public DateTime ExpiryTime { get; set; }
        public ConfirmedTrustedDeviceType ConfirmedTrustedDevice { get; set; }
    }
}