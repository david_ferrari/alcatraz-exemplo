﻿using Easynvest.Alcatraz.Domain.Models.Entities;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Easynvest.Alcatraz.Domain.Services
{
    public static class EncodePasswords
    {
        private static byte[] Encode(byte[] textBytes, byte[] saltBytes)
        {
            byte[] plainTextWithSaltBytes = Combine(textBytes, saltBytes);
            using SHA512 hash = new SHA512Managed();
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            return hashBytes;
        }

        public static string Encode(string username, string password, string salt)
        {
            if (salt == null)
                throw new ArgumentNullException(nameof(salt));

            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            byte[] usernameBytes = Encoding.UTF8.GetBytes(username);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            var textBytes = Combine(usernameBytes, passwordBytes);

            for (int i = 0; i < 1000; i++)
            {
                textBytes = Encode(textBytes, saltBytes);
            }

            string hashValue = Convert.ToBase64String(textBytes);
            return hashValue;
        }

        public static string Encode(string username, string password, PasswordVersion version = PasswordVersion.Old, string salt = null)
        {
            return version == PasswordVersion.Old ? EncodeString(password) : Encode(username, password, salt);
        }

        public static string EncodeString(string clearText)
        {
            clearText = clearText.ToLowerInvariant();

            var bytes = Encoding.Unicode.GetBytes(clearText);

            using (var stream = new MemoryStream())
            {
                stream.WriteByte(0);

                using (var md5 = new MD5CryptoServiceProvider())
                {
                    var hash = md5.ComputeHash(bytes);

                    stream.Write(hash, 0, hash.Length);
                    stream.WriteByte(0);

                    using (var sha1 = new SHA1Managed())
                    {
                        hash = sha1.ComputeHash(bytes);

                        stream.Write(hash, 0, hash.Length);

                        bytes = stream.ToArray();

                        var result = Convert.ToBase64String(bytes);

                        return result;
                    }
                }
            }
        }

        public static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
    }
}