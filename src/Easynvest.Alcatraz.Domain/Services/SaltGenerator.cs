﻿using Easynvest.Alcatraz.Domain.Models.Entities;
using System;
using System.Security.Cryptography;

namespace Easynvest.Alcatraz.Domain.Services
{
    public static class SaltGenerator
    {
        private const int SaltSize = 8;

        public static SaltData Generate(int size = SaltSize)
        {
            var saltBytes = new byte[size];
            using RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetNonZeroBytes(saltBytes);
            return new SaltData(Convert.ToBase64String(saltBytes));
        }
    }
}