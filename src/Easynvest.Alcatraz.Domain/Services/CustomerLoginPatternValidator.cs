﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using System.Text.RegularExpressions;

namespace Easynvest.Alcatraz.Domain.Services
{
    public static class CustomerLoginPatternValidator
    {
        private const string _emailPattern =
            @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
            + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
            + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

        private const string _cpfPattern =
            @"^([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})$";

        private const string _accountNumberPattern = @"^([0-9]{3,7})$";

        public static Response<CustomerLoginType> Match(string value)
        {
            if (IsCpf(value))
                return Response<CustomerLoginType>.Ok(CustomerLoginType.CustomerLoginDocument);
            
            if (IsEmail(value))
                return Response<CustomerLoginType>.Ok(CustomerLoginType.CustomerLoginEmail);

            if (IsAccountNumber(value))
                return Response<CustomerLoginType>.Ok(CustomerLoginType.CustomerLoginAccountNumber);

            return Response<CustomerLoginType>.Fail("");
        }

        private static bool IsEmail(string value) => Regex.IsMatch(value, _emailPattern, RegexOptions.IgnoreCase);

        private static bool IsCpf(string value) => Regex.IsMatch(value, _cpfPattern);

        private static bool IsAccountNumber(string value) => Regex.IsMatch(value, _accountNumberPattern);
    }
}
