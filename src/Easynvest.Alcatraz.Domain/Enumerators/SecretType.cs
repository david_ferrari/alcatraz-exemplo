﻿using System.ComponentModel;

namespace Easynvest.Alcatraz.Domain.Enumerators
{
    public enum SecretType
    {
        [Description("Senha")]
        Password = 1,
        [Description("Assinatura Eletrônica")]
        EletronicSignature = 2
    }
}
