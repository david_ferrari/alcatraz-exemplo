﻿using System.ComponentModel;

namespace Easynvest.Alcatraz.Domain.Enumerators
{
    public class PushNotificationEnum
    {
        public enum PushDataAction
        {
            [Description("Logout")]
            Logout,
            [Description("Refresh Token JWT")]
            RefreshToken
        }

        public enum PushDataType
        {
            [Description("Reset Password")]
            ResetPassword = 1,
            [Description("Reset Eletronic Signature")]
            ResetEletronicSignature = 2
        }
    }
}
