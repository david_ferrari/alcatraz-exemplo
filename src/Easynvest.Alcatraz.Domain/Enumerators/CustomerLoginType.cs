﻿using Easynvest.Alcatraz.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Domain.Enumerators
{
    public class CustomerLoginType : Enumeration
    {
        public CustomerLoginType(string value, string display)
            : base(value, display)
        {

        }

        public readonly static CustomerLoginType CustomerLoginDocument = new CustomerLoginType("CPF", "Modo de autenticação do usuário pelo documento.");
        public readonly static CustomerLoginType CustomerLoginEmail = new CustomerLoginType("EMAIL", "Modo de autenticação do usuário pelo email.");
        public readonly static CustomerLoginType CustomerLoginAccountNumber = new CustomerLoginType("ACCOUNTNUMBER", "Modo de autenticação do usuário pelo número da conta.");

        public static IEnumerable<CustomerLoginType> List()
        {
            return new[] { CustomerLoginDocument, CustomerLoginEmail, CustomerLoginAccountNumber };
        }

        public static CustomerLoginType GetByValue(string value)
        {
            var customerLoginType = List().FirstOrDefault(x => x.Value.ToLowerInvariant().Equals(value.ToLowerInvariant()));

            if (customerLoginType == null)
            {
                throw new InvalidOperationException();
            }

            return customerLoginType;
        }
    }
}
