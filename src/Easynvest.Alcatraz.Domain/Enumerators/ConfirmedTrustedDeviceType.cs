﻿using System.ComponentModel;

namespace Easynvest.Alcatraz.Domain.Enumerators
{
    public enum ConfirmedTrustedDeviceType
    {
        [Description("Não Confirmado")]
        NotConfirmed = 0,
        [Description("Dispositivo Confiável")]
        Trusted = 1,
        [Description("Temporário")]
        Temporary = 2,
    }
}