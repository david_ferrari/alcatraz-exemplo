﻿using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Alcatraz.Domain.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Domain.Enumerators
{
    public class ValidateAuthenticateType : Enumeration
    {
        public ValidateAuthenticateType(string value, string display)
            : base(value, display)
        {

        }

        public readonly static ValidateAuthenticateType AccessGrant = new ValidateAuthenticateType("ACCESSGRANT", "Usuário Autenticado.");
        public readonly static ValidateAuthenticateType BlockedUser = new ValidateAuthenticateType("BLOCKEDUSER", "Usuário bloqueado.");
        public readonly static ValidateAuthenticateType DisabledUser = new ValidateAuthenticateType("DISABLEDUSER", ErrorMessages.USER_AUTHENTICATION_ERROR_DISABLEDUSER);
        public readonly static ValidateAuthenticateType InvalidCredential = new ValidateAuthenticateType("INVALIDCREDENTIAL", "Usuário e/ou senha inválidos.");
        public readonly static ValidateAuthenticateType Err_AuthenticateMode_Not_Found = new ValidateAuthenticateType("ERR_AUTHENTICATEMODE_NOT_FOUND", "Usuário e/ou senha inválidos.");
        public readonly static ValidateAuthenticateType Err_Authenticate_Fail = new ValidateAuthenticateType("ERR_AUTHENTICATE_FAIL", "Erro ao autenticar, tente novamente.");
        public static IEnumerable<ValidateAuthenticateType> List() => new[] { InvalidCredential, BlockedUser };

        public static ValidateAuthenticateType GetByValue(string value)
        {
            var validateAuthenticateType = List().FirstOrDefault(x => x.Value.ToLowerInvariant().Equals(value.ToLowerInvariant()));

            if (validateAuthenticateType == null)
                throw new InvalidOperationException();

            return validateAuthenticateType;
        }
    }
}