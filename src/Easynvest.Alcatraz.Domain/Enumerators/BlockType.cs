﻿using System.ComponentModel;

namespace Easynvest.Alcatraz.Domain.Enumerators
{
    public enum BlockType
    {
        [Description("Segurança")]
        Security = 0,
        [Description("Fraude")]
        Registry = 1
    }
}
