﻿namespace Easynvest.Alcatraz.Domain.Enumerators
{
    public enum SecretChangeType : byte
    {
        NewSecret = 1,
        ChangeSecret = 2
    }
}
