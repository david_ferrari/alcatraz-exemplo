﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Domain.Responses
{
    public class Response<TValue>
    {
        public Response()
        {
            Messages = new HashSet<string>();
        }

        public Response(TValue value) : this()
        {
            Value = value;
        }

        public TValue Value { get; }

        public ISet<string> Messages { get; }

        public string Message => string.Join(",", Messages);

        [JsonIgnore]
        public bool IsFailure => !IsSuccess;

        [JsonIgnore]
        public bool IsSuccess => Messages.Count == 0;

        public static Response<TValue> Ok()
        {
            return new Response<TValue>();
        }

        public static Response<TValue> Ok(TValue value)
        {
            return new Response<TValue>(value);
        }

        public static Response<TValue> Fail(string message, TValue value)
        {
            var response = new Response<TValue>(value);
            response.Messages.Add(message);

            return response;
        }

        public static Response<TValue> Fail(TValue value, IEnumerable<string> messsages)
        {
            var response = new Response<TValue>(value);
            response.Messages.UnionWith(messsages);

            return response;
        }

        public static Response<TValue> Fail(string message)
        {
            var response = new Response<TValue>(default(TValue));
            response.Messages.Add(message);

            return response;
        }

        public static Response<TValue> Fail(IEnumerable<string> messages)
        {
            var response = new Response<TValue>(default(TValue));
            response.Messages.UnionWith(messages);

            return response;
        }

        public void AddError(string message) => Messages.Add(message);
    }

    public class Response
    {
        protected Response()
        {
            Messages = new HashSet<string>();
        }

        protected Response(string message) : this()
        {
            Messages.Add(message);
        }

        public ISet<string> Messages { get; }

        public string Message => string.Join(",", Messages);

        public bool IsFailure => !IsSuccess;

        public bool IsSuccess => Messages.Count == 0;

        public static Response Ok()
        {
            return new Response();
        }

        public static Response Fail(string message)
        {
            return new Response(message);
        }

        public static Response Fail(IEnumerable<string> messages)
        {
            var response = new Response();
            response.Messages.UnionWith(messages);

            return response;
        }
    }
}
