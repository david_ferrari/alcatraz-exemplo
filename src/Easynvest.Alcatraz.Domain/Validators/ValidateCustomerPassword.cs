﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Notificator.Notifications;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Domain.Validators
{
    internal class ValidateCustomerPassword : ValidateCustomerSecret
    {
        public ValidateCustomerPassword(Password password, SecretChangeType SecretChangeType, PasswordBlacklist blacklist)
            : base(password, SecretType.Password, SecretChangeType, blacklist)
        {

        }

        protected override void CheckPreviousPasswords(string cleanSecret, string customerId, List<Notification> notifications)
        {
            if (Password.LastSixPasswords == null)
                return;

            if (Password.LastSixPasswords.Any(s => s.IsEqual(cleanSecret, customerId)))
            {
                notifications.Add(new Notification("Password", "Não podem ser utilizadas as últimas 6 senhas."));
            }
        }

        protected override void CheckSecretAgainstEncodedSecret(string secretCandidate, string customerId, List<Notification> notifications)
        {
            if (Password.PasswordDetails == null || Password.PasswordDetails.EncodedEletronicSignature == null)
                return;

            if (VerifySecret(secretCandidate, customerId, Password.PasswordDetails.EletronicSignatureSalt?.Salt,
                Password.PasswordDetails.EletronicSignatureVersion, Password.PasswordDetails.EncodedEletronicSignature))
                notifications.Add(new Notification("Password", "A senha de acesso ao portal e assinatura eletrônica não podem ser iguais."));
        }
    }
}