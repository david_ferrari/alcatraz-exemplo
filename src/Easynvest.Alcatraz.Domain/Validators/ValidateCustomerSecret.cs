﻿using Easynvest.Alcatraz.Domain.Common;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Services;
using Easynvest.Notificator.Notifications;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Easynvest.Alcatraz.Domain.Validators
{
    internal abstract class ValidateCustomerSecret
    {
        private readonly Password _password;
        private SecretChangeType SecretChangeType { get; set; }
        public SecretType SecretType { get; }
        public PasswordBlacklist Blacklist { get; }

        protected Password Password
        {
            get { return _password; }
        }

        protected ValidateCustomerSecret(Password password, SecretType passwordType, SecretChangeType secretChangeType, PasswordBlacklist blacklist)
        {
            _password = password;
            SecretChangeType = secretChangeType;
            SecretType = passwordType;
            Blacklist = blacklist;
        }

        public virtual IReadOnlyList<Notification> Verify(string passwordCandidate, string rawElectronicSignature, bool ignoreHistory = false)
        {
            var notifications = new List<Notification>();

            CheckPasswordLength(passwordCandidate, notifications);
            CheckPasswordComplexity(passwordCandidate, notifications);
            if (!ignoreHistory)
                CheckPreviousPasswords(passwordCandidate, _password.Customer?.CustomerId, notifications);
            CheckElectronicSignature(rawElectronicSignature, notifications);
            CheckBlacklist(passwordCandidate, notifications);

            if (SecretChangeType == SecretChangeType.ChangeSecret)
            {
                CheckPersonalDataInPassword(passwordCandidate, notifications);
                CheckSecretAgainstEncodedSecret(passwordCandidate, _password.Customer.CustomerId, notifications);
            }

            return notifications;
        }

        private static void CheckPasswordLength(string passwordCandidate, List<Notification> notifications)
        {
            if (passwordCandidate.Length < 6)
            {
                notifications.Add(new Notification("Password", "Senha deve possuir no mínimo 6 caracteres."));
            }
        }
        private static void CheckPasswordComplexity(string passwordCandidate, List<Notification> notifications)
        {
            var hasNumber = new Regex("[0-9]+").IsMatch(passwordCandidate);
            var hasAlphabeticalCharacter = new Regex(@"[^\W_^\d]").IsMatch(passwordCandidate);
            var hasExcessiveNumbersInSequence = new Regex("012|123|234|345|456|567|678|789|987|876|765|654|543|432|321|210").IsMatch(passwordCandidate);

            if (!hasNumber || !hasAlphabeticalCharacter)
            {
                notifications.Add(
                    new Notification("Password", "Senha deve possuir letras e números."));
            }

            if (hasExcessiveNumbersInSequence)
            {
                notifications.Add(new Notification("Password", "Senha deve possuir no máximo 2 números em sequência."));
            }
        }
        private void CheckPersonalDataInPassword(string passwordCandidate, List<Notification> notifications)
        {
            var hasBirthDate = Password.Customer.BirthDate != null
                ? passwordCandidate.Contains(Password.Customer.BirthDate.Value?.ToString("ddMMyyyy"))
                : false;

            var hasFirstName = new Regex(Password.Customer.Name.FirstName.RemoveDiacritics(), RegexOptions.IgnoreCase).IsMatch(passwordCandidate.RemoveDiacritics());

            if (hasBirthDate)
            {
                notifications.Add(new Notification("Password", "Data de aniversário não pode ser usada como senha."));
            }

            if (hasFirstName)
            {
                notifications.Add(new Notification("Password", "Nome não pode ser usado como senha."));
            }
        }

        protected static bool VerifyCore(string plainText, string customerId, string salt, PasswordVersion version, string encodedSecret)
        {
            var encodedPlainText = EncodePasswords.Encode(customerId, plainText, version, salt);
            return encodedSecret.Equals(encodedPlainText);
        }

        protected bool VerifySecret(string plainText, string customerId, string salt, PasswordVersion version, string encodedSecretToCompare)
        {
            if (string.IsNullOrEmpty(encodedSecretToCompare))
                return false;

            return VerifyCore(plainText, customerId, salt, version, encodedSecretToCompare);
        }
        private void CheckElectronicSignature(string rawElectronicSignature, List<Notification> notifications)
        {
            if (!string.IsNullOrEmpty(rawElectronicSignature) &&
                !Password.PasswordDetails.EncodedEletronicSignature.Equals(EncodePasswords.Encode(_password.Customer.CustomerId,
                rawElectronicSignature, _password.PasswordDetails.EletronicSignatureVersion, _password.PasswordDetails.EletronicSignatureSalt?.Salt)))
            {
                notifications.Add(new Notification("Electronic Signature", "Assinatura Eletrônica inválida."));
            }
        }

        private void CheckBlacklist(string passwordCandidate, List<Notification> notifications)
        {
            if (Blacklist != null && !Blacklist.IsValid(passwordCandidate))
                notifications.Add(new Notification("Password", "A senha informada não atende os requisitos mínimos de segurança."));
        }

        protected abstract void CheckPreviousPasswords(string cleanSecret, string customerId, List<Notification> notifications);
        protected abstract void CheckSecretAgainstEncodedSecret(string secretCandidate, string customerId, List<Notification> notifications);
    }
}