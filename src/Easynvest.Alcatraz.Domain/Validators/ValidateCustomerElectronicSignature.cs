﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Notificator.Notifications;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Domain.Validators
{
    internal class ValidateCustomerElectronicSignature : ValidateCustomerSecret
    {
        public ValidateCustomerElectronicSignature(Password password, SecretChangeType SecretChangeType, PasswordBlacklist blacklist)
            : base(password, SecretType.EletronicSignature, SecretChangeType, blacklist)
        {

        }

        protected override void CheckPreviousPasswords(string cleanSecret, string customerId, List<Notification> notifications)
        {
            if (Password.LastSixElectronicSignatures.Any(s => s.IsEqual(cleanSecret, customerId)))
            {
                notifications.Add(new Notification("Password", "Não podem ser utilizadas as últimas 6 senhas."));
            }
        }

        protected override void CheckSecretAgainstEncodedSecret(string secretCandidate, string customerId, List<Notification> notifications)
        {
            if (VerifySecret(secretCandidate, customerId, Password.PasswordDetails.PasswordSalt?.Salt,
                Password.PasswordDetails.PasswordVersion, Password.PasswordDetails.EncodedPassword))
            {
                notifications.Add(new Notification("Password", "A senha de acesso ao portal e assinatura eletrônica não podem ser iguais."));
            }
        }
    }
}