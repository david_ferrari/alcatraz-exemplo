﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface IUserBlockRepository
    {
        Task<IReadOnlyCollection<UserBlock>> Get(string customerId);
        Task<IReadOnlyCollection<UserBlock>> Get(BlockType blockType);

        Task Add(UserBlock user);

        Task Remove(string customerId, BlockType blockType);
    }
}
