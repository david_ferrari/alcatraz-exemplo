﻿using System.Threading.Tasks;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface IBopeRepository
    {
        Task<bool> Authenticate(string requestId, string accountId, string otpCode, string deviceUid, BopeIntegrationType integration);

        Task<DeviceRegisteredDto> GetTokenInfo(string requestId, string accountId);
    }
}