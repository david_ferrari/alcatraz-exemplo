﻿using System.Threading.Tasks;
using Easynvest.Alcatraz.Domain.Models.Entities;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface IValidateTimeTokenRepository
    {
        Task<ValidateTimeToken> GetValidateTimeTokenByDeviceUid(string deviceUid, string customerId);

        Task RegisterTimeToken(ValidateTimeToken validateTimeToken);

        Task UpdateTimeToken(ValidateTimeToken validateTimeToken);
    }
}