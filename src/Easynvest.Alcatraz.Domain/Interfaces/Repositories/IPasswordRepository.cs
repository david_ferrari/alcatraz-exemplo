﻿using Easynvest.Alcatraz.Domain.Models.Entities;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface IPasswordRepository
    {
        Task<Password> GetByCustomerId(string customerId);

        Task<Password> GetByCustomerId(string customerId, string password);

        Task UpdateSecret(Password password);

        Task InsertPassword(Password password);
    }
}
