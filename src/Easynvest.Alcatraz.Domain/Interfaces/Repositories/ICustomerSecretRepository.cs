﻿using Easynvest.Alcatraz.Domain.Entities;
using Easynvest.Alcatraz.Domain.Enumerators;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface ICustomerSecretRepository
    {
        Task<CustomerSecret> Get(string customerId, SecretType passwordType);
        
        Task UpdateCustomerSecret(CustomerSecret customerSecret, SecretType type);

        Task UpdateFcmToken(CustomerSecret customerSecret);
    }
}
