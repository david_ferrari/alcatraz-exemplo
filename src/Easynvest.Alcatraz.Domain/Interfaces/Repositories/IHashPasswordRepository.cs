﻿using Easynvest.Alcatraz.Domain.Models.Entities;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface IHashPasswordRepository
    {
        Task Create(PasswordRecoveryRequest passwordRecoveryRequest);
        Task<PasswordRecoveryRequest> GetByHash(string hash);
        Task UpdateExpirationDate(PasswordRecoveryRequest passwordRecoveryRequest);

    }
}
