﻿using Easynvest.Alcatraz.Domain.Models.Entities;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface ICustomerRepository
    {
        Task<Customer> GetCustomerInfoByCustomerId(string customerId);
    }
}
