﻿using Easynvest.Alcatraz.Domain.Models.Entities;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface IAuthCodeRepository
    {
        Task InsertCode(AuthCode code);

        Task<AuthCode> GetLatestCode(string customerId);
    }
}
