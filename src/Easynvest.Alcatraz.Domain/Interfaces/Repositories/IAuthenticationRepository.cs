﻿using System.Threading.Tasks;
using Easynvest.Alcatraz.Domain.Dtos;

namespace Easynvest.Alcatraz.Domain.Interfaces.Repositories
{
    public interface IAuthenticationRepository
    {
        Task<AuthenticationDto> Authenticate(string userName, string password, string clientId);
    }
}
