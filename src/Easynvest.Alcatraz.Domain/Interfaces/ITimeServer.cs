﻿using System;

namespace Easynvest.Alcatraz.Domain.Interfaces
{
    public interface ITimeServer
    {
        DateTime Current { get; }
    }
}
