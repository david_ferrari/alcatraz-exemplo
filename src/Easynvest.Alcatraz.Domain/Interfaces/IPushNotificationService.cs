﻿using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.PushNotification.FirebaseCloudMessage.Messages;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces
{
    public interface IPushNotificationService
    {
        Task<ResponseContent> SendDataAsync(Customer user, object data);
    }
}
