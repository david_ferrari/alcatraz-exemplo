﻿using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Enumerators;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Domain.Interfaces
{
    public interface IAuthenticateCustomerRepository
    {
        Task<IEnumerable<CustomerUserDto>> GetAuthenticatedCustomer(string value, CustomerLoginType customerLoginType);
    }
}
