﻿using Easynvest.Alcatraz.Application.Query.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Query.Queries
{
    public class ValidateAuthenticatedCustomerQuery : IRequest<ValidateAuthenticatedCustomerResponse>
    {
        public ValidateAuthenticatedCustomerQuery(string userName, string password, string fcmToken, string ipAddress)
        {
            CustomerAuthenticatedValue = userName;
            Password = password;
            FcmToken = fcmToken;
            IpAddress = ipAddress;
        }

        public string CustomerAuthenticatedValue { get; }
        public string Password { get; }
        public string FcmToken { get; }
        public string IpAddress { get; }
        public override string ToString() => $"{CustomerAuthenticatedValue}  - ip - {IpAddress}";
    }
}