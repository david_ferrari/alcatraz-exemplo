﻿using Easynvest.Alcatraz.Application.Query.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Query.Queries
{
    public class RefreshAuthenticatedCustomerQuery : IRequest<ValidateAuthenticatedCustomerResponse>
    {
        public RefreshAuthenticatedCustomerQuery(string customerId)
        {
            CustomerAuthenticatedValue = customerId;
        }

        public string CustomerAuthenticatedValue { get; }
    }
}
