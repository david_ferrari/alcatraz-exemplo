﻿using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Application.Requests;

namespace Easynvest.Alcatraz.Application.Query.Queries
{
    public class ValidateAuthenticationCodeQuery : Request<ValidateAuthenticationCodeResponse>
    {
        public ValidateAuthenticationCodeQuery(string username, string password, string temporaryCode, string fcmToken, string ipAddress, string deviceUid)
        {
            Username = username;
            Password = password;
            TemporaryCode = temporaryCode;
            FcmToken = fcmToken;
            IpAddress = ipAddress;
            DeviceUid = deviceUid?.ToUpper();
        }

        public string TemporaryCode { get; }
        public string Username { get; }
        public string Password { get; }
        public string FcmToken { get; }
        public string IpAddress { get; }
        public string DeviceUid { get; }

        public override string ToString() => $"{Username}  - ip - {IpAddress}";

        public override ValidateAuthenticationCodeResponse Response => new ValidateAuthenticationCodeResponse(RequestId);
    }
}