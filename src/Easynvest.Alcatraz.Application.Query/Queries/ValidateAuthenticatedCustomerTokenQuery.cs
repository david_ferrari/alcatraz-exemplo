﻿using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Application.Requests;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using MediatR;

namespace Easynvest.Alcatraz.Application.Query.Queries
{
    public class ValidateAuthenticatedCustomerTokenQuery : Request<ValidateAuthenticatedCustomerTokenResponse>, IRequest<ValidateAuthenticatedCustomerTokenResponse>
    {
        public ValidateAuthenticatedCustomerTokenQuery(string username, string password, string fcmToken, string deviceUid, string otpCode, string ipAddress, bool trustedDevice)
        {
            Username = username;
            Password = password;
            FcmToken = fcmToken;
            DeviceUid = deviceUid?.ToUpper();
            OtpCode = otpCode;
            IpAddress = ipAddress;
            TrustedDevice = trustedDevice;
            ConfirmedTrustedDevice = trustedDevice ? ConfirmedTrustedDeviceType.Trusted : ConfirmedTrustedDeviceType.NotConfirmed;
        }

        public string Username { get; }

        public string Password { get; }

        public string FcmToken { get; }

        public string DeviceUid { get; }

        public string OtpCode { get; }

        public BopeIntegrationType Integration { get { return BopeIntegrationType.Login; } }

        public string IpAddress { get; }

        public bool TrustedDevice { get; }

        public ConfirmedTrustedDeviceType ConfirmedTrustedDevice { get; }

        public override ValidateAuthenticatedCustomerTokenResponse Response => new ValidateAuthenticatedCustomerTokenResponse(RequestId);
    }
}