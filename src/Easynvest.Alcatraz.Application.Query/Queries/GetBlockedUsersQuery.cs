﻿using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Query.Queries
{
    public class GetBlockedUsersQuery : IRequest<Response<GetBlockedUsersResponse>>
    {
        public GetBlockedUsersQuery(BlockType type)
        {
            Type = type;
        }

        public BlockType Type { get; }
    }
}
