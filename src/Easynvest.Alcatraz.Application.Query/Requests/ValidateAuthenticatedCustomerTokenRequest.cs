﻿namespace Easynvest.Alcatraz.Application.Query.Requests
{
    public class ValidateAuthenticatedCustomerTokenRequest
    {
        public ValidateAuthenticatedCustomerTokenRequest(string username, string password, string fcmToken, string deviceUid, string otpCode, bool trustedDevice)
        {
            Username = username;
            Password = password;
            FcmToken = fcmToken;
            DeviceUid = deviceUid;
            OtpCode = otpCode;
            TrustedDevice = trustedDevice;
        }

        public string AccountId { get; }

        public string Username { get; }

        public string Password { get; }

        public string FcmToken { get; }

        public string DeviceUid { get; }

        public string OtpCode { get; }

        public bool TrustedDevice { get; }
    }
}