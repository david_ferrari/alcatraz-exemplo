﻿namespace Easynvest.Alcatraz.Application.Query.Requests
{
    public class AuthenticatedCustomerRequest
    {
        public AuthenticatedCustomerRequest(string userName, string password, string fcmToken)
        {
            UserName = userName;
            Password = password;
            FcmToken = fcmToken;
        }

        public string UserName { get; }
        public string Password { get; }
        public string FcmToken { get; }
    }
}
