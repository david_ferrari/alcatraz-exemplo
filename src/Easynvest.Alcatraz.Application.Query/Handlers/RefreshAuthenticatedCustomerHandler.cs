﻿using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Query.Handlers
{
    public class RefreshAuthenticatedCustomerHandler : IRequestHandler<RefreshAuthenticatedCustomerQuery, ValidateAuthenticatedCustomerResponse>
    {
        private readonly ILogger<RefreshAuthenticatedCustomerHandler> _logger;
        private readonly IAuthenticateCustomerRepository _authenticateCustomerRepository;

        public RefreshAuthenticatedCustomerHandler(ILogger<RefreshAuthenticatedCustomerHandler> logger,
                                                   IAuthenticateCustomerRepository authenticateCustomerRepository)
        {
            _authenticateCustomerRepository = authenticateCustomerRepository;
            _logger = logger;
        }

        public async Task<ValidateAuthenticatedCustomerResponse> Handle(RefreshAuthenticatedCustomerQuery request, CancellationToken cancellationToken)
        {
            var response = new ValidateAuthenticatedCustomerResponse();
            try
            {
                _logger.LogExInformation($"Início do processo de obtenção de usuário para atualização de token. {request.CustomerAuthenticatedValue}");

                var validateRequest = ValidateRequest(request);
                if (validateRequest.IsFailure)
                {
                    response.AuthenticatedMessages.Add("CustomerId", validateRequest.Message);
                    _logger.LogExWarning($"{response.AuthenticatedMessages}");
                    return response;
                }

                var getAuthenticatedCustomer = await GetAuthenticatedCustomer(request.CustomerAuthenticatedValue);
                if (getAuthenticatedCustomer.IsFailure)
                {
                    response.AuthenticatedMessages.Add(ValidateAuthenticateType.InvalidCredential.Value, ValidateAuthenticateType.InvalidCredential.Display);
                    _logger.LogExWarning($"Falha na obtenção de usuário para refresh token. {getAuthenticatedCustomer.Message}");

                    return response;
                }

                response.CustomerUser = getAuthenticatedCustomer.Value;

                _logger.LogExInformation($"Fim do processo de obtenção de usuário para atualização de token. {request.CustomerAuthenticatedValue}");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao executar {typeof(RefreshAuthenticatedCustomerHandler)}");
                response.AddAuthenticatedMessages("ERROR", $"Falha ao atualizar logon.");
                return response;
            }
        }

        private async Task<Response<CustomerUserDto>> GetAuthenticatedCustomer(string customerAuthenticatedValue)
        {
            var customersUser = await _authenticateCustomerRepository.GetAuthenticatedCustomer(customerAuthenticatedValue, CustomerLoginType.CustomerLoginDocument);

            if (!customersUser.Any())
            {
                var msg = $"Usuário {customerAuthenticatedValue} não localizado. {CustomerLoginType.CustomerLoginDocument.Display}";
                return Response<CustomerUserDto>.Fail(msg);
            }

            return Response<CustomerUserDto>.Ok(customersUser.First());
        }

        private Response ValidateRequest(RefreshAuthenticatedCustomerQuery request)
        {
            if (string.IsNullOrEmpty(request.CustomerAuthenticatedValue))
                return Response.Fail("CustomerId vazio");

            return Response.Ok();
        }
    }
}
