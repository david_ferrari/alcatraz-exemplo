﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Entities;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Easynvest.Alcatraz.Application.Query.Handlers
{
    public class ValidateAuthenticatedCustomerHandler : IRequestHandler<ValidateAuthenticatedCustomerQuery, ValidateAuthenticatedCustomerResponse>
    {
        private CustomerSecret _customerSecret;
        private CustomerLoginType _customerLoginType;
        private CustomerUserDto _customerUser;
        private readonly IMediator _mediator;
        private readonly CustomerSecretOption _customerSecretOptions;
        private readonly ILogger<ValidateAuthenticatedCustomerHandler> _logger;
        private readonly ICustomerSecretRepository _customerSecretRepository;
        private readonly IUserBlockRepository _blockRepository;
        private readonly LoginConfigurationOptions _loginConfigurationOptions;
        private readonly IAuthenticateCustomerRepository _authenticateCustomerRepository;

        public ValidateAuthenticatedCustomerHandler(IMediator mediator,
                                                    ILogger<ValidateAuthenticatedCustomerHandler> logger,
                                                    IAuthenticateCustomerRepository authenticateCustomerRepositor,
                                                    IUserBlockRepository blockRepository,
                                                    ICustomerSecretRepository customerSecretRepository,
                                                    IOptions<CustomerSecretOption> options,
                                                    IOptions<LoginConfigurationOptions> loginConfigurationOptions)
        {
            _mediator = mediator;
            _customerSecretOptions = options.Value;
            _customerSecretRepository = customerSecretRepository ?? throw new ArgumentNullException(nameof(customerSecretRepository));
            _blockRepository = blockRepository;
            _authenticateCustomerRepository = authenticateCustomerRepositor ?? throw new ArgumentNullException(nameof(authenticateCustomerRepositor));
            _loginConfigurationOptions = loginConfigurationOptions.Value;
            _logger = logger;
        }

        public async Task<ValidateAuthenticatedCustomerResponse> Handle(ValidateAuthenticatedCustomerQuery request, CancellationToken cancellationToken)
        {
            var response = new ValidateAuthenticatedCustomerResponse();
            try
            {
                _logger.LogExInformation("Iniciando o processo de autenticação do usuário {RequestMessage}", request.ToString());

                var result = await ValidateAuthenticatedCustomer(request);
                if (result.IsFailure)
                    return result.Value;

                var validateCustomerSecret = await ValidateCustomerSecret(request, response);

                response.CustomerUser = _customerUser;
                await UpdateCustomerSecret(request);
                if (validateCustomerSecret.IsFailure)
                    return response;

                var validateCustomerUser = await ValidateUser(request, response);
                if (validateCustomerUser.IsFailure)
                    return response;

                _logger.LogExInformation("Fim do processo de autenticação do usuário {RequestMessage} - {AccessGrant}.", request.ToString(), ValidateAuthenticateType.AccessGrant.Value);
                _logger.LogExInformation("Login do cliente CPF: {CustomerId} feito com sucesso!", _customerUser?.CustomerId);

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Erro ao executar {SourceClass}", typeof(ValidateAuthenticatedCustomerHandler));
                response.AddAuthenticatedMessages("ERROR", $"Falha ao recuperar o usuário.");
                return response;
            }
        }

        private async Task<Response<ValidateAuthenticatedCustomerResponse>> ValidateAuthenticatedCustomer(ValidateAuthenticatedCustomerQuery request)
        {
            return await GetCustomerLoginType(request,
                        () => GetAuthenticatedCustomer(request,
                        () => GetCustomerSecret(request))
                    );
        }

        private async Task<Response<ValidateAuthenticatedCustomerResponse>> GetCustomerLoginType(ValidateAuthenticatedCustomerQuery request,
            Func<Task<Response<ValidateAuthenticatedCustomerResponse>>> next)
        {
            Response<ValidateAuthenticatedCustomerResponse> result;

            var response = new ValidateAuthenticatedCustomerResponse();

            try
            {
                var match = CustomerLoginPatternValidator.Match(request.CustomerAuthenticatedValue);

                if (match.IsFailure)
                {
                    var msg = string.Format(Messages.ERR_AUTHENTICATEMODE_NOT_FOUND, request.CustomerAuthenticatedValue);

                    _logger.LogExWarning("{ErrorType} - {MessageError}", ValidateAuthenticateType.Err_AuthenticateMode_Not_Found.Value, msg);
                    response.AddAuthenticatedMessages(ValidateAuthenticateType.InvalidCredential.Value, msg);

                    result = Response<ValidateAuthenticatedCustomerResponse>.Fail(msg, response);
                }
                else
                {
                    _customerLoginType = match.Value;

                    if (IsEmailLoginType() && IsEmailLoginDisabled())
                    {
                        var message = ValidateAuthenticateType.Err_AuthenticateMode_Not_Found.Display;

                        _logger.LogExWarning("{ErrorType} - {MessageError}. Login por e-mail desabilitado.", ValidateAuthenticateType.Err_AuthenticateMode_Not_Found.Value, message);
                        response.AddAuthenticatedMessages(ValidateAuthenticateType.InvalidCredential.Value, message);

                        result = Response<ValidateAuthenticatedCustomerResponse>.Fail(message, response);
                    }
                    else
                    {
                        _logger.LogExInformation("Usuário {RequestMessage} Login do tipo: {LoginType}", request.ToString(), _customerLoginType.Display);
                        result = Response<ValidateAuthenticatedCustomerResponse>.Ok(response);
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMsg = $"ERR_AUTHENTICATEMODE_NOT_FOUND - Erro ao definir modo de autenticação não localizado para o usuário: {request.CustomerAuthenticatedValue}. Erro: {ex}";

                _logger.LogExError(ex, "ERR_AUTHENTICATEMODE_NOT_FOUND - Erro ao definir modo de autenticação não localizado para o usuário: {CustomerId}", request.CustomerAuthenticatedValue);
                response.AddAuthenticatedMessages(ValidateAuthenticateType.InvalidCredential.Value, errorMsg);

                result = Response<ValidateAuthenticatedCustomerResponse>.Fail(errorMsg, response);
            }

            if (result.IsFailure)
                return result;

            return await next.Invoke();
        }

        private async Task<Response<ValidateAuthenticatedCustomerResponse>> GetAuthenticatedCustomer(ValidateAuthenticatedCustomerQuery request,
            Func<Task<Response<ValidateAuthenticatedCustomerResponse>>> next)
        {
            Response<ValidateAuthenticatedCustomerResponse> result;

            var response = new ValidateAuthenticatedCustomerResponse();
            try
            {
                var customersUser = await _authenticateCustomerRepository.GetAuthenticatedCustomer(request.CustomerAuthenticatedValue, _customerLoginType);

                if (customersUser.Count() == 1)
                {
                    _customerUser = customersUser.Single();
                    result = Response<ValidateAuthenticatedCustomerResponse>.Ok(response);
                }
                else
                {
                    if (customersUser.Any())
                        _logger.LogExWarning("Múltiplos usuários localizados para {RequestMessage}. {LoginType}", request.ToString(), _customerLoginType.Display);
                    else
                        _logger.LogExWarning("RequestMessage} não localizado. {LoginType}", request.ToString(), _customerLoginType.Display);
                    

                    response.AddAuthenticatedMessages(ValidateAuthenticateType.InvalidCredential.Value, ValidateAuthenticateType.InvalidCredential.Display);
                    result = Response<ValidateAuthenticatedCustomerResponse>.Fail(ValidateAuthenticateType.InvalidCredential.Display, response);
                }
            }
            catch (Exception ex)
            {                
                _logger.LogExError(ex, "Falha ao recuperar o usuário: {RequestMessage} - {LoginType}", request.ToString(), _customerLoginType?.Display);
                response.AddAuthenticatedMessages("ERROR", "Falha ao recuperar o usuário");
                result = Response<ValidateAuthenticatedCustomerResponse>.Fail("Falha ao recuperar o usuário", response);
            }

            if (result.IsFailure)
                return result;

            return await next.Invoke();
        }

        private async Task<Response<ValidateAuthenticatedCustomerResponse>> GetCustomerSecret(ValidateAuthenticatedCustomerQuery request)
        {
            var response = new ValidateAuthenticatedCustomerResponse();
            try
            {
                _customerSecret = await _customerSecretRepository.Get(_customerUser.CustomerId, SecretType.Password);

                if (_customerSecret == null)
                {
                    _logger.LogExWarning("ERR_GET_USER - Erro ao montar o retorno do Usuário {RequestMessage} para o modo de autenticação {LoginType}.", request.ToString(), _customerLoginType.Display);
                    response.AuthenticatedMessages.Add("ERROR", $"ERR_GET_USER - Erro ao montar o retorno do Usuário {request} para o modo de autenticação {_customerLoginType.Display}.");

                    return Response<ValidateAuthenticatedCustomerResponse>.Fail(response.AuthenticatedMessages.LastOrDefault().Value, response);
                }

                return Response<ValidateAuthenticatedCustomerResponse>.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Falha ao recuperar o secret: {RequestMessage}", request.ToString());
                response.AddAuthenticatedMessages("ERROR", "Falha ao recuperar o usuário");
                return Response<ValidateAuthenticatedCustomerResponse>.Fail("Falha ao recuperar o usuário", response);
            }
        }

        private async Task<Response<ValidateAuthenticatedCustomerResponse>> ValidateCustomerSecret(ValidateAuthenticatedCustomerQuery request, ValidateAuthenticatedCustomerResponse response)
        {

            try
            {
                if (!_customerSecret.VerifyLoginAttempts(_customerSecretOptions.MaxInvalidLoginAttempts))
                {
                    _logger.LogExWarning("Tentativa de login com o Usuário Bloqueado - Cliente: {CustomerId}", request.CustomerAuthenticatedValue);
                    response.AuthenticatedMessages.Add(ValidateAuthenticateType.BlockedUser.Value, ValidateAuthenticateType.BlockedUser.Display);
                    return Response<ValidateAuthenticatedCustomerResponse>.Fail(ValidateAuthenticateType.BlockedUser.Display, response);
                }

                if (!_customerSecret.ValidateCustomerSecret(request.Password))
                {
                    if (_customerSecret.LoginAttempts >= _customerSecretOptions.MaxInvalidLoginAttempts)
                        _logger.LogExWarning("Muitas tentativas de login inválido, Usuário bloqueado - Cliente: {CustomerId}", request.CustomerAuthenticatedValue);

                    _logger.LogExWarning("Credenciais inválidas, tentativas de login = {LoginAttempts} - Cliente: {CustomerId}", _customerSecret.LoginAttempts, request.CustomerAuthenticatedValue);

                    response.AuthenticatedMessages.Add(ValidateAuthenticateType.InvalidCredential.Value, ValidateAuthenticateType.InvalidCredential.Display);
                    return Response<ValidateAuthenticatedCustomerResponse>.Fail(ValidateAuthenticateType.InvalidCredential.Display, response);
                }

                if (_customerSecret.NeedToUpdate())
                {
                    var updateResult = await _mediator.Send(new UpdateSecretVersionCommand(_customerSecret.CustomerId,
                        SecretType.Password, request.Password));

                    if (updateResult == null || updateResult.IsFailure)
                        _logger.LogExError("Erro ao atualizar a versão da senha do usuário", updateResult?.Message);
                    else if(updateResult.Value)
                    {
                        var secret = await _customerSecretRepository.Get(_customerUser.CustomerId, SecretType.Password);

                        // update com sucesso
                        if (secret.Version == PasswordVersion.New)
                            _customerSecret = secret;
                    }
                }

                return Response<ValidateAuthenticatedCustomerResponse>.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Falha ao verificar tentativas de login: {RequestMessage} - {LoginType}", request.ToString(), _customerLoginType?.Display);
                response.AddAuthenticatedMessages("ERROR", "Falha ao verificar usuário");
                return Response<ValidateAuthenticatedCustomerResponse>.Fail(response.AuthenticatedMessages.LastOrDefault().Value, response);
            }
        }

        private async Task<Response<ValidateAuthenticatedCustomerResponse>> ValidateUser(ValidateAuthenticatedCustomerQuery request, ValidateAuthenticatedCustomerResponse response)
        {
            try
            {
                if (_customerUser.Status.Equals("I"))
                {
                    _logger.LogExWarning("Usuário desativado - Cliente: {CustomerId}", request.CustomerAuthenticatedValue);
                    response.AuthenticatedMessages.Add(ValidateAuthenticateType.DisabledUser.Value, ValidateAuthenticateType.DisabledUser.Display);
                    return Response<ValidateAuthenticatedCustomerResponse>.Fail(ValidateAuthenticateType.DisabledUser.Display, response);
                }

                var blocks = await _blockRepository.Get(_customerSecret.CustomerId);
                if (blocks != null && blocks.Any())
                {
                    _logger.LogExWarning("Tentativa de login com usuário com bloqueio ativo - Cliente: {CustomerId}", request.CustomerAuthenticatedValue);
                    response.AuthenticatedMessages.Add(ValidateAuthenticateType.DisabledUser.Value, ValidateAuthenticateType.DisabledUser.Display);
                    return Response<ValidateAuthenticatedCustomerResponse>.Fail(ValidateAuthenticateType.DisabledUser.Display, response);
                }

                return Response<ValidateAuthenticatedCustomerResponse>.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Falha ao verificar usuario: {RequestMessage} - {LoginType}", request.ToString(), _customerLoginType?.Display);
                response.AddAuthenticatedMessages("ERROR", "Falha ao verificar usuário");
                return Response<ValidateAuthenticatedCustomerResponse>.Fail(response.AuthenticatedMessages.LastOrDefault().Value, response);
            }
        }

        private async Task UpdateCustomerSecret(ValidateAuthenticatedCustomerQuery request)
        {
            using (var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                if(_customerSecret.ShouldUpdateLoginAttempts())
                    await _customerSecretRepository.UpdateCustomerSecret(_customerSecret, SecretType.Password);
                await UpdateFcmTokenSecret(request);
                ts.Complete();
            }
        }

        private async Task UpdateFcmTokenSecret(ValidateAuthenticatedCustomerQuery request)
        {
            if (!string.IsNullOrEmpty(request.FcmToken))
            {
                _customerSecret.AccessToken = new AccessToken(request.FcmToken);
                await _customerSecretRepository.UpdateFcmToken(_customerSecret);
            }
        }

        private bool IsEmailLoginType() => _customerLoginType.Value.Equals(CustomerLoginType.CustomerLoginEmail.Value);

        private bool IsEmailLoginDisabled() => _loginConfigurationOptions.EmailLoginDisabled;
    }
}