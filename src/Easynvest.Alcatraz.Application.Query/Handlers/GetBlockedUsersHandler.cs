﻿using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Query.Handlers
{
    public class GetBlockedUsersHandler : IRequestHandler<GetBlockedUsersQuery, Response<GetBlockedUsersResponse>>
    {
        private readonly IUserBlockRepository _repository;
        private readonly ILogger<GetBlockedUsersHandler> _logger;

        public GetBlockedUsersHandler(IUserBlockRepository repository, ILogger<GetBlockedUsersHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<Response<GetBlockedUsersResponse>> Handle(GetBlockedUsersQuery request, CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogExInformation("Obtendo todos os usuários bloqueados", request);
                var users = await _repository.Get(request.Type);
                return Response<GetBlockedUsersResponse>.Ok(new GetBlockedUsersResponse(users));
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Erro ao obter os usuários bloqueados", request);
                return Response<GetBlockedUsersResponse>.Fail("Erro ao obter os usuários bloqueados.");
            }
        }
    }
}
