﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Easynvest.Alcatraz.Application.Query.Handlers
{
    public class ValidateAuthenticationCodeHandler : IRequestHandler<ValidateAuthenticationCodeQuery, ValidateAuthenticationCodeResponse>
    {
        private readonly ILogger<ValidateAuthenticationCodeHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IAuthCodeRepository _codeRepository;
        private readonly IValidateTimeTokenRepository _validateTimeTokenRepository;

        public ValidateAuthenticationCodeHandler(IMediator mediator, IAuthCodeRepository codeRepository, IValidateTimeTokenRepository validateTimeTokenRepository,
            ILogger<ValidateAuthenticationCodeHandler> logger)
        {
            _logger = logger;
            _mediator = mediator;
            _codeRepository = codeRepository;
            _validateTimeTokenRepository = validateTimeTokenRepository;
        }

        public async Task<ValidateAuthenticationCodeResponse> Handle(ValidateAuthenticationCodeQuery request, CancellationToken cancellationToken)
        {
            var response = request.Response;

            try
            {
                _logger.LogExInformation($"[{request.RequestId}] - Iniciando o processo de autorização via código temporário para a conta: {request}");

                var authResponse = await ValidateAuthenticatedCustomer(request, response);
                if (response.IsFailure)
                    return response;

                _logger.LogExInformation($"[{request.RequestId}] - Login válido para a conta: {request}");

                var authCode = await _codeRepository.GetLatestCode(authResponse.CustomerUser.CustomerId);
                if (authCode == null)
                {
                    _logger.LogExInformation($"[{request.RequestId}] - Código temporário não encontrado para a conta: {request}");

                    response.AddError(Errors.ErrorsToken.NotFoundCode());
                    return response;
                }

                if (!authCode.IsValid(request.TemporaryCode))
                {
                    _logger.LogExInformation($"[{request.RequestId}] - Código temporário inválido para a conta: {request}");

                    response.AddError(Errors.ErrorsToken.InvalidCode());
                    return response;
                }

                if (authCode.IsExpired())
                {
                    _logger.LogExInformation($"[{request.RequestId}] - Código temporário expirado para a conta: {request}");

                    response.AddError(Errors.ErrorsToken.ExpiredCode());
                    return response;
                }

                var registerTimeToken = new ValidateTimeToken(response.CustomerUser.CustomerId, request.DeviceUid, ConfirmedTrustedDeviceType.Temporary);
                await _validateTimeTokenRepository.RegisterTimeToken(registerTimeToken);
            }
            catch (Exception ex)
            {
                response.AddError(Errors.General.InternalProcessError());

                _logger.LogExError(ex, "{@ErrorResponse}", response.ErrorResponse);
                _logger.LogExError(ex, $"Erro ao autorizar a conta: {request}");
            }

            _logger.LogExInformation($"[{request.RequestId}] - Finalizando o processo de autorização via código temporário para a conta: {request}");

            return response;
        }

        private async Task<ValidateAuthenticatedCustomerResponse> ValidateAuthenticatedCustomer(ValidateAuthenticationCodeQuery request, ValidateAuthenticationCodeResponse response)
        {
            var validateAuthenticatedCustomerQuery = new ValidateAuthenticatedCustomerQuery(request.Username, request.Password, request.FcmToken, request.IpAddress);
            var validateAuthenticatedCustomerResponse = await _mediator.Send(validateAuthenticatedCustomerQuery);
            if (!validateAuthenticatedCustomerResponse.Authenticated)
            {
                foreach (var item in Errors.ErrorsToken.ErrorsAuthenticate(validateAuthenticatedCustomerResponse.AuthenticatedMessages))
                {
                    response.AddError(item);
                }

                response.Authenticated = validateAuthenticatedCustomerResponse.Authenticated;

                return validateAuthenticatedCustomerResponse;
            }

            response.Authenticated = validateAuthenticatedCustomerResponse.Authenticated;
            response.CustomerUser = validateAuthenticatedCustomerResponse.CustomerUser;

            return validateAuthenticatedCustomerResponse;
        }
    }
}