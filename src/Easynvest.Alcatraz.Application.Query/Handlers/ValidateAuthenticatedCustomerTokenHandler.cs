﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Exceptions;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Easynvest.Alcatraz.Application.Query.Handlers
{
    public class ValidateAuthenticatedCustomerTokenHandler : IRequestHandler<ValidateAuthenticatedCustomerTokenQuery, ValidateAuthenticatedCustomerTokenResponse>
    {
        private readonly ILogger<ValidateAuthenticatedCustomerTokenHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IBopeRepository _bopeRepository;
        private readonly IValidateTimeTokenRepository _validateTimeTokenRepository;

        public ValidateAuthenticatedCustomerTokenHandler(IMediator mediator, ILogger<ValidateAuthenticatedCustomerTokenHandler> logger,
            IBopeRepository bopeRepository, IValidateTimeTokenRepository validateTimeTokenRepository)
        {
            _mediator = mediator;
            _logger = logger;
            _bopeRepository = bopeRepository;
            _validateTimeTokenRepository = validateTimeTokenRepository;
        }

        public async Task<ValidateAuthenticatedCustomerTokenResponse> Handle(ValidateAuthenticatedCustomerTokenQuery request, CancellationToken cancellationToken)
        {
            var response = request.Response;

            try
            {
                _logger.LogExInformation($"[{{RequestId}}] - Iniciando o processo de autorização para a conta: {request.Username} e DeviceUid: {request.DeviceUid}", request.RequestId);

                var validateAuthenticatedCustomerResponse = await ValidateAuthenticatedCustomer(request, response);
                if (response.IsFailure)
                    return response;

                var tokenIsActive = await _bopeRepository.GetTokenInfo(request.RequestId, validateAuthenticatedCustomerResponse.CustomerUser.CustomerId);
                if (tokenIsActive == null)
                {
                    return response;
                }
                else if (!tokenIsActive.IsActive)
                {
                    _logger.LogExInformation($"[{{RequestId}}] - A conta {request.Username} não possui TOKEN ativo, processado login padrão.", request.RequestId);
                    return response;
                }

                var timeToken = await _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(request.DeviceUid, validateAuthenticatedCustomerResponse.CustomerUser.CustomerId);
                if (timeToken != null && timeToken.Valid())
                    return response;

                if (string.IsNullOrEmpty(request.OtpCode))
                {
                    _logger.LogExInformation($"[{{RequestId}}] - OTP CODE vazio para a conta: {request.Username} e DeviceUid: {request.DeviceUid}", request.RequestId);

                    response.AddError(Errors.ErrorsToken.NeedToken(
                        CreateInnerError(
                            validateAuthenticatedCustomerResponse.CustomerUser.CustomerId,
                            validateAuthenticatedCustomerResponse.CustomerUser.Name,
                            validateAuthenticatedCustomerResponse.CustomerUser.Email, true)));

                    return response;
                }

                var authenticateTransactionResponse = await AuthenticateTransaction(request, response, validateAuthenticatedCustomerResponse);
                if (authenticateTransactionResponse.IsFailure)
                {
                    _logger.LogExWarning($"[{{RequestId}}] - Falha ao autenticar TOKEN para a conta: {request.Username} e DeviceUid: {request.DeviceUid}", request.RequestId);
                    return response;
                }

                var registerTimeToken = new ValidateTimeToken(validateAuthenticatedCustomerResponse.CustomerUser.CustomerId, request.DeviceUid, request.ConfirmedTrustedDevice);
                if (timeToken != null)
                    await _validateTimeTokenRepository.UpdateTimeToken(registerTimeToken);
                else
                    await _validateTimeTokenRepository.RegisterTimeToken(registerTimeToken);
            }
            catch (BopeApiCallException)
            {
                _logger.LogExWarning($"[{{RequestId}}] Erro na chamada da API Bope no endpoint /v1/devices/:customerId com status do nível 500 para o usuário {request.Username}, login será realizado sem validação do MFA.",
                                        request.RequestId);
                return response;
            }
            catch (Exception ex)
            {
                response.AddError(Errors.General.InternalProcessError());
                _logger.LogExError(ex, "{@ErrorResponse}", response.ErrorResponse);
                _logger.LogExError(ex, $"Erro ao autorizar a conta: {request.Username}");
            }

            _logger.LogExInformation($"[{{RequestId}}] - Finalizando o processo de autorização para a conta: {request.Username} e DeviceUid: {request.DeviceUid}", request.RequestId);

            return response;
        }

        private async Task<ValidateAuthenticatedCustomerResponse> ValidateAuthenticatedCustomer(ValidateAuthenticatedCustomerTokenQuery request, ValidateAuthenticatedCustomerTokenResponse response)
        {
            var validateAuthenticatedCustomerQuery = new ValidateAuthenticatedCustomerQuery(request.Username, request.Password, request.FcmToken, request.IpAddress);
            var validateAuthenticatedCustomerResponse = await _mediator.Send(validateAuthenticatedCustomerQuery);
            if (!validateAuthenticatedCustomerResponse.Authenticated)
            {
                foreach (var item in Errors.ErrorsToken.ErrorsAuthenticate(validateAuthenticatedCustomerResponse.AuthenticatedMessages))
                {
                    response.AddError(item);
                }

                return validateAuthenticatedCustomerResponse;
            }

            response.CustomerUser = validateAuthenticatedCustomerResponse.CustomerUser;
            response.Authenticated = validateAuthenticatedCustomerResponse.Authenticated;

            return validateAuthenticatedCustomerResponse;
        }

        private async Task<Domain.Responses.Response<AuthenticateTransactionResponse>> AuthenticateTransaction(ValidateAuthenticatedCustomerTokenQuery request, ValidateAuthenticatedCustomerTokenResponse response, ValidateAuthenticatedCustomerResponse validateAuthenticatedCustomer)
        {
            var authenticateTransactionCommand = new AuthenticateTransactionCommand(request.RequestId, validateAuthenticatedCustomer.CustomerUser.Username, request.OtpCode, request.DeviceUid, request.Integration);
            var authenticateTransactionResponse = await _mediator.Send(authenticateTransactionCommand);
            if (authenticateTransactionResponse.IsFailure)
            {
                var innerError = CreateInnerError(validateAuthenticatedCustomer.CustomerUser.CustomerId, validateAuthenticatedCustomer.CustomerUser.Name, validateAuthenticatedCustomer.CustomerUser.Email, null);
                foreach (var item in Errors.ErrorsToken.ErrorsAuthenticate("AuthenticateTransaction", authenticateTransactionResponse.Messages.ToList(), innerError))
                {
                    response.AddError(item);
                }

                response.Authenticated = false;
            }

            return authenticateTransactionResponse;
        }

        private InnerErrorAuthenticateNeedToken CreateInnerError(string customerId, string name, string email, bool? needToken)
            => new InnerErrorAuthenticateNeedToken(customerId, name, email, needToken);
    }
}