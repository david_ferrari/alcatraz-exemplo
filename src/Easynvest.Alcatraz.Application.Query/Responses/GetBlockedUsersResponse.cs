﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Models.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Application.Query.Responses
{
    public class GetBlockedUsersResponse
    {
        public GetBlockedUsersResponse(IReadOnlyCollection<UserBlock> users)
        {
            Users = users.Select(u => new UserBlockResponse(u)).ToList().AsReadOnly();
        }

        public IReadOnlyCollection<UserBlockResponse> Users { get; set; }
    }
}
