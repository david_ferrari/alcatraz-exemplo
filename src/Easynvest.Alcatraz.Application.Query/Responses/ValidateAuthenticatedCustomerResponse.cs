﻿using Easynvest.Alcatraz.Domain.Dtos;
using MediatR;
using System.Collections.Generic;
using System.Linq;

namespace Easynvest.Alcatraz.Application.Query.Responses
{
    public class ValidateAuthenticatedCustomerResponse : IRequest<ValidateAuthenticatedCustomerResponse>
    {
        public ValidateAuthenticatedCustomerResponse()
        {

        }      

        public bool Authenticated
        {
            get
            {
                return !AuthenticatedMessages.Any();
            }
        }

        public CustomerUserDto CustomerUser { get; set; }
        public Dictionary<string, string> AuthenticatedMessages { get; set; } = new Dictionary<string, string>();

        public void AddAuthenticatedMessages(string key, string value)
        {
            AuthenticatedMessages.Add(key, value);
        }
    }
}
