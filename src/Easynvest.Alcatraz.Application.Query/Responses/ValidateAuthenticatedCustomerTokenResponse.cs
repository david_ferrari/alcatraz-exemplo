﻿using Easynvest.Alcatraz.Application.Responses.v3;
using Easynvest.Alcatraz.Domain.Dtos;

namespace Easynvest.Alcatraz.Application.Query.Responses
{
    public class ValidateAuthenticatedCustomerTokenResponse : Response
    {
        public ValidateAuthenticatedCustomerTokenResponse(string requestId)
            : base(requestId)
        {
        }

        public bool Authenticated { get; set; }

        public CustomerUserDto CustomerUser { get; set; }
    }
}