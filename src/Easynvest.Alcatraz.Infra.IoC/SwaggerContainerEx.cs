﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;

namespace Easynvest.Alcatraz.Infra.IoC
{
    public static class SwaggerContainerEx
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {           
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Easynvest.Alcatraz.Api",
                    Version = "v1",
                    Description = "Api de autenticação e gerenciamento de senhas",
                    Contact = new OpenApiContact { Url = new Uri("https://bitbucket.org/easynvest/easynvest.alcatraz") }
                });
                options.AddSecurityDefinition(
                    "bearer",
                    new OpenApiSecurityScheme
                    {
                        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
                        Description = "Favor gerar um token jwt para efetuar a requisição",
                        Name = "Authorization",
                        Type = Microsoft.OpenApi.Models.SecuritySchemeType.ApiKey
                    });
            });

            return services;
        }
    }
}