﻿using Easynvest.Alcatraz.Application.Services;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Infra.Data;
using Easynvest.Alcatraz.Infra.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.Alcatraz.Infra.IoC
{
    public static class RepositoryContainerEx
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services
                .AddSingleton<IRepository, Repository>()
                .AddTransient<ICustomerSecretRepository, CustomerSecretRepository>()
                .AddTransient<ICustomerRepository, CustomerRepository>()
                .AddTransient<IPasswordRepository, PasswordRepository>()
                .AddTransient<IHashPasswordRepository, HashPasswordRepository>()
                .AddTransient<IAuthenticateCustomerRepository, CustomerUserRepository>()
                .AddTransient<IAuthenticationRepository, AuthenticationRepository>()
                .AddTransient<IValidateTimeTokenRepository, ValidateTimeTokenRepository>()
                .AddTransient<IAuthCodeRepository, AuthCodeRepository>()
                .AddTransient<IUserBlockRepository, UserBlockRepository>()
                .AddTransient<IBopeRepository, BopeRepository>()
                .AddTransient<IPushNotificationService, PushNotificationService>();
        }
    }
}