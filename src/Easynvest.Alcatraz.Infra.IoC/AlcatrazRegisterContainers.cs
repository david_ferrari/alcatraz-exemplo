﻿using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.CrossCutting.Policies;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Serilog;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading;
using Polly.Extensions.Http;
using Polly.Registry;
using Polly.Timeout;
using Polly.Contrib.Simmy;
using Polly.Contrib.Simmy.Outcomes;
using System.Net;

namespace Easynvest.Alcatraz.Infra.IoC
{
    [ExcludeFromCodeCoverage]
    public static class AlcatrazRegisterContainers
    {
        public static IServiceCollection AddAlcatrazServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IPolicyRulesService, PolicyRulesService>();

            HttpPolicySettingsOptions options = new HttpPolicySettingsOptions();
            configuration.GetSection("HttpPolicySettings").Bind(options);


            services.RegisterPolicies(options);
            services.AddHttpClientBope(configuration, options);
            services.AddHttpClientAuth(configuration, options);

            services.RegisterHandlers();
            services.RegisterRepositories();

            return services;
        }

        private static void RegisterPolicies(this IServiceCollection services, HttpPolicySettingsOptions options)
        {
            var retryPolicy = HttpPolicyExtensions
                              .HandleTransientHttpError()
                              .Or<TimeoutRejectedException>()
                              .WaitAndRetryAsync(
                                  options.RetryCount,
                                  _ => TimeSpan.FromSeconds(options.DelayBetweenRetries),
                                  (exception, timeSpan, retries, context) =>
                                  {
                                      var msg = $"#Polly - Retentativa {retries} " +
                                                $"de {context.PolicyKey} " +
                                                $"em {context.OperationKey}, " +
                                                "devido: {@Exception}.";
                                      Log.Warning(msg, exception);
                                  });

            var timeoutPolicy = Policy.TimeoutAsync<HttpResponseMessage>(options.HttpRequestTimeoutInSeconds);
            var resiliencePolicy = retryPolicy.WrapAsync(timeoutPolicy);

            services.AddPolicyRegistry(new PolicyRegistry
            {
                {"ResiliencePolicy", resiliencePolicy},
            });
        }

        private static void AddHttpClientBope(this IServiceCollection services, IConfiguration configuration, HttpPolicySettingsOptions options)
        {
            var endpointBope = new BopeOptions();
            configuration.Bind("Bope", endpointBope);

            services
              .AddHttpClient("bope", bope =>
              {
                  bope.BaseAddress = new System.Uri(endpointBope.BaseUrl);
                  bope.Timeout = TimeSpan.FromSeconds(options.TotalResponseTimeoutInSeconds);
              })
              .AddPolicyHandlerFromRegistry("ResiliencePolicy")
              .AddTransientHttpErrorPolicy(p =>
                  p.CircuitBreakerAsync(
                      options.NumberOfExceptionsAllowedBeforeBreaking,
                      TimeSpan.FromSeconds(options.DurationOfBreakInSeconds),
                      OnBreak,
                      OnReset))
              .SetHandlerLifetime(Timeout.InfiniteTimeSpan);
        }

        private static void AddHttpClientAuth(this IServiceCollection services, IConfiguration configuration, HttpPolicySettingsOptions options)
        {
            var endpointAuth = new AuthOptions();
            configuration.Bind("Auth", endpointAuth);

            services
              .AddHttpClient("auth", auth =>
              {
                  auth.BaseAddress = new System.Uri(endpointAuth.BaseEndpoint);
              })
              .AddPolicyHandlerFromRegistry("ResiliencePolicy")
              .AddTransientHttpErrorPolicy(p =>
                  p.CircuitBreakerAsync(
                      options.NumberOfExceptionsAllowedBeforeBreaking,
                      TimeSpan.FromSeconds(options.DurationOfBreakInSeconds),
                      OnBreak,
                      OnReset))
              .SetHandlerLifetime(Timeout.InfiniteTimeSpan);
        }

        private static void OnBreak(DelegateResult<HttpResponseMessage> message, TimeSpan timespan)
        {
            Log.Warning(message.Exception, "Serviço fora do ar! {WaitTime}:", timespan.TotalSeconds);
        }

        private static void OnReset()
        {
            Log.Warning("Serviço voltou!");
        }
    }
}