﻿using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Infra.Data.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.IoC
{
    [ExcludeFromCodeCoverage]
    public static class ConfigureOptionsContainerEx
    {
        public static IServiceCollection AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .Configure<CustomerSecretOption>(opt => configuration.GetSection("PasswordConfigs").Bind(opt))
                .Configure<AuthenticationOption>(opt => configuration.GetSection("JWTAuthentication").Bind(opt))
                .Configure<EmailTemplateOption>(opt => configuration.GetSection("Mail").Bind(opt))
                .Configure<EmailMessaging>(opt => configuration.GetSection("EmailMessaging").Bind(opt))
                .Configure<AuthOptions>(opt => configuration.GetSection("Auth").Bind(opt))
                .Configure<LoginConfigurationOptions>(opt => configuration.GetSection("LoginConfiguration").Bind(opt))
                .Configure<BopeOptions>(opt => configuration.GetSection("Bope").Bind(opt))
                .Configure<HealthCheckEndpoint>(option => configuration.GetSection("HealthCheckEndpoint").Bind(option))
                .Configure<ConfigureHttpHealthCheck>(option => configuration.GetSection("ConfigureHttpHealthCheck").Bind(option))
                .Configure<ConfigureHttpHealthCheck>(option => configuration.GetSection("ConfigureHttpHealthCheck").Bind(option))
                .Configure<HealthCheckEndpoint>(option => configuration.GetSection("HealthCheckEndpoint").Bind(option))
                .Configure<WriteRedisOptions>(options => options.Configuration = configuration["RedisOptions:RedisConnectionString"])
                .Configure<ReadRedisOptions>(options => options.Configuration = configuration["RedisOptions:RedisConnectionString"])
                .Configure<AuthCodeOptions>(options => configuration.GetSection("AuthCode").Bind(options))
                .Configure<ConnectionStringOptions>(connectionStringOptions => connectionStringOptions.ConnectionString = configuration.GetConnectionString("OracleConnection"));

            return services;
        }
    }
}
