﻿using System;
using System.Net;
using System.Net.Http;
using Polly;
using Polly.Contrib.Simmy;
using Polly.Contrib.Simmy.Latency;
using Polly.Contrib.Simmy.Outcomes;
using Polly.Registry;

namespace Easynvest.Alcatraz.Infra.IoC
{
    public static class ChaosContainer
    {
        public static IPolicyRegistry<string> AddChaosInjectors(this IPolicyRegistry<string> registry)

        {
            foreach (var policyEntry in registry)

                if (policyEntry.Value is IAsyncPolicy<HttpResponseMessage> policy)

                    registry[policyEntry.Key] = policy
                                                .WrapAsync(MonkeyPolicy.InjectExceptionAsync(with =>
                                                    with.Fault<HttpResponseMessage>(
                                                            new HttpRequestException("Resource Temporarily Unavailable"))
                                                        .InjectionRate(0.05)
                                                        .Enabled(true)))
                                                .WrapAsync(MonkeyPolicy.InjectResultAsync<HttpResponseMessage>(with =>
                                                    with.Result(new HttpResponseMessage(HttpStatusCode.ServiceUnavailable))
                                                        .InjectionRate(0.05)
                                                        .Enabled(true)))
                                                .WrapAsync(MonkeyPolicy.InjectLatencyAsync<HttpResponseMessage>(with =>
                                                    with.Latency(TimeSpan.FromSeconds(12))
                                                        .InjectionRate(0.05)
                                                        .Enabled(true)));

            return registry;
        }
    }
}
