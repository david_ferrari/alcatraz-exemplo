﻿using Easynvest.KingsCross.Extensions;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Easynvest.Alcatraz.Infra.IoC
{
    public static class MassTransitContainer
    {
        public static IServiceCollection AddMassTransit(this IServiceCollection services)
        {
            services.AddMassTransit(configure =>
            {
                configure.AddBus(serviceProvider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var settings = serviceProvider.Container.GetRequiredService<IOptions<RabbitMQConnectionSetting>>().Value;
                    cfg.Host($"rabbitmq://{settings.Host}", h =>
                    {
                        h.Username(settings.UserName);
                        h.Password(settings.Password);
                    });
                }));
            });

            return services;
        }
    }
}
