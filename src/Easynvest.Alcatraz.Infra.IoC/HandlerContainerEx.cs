﻿using Easynvest.Alcatraz.Application.Handlers;
using Easynvest.Alcatraz.Application.Query.Handlers;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.Alcatraz.Infra.IoC
{
    public static class HandlerContainerEx
    {
        public static void RegisterHandlers(this IServiceCollection services)
        {
            services.AddMediatR(typeof(GetBlockedUsersHandler)); 
            services.AddMediatR(typeof(RefreshAuthenticatedCustomerHandler)); 
            services.AddMediatR(typeof(ValidateAuthenticatedCustomerHandler)); 
            services.AddMediatR(typeof(ValidateAuthenticatedCustomerTokenHandler)); 
            services.AddMediatR(typeof(ValidateAuthenticationCodeHandler)); 
            services.AddMediatR(typeof(AuthenticateTransactionCommandHandler)); 
            services.AddMediatR(typeof(AuthenticateWithChangedPasswordHandler)); 
            services.AddMediatR(typeof(BlockUserHandler));
            services.AddMediatR(typeof(ChangeForgottenElectronicSignatureHandler));
            services.AddMediatR(typeof(ChangeForgottenPasswordHandler));
            services.AddMediatR(typeof(ChangeLoggedElectronicSignatureHandler));
            services.AddMediatR(typeof(ChangeLoggedPasswordHandler));
            services.AddMediatR(typeof(CreateElectronicSignatureHandler));
            services.AddMediatR(typeof(CreatePasswordHandler));
            services.AddMediatR(typeof(CreateSecretHandler));
            services.AddMediatR(typeof(RequestHashElectronicSignatureRecoveryHandler));
            services.AddMediatR(typeof(RequestHashPasswordCreateHandler));
            services.AddMediatR(typeof(RequestHashPasswordRecoveryHandler));
            services.AddMediatR(typeof(SendAuthenticationCodeHandler));
            services.AddMediatR(typeof(SendEmailHandler));
            services.AddMediatR(typeof(UnblockUserHandler));
            services.AddMediatR(typeof(UpdateSecretVersionHandler));
            services.AddMediatR(typeof(ValidateElectronicSignatureHandler));
            services.AddMediatR(typeof(ValidateElectronicSignatureHandler));
            services.AddMediatR(typeof(ValidHashQueryHandler));
        }
    }
}