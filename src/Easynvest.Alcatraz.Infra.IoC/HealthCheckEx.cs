﻿using System;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.CrossCutting.Service;
using Easynvest.KingsCross.Extensions;
using MassTransit.Monitoring.Health;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Easynvest.Alcatraz.Infra.IoC
{
    public static class HealthCheckEx
    {
        public static IServiceCollection AddHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            var healthCheckConfiguration = new ConfigureHttpHealthCheck();
            configuration.GetSection("ConfigureHttpHealthCheck").Bind(healthCheckConfiguration);
            var oracleDbConnectionString = configuration.GetConnectionString("OracleConnection");
            var healthCheckEndpoints = new HealthCheckEndpoint();
            configuration.GetSection("HealthCheckEndpoint").Bind(healthCheckEndpoints);
            var rabbitMqOptions = new RabbitMQConnectionSetting();
            configuration.GetSection("RabbitMQConnectionSetting").Bind(rabbitMqOptions);
            var rabbitMqConnectionString =
                $"amqp://{rabbitMqOptions.UserName}:{rabbitMqOptions.Password}@{rabbitMqOptions.Host}:{rabbitMqOptions.Port}";

            services.AddSingleton(provider => new BusHealth("RabbitMQ"));

            services.AddHealthChecks()
                    .AddRabbitMQ(rabbitMqConnectionString, name: "RabbitMQ", failureStatus: HealthStatus.Unhealthy)
                    .AddOracle(oracleDbConnectionString, name: "Oracle", failureStatus: HealthStatus.Unhealthy)
                    .AddUrlGroup(name: "Bope", uri: new Uri(healthCheckEndpoints.Bope), failureStatus: HealthStatus.Unhealthy,
                        tags: new[] {healthCheckEndpoints.Bope}, timeout: healthCheckConfiguration.TimeoutPerRequest);

            return services;
        }

        public static IEndpointRouteBuilder UseHealthChecks(this IEndpointRouteBuilder endpoints)
        {
            endpoints.MapHealthChecks("/apis/health", new HealthCheckOptions
            {
                ResponseWriter = HealthCheckResponseWriterService.WriteResponse,
                ResultStatusCodes =
                {
                    [HealthStatus.Healthy] = StatusCodes.Status200OK,
                    [HealthStatus.Degraded] = StatusCodes.Status200OK,
                    [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
                }
            });

            endpoints.MapHealthChecks("/ready", new HealthCheckOptions
                {
                    Predicate = check => check.Name.Contains("Oracle")
                }
            );

            endpoints.MapGet("/ping", async context => await context.Response.WriteAsync("pong"));

            return endpoints;
        }
    }
}
