﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Application.Validators;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class AuthenticateWithChangedPasswordHandler : IRequestHandler<AuthenticateWithChangedPasswordCommand, Response<AuthenticateWithChangedPasswordResponse>>
    {
        private readonly ILogger<AuthenticateWithChangedPasswordHandler> _logger;
        private readonly IAuthenticationRepository _authenticateRepository;
        private readonly AuthenticateWithChangedPasswordValidator _validator;

        public AuthenticateWithChangedPasswordHandler(
            ILogger<AuthenticateWithChangedPasswordHandler> logger,
            IAuthenticationRepository authenticateRepository
        )
        {
            _logger = logger;
            _authenticateRepository = authenticateRepository;
            _validator = new AuthenticateWithChangedPasswordValidator();
        }

        public async Task<Response<AuthenticateWithChangedPasswordResponse>> Handle(AuthenticateWithChangedPasswordCommand request, CancellationToken cancellationToken)
        {
            _logger.LogExInformation($"[{request.ClientId}] Iniciando autenticação do usuário {request.UserName}");

            var validateCommand = ValidateCommand(request);
            if (validateCommand.Any())
            {
                _logger.LogExWarning($"[{request.ClientId}] {validateCommand}");
                return Response<AuthenticateWithChangedPasswordResponse>.Fail(validateCommand);
            }

            var authenticationResponse = await _authenticateRepository.Authenticate(request.UserName, request.Password, request.ClientId).ConfigureAwait(false);

            if (authenticationResponse == null)
            {
                _logger.LogExWarning($"[{request.ClientId}] Erro ao autenticar o usuario");
                return Response<AuthenticateWithChangedPasswordResponse>.Fail("Erro ao autenticar o usuario");
            }

            var response = new AuthenticateWithChangedPasswordResponse
            {
                RefreshToken = authenticationResponse.RefreshToken,
                AccessToken = authenticationResponse.AccessToken,
                TokenType = authenticationResponse.TokenType,
                ClientId = authenticationResponse.ClientId,
                Expires = authenticationResponse.Expires,
                Issued = authenticationResponse.Issued
            };

            _logger.LogExInformation($"[{request.ClientId}] Usuário autenticado");
            return Response<AuthenticateWithChangedPasswordResponse>.Ok(response);
        }

        private IEnumerable<string> ValidateCommand(AuthenticateWithChangedPasswordCommand request)
        {
            var errors = new List<string>();

            var result = _validator.Validate(request);
            if (!result.IsValid)
                errors.AddRange(result.Errors.Select(x => x.ErrorMessage));

            return errors;
        }
    }
}
