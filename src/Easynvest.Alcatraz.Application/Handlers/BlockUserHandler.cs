﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Messages.Events.Security.User;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class BlockUserHandler : IRequestHandler<BlockUserCommand, Domain.Responses.Response<BlockUserCommandResponse>>
    {
        private readonly IUserBlockRepository _repository;
        private readonly ILogger<BlockUserHandler> _logger;
        private readonly IBusControl _busControl;
        private readonly IAuthenticateCustomerRepository _customerRepository;

        public BlockUserHandler(IUserBlockRepository repository, IAuthenticateCustomerRepository customerRepository,
            IBusControl busControl, ILogger<BlockUserHandler> logger)
        {
            _repository = repository;
            _logger = logger;
            _busControl = busControl;
            _customerRepository = customerRepository;
        }

        public async Task<Domain.Responses.Response<BlockUserCommandResponse>> Handle(BlockUserCommand request, CancellationToken cancellationToken)
        {
            _logger.LogExInformation($"Bloqueando o usuário {request.CustomerId}");
            try
            {
                var customer = await _customerRepository.GetAuthenticatedCustomer(request.CustomerId, CustomerLoginType.CustomerLoginDocument);
                if (customer == null || !customer.Any())
                    return Domain.Responses.Response<BlockUserCommandResponse>.Fail(Errors.General.NotFound("Usuário").Message);

                var blocks = await _repository.Get(request.CustomerId);
                if (blocks != null && blocks.Any(b => b.BlockType == request.Type))
                    return Domain.Responses.Response<BlockUserCommandResponse>.Fail(Errors.BlockErrors.AlreadyBlocked().Message);

                var block = new UserBlock(request.CustomerId, request.Type);
                await _repository.Add(block);
                await _busControl.Publish<UserBlocked>(new
                {
                    block.CustomerId,
                    Date = block.BlockDate
                });
                _logger.LogExInformation($"Usuário {request.CustomerId} bloqueado com sucesso");
                return Domain.Responses.Response<BlockUserCommandResponse>.Ok(new BlockUserCommandResponse(block));
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao bloquear o usuário {request.CustomerId}");
                return Domain.Responses.Response<BlockUserCommandResponse>.Fail(Errors.General.InternalProcessError().Message);
            }
        }
    }
}
