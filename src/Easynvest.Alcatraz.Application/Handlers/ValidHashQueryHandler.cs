﻿using Easynvest.Alcatraz.Application.Queries;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class ValidHashQueryHandler : IRequestHandler<ValidHashQuery, Response<ValidHashQueryResponse>>
    {
        private const string GENERIC_VALIDATION_MESSAGE = "Hash expirado.";
        private readonly IHashPasswordRepository _passwordRecoveryRepository;
        private readonly ILogger<ValidHashQueryHandler> _logger;

        public ValidHashQueryHandler(IHashPasswordRepository passwordRecoveryRepository, ILogger<ValidHashQueryHandler> logger)
        {
            _passwordRecoveryRepository = passwordRecoveryRepository;
            _logger = logger;
        }

        public async Task<Response<ValidHashQueryResponse>> Handle(ValidHashQuery request, CancellationToken cancellationToken)
        {
            _logger.LogExInformation("Iniciando validação de Hash.");

            if (string.IsNullOrWhiteSpace(request.Hash))
            {
                _logger.LogExWarning("O hash não foi informado.");
                return Response<ValidHashQueryResponse>.Fail("O hash não foi informado.");
            }

            var entity = await _passwordRecoveryRepository.GetByHash(request.Hash);

            if (entity == null)
            {
                _logger.LogExInformation($"Hash não localizado. O seguinte hash não é válido: '{request.Hash}'.");
                return Response<ValidHashQueryResponse>.Fail(GENERIC_VALIDATION_MESSAGE);
            }

            if (entity.ExpirationDate.IsExpired)
            {
                _logger.LogExInformation($"Hash expirado. O seguinte hash esta expirado: '{request.Hash}'.");
                return Response<ValidHashQueryResponse>.Fail(GENERIC_VALIDATION_MESSAGE);
            }

            var response = new ValidHashQueryResponse()
            {
                CustomerId = entity.Customer.CustomerId
            };

            _logger.LogExInformation("Hash válido.");

            return Response<ValidHashQueryResponse>.Ok(response);
        }
    }
}
