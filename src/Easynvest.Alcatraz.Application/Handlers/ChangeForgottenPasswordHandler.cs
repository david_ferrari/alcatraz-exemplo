﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Application.Services;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using static Easynvest.Alcatraz.Domain.Enumerators.PushNotificationEnum;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class ChangeForgottenPasswordHandler : PasswordBaseHandler<ChangeForgottenPasswordCommand, ChangeForgottenPasswordResponse>
    {
        private readonly IPushNotificationService _pushNotification;

        public ChangeForgottenPasswordHandler(IMediator mediator,
                                              IPasswordRepository passwordRepository,
                                              IHashPasswordRepository passwordRecoveryRepository,
                                              IPushNotificationService pushNotification,
                                              IOptions<EmailTemplateOption> emailTemplateOption,
                                              ILogger<ChangeForgottenPasswordHandler> logger)
            : base(mediator, passwordRepository, passwordRecoveryRepository, emailTemplateOption, logger)
        {
            _pushNotification = pushNotification;
        }

        public override int EmailId => _emailTemplateOption.ChangedLoggedPassword;

        protected async override Task<Response<Password>> CreateDomain(ChangeForgottenPasswordCommand request)
        {
            var password = await _passwordRepository.GetByCustomerId(request.CustomerId);

            var result = password.TryChangeSecret(request.SecretCandidate, SecretType.Password, request.ChangeType).ToList();

            if (result.Any())
            {
                var errors = result.Select(x => x.Message);
                _logger.LogExWarning($"[{request.CustomerId}] {errors}");
                return Response<Password>.Fail(errors);
            }

            return Response<Password>.Ok(password);
        }

        protected async override Task PersistSecret(ChangeForgottenPasswordCommand request, Password password)
        {
            using (var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                var passwordRecovery = await _passwordRecoveryRepository.GetByHash(request.Hash);

                password.PasswordDetails.ResetLoginAttempts();
                await _passwordRepository.UpdateSecret(password);

                passwordRecovery.ExpireHash();
                await _passwordRecoveryRepository.UpdateExpirationDate(passwordRecovery);

                await _pushNotification.SendDataAsync(password.Customer, PushNotificationService.GetDataPush(PushDataType.ResetPassword));

                ts.Complete();
            }
        }
    }
}
