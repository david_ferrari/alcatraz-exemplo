﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Application.Validators;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Alcatraz.Domain.Responses;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class RequestHashPasswordRecoveryHandler : RequestBaseHashPasswordHandler<RequestHashPasswordRecoveryCommand, RequestHashPasswordRecoveryResponse>
    {
        private new readonly ILogger<RequestHashPasswordRecoveryHandler> _logger;
        private readonly ICustomerRepository _repositoryCustomer;
        private readonly AbstractValidator<RequestHashPasswordRecoveryCommand> _commandValidator;
        protected override string TemplateEmailKey => "Mail:EmailRecoveryId";
        protected override string LabelCreatedMsg => "Resgate de Senha";

        public RequestHashPasswordRecoveryHandler(IConfiguration configuration, IHashPasswordRepository repository,
                                                  ICustomerRepository repositoryCustomer, IMediator mediator,
                                                  ILogger<RequestHashPasswordRecoveryHandler> logger)
            : base(repository, logger, mediator, configuration)
        {
            _repositoryCustomer = repositoryCustomer;
            _logger = logger;
            _commandValidator = new RequestHashPasswordRecoveryCommandValidator();
        }

        protected async override Task<Response<Customer>> ValidateCustomerInfo(RequestHashPasswordRecoveryCommand request)
        {
            try
            {
                var customer = await _repositoryCustomer.GetCustomerInfoByCustomerId(request.CustomerId);

                if (customer == null)
                {
                    _logger.LogExWarning($"[{request.CustomerId}] Cliente não encontrado.");
                    return Response<Customer>.Fail("CustomerId - Cliente não encontrado.");
                }

                if (customer.Status.Equals("I"))
                {
                    _logger.LogExWarning($"[{request.CustomerId}] Cliente desativado");
                    return Response<Customer>.Fail(ErrorMessages.USER_AUTHENTICATION_ERROR_DISABLEDUSER);
                }

                if (!customer.CustomerId.Equals(request.CustomerId) || !customer.Email.Equals(request.Email, StringComparison.InvariantCultureIgnoreCase))
                {
                    _logger.LogExWarning($"[{request.CustomerId}] CustomerId-Email - Dados informados inválidos.");
                    return Response<Customer>.Fail("CustomerId-Email - Dados informados inválidos.");
                }

                _logger.LogExInformation($"[{request.CustomerId}] Dados do cliente válidos");
                return Response<Customer>.Ok(customer);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, ex.Message);
                throw;
            }
        }

        protected override Response ValidateCommand(RequestHashPasswordRecoveryCommand request)
        {
            var result = _commandValidator.Validate(request);

            if (!result.IsValid)
                return Response.Fail(result.Errors.Select(x => x.ErrorMessage));

            return Response.Ok();
        }
    }
}