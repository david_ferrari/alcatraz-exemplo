﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Resources;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class AddTrustedDeviceHandler : IRequestHandler<AddTrustedDeviceCommand, AddTrustedDeviceResponse>
    {
        private readonly IValidateTimeTokenRepository _timeTokenRepository;
        private readonly ILogger<AddTrustedDeviceHandler> _logger;

        public AddTrustedDeviceHandler(IValidateTimeTokenRepository timeTokenRepository, ILogger<AddTrustedDeviceHandler> logger)
        {
            _timeTokenRepository = timeTokenRepository;
            _logger = logger;
        }

        public async Task<AddTrustedDeviceResponse> Handle(AddTrustedDeviceCommand request, CancellationToken cancellationToken)
        {
            var response = request.Response;
            try
            {
                var token = await _timeTokenRepository.GetValidateTimeTokenByDeviceUid(request.DeviceUid?.ToUpper(), request.CustomerId);
                if (token == null || !token.Valid())
                {
                    response.AddError(Errors.ErrorsToken.DeviceNotFound());
                    return response;
                }

                token.TrustDevice();
                await _timeTokenRepository.UpdateTimeToken(token);

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao cadastrar device como confiável para o usuario {request.CustomerId} e device {request.DeviceUid}");
                response.AddError(Errors.General.InternalProcessError());
                return response;
            }
        }
    }
}
