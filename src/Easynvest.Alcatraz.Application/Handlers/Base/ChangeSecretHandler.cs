﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Commands.Base;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Notificator.Notifications;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Easynvest.Alcatraz.Application.Handlers.Base
{
    public abstract class ChangeSecretHandler<TRequest, TResponse> : IRequestHandler<TRequest, Response<TResponse>>
			where TRequest : ChangeSecretCommand<TResponse>
			where TResponse : ChangeLoggedPasswordResponse
	{		
		protected readonly IMediator _mediator;
		protected readonly IPasswordRepository _passwordRepository;
		protected readonly EmailTemplateOption _emailTemplateOption;
		protected readonly ILogger<ChangeSecretHandler<TRequest, TResponse>> _logger;

		protected ChangeSecretHandler(IMediator mediator, IPasswordRepository passwordRepository, IOptions<EmailTemplateOption> templateOption, ILogger<ChangeSecretHandler<TRequest, TResponse>> logger)
		{
			_mediator = mediator;
			_passwordRepository = passwordRepository;
			_emailTemplateOption = templateOption.Value;
			_logger = logger;
		}

		public abstract Task UpdateSecret(TRequest request, Password password);
		public abstract Task<Response<Password>> CreateDomain(TRequest request);
		public abstract int EmailId { get; }

		public async Task<Response<TResponse>> Handle(TRequest request, CancellationToken cancellationToken)
		{
			_logger.LogExDebug("[{CustomerId}] Iniciando processo de alteração de {RequestMessage}", request.CustomerId, request.ToString());

			var requestValidation = await ValidateRequest(request).ConfigureAwait(false);

			if (requestValidation.Any())
			{
				_logger.LogExWarning("[{CustomerId}] {MessageError}", request.CustomerId, requestValidation.Select(x => x.Message));
				return Response<TResponse>.Fail(requestValidation.Select(x => x.Message));
			}

			var responseDomain = await CreateDomain(request);
			if (responseDomain.IsFailure)
			{
				_logger.LogExWarning("[{CustomerId}] {MessageError}", request.CustomerId, responseDomain.Message);
				return Response<TResponse>.Fail(responseDomain.Messages);
			}

			var password = responseDomain.Value;
			using (var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
			{
				await UpdateSecret(request, password);

				var responseEmail = await _mediator.Send(new SendEmailCommand(EmailId, 
					GetEmailParams(password.Customer?.Name?.FirstName), 
					password.Customer.Email, 
					$"Alteração de {request.SecretType.GetDescription()}"));

				if (responseEmail.IsFailure)
				{
					_logger.LogExError(string.Format(ErrorMessages.ALTER_SECRET_EMAIL_ERROR, request.SecretType.GetDescription(), string.Join(",", responseEmail.Messages)));
					return Response<TResponse>.Fail(string.Format(ErrorMessages.ALTER_SECRET_EMAIL_ERROR, request.SecretType.GetDescription(), "Operação não concluida"));
				}

				ts.Complete();
			}

			_logger.LogExDebug("[{CustomerId}] Finalizado processo de alteração de {RequestMessage}", request.CustomerId, request.ToString());
			return Response<TResponse>.Ok(default(TResponse));
		}

		protected async Task<IEnumerable<Notification>> ValidateRequest(TRequest request)
		{
			if (request == default(TRequest) || string.IsNullOrEmpty(request.NewSecret) || string.IsNullOrEmpty(request.ConfirmSecret) || string.IsNullOrEmpty(request.ElectronicSignature))
				return new[] { new Notification("Request", ErrorMessages.ALTER_SECRET_INVALID_REQUEST) };

			if (!request.NewSecret.Equals(request.ConfirmSecret))
				return new[] { new Notification(request.SecretType.GetDescription(), ErrorMessages.ALTER_SECRET_NOT_EQUAL) };

			var command = new ValidateElectronicSignatureCommand(request.ElectronicSignature, request.CustomerId);

			var validateSignature = await _mediator
				.Send(command)
				.ConfigureAwait(false);

			return validateSignature
				.Messages
				.Select(m => new Notification(SecretType.EletronicSignature.GetDescription(), m));
		}

		private Dictionary<string, string> GetEmailParams(string firstName)
		{
			var date = DateTime.Now;

			return new Dictionary<string, string>()
						{
								{ "[|NOME|]", firstName },
								{ "[|DATA|]", date.ToString("dd/MM/yyyy") },
								{ "[|HORA|]", date.ToString("HH:mm:ss") }
						};
		}
	}
}
