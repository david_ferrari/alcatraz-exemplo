﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Queries;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public abstract class PasswordBaseHandler<TRequest, TValue> : IRequestHandler<TRequest, Response<TValue>>
        where TRequest : PasswordBaseCommand<TValue, Response<TValue>>
        where TValue : PasswordBaseResponse
    {
        protected readonly IMediator _mediator;
        protected readonly IPasswordRepository _passwordRepository;
        protected readonly IHashPasswordRepository _passwordRecoveryRepository;
        protected readonly ILogger<PasswordBaseHandler<TRequest, TValue>> _logger;
        protected readonly EmailTemplateOption _emailTemplateOption;

        protected PasswordBaseHandler(IMediator mediator, 
                                      IPasswordRepository passwordRepository, 
                                      IHashPasswordRepository passwordRecoveryRepository,
                                      IOptions<EmailTemplateOption> emailTemplateOption,
                                      ILogger<PasswordBaseHandler<TRequest, TValue>> logger                                      )
        {
            _mediator = mediator;
            _passwordRepository = passwordRepository;
            _passwordRecoveryRepository = passwordRecoveryRepository;
            _logger = logger;
            _emailTemplateOption = emailTemplateOption.Value;
        }

        protected abstract Task PersistSecret(TRequest request, Password password);
        protected abstract Task<Response<Password>> CreateDomain(TRequest request);
        public abstract int EmailId { get; }

        public async Task<Response<TValue>> Handle(TRequest request, CancellationToken cancellationToken)
        {
            var validateHash = await ValidateHash(request);

            if (validateHash.IsFailure)
            {
                _logger.LogExError(string.Format(ErrorMessages.ALTER_SECRET_INVALID, request.SecretType.GetDescription(), string.Join(",", validateHash.Messages)));
                return Response<TValue>.Fail(validateHash.Messages);
            }
            
            request.SetCustomerId(validateHash.Value.CustomerId);

            var createDomain = await CreateDomain(request);
            if (createDomain.IsFailure)
            {
                _logger.LogExError(string.Format(ErrorMessages.ALTER_SECRET_INVALID, request.SecretType.GetDescription(), string.Join(",", createDomain.Messages)));
                return Response<TValue>.Fail(createDomain.Messages);
            }

            var password = createDomain.Value;
            await PersistSecret(request, password);

            var responseEmail = await _mediator.Send(new SendEmailCommand(EmailId, 
                GetEmailParams(request, password.Customer?.Name?.FirstName), 
                password.Customer.Email, 
                $"Alteração de {request.SecretType.GetDescription()}"));

            if (responseEmail.IsFailure)
            {
                _logger.LogExError(string.Format(ErrorMessages.ALTER_SECRET_EMAIL_ERROR, request.SecretType.GetDescription(), string.Join(",", responseEmail.Messages)));
                return Response<TValue>.Fail(string.Format(ErrorMessages.ALTER_SECRET_EMAIL_ERROR, request.SecretType.GetDescription(), "Operação não concluida"));
            }

            return Response<TValue>.Ok(null);
        }

        private async Task<Response<ValidHashQueryResponse>> ValidateHash(TRequest request)
        {
            var hashResponse = await _mediator.Send(new ValidHashQuery(request.Hash));
            if (hashResponse.IsFailure)
                return Response<ValidHashQueryResponse>.Fail(hashResponse.Messages);

            return Response<ValidHashQueryResponse>.Ok(hashResponse.Value);
        }

        private Dictionary<string, string> GetEmailParams(TRequest command, string firstName)
        {
            var date = DateTime.Now;

            return new Dictionary<string, string>()
            {
                { "[|NOME|]", firstName },
                { "[|IP|]", command.IpAddress },
                { "[|DATA|]", date.ToString("dd/MM/yyyy") },
                { "[|HORA|]", date.ToString("HH:mm:ss") }
            };
        }

    }
}
