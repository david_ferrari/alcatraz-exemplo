﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public abstract class RequestBaseHashPasswordHandler<TRequest, TValue> : IRequestHandler<TRequest, Response<TValue>>
        where TRequest : RequestBaseHashSecretCommand<TValue, Response<TValue>>
        where TValue : RequestBaseHashPasswordResponse
    {
        private readonly IConfiguration _configuration;
        private readonly IHashPasswordRepository _hashPasswordRepository;
        protected readonly ILogger _logger;
        private readonly IMediator _mediator;

        protected RequestBaseHashPasswordHandler(
            IHashPasswordRepository passwordRecoveryRepository,
            ILogger logger,
            IMediator mediator,
            IConfiguration configuration)
        {
            _logger = logger;
            _hashPasswordRepository = passwordRecoveryRepository;
            _mediator = mediator;
            _configuration = configuration;
        }

        protected abstract string LabelCreatedMsg { get; }
        protected abstract string TemplateEmailKey { get; }

        public async Task<Response<TValue>> Handle(TRequest request, CancellationToken cancellationToken)
        {
            var validateCommand = ValidateCommand(request);
            if (validateCommand.IsFailure)
                return Response<TValue>.Fail(validateCommand.Messages);

            var validateCustomerInfo = await ValidateCustomerInfo(request);
            if (validateCustomerInfo.IsFailure)
                return Response<TValue>.Fail(validateCustomerInfo.Messages);

            var adapterCommandToEntity = AdapterCommandToEntity(request, validateCustomerInfo.Value);
            if (adapterCommandToEntity.IsFailure)
                return Response<TValue>.Fail(adapterCommandToEntity.Messages);

            using (var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                var persistSecret = await PersistSecret(adapterCommandToEntity.Value);
                if (persistSecret.IsFailure)
                    return Response<TValue>.Fail(persistSecret.Messages);

                var sendEmail = await SendMailAsync(adapterCommandToEntity.Value);
                if (sendEmail.IsFailure)
                    return Response<TValue>.Fail(sendEmail.Messages);

                ts.Complete();
            }

            return Response<TValue>.Ok();
        }

        protected abstract Response ValidateCommand(TRequest request);

        protected abstract Task<Response<Customer>> ValidateCustomerInfo(TRequest request);

        private Response<PasswordRecoveryRequest> AdapterCommandToEntity(TRequest request, Customer customer)
        {
            if (!int.TryParse(_configuration.GetSection("PasswordConfigs")["ExpirationTimeInDay"], out int expirationTimeInDay))
                return Response<PasswordRecoveryRequest>.Fail("ExpirationTimeInDay - ExpirationTimeInDay not found.");

            var hashPasswordRequest = PasswordRecoveryRequest.Create(request.Email, customer, new ExpirationDate(expirationTimeInDay));
            return Response<PasswordRecoveryRequest>.Ok(hashPasswordRequest);
        }

        private async Task<Response> PersistSecret(PasswordRecoveryRequest hashPasswordRequest)
        {
            try
            {
                await _hashPasswordRepository.Create(hashPasswordRequest);
                return Response.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, ex.Message);
                return Response.Fail("Falha ao criar senha.");
            }
        }

        private async Task<Response> SendMailAsync(PasswordRecoveryRequest hashPasswordRequest)
        {
            try
            {
                if (!int.TryParse(_configuration.GetSection(TemplateEmailKey).Value, out int emailId))
                    return Response.Fail("EmailIdNotFound - EmailId not found.");

                var dic = new Dictionary<string, string>
                {
                    { "[|NOME|]", hashPasswordRequest.Customer.Name.FirstName },
                    { "[|LINK|]", hashPasswordRequest.Hash }
                };

                var response = await _mediator.Send(new SendEmailCommand(emailId, dic, hashPasswordRequest.Email, LabelCreatedMsg)).ConfigureAwait(false);
                if (response.IsFailure)
                    return Response.Fail(response.Messages);

                return Response.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, ex.Message);
                throw;
            }
        }
    }
}
