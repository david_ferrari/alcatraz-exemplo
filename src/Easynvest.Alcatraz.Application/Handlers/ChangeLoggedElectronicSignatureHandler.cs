﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Handlers.Base;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Application.Services;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;
using static Easynvest.Alcatraz.Domain.Enumerators.PushNotificationEnum;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class ChangeLoggedElectronicSignatureHandler : ChangeSecretHandler<ChangeElectronicSignatureCommand, ChangeLoggedPasswordResponse>
    {
        private readonly IPushNotificationService _pushNotification;

        public ChangeLoggedElectronicSignatureHandler(IMediator mediator,
                                                    IPasswordRepository passwordRepository,
                                                    IOptions<EmailTemplateOption> connectionOption,
                                                    ILogger<ChangeLoggedElectronicSignatureHandler> logger,
                                                    IPushNotificationService pushNotification)
                : base(mediator, passwordRepository, connectionOption, logger)
        {
            _pushNotification = pushNotification;
        }

        public override int EmailId { get => _emailTemplateOption.ChangedLoggedElectronicSignature; }

        public override async Task<Response<Password>> CreateDomain(ChangeElectronicSignatureCommand request)
        {
            var password = await _passwordRepository.GetByCustomerId(request.CustomerId);

            var result = password.TryChangeSecret(request.NewSecret, request.ElectronicSignature, SecretType.EletronicSignature,
                SecretChangeType.ChangeSecret).ToList();

            if (result.Any())
            {
                var errors = result.Select(x => x.Message);
                _logger.LogExWarning($"[{request.CustomerId}] {errors}");
                return Response<Password>.Fail(errors);
            }

            return Response<Password>.Ok(password);
        }

        public override async Task UpdateSecret(ChangeElectronicSignatureCommand request, Password password)
        {
            password.PasswordDetails.ResetSignatureAttempts();

            await _passwordRepository.UpdateSecret(password);

            await _pushNotification.SendDataAsync(password.Customer,
                PushNotificationService.GetDataPush(PushDataType.ResetEletronicSignature));
        }
    }
}
