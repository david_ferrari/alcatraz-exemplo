﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class CreateElectronicSignatureHandler : PasswordBaseHandler<CreateElectronicSignatureCommand, CreateElectronicSignatureResponse>
    {
        public CreateElectronicSignatureHandler(
            IMediator mediator,
            IPasswordRepository passwordRepository,
            IHashPasswordRepository passwordRecoveryRepository,
            IOptions<EmailTemplateOption> emailTemplateOption,
            ILogger<CreateElectronicSignatureHandler> logger)
            : base(mediator, passwordRepository, passwordRecoveryRepository, emailTemplateOption, logger)
        {

        }

        public override int EmailId => _emailTemplateOption.ChangedLoggedElectronicSignature;

        protected async override Task<Response<Password>> CreateDomain(CreateElectronicSignatureCommand request)
        {
            var password = await _passwordRepository.GetByCustomerId(request.CustomerId);
            var result = password.TryChangeSecret(request.SecretCandidate, SecretType.EletronicSignature, SecretChangeType.NewSecret);

            if (result.Any())
            {
                var errors = result.Select(x => x.Message);
                _logger.LogExWarning($"[{request.CustomerId}] {errors}");
                return Response<Password>.Fail(errors);
            }

            return Response<Password>.Ok(password);
        }

        protected async override Task PersistSecret(CreateElectronicSignatureCommand request, Password password)
        {
            using (var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                var secretRecovery = await _passwordRecoveryRepository.GetByHash(request.Hash);
                await _passwordRepository.UpdateSecret(password);

                secretRecovery.ExpireHash();
                await _passwordRecoveryRepository.UpdateExpirationDate(secretRecovery);

                ts.Complete();
            }
        }
    }
}
