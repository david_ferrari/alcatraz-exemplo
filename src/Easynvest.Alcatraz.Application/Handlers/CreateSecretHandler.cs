﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class CreateSecretHandler : IRequestHandler<CreateSecretCommand, Response>
    {
        private readonly IPasswordRepository _passwordRepository;
        private readonly ILogger<CreateSecretHandler> _logger;

        public CreateSecretHandler(IPasswordRepository passwordRepository, ILogger<CreateSecretHandler> logger)
        {
            _passwordRepository = passwordRepository;
            _logger = logger;
        }

        public async Task<Response> Handle(CreateSecretCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogExInformation($"Criando segredo para o cliente {request.CustomerId}");

                bool newPassword = request.Type == SecretType.Password && request.ChangeType == SecretChangeType.NewSecret;

                Password password;
                if (newPassword)
                {
                    var passwordHistory = new List<PasswordHistory>();
                    var electronicSignatureHistory = new List<PasswordHistory>();


                    password = new Password(new Customer(request.CustomerId, null, null, null), passwordHistory,
                        electronicSignatureHistory, null);
                }
                else
                    password = await _passwordRepository.GetByCustomerId(request.CustomerId);

                if (password.PasswordDetails == null)
                {
                    newPassword = true;
                    var encodedPassword = EncodePasswords.EncodeString(request.SecretCandidate);
                    var passwordDetails = new PasswordDetails
                       (
                           request.Type == SecretType.Password ? encodedPassword : string.Empty,
                           request.Type == SecretType.EletronicSignature ? encodedPassword : string.Empty,
                           0,
                           0,
                           null,
                           null
                       );
                    password.PasswordDetails = passwordDetails;
                }

                var notifications = password.TryChangeSecret(request.SecretCandidate, request.Type, request.ChangeType);
                if (notifications != null && notifications.Any())
                {
                    _logger.LogExError($"Erro de validação ao criar o segredo para o cliente {request.CustomerId}", notifications, request);
                    return Response.Fail(notifications.Select(n => n.Message));
                }

                using var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled);

                if (newPassword)
                    await _passwordRepository.InsertPassword(password);
                else
                    await _passwordRepository.UpdateSecret(password);

                ts.Complete();

                _logger.LogExInformation($"Segredo criado com sucesso para o cliente {request.CustomerId}");

                return Response.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao criar o segredo para o cliente {request.CustomerId}: Exception: {ex.Message}: Stack: {ex.StackTrace ?? string.Empty}", request);
                return Response.Fail("Erro ao criar o segredo para o cliente");
            }
        }
    }
}
