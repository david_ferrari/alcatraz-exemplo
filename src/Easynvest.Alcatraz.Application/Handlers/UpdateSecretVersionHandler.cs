﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class UpdateSecretVersionHandler : IRequestHandler<UpdateSecretVersionCommand, Response<bool>>
    {
        private readonly IPasswordRepository _repository;
        private readonly ILogger<UpdateSecretVersionHandler> _logger;

        public UpdateSecretVersionHandler(IPasswordRepository passwordRepository, ILogger<UpdateSecretVersionHandler> logger)
        {
            _repository = passwordRepository;
            _logger = logger;
        }

        public async Task<Response<bool>> Handle(UpdateSecretVersionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var password = await _repository.GetByCustomerId(request.CustomerId);
                var result = password.TryChangeSecret(request.Password, request.SecretType, SecretChangeType.ChangeSecret, true, validate: false);

                if (result.Any())
                {
                    _logger.LogExError($"Erro ao atualizar a versão do usuário. Usuario: {request.CustomerId} SecretType: {request.SecretType.ToString()}", result);
                    return Response<bool>.Fail(result.Select(r => r.Message));
                }

                using (var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
                {
                    await _repository.UpdateSecret(password);
                    ts.Complete();
                }

                return Response<bool>.Ok(true);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao atualizar a versão do usuário. Usuario: {request.CustomerId} SecretType: {request.SecretType.ToString()}");
                return Response<bool>.Fail("Erro ao atualizar a versão do usuário");
            }
        }
    }
}
