﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class SendAuthenticationCodeHandler : IRequestHandler<SendAuthenticationCodeCommand, SendAuthenticationCodeResponse>
    {
        private CustomerLoginType _customerLoginType;
        private CustomerUserDto _customerUser;
        private readonly LoginConfigurationOptions _loginConfigurationOptions;
        private readonly ILogger<SendAuthenticationCodeHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IAuthCodeRepository _codeRepository;
        private readonly IAuthenticateCustomerRepository _authenticateCustomerRepository;
        private readonly AuthCodeOptions _codeOptions;

        public SendAuthenticationCodeHandler(IMediator mediator, IAuthCodeRepository codeRepository, IAuthenticateCustomerRepository authenticateCustomerRepository,
           IOptions<AuthCodeOptions> codeOptions, ILogger<SendAuthenticationCodeHandler> logger, IOptions<LoginConfigurationOptions> loginConfigurationOptions)
        {
            _mediator = mediator;
            _logger = logger;
            _codeRepository = codeRepository;
            _codeOptions = codeOptions.Value;
            _loginConfigurationOptions = loginConfigurationOptions.Value;
            _authenticateCustomerRepository = authenticateCustomerRepository;
        }

        public async Task<SendAuthenticationCodeResponse> Handle(SendAuthenticationCodeCommand request, CancellationToken cancellationToken)
        {
            _logger.LogExInformation($"[{request.RequestId}] - Iniciando processo de envio de código temporário para o usuário: {request.Username}");

            var response = request.Response;

            try
            {
                if (string.IsNullOrEmpty(request.Username?.Trim()))
                {
                    _logger.LogExWarning($"[{request.RequestId}] - Usuário vazio");
                    response.AddError(Errors.ErrorsToken.InvalidUsername());
                    return response;
                }

                var result = await ValidateAuthenticatedCustomer(request, response);
                if (result.IsFailure)
                    return result;

                _logger.LogExInformation($"[{request.RequestId}] - Registrando código temporário no banco de dados para o usuário: {request.Username}");

                var authCode = new AuthCode(_customerUser.CustomerId, _codeOptions.ExpirationInMinutes);
                await _codeRepository.InsertCode(authCode);

                _logger.LogExInformation($"[{request.RequestId}] - Registrado código temporário no banco de dados para o usuário: {request.Username}");

                var dic = new Dictionary<string, string>
                {
                    { "[|NOME|]", _customerUser.Name },
                    { "[|CODIGO|]", authCode.Code }
                };

                _logger.LogExInformation($"[{request.RequestId}] - Enviando o código temporário por e-mail para o usuário: {request.Username}");

                await _mediator.Send(new SendEmailCommand(_codeOptions.EmailId, dic, _customerUser.Email, null));
            }
            catch (Exception ex)
            {
                response.AddError(Errors.General.InternalProcessError());

                _logger.LogExError(ex, "{@ErrorResponse}", response.ErrorResponse);
                _logger.LogExError(ex, $"Erro ao autorizar a conta: {request.Username}");
            }

            _logger.LogExInformation($"[{request.RequestId}] - Finalizando processo de envio de código temporário para o usuário: {request.Username}");

            return response;
        }

        private async Task<SendAuthenticationCodeResponse> ValidateAuthenticatedCustomer(SendAuthenticationCodeCommand request, SendAuthenticationCodeResponse response)
            => await GetCustomerLoginType(request, response, () => GetAuthenticatedCustomer(request, response));

        private async Task<SendAuthenticationCodeResponse> GetCustomerLoginType(SendAuthenticationCodeCommand request, SendAuthenticationCodeResponse response, Func<Task<SendAuthenticationCodeResponse>> next)
        {
            var match = CustomerLoginPatternValidator.Match(request.Username);

            if (match.IsFailure)
            {
                _logger.LogExWarning($"{ValidateAuthenticateType.Err_AuthenticateMode_Not_Found.Value} - {ValidateAuthenticateType.Err_AuthenticateMode_Not_Found.Display}");
                response.AddError(new Ops.Error(ValidateAuthenticateType.InvalidCredential.Value, ValidateAuthenticateType.InvalidCredential.Display));

                return response;
            }
            else
            {
                _customerLoginType = match.Value;

                if (IsEmailLoginType() && IsEmailLoginDisabled())
                {
                    var message = ValidateAuthenticateType.InvalidCredential.Display;

                    _logger.LogExWarning($"{ValidateAuthenticateType.Err_AuthenticateMode_Not_Found.Value} - {message}. Login por e-mail desabilitado.");

                    response.AddError(new Ops.Error(ValidateAuthenticateType.InvalidCredential.Value, message));
                }

                _logger.LogExInformation($"Usuário {request} Login do tipo: {_customerLoginType.Display}");
            }

            if (response.IsFailure)
                return response;

            return await next.Invoke();
        }

        private async Task<SendAuthenticationCodeResponse> GetAuthenticatedCustomer(SendAuthenticationCodeCommand request, SendAuthenticationCodeResponse response)
        {
            try
            {
                var customersUser = await _authenticateCustomerRepository.GetAuthenticatedCustomer(request.Username, _customerLoginType);

                if (customersUser.Count() == 1)
                {
                    _customerUser = customersUser.Single();
                }
                else
                {
                    var msg = customersUser.Any()
                    ? $"Múltiplos usuários localizados para {request}. {_customerLoginType.Display}"
                    : $"Usuário {request} não localizado. {_customerLoginType.Display}";

                    _logger.LogExWarning(msg);
                    response.AddError(new Ops.Error(ValidateAuthenticateType.InvalidCredential.Value, ValidateAuthenticateType.InvalidCredential.Display));
                }
            }
            catch (Exception ex)
            {
                var msgError = $"Falha ao recuperar o usuário: {request} - {_customerLoginType?.Display}";
                _logger.LogExError(ex, msgError);
                response.AddError(Errors.ErrorsToken.FailRecoverUser());
            }

            if (response.IsFailure)
                return response;

            return response;
        }

        private bool IsEmailLoginType() => _customerLoginType.Value.Equals(CustomerLoginType.CustomerLoginEmail.Value);

        private bool IsEmailLoginDisabled() => _loginConfigurationOptions.EmailLoginDisabled;
    }
}