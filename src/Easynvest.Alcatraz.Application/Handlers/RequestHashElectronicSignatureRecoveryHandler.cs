﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class RequestHashElectronicSignatureRecoveryHandler
        : RequestBaseHashPasswordHandler<RequestHashElectronicSignatureRecoveryCommand, RequestHashElectronicSignatureRecoveryResponse>
    {
        private readonly ICustomerRepository _repositoryCustomer;

        public RequestHashElectronicSignatureRecoveryHandler(
            IHashPasswordRepository passwordRecoveryRepository,
            ILogger<RequestHashElectronicSignatureRecoveryHandler> logger,
            IConfiguration configuration,
            IMediator mediator,
            ICustomerRepository repositoryCustomer)
            : base(passwordRecoveryRepository, logger, mediator, configuration)
        {
            _repositoryCustomer = repositoryCustomer;
        }
        protected override string LabelCreatedMsg => "Resgate de Assinatura Eletrônica";
        protected override string TemplateEmailKey => "Mail:EmailRequestRecoveryEletronicSignatureId";
        protected override Response ValidateCommand(RequestHashElectronicSignatureRecoveryCommand request) => Response.Ok();

        protected override async Task<Response<Customer>> ValidateCustomerInfo(RequestHashElectronicSignatureRecoveryCommand request)
        {
            var customer = await _repositoryCustomer.GetCustomerInfoByCustomerId(request.CustomerId);

            if (customer == null)
            {
                _logger.LogExWarning($"[{request.CustomerId}] Cliente não encontrado.");
                return Response<Customer>.Fail("CustomerId - Cliente não encontrado.");
            }

            if (!customer.CustomerId.Equals(request.CustomerId) || !customer.Email.Equals(request.Email, StringComparison.InvariantCultureIgnoreCase))
            {
                _logger.LogExWarning($"[{request.CustomerId}] CustomerId-Email - Dados informados inválidos.");
                return Response<Customer>.Fail("CustomerId-Email - Dados informados inválidos.");
            }

            _logger.LogExInformation($"[{request.CustomerId}] Dados do cliente válidos");
            return Response<Customer>.Ok(customer);
        }

    }
}
