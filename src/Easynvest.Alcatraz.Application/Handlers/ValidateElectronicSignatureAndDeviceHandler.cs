﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Resources;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Ops;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class ValidateElectronicSignatureAndDeviceHandler : IRequestHandler<ValidateElectronicSignatureAndDeviceCommand, ValidateElectronicSignatureAndDeviceResponse>
    {
        private readonly ILogger<ValidateElectronicSignatureAndDeviceHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IBopeRepository _bopeRepository;
        private readonly IValidateTimeTokenRepository _validateTimeTokenRepository;

        public ValidateElectronicSignatureAndDeviceHandler
            (
                IMediator mediator,
                IBopeRepository bopeRepository,
                IValidateTimeTokenRepository validateTimeTokenRepository,
                ILogger<ValidateElectronicSignatureAndDeviceHandler> logger
            )
        {
            _mediator = mediator;
            _bopeRepository = bopeRepository;
            _validateTimeTokenRepository = validateTimeTokenRepository;
            _logger = logger;
        }

        public async Task<ValidateElectronicSignatureAndDeviceResponse> Handle(ValidateElectronicSignatureAndDeviceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var response = request.Response;

                _logger.LogExDebug($"Validando assinatura eletrônica para o cliente {request.CustomerId}.");
                var eletronicSignatureValidationResponse = await _mediator.Send(new ValidateElectronicSignatureCommand(request.ElectronicSignature, request.CustomerId), cancellationToken);

                if (eletronicSignatureValidationResponse.IsFailure)
                {
                    _logger.LogExWarning($"[{request.CustomerId}] {eletronicSignatureValidationResponse.Message}");
                    response.AddError(new Error("ELETRONIC_SIGNATURE_ERRORS", eletronicSignatureValidationResponse.Message));
                    return response;
                }

                if (!string.IsNullOrEmpty(request?.DeviceId))
                {
                    var validateDeviceResponse = await ValidateDevice(request);
                    if (validateDeviceResponse.IsFailure)
                    {
                        return validateDeviceResponse;
                    }
                }

                _logger.LogExInformation($"Assinatura eletrônica válida para o cliente {request.CustomerId} com o dispostivo {request.DeviceId}");
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao validar a assinatura eletrônica do cliente {request?.CustomerId}.");
                throw;
            }
        }

        private async Task<ValidateElectronicSignatureAndDeviceResponse> ValidateDevice(ValidateElectronicSignatureAndDeviceCommand request)
        {
            _logger.LogExDebug($"Validando o dispositivo {request.DeviceId}...");
            var response = request.Response;
            var timeToken = await _validateTimeTokenRepository.GetValidateTimeTokenByDeviceUid(request.DeviceId, request.CustomerId);

            if (timeToken is null || !timeToken.TrustedDevice())
            {
                _logger.LogExDebug($"Cliente: [{request.CustomerId}] Device: [{request.DeviceId}], Dispositivo não confirmado ou temporário.");
                _logger.LogExDebug($"Verificando existência de token para o cliente {request.CustomerId}");
                var result  = await _bopeRepository.GetTokenInfo(request.RequestId, request.CustomerId);
                if(result != null)
                {
                    _logger.LogExWarning($"Dispostivo {request.DeviceId} do cliente {request.CustomerId} não é confiável");
                    response.AddError(Errors.ErrorsToken.UntrustedDevice());
                    return response;
                }
            }
            return response;
        }
    }
}
