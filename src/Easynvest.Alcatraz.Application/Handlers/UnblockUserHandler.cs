﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Resources;
using Easynvest.Messages.Events.Security.User;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class UnblockUserHandler : IRequestHandler<UnblockUserCommand, Domain.Responses.Response<UnblockUserCommandResponse>>
    {
        private readonly IUserBlockRepository _repository;
        private readonly ILogger<UnblockUserHandler> _logger;
        private readonly IBusControl _busControl;

        public UnblockUserHandler(IUserBlockRepository repository, IBusControl busControl, ILogger<UnblockUserHandler> logger)
        {
            _repository = repository;
            _logger = logger;
            _busControl = busControl;
        }

        public async Task<Domain.Responses.Response<UnblockUserCommandResponse>> Handle(UnblockUserCommand request, CancellationToken cancellationToken)
        {
            _logger.LogExInformation($"Desbloqueando o usuário {request.CustomerId}");
            try
            {
                await _repository.Remove(request.CustomerId, request.Type);
                var blocks = await _repository.Get(request.CustomerId);
                if (blocks == null || !blocks.Any())
                {
                    await _busControl.Publish<UserUnblocked>(new
                    {
                        request.CustomerId,
                        Date = DateTime.Now
                    });
                }
                _logger.LogExInformation($"Usuário {request.CustomerId} desbloqueado com sucesso");
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao desbloquear o usuário {request.CustomerId}");
                return Domain.Responses.Response<UnblockUserCommandResponse>.Fail(Errors.General.InternalProcessError().Message);
            }

            return Domain.Responses.Response<UnblockUserCommandResponse>.Ok(new UnblockUserCommandResponse());
        }
    }
}
