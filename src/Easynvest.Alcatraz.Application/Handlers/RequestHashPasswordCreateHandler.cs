﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Application.Validators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class RequestHashPasswordCreateHandler
        : RequestBaseHashPasswordHandler<RequestHashPasswordCreateCommand, RequestHashPasswordCreateResponse>
    {
        private readonly AbstractValidator<RequestHashPasswordCreateCommand> _commandValidator;
        protected override string TemplateEmailKey => "Mail:EmailCreateHashPasswordId";
        protected override string LabelCreatedMsg => "Criação de Senha";

        public RequestHashPasswordCreateHandler(IConfiguration configuration,
                                                IHashPasswordRepository repository,
                                                IMediator mediator,
                                                ILogger<RequestHashPasswordCreateHandler> logger
        )
            : base(repository, logger, mediator, configuration)
        {
            _commandValidator = new RequestHashPasswordCreateCommandValidator();
        }

        protected async override Task<Response<Customer>> ValidateCustomerInfo(RequestHashPasswordCreateCommand request)
        {
            return await Task.FromResult(
                 Response<Customer>.Ok(new Customer(
                    request.CustomerId,
                    Name.Create(request.Name),
                    request.Email,
                    string.Empty
                ))
            );
        }

        protected override Response ValidateCommand(RequestHashPasswordCreateCommand request)
        {
            var result = _commandValidator.Validate(request);

            if (!result.IsValid)
                return Response.Fail(result.Errors.Select(x => x.ErrorMessage));

            return Response.Ok();
        }
    }
}