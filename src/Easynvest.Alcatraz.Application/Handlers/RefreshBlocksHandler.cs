﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Messages.Events.Security.User;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class RefreshBlocksHandler : IRequestHandler<RefreshBlocksCommand, Response>
    {
        private readonly IUserBlockRepository _repository;
        private readonly ILogger<BlockUserHandler> _logger;
        private readonly IBusControl _busControl;

        public RefreshBlocksHandler(IUserBlockRepository repository,
            IBusControl busControl, ILogger<BlockUserHandler> logger)
        {
            _repository = repository;
            _logger = logger;
            _busControl = busControl;
        }

        public async Task<Response> Handle(RefreshBlocksCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var blocks = await _repository.Get(request.BlockType);
                foreach (var block in blocks)
                {
                    await _busControl.Publish<UserBlocked>(new
                    {
                        block.CustomerId,
                        Date = block.BlockDate
                    });
                }
                return Response.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao atualizar bloqueios");
                return Response.Fail("Erro");
            }
        }
    }
}
