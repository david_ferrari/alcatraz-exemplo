﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Alcatraz.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class ValidateElectronicSignatureHandler : IRequestHandler<ValidateElectronicSignatureCommand, Response<ValidateElectronicSignatureResponse>>
    {
        private readonly ILogger<ValidateElectronicSignatureHandler> _logger;
        private readonly IMediator _mediator;
        private readonly ICustomerSecretRepository _customerSecretRepository;
        private readonly IPasswordRepository _passwordRepository;
        private readonly CustomerSecretOption _customerSecretOptions;

        public ValidateElectronicSignatureHandler
            (
                IMediator mediator,
                ICustomerSecretRepository customerSecretRepository,
                IPasswordRepository passwordRepository,
                ILogger<ValidateElectronicSignatureHandler> logger,
                IOptions<CustomerSecretOption> options
            )
        {
            _mediator = mediator;
            _customerSecretRepository = customerSecretRepository;
            _passwordRepository = passwordRepository;
            _logger = logger;
            _customerSecretOptions = options.Value;
        }

        public async Task<Response<ValidateElectronicSignatureResponse>> Handle(ValidateElectronicSignatureCommand request, CancellationToken cancellationToken)
        {
            try
            {
                if (string.IsNullOrEmpty(request?.ElectronicSignature))
                    return Response<ValidateElectronicSignatureResponse>
                        .Fail("Informe uma Assinatura Eletrônica.");

                var response = await ValidateCustomerSecret(request);

                if (response.IsFailure)
                {
                    _logger.LogExWarning($"[{request.CustomerId}] {response.Message}");
                    return Response<ValidateElectronicSignatureResponse>.Fail(response.Messages);
                }

                _logger.LogExInformation($"Assinatura eletrônica válida para o cliente {request.CustomerId}");
                return Response<ValidateElectronicSignatureResponse>.Ok();
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao validar a assinatura eletrônica do cliente {request?.CustomerId}.");
                throw;
            }
        }

        private async Task<Response> ValidateCustomerSecret(ValidateElectronicSignatureCommand request)
        {
            _logger.LogExDebug($"Validando assinatura eletrônica para o cliente {request.CustomerId}...");

            var customerSecret = await _customerSecretRepository.Get(request.CustomerId, SecretType.EletronicSignature);

            if (customerSecret == null)
            {
                _logger.LogExWarning($"[{request.CustomerId}] Cliente não localizado.");
                return Response.Fail("Cliente não localizado.");
            }

            if (!customerSecret.VerifyLoginAttempts(_customerSecretOptions.MaxInvalidLoginAttempts))
            {
                _logger.LogExWarning($"[{request.CustomerId}] Assinatura eletrônica bloqueada.");
                return Response.Fail("Assinatura Eletrônica bloqueada.");
            }

            var errorMessage = string.Empty;

            if (!customerSecret.ValidateCustomerSecret(request.ElectronicSignature))
                errorMessage = await GetErrorDescription(request).ConfigureAwait(false);

            using (var ts = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                if (customerSecret.ShouldUpdateLoginAttempts())
                    await _customerSecretRepository
                        .UpdateCustomerSecret(customerSecret, SecretType.EletronicSignature);

                ts.Complete();
            }

            if (string.IsNullOrEmpty(errorMessage))
            {
                if (customerSecret.NeedToUpdate())
                {
                    var updateResult = await _mediator.Send(new UpdateSecretVersionCommand(customerSecret.CustomerId,
                        SecretType.EletronicSignature, request.ElectronicSignature));
                    if (updateResult == null || updateResult.IsFailure)
                        _logger.LogExError("Erro ao atualizar a versão da senha do usuário", updateResult?.Message);
                }

                return Response.Ok();
            }

            _logger.LogExWarning($"Assinatura eletrônica inválida para o cliente {request.CustomerId}. - Message: {errorMessage}");
            return Response.Fail(errorMessage);
        }

        private async Task<string> GetErrorDescription(ValidateElectronicSignatureCommand request)
        {
            var password = await _passwordRepository.GetByCustomerId(request.CustomerId);

            if (password == null)
                return "Password não localizado.";

            var encodedESG = EncodePasswords.Encode(request.CustomerId, request.ElectronicSignature,
               password.PasswordDetails.EletronicSignatureVersion, password.PasswordDetails.EletronicSignatureSalt?.Salt);

            if (password.PasswordDetails.EncodedPassword.Equals(encodedESG))
                return "Você está utilizando a sua Senha ao invés da sua Assinatura Eletrônica.";

            var isOldElectronicSignature = password
                    .LastSixElectronicSignatures
                    .Any(x => x.IsEqual(request.ElectronicSignature, request.CustomerId));

            if (isOldElectronicSignature)
                return "Você está utilizando uma Assinatura Eletrônica antiga, utilize a mais atual.";

            return "Assinatura Eletrônica inválida.";
        }
    }
}
