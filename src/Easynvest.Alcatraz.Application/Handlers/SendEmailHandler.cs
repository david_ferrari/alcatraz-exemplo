﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Factories;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.CrossCutting.Options;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.KingsCross.Bus.Contracts;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class SendEmailHandler : IRequestHandler<SendEmailCommand, Response>
    {
        private readonly IKingsCrossServiceBus _serviceBus;
        private readonly EmailMessaging _emailMessaging;
        private readonly ILogger<SendEmailHandler> _logger;

        public SendEmailHandler(
            IKingsCrossServiceBus serviceBus,
            IOptions<EmailMessaging> option,
            ILogger<SendEmailHandler> logger)
        {
            _serviceBus = serviceBus;
            _emailMessaging = option.Value;
            _logger = logger;
        }

        public async Task<Response> Handle(SendEmailCommand request, CancellationToken cancellationToken)
        {
            _logger.LogExDebug($" Iniciando Envio de email id {request.EmailId}. Destinatario: {request.To}");

            var validatedCommand = ValidateCommand(request);
            if (validatedCommand.IsFailure)
            {
                _logger.LogExWarning($" {validatedCommand.Messages}");
                return Response.Fail(validatedCommand.Messages);
            }

            var mailEvent = new EmailQueue(request.EmailId, request.EmailParameters, request.To);

            await _serviceBus.Publish(mailEvent, KingsCrossConfigurationFactory.CreateEndpointConfiguration(
                _emailMessaging.Exchange,
                ExchangeType.Direct,
                _emailMessaging.Queue,
                _emailMessaging.RoutingKey));

            _logger.LogExDebug($" Envio de email para Destinatario: {request.To} feito com sucesso.");

            return Response.Ok();
        }

        private static Response ValidateCommand(SendEmailCommand command)
        {
            var response = Response.Ok();

            if (command.EmailId == default)
                response.Messages.Add("Id de template de email inválido.");

            if (string.IsNullOrEmpty(command.To))
                response.Messages.Add("É necessario um destinatário.");

            if (command.EmailParameters == null)
                response.Messages.Add("Parâmetros de email inválidos.");

            return response;
        }
    }
}
