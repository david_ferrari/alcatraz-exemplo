﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Application.Handlers
{
    public class AuthenticateTransactionCommandHandler : IRequestHandler<AuthenticateTransactionCommand, Response<AuthenticateTransactionResponse>>
    {
        private readonly IBopeRepository _bopeRepository;
        private readonly ILogger<AuthenticateTransactionCommandHandler> _logger;

        public AuthenticateTransactionCommandHandler(IBopeRepository bopeRepository, ILogger<AuthenticateTransactionCommandHandler> logger)
        {
            _bopeRepository = bopeRepository;
            _logger = logger;
        }

        public async Task<Response<AuthenticateTransactionResponse>> Handle(AuthenticateTransactionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogExInformation("[{RequestId}] Início de autorização para a conta {AccountId}", request.RequestId, request.AccountId);

                var isAuthenticated = await _bopeRepository.Authenticate(request.RequestId, request.AccountId, request.OtpCode, request.DeviceUid, request.Integration);

                if (!isAuthenticated)
                {
                    _logger.LogExWarning("[{RequestId}] Transação não autorizada para a conta {AccountId}", request.RequestId, request.AccountId);
                    return Response<AuthenticateTransactionResponse>.Fail("Transação não autorizada.");
                }

                _logger.LogExDebug("[{RequestId}] Transação autorizada para a conta {AccountId}", request.RequestId, request.AccountId);

                return Response<AuthenticateTransactionResponse>.Ok(new AuthenticateTransactionResponse() { Authorized = isAuthenticated });
            }
            catch (System.Exception ex)
            {
                _logger.LogExError(ex, "Erro ao autorizar a conta {AccountId}", request.AccountId);
                return Response<AuthenticateTransactionResponse>.Fail("Erro ao autorizar, tente novamente mais tarde.");
            }
        }
    }
}
