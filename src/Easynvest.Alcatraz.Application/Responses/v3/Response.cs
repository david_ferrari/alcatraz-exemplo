﻿using Easynvest.Ops;
using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Application.Responses.v3
{
    [ExcludeFromCodeCoverage]
    public abstract class Response
    {
        protected Response(string requestId) => RequestId = requestId;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ErrorResponse ErrorResponse { get; private set; }

        public void AddError(Error error)
        {
            if (error == null)
                return;

            ErrorResponse = new ErrorResponse(RequestId, error);
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsFailure => !IsSuccess;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsSuccess => VerifyResponseIsSuccess();

        public string RequestId { get; }

        private bool VerifyResponseIsSuccess()
            => ErrorResponse == null || ErrorResponse.Error == null;
    }
}