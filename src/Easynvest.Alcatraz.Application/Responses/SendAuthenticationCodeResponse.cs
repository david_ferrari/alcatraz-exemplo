﻿using Easynvest.Alcatraz.Application.Responses.v3;

namespace Easynvest.Alcatraz.Application.Responses
{
    public class SendAuthenticationCodeResponse : Response
    {
        public SendAuthenticationCodeResponse(string requestId)
            : base(requestId)
        {
        }
    }
}