﻿namespace Easynvest.Alcatraz.Application.Responses
{
    public class ValidHashQueryResponse
    {
        public bool NotFound { get; set; }

        public string CustomerId { get; set; }
    }
}
