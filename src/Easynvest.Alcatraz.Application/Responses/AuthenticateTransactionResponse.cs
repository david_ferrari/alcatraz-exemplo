﻿namespace Easynvest.Alcatraz.Application.Responses
{
    public class AuthenticateTransactionResponse
    {
        public bool Authorized { get; set; }
    }
}
