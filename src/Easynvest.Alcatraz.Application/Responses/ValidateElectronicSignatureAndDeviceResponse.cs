﻿using Easynvest.Alcatraz.Application.Responses.v3;

namespace Easynvest.Alcatraz.Application.Responses
{
    public class ValidateElectronicSignatureAndDeviceResponse : Response
    {
        public ValidateElectronicSignatureAndDeviceResponse(string requestId) : base(requestId)
        {
        }
    }
}
