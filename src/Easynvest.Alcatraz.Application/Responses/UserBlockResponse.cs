﻿using Easynvest.Alcatraz.Domain.Extensions;
using Easynvest.Alcatraz.Domain.Models.Entities;
using System;

namespace Easynvest.Alcatraz.Application.Responses
{
    public class UserBlockResponse
    {
        public UserBlockResponse(UserBlock block)
        {
            CustomerId = block.CustomerId;
            BlockType = block.BlockType.GetDescription();
            BlockDate = block.BlockDate;
        }

        public string CustomerId { get; }
        public string BlockType { get; }
        public DateTime BlockDate { get; }
    }
}
