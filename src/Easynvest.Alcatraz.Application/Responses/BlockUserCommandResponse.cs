﻿using Easynvest.Alcatraz.Domain.Models.Entities;

namespace Easynvest.Alcatraz.Application.Responses
{
    public class BlockUserCommandResponse
    {
        public BlockUserCommandResponse(UserBlock user)
        {
            User = new UserBlockResponse(user);
        }

        public UserBlockResponse User { get; }
    }
}
