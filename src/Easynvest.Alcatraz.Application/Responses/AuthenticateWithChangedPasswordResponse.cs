﻿using System;

namespace Easynvest.Alcatraz.Application.Responses
{
    public class AuthenticateWithChangedPasswordResponse
    {
        public string AccessToken { get; set; }
        public string TokenType { get; set; }
        public string RefreshToken { get; set; }
        public string ClientId { get; set; }
        public DateTime Issued { get; set; }
        public DateTime Expires { get; set; }
    }
}
