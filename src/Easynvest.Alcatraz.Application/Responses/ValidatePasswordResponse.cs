﻿using Easynvest.Application.Common.Messages;

namespace Easynvest.Alcatraz.Application.Responses
{
    public class ValidatePasswordResponse : Response
    {
        public ValidatePasswordResponse(Command command) : base(command)
        {
        }
    }
}
