﻿using Easynvest.Alcatraz.Application.Responses.v3;

namespace Easynvest.Alcatraz.Application.Responses
{
    public class AddTrustedDeviceResponse : Response
    {
        public AddTrustedDeviceResponse(string requestId) : base(requestId)
        {
        }
    }
}
