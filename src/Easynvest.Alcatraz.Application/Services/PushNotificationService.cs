﻿using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.PushNotification.FirebaseCloudMessage.Contracts;
using Easynvest.PushNotification.FirebaseCloudMessage.Messages;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using static Easynvest.Alcatraz.Domain.Enumerators.PushNotificationEnum;

namespace Easynvest.Alcatraz.Application.Services
{
    public class PushNotificationService : IPushNotificationService
    {
        private readonly ILogger<PushNotificationService> _logger;
        private readonly IFirebaseCloudMessageProvider _pushProvider;

        public PushNotificationService(ILogger<PushNotificationService> logger, IFirebaseCloudMessageProvider pushProvider)
        {
            _logger = logger;
            _pushProvider = pushProvider;
        }

        public async Task<ResponseContent> SendDataAsync(Customer user, object data)
        {
            ResponseContent result = null;
            if (!string.IsNullOrWhiteSpace(user.FcmToken))
            {
                var fcmUser = $"[CustomerId: {user.CustomerId}, FcmToken: {user.FcmToken}]";
                try
                {
                    _logger.LogExDebug($"Enviando PushNotification para o cliente {fcmUser}. Data {data}");
                    result = await _pushProvider.SendDataAsync(user.FcmToken, data).ConfigureAwait(false);
                    _logger.LogExDebug($"Resultado do PushNotification para {fcmUser}: {JsonConvert.SerializeObject(result)}");
                }
                catch (Exception ex)
                {
                    _logger.LogExError(ex, $"Erro no envio de PushNotification para {fcmUser}");
                }
            }
            return result;
        }

        public static object GetDataPush(PushDataType type)
        {
            object result = null;

            switch (type)
            {
                case PushDataType.ResetPassword:
                    result = new
                    {
                        reason = "Reset Password",
                        code = 1,
                        action = "Logout"
                    };
                    break;
                case PushDataType.ResetEletronicSignature:
                    result = new
                    {
                        reason = "ResetEletronicSignature",
                        code = 2,
                        action = "RefreshToken"
                    };
                    break;
            }

            return result;
        }

    }
}
