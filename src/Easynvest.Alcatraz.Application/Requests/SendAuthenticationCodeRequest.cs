﻿namespace Easynvest.Alcatraz.Application.Requests
{
    public class SendAuthenticationCodeRequest
    {
        public SendAuthenticationCodeRequest(string username, string password, string fcmToken)
        {
            Username = username;
            Password = password;
            FcmToken = fcmToken;
        }

        public string Username { get; }
        public string Password { get; }
        public string FcmToken { get; }
    }
}