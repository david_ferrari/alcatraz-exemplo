﻿using System;
using System.Diagnostics.CodeAnalysis;
using Easynvest.Alcatraz.Application.Responses.v3;
using MediatR;

namespace Easynvest.Alcatraz.Application.Requests
{
    [ExcludeFromCodeCoverage]
    public abstract class Request<T> : IRequest<T>
        where T : Response
    {
        protected Request(string requestId)
        {
            RequestId = requestId;
        }

        protected Request()
            : this(Guid.NewGuid().ToString())
        {
        }

        public string RequestId { get; }

        public DateTime RequestedAt { get; } = DateTime.Now;

        public abstract T Response { get; }
    }
}