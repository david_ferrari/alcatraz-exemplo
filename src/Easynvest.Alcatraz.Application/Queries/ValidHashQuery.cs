﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Queries
{
    public class ValidHashQuery : IRequest<Response<ValidHashQueryResponse>>
    {
        public string Hash { get; }

        public ValidHashQuery(string hash)
        {
            Hash = hash;
        }
    }
}
