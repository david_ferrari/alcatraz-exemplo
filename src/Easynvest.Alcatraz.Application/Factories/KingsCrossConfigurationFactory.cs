﻿using Easynvest.KingsCross.Bus.Topology;
using Easynvest.KingsCross.Bus.Topology.Contracts;
using Easynvest.KingsCross.RabbitMq.Model;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Application.Factories
{
    public static class KingsCrossConfigurationFactory
    {
        public static IEndPointConfigurator CreateEndpointConfiguration(string exchangeName, string exchangeType, string queueName, string routingKey)
        {
            var exchange = new ExchangeConfiguration(exchangeName)
            {
                Type = exchangeType,
                Durable = true,
                AutoDelete = false,
                Arguments = new Dictionary<string, object>()
            };

            var queue = new QueueConfiguration(queueName)
            {
                Durable = true,
                Exclusive = false,
                AutoDelete = false,
                Arguments = new Dictionary<string, object>()
            };

            return new EndPointConfigurator(
              exchange,
              queue,
              routingKey: routingKey
          );
        }
    }
}
