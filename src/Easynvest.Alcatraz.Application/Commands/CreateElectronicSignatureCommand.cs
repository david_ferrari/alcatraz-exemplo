﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class CreateElectronicSignatureCommand : PasswordBaseCommand<CreateElectronicSignatureResponse, Response<CreateElectronicSignatureResponse>>
    {
        public CreateElectronicSignatureCommand(string hash, string secret, string ipAddress)
            : base(hash, secret, ipAddress, SecretChangeType.NewSecret, SecretType.EletronicSignature) { }

        public override Response<CreateElectronicSignatureResponse> Response => new Response<CreateElectronicSignatureResponse>();
    }
}
