﻿using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class SendEmailCommand : IRequest<Response>
    {
        public SendEmailCommand(int emailId, Dictionary<string, string> emailParameters, string to, string label)
        {
            EmailId = emailId;
            EmailParameters = emailParameters;
            To = to;
            Label = label;
        }

        public int EmailId { get; }
        public Dictionary<string,string> EmailParameters { get; }
        public string To { get; }
        public string Label { get; }
    }
}
