﻿using Easynvest.Alcatraz.Application.Requests;
using Easynvest.Alcatraz.Application.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class AddTrustedDeviceCommand : Request<AddTrustedDeviceResponse>
    {
        public AddTrustedDeviceCommand(string deviceUid)
        {
            DeviceUid = deviceUid;
        }

        public override AddTrustedDeviceResponse Response => new AddTrustedDeviceResponse(RequestId);

        public string DeviceUid { get; }

        public string CustomerId { get; set; }
    }
}
