﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public abstract class RequestBaseHashSecretCommand<TValue, TResponse> : IRequest<TResponse>
        where TValue : RequestBaseHashPasswordResponse
        where TResponse : Response<TValue>
    {
        protected RequestBaseHashSecretCommand(string email, string customerId)
        {
            Email = email;
            CustomerId = customerId;
        }

        public abstract TResponse Response { get; }

        public string Email { get; }
        public string CustomerId { get; }
    }
}

