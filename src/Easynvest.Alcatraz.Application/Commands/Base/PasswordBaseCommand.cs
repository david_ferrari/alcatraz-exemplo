﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public abstract class PasswordBaseCommand<TValue, TResponse> : IRequest<TResponse>
        where TValue : PasswordBaseResponse
        where TResponse : Response<TValue>
    {
        public SecretChangeType ChangeType { get; }

        public string Hash { get; }

        public string SecretCandidate { get; }

        public string CustomerId { get; protected set; }

        public Password Password { get; protected set; }

        public abstract TResponse Response { get; }

        public SecretType SecretType { get; }

        public string IpAddress { get; }

        public void SetCustomerId(string customerId)
        {
            CustomerId = customerId;
        }

        protected PasswordBaseCommand(string hash, 
            string secretCandidate,
            string ipAddress,
            SecretChangeType changeType, 
            SecretType secretType)
        {
            Hash = hash;
            SecretCandidate = secretCandidate;
            ChangeType = changeType;
            SecretType = secretType;
            IpAddress = ipAddress;
        }
    }
}
