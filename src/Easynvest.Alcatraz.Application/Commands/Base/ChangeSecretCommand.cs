﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Easynvest.Alcatraz.Domain.Extensions;

namespace Easynvest.Alcatraz.Application.Commands.Base
{
    public abstract class ChangeSecretCommand<TResponse> : IRequest<Response<TResponse>>
    {
        public ChangeSecretCommand(string customerId,
            string newSecret,
            string confirmSecret,
            string electronicSignature,
            SecretType secretType)
        {
            CustomerId = customerId;
            NewSecret = newSecret;
            ConfirmSecret = confirmSecret;
            SecretType = secretType;
            ElectronicSignature = electronicSignature;
        }

        public string CustomerId { get; }
        public string ElectronicSignature { get; }
        public string NewSecret { get; }
        public string ConfirmSecret { get; }
        public SecretType SecretType { get; }
        public abstract Response<TResponse> Response { get; }
        public override string ToString() => $"Cliente: {CustomerId} - SecretType: {SecretType.GetDescription()}";
    }
}
