﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Application.Common.Messages;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class ValidatePasswordCommand : Command, IRequest<ValidatePasswordResponse>
    {
        public string Password { get; }

        public string CustomerId { get; private set; }
        
        public ValidatePasswordCommand(string password)
        {
            Password = password;
        }

        public void SetCustomerId(string customerId)
        {
            CustomerId = customerId;
        }
    }
}

