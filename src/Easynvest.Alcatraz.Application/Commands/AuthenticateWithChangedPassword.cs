﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class AuthenticateWithChangedPasswordCommand : IRequest<Response<AuthenticateWithChangedPasswordResponse>>
    {
        public string UserName { get; internal set; }
        public string Password { get; internal set; }
        public string ClientId { get; internal set; }

        public AuthenticateWithChangedPasswordCommand(string userName, string password, string clientId)
        {
            UserName = userName;
            Password = password;
            ClientId = clientId;
        }
    }
}
