﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Application.Common.Messages;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class ValidateElectronicSignatureCommand : Command, IRequest<Response<ValidateElectronicSignatureResponse>>
    {
        public string ElectronicSignature { get; }

        public string CustomerId { get; private set; }

        public ValidateElectronicSignatureCommand(string electronicSignature, string customerId)
        {
            ElectronicSignature = electronicSignature;
            CustomerId = customerId;
        }
    }
}