﻿using Easynvest.Alcatraz.Application.Commands.Base;
using Easynvest.Alcatraz.Application.Commands.Request;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class ChangeLoggedPasswordCommand : ChangeSecretCommand<ChangeLoggedPasswordResponse>
    {
        public ChangeLoggedPasswordCommand(ChangeLoggedPasswordRequest requestChangePassword, string customerId)
            : base(customerId, requestChangePassword.Password, requestChangePassword.ConfirmationPassword, requestChangePassword.ElectronicSignature, SecretType.Password)
        {
        }

        public override Response<ChangeLoggedPasswordResponse> Response => new Response<ChangeLoggedPasswordResponse>();
    }
}
