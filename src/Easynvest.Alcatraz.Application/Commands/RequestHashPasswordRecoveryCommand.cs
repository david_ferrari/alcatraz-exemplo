﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class RequestHashPasswordRecoveryCommand 
        : RequestBaseHashSecretCommand<RequestHashPasswordRecoveryResponse, Response<RequestHashPasswordRecoveryResponse>>
    {
        public RequestHashPasswordRecoveryCommand(string email, string customerId)
            :base(email, customerId)
        {
        }

        public override Response<RequestHashPasswordRecoveryResponse> Response => new Response<RequestHashPasswordRecoveryResponse>();
    }
}

