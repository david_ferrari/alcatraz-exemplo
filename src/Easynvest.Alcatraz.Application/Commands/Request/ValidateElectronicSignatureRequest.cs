﻿namespace Easynvest.Alcatraz.Application.Commands.Request
{
    public class ValidateElectronicSignatureRequest
    {
        public ValidateElectronicSignatureRequest(string electronicSignature)
        {
            ElectronicSignature = electronicSignature;
        }

        public string ElectronicSignature { get; }
        
    }
}
