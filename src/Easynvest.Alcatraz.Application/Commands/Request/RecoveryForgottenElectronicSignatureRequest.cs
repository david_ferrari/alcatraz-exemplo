﻿namespace Easynvest.Alcatraz.Application.Commands.Request
{
    public class RecoveryForgottenElectronicSignatureRequest
    {
        public string AccountEmail { get; }
        public string UserName { get; }

        public RecoveryForgottenElectronicSignatureRequest(string accountEmail, string userName)
        {
            AccountEmail = accountEmail;
            UserName = userName;
        }
    }
}
