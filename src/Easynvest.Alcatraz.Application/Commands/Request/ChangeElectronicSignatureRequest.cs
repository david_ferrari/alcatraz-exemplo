﻿namespace Easynvest.Alcatraz.Application.Commands.Request
{
    public class ChangeElectronicSignatureRequest
    {
        public ChangeElectronicSignatureRequest(string newElectronicSignature, string confirmElectronicSignature, string electronicSignature)
        {
            NewElectronicSignature = newElectronicSignature;
            ConfirmElectronicSignature = confirmElectronicSignature;
            ElectronicSignature = electronicSignature;
        }

        public string ElectronicSignature { get; }
        public string NewElectronicSignature { get; }
        public string ConfirmElectronicSignature { get; }
    }
}
