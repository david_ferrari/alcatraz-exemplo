﻿using Easynvest.Alcatraz.Domain.Models.ValueObjects;

namespace Easynvest.Alcatraz.Application.Commands.Request
{
    public class TransactionAuthenticationRequest
    {
        public string OtpCode { get; set; }
        public string DeviceUid { get; set; }
        public BopeIntegrationType Integration { get; set; }
    }
}
