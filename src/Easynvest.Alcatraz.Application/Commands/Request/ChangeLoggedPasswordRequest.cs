﻿namespace Easynvest.Alcatraz.Application.Commands.Request
{
    public class ChangeLoggedPasswordRequest
    {
        public ChangeLoggedPasswordRequest(string electronicSignature, string password, string confirmationPassword)
        {
            ElectronicSignature = electronicSignature;
            Password = password;
            ConfirmationPassword = confirmationPassword;
        }

        public string ElectronicSignature { get; private set; }
        public string Password { get; }
        public string ConfirmationPassword { get; }
    }
}
