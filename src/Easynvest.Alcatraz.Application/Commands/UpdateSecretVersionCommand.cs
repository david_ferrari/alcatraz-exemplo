﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class UpdateSecretVersionCommand : IRequest<Response<bool>>
    {
        public UpdateSecretVersionCommand(string customerId, SecretType secretType, string password)
        {
            CustomerId = customerId;
            Password = password;
            SecretType = secretType;
        }

        public string CustomerId { get; }
        public string Password { get; }
        public SecretType SecretType { get; }
    }
}
