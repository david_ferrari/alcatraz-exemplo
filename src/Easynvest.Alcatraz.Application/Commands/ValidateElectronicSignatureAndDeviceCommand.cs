﻿using Easynvest.Alcatraz.Application.Requests;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using Easynvest.Application.Common.Messages;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class ValidateElectronicSignatureAndDeviceCommand : Request<ValidateElectronicSignatureAndDeviceResponse>
    {
        public string ElectronicSignature { get; }

        public string DeviceId { get; private set; }

        public string CustomerId { get; private set; }

        public override ValidateElectronicSignatureAndDeviceResponse Response => new ValidateElectronicSignatureAndDeviceResponse(RequestId);

        public ValidateElectronicSignatureAndDeviceCommand(string electronicSignature, string customerId, string deviceId)
        {
            ElectronicSignature = electronicSignature;
            DeviceId = deviceId;
            CustomerId = customerId;
        }
    }
}