﻿using Easynvest.Alcatraz.Application.Commands.Base;
using Easynvest.Alcatraz.Application.Commands.Request;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class ChangeElectronicSignatureCommand : ChangeSecretCommand<ChangeLoggedPasswordResponse>
    {
        public ChangeElectronicSignatureCommand(ChangeElectronicSignatureRequest request, string customerId)
            : base(customerId, request.NewElectronicSignature, request.ConfirmElectronicSignature, request.ElectronicSignature, SecretType.EletronicSignature)
        {
        }

        public override Response<ChangeLoggedPasswordResponse> Response => new Response<ChangeLoggedPasswordResponse>();
    }
}
