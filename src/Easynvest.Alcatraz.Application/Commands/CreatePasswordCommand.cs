﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class CreatePasswordCommand : PasswordBaseCommand<CreatePasswordResponse, Response<CreatePasswordResponse>>
    {
        public CreatePasswordCommand(string hash, string password, string ipAddress)
            : base(hash, password, ipAddress, SecretChangeType.NewSecret, SecretType.Password) { }

        public override Response<CreatePasswordResponse> Response => new Response<CreatePasswordResponse>();
    }
}
