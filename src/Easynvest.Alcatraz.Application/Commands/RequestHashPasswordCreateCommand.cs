﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class RequestHashPasswordCreateCommand 
        : RequestBaseHashSecretCommand<RequestHashPasswordCreateResponse, Response<RequestHashPasswordCreateResponse>>
    {

        public RequestHashPasswordCreateCommand(string name, string email, string customerId)
            : base(email, customerId)
        {
            Name = name;
        }

        public string Name { get; }

        public override Response<RequestHashPasswordCreateResponse> Response => new Response<RequestHashPasswordCreateResponse>();
    }
}
