﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class ChangeForgottenElectronicSignatureCommand : PasswordBaseCommand<ChangeForgottenElectronicSignatureResponse, Response<ChangeForgottenElectronicSignatureResponse>>
    {
        public ChangeForgottenElectronicSignatureCommand(string hash, string secret, string ipAddress)
            : base(hash, secret, ipAddress, SecretChangeType.ChangeSecret, SecretType.EletronicSignature)
        {
        }

        public override Response<ChangeForgottenElectronicSignatureResponse> Response => new Response<ChangeForgottenElectronicSignatureResponse>();
    }
}
