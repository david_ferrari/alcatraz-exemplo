﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class ChangeForgottenPasswordCommand : PasswordBaseCommand<ChangeForgottenPasswordResponse, Response<ChangeForgottenPasswordResponse>>
    {
        public string ClientId { get; private set; }

        public ChangeForgottenPasswordCommand(string hash, string password, string ipAddress, string clientId)
            : base(hash, password, ipAddress, SecretChangeType.ChangeSecret, SecretType.Password) => ClientId = clientId;

        public override Response<ChangeForgottenPasswordResponse> Response => new Response<ChangeForgottenPasswordResponse>();
    }
}
