﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class AuthenticateTransactionCommand : IRequest<Response<AuthenticateTransactionResponse>>
    {
        public AuthenticateTransactionCommand(string requestId, string accountId, string otpCode, string deviceUid, BopeIntegrationType integration)
        {
            RequestId = requestId;
            AccountId = accountId;
            OtpCode = otpCode;
            DeviceUid = deviceUid;
            Integration = integration;
        }

        public string RequestId { get; }
        public string AccountId { get; }
        public string OtpCode { get; }
        public string DeviceUid { get; }
        public BopeIntegrationType Integration { get; }
    }
}
