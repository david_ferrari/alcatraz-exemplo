﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class RefreshBlocksCommand : IRequest<Response>
    {
        public BlockType BlockType { get; set; }

        public RefreshBlocksCommand(BlockType blockType)
        {
            BlockType = blockType;
        }
    }
}
