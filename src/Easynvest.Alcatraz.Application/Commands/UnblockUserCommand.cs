﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class UnblockUserCommand : IRequest<Response<UnblockUserCommandResponse>>
    {
        public UnblockUserCommand(string customerId, BlockType type)
        {
            CustomerId = customerId;
            Type = type;
        }

        public string CustomerId { get; }
        public BlockType Type { get; }
    }
}
