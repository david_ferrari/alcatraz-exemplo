﻿using Easynvest.Alcatraz.Application.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class SendAuthenticationCodeCommand : Requests.Request<SendAuthenticationCodeResponse>
    {
        public SendAuthenticationCodeCommand(string username, string password, string fcmToken, string ipAddress)
        {
            Username = username;
            Password = password;
            FcmToken = fcmToken;
            IpAddress = ipAddress;
        }

        public string Username { get; }
        public string Password { get; }
        public string FcmToken { get; }
        public string IpAddress { get; }

        public override SendAuthenticationCodeResponse Response => new SendAuthenticationCodeResponse(RequestId);

        public override string ToString() => $"{Username}  - ip - {IpAddress}";
    }
}