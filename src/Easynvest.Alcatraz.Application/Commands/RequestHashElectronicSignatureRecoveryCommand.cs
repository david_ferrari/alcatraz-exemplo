﻿using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class RequestHashElectronicSignatureRecoveryCommand
        : RequestBaseHashSecretCommand<RequestHashElectronicSignatureRecoveryResponse, Response<RequestHashElectronicSignatureRecoveryResponse>>
    {
        public RequestHashElectronicSignatureRecoveryCommand(string email, string customerId)
            :base(email, customerId)
        {
        }

        public override Response<RequestHashElectronicSignatureRecoveryResponse> Response => new Response<RequestHashElectronicSignatureRecoveryResponse>();
    }
}
