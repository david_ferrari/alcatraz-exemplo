﻿using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;

namespace Easynvest.Alcatraz.Application.Commands
{
    public class CreateSecretCommand : IRequest<Response>
    {
        public CreateSecretCommand(string customerId, string secretCandidate, SecretType type, SecretChangeType changeType)
        {
            CustomerId = customerId;
            SecretCandidate = secretCandidate;
            Type = type;
            ChangeType = changeType;
        }

        public string CustomerId { get; }

        public string SecretCandidate { get; }

        public SecretType Type { get; }

        public SecretChangeType ChangeType { get; }
    }
}
