﻿using Easynvest.Alcatraz.Application.Commands;
using FluentValidation;

namespace Easynvest.Alcatraz.Application.Validators
{
    public class RequestHashPasswordRecoveryCommandValidator : AbstractValidator<RequestHashPasswordRecoveryCommand>
    {
        public RequestHashPasswordRecoveryCommandValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.CustomerId);
            RuleFor(x => x.Email)
              .SetValidator(new EmailValidator());
        }
    }
}
