﻿using FluentValidation;
using FluentValidation.Internal;
using FluentValidation.Resources;

namespace Easynvest.Alcatraz.Application.Validators
{
    public static class FluentExtensions
    {
        public static IRuleBuilderOptions<T, TProperty> WithGlobalMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, string errorMessage)
        {
            foreach (var item in (rule as RuleBuilder<T, TProperty>).Rule.Validators)
            {
                item.Options.ErrorMessageSource = new StaticStringSource(errorMessage);
                item.Options.ErrorCodeSource = new StaticStringSource(string.Empty);
            }

            return rule;
        }
    }
}
