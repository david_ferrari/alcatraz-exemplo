﻿using Easynvest.Alcatraz.Application.Commands;
using FluentValidation;

namespace Easynvest.Alcatraz.Application.Validators
{
    public class AuthenticateWithChangedPasswordValidator : AbstractValidator<AuthenticateWithChangedPasswordCommand>
    {
        public AuthenticateWithChangedPasswordValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.UserName).NotEmpty().WithGlobalMessage("UserName deve ser informada");
            RuleFor(x => x.Password).NotEmpty().WithGlobalMessage("Senha deve ser informada");
            RuleFor(x => x.ClientId).NotEmpty().WithGlobalMessage("ClientId deve ser informada");
        }
    }
}
