﻿using Easynvest.Alcatraz.Application.Commands;
using FluentValidation;

namespace Easynvest.Alcatraz.Application.Validators
{
    public class ChangeLoggedPasswordCommandValidator : AbstractValidator<ChangeLoggedPasswordCommand>
    {
        public ChangeLoggedPasswordCommandValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;
            
            RuleFor(x => x.ConfirmSecret).NotEmpty().WithGlobalMessage("Confirmação de senha deve ser informada");
            RuleFor(x => x.NewSecret).NotEmpty().WithGlobalMessage("Senha deve ser informada");
            RuleFor(x => x.ConfirmSecret).NotEmpty().WithGlobalMessage("Confirmação de senha deve ser informada");
        }
    }
}
