﻿using Easynvest.Alcatraz.Application.Commands;
using FluentValidation;

namespace Easynvest.Alcatraz.Application.Validators
{
    public class RequestHashPasswordCreateCommandValidator : AbstractValidator<RequestHashPasswordCreateCommand>
    {
        public RequestHashPasswordCreateCommandValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.CustomerId);
            RuleFor(x => x.Name);
            RuleFor(x => x.Email)
              .SetValidator(new EmailValidator());
        }
    }
}
