﻿using FluentValidation;

namespace Easynvest.Alcatraz.Application.Validators
{
    public class EmailValidator : AbstractValidator<string>
    {  
        public EmailValidator()
        {
            RuleFor(email => email)
                .NotEmpty()
                .MaximumLength(100)
                .EmailAddress()
                .WithGlobalMessage("E-mail inválido");
        }
    }
}
