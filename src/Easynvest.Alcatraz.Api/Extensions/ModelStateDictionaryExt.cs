﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Api
{
    public static class ModelStateDictionaryExt
    {
        public static ModelStateDictionary AddModelError(this ModelStateDictionary msd, HashSet<KeyValuePair<string, string>> errorMessages)
        {
            foreach (var errorMessage in errorMessages)
            {
                msd.AddModelError(errorMessage.Key, errorMessage.Value);
            }

            return msd;
        }
    }
}
