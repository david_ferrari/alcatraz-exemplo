﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Easynvest.Alcatraz.Api.Extensions
{
	public static class SerilogHttpContextEx
	{
		public static void CustomEnrichLogic(IDiagnosticContext diagnosticContext, HttpContext httpContext)
		{
			try
			{
				FromClaims(diagnosticContext, httpContext.User);
				FromRequest(diagnosticContext, httpContext.Request);
				FromHeaders(diagnosticContext, httpContext.Request.Headers);

				diagnosticContext.Set("IpAddress", JsonConvert.SerializeObject(httpContext.Connection.RemoteIpAddress.ToString()));
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Erro ao enriquecer log");
			}
		}

		private static void FromClaims(IDiagnosticContext diagnosticContext, ClaimsPrincipal claims)
		{
			diagnosticContext.Set("AccountID", claims?.FindFirst("acc")?.Value);
		}

		private static void FromHeaders(IDiagnosticContext diagnosticContext, IHeaderDictionary headers)
		{
			var _headers = headers.ToDictionary(x => x.Key, y => y.Value);
			diagnosticContext.Set("Referer", _headers?.GetValueOrDefault("Referer").ToString());
		}

		private static void FromRequest(IDiagnosticContext diagnosticContext, HttpRequest request)
		{
			diagnosticContext.Set("RequestHost", request.Host.Value);
			diagnosticContext.Set("QueryString", request.QueryString.ToString());
			diagnosticContext.Set("Path", request.Path.ToString());
			diagnosticContext.Set("ContentType", request.ContentType);
			diagnosticContext.Set("Host", request.Host);
			diagnosticContext.Set("IsHttps", request.IsHttps);
			diagnosticContext.Set("Scheme", request.Scheme);
			diagnosticContext.Set("Method", request.Method);
			diagnosticContext.Set("Protocol", request.Protocol);
			diagnosticContext.Set("Headers", request.Headers.Where(h => !h.Key.ToLowerInvariant().Equals("authorization")).ToDictionary(x => x.Key, y => y.Value.ToString()));
			diagnosticContext.Set("Query", request.Query.ToDictionary(x => x.Key, y => y.Value.ToString()));
		}

		public static string GetCorrelationId(this HttpContext httpContext)
		{
			httpContext.Request.Headers.TryGetValue("Request-ID", out StringValues correlationId);
			return correlationId.FirstOrDefault() ?? httpContext.TraceIdentifier;
		}
	}
}