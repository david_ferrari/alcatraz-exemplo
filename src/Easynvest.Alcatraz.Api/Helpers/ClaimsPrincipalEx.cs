﻿using System.Linq;
using System.Security.Claims;

namespace Easynvest.Alcatraz.Api.Helpers
{
    public static class ClaimsPrincipalEx
    {
        private const string CLAIM_TIPE_ESG = "esg";

        /// <summary>
        /// Recupera CPF do cliente do Token JWT
        /// </summary>
        /// <param name="claimsPrincipal"></param>
        /// <returns></returns>
        public static string GetUsername(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.FirstOrDefault(a => a.Type == ClaimTypes.Name)?.Value;
        }

        public static string GetElectronicSignature(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.FirstOrDefault(x => x.Type == CLAIM_TIPE_ESG)?.Value;
        }

        public static string GetAccountEmail(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.FirstOrDefault(a => a.Type == ClaimTypes.Email)?.Value;
        }

        /// <summary>
        /// Recupera o device_id presente no Token JWT
        /// </summary>
        /// <param name="claimsPrincipal"></param>
        /// <returns></returns>
        public static string GetDeviceId(this ClaimsPrincipal claimsPrincipal) => claimsPrincipal.Claims.FirstOrDefault(a => a.Type == "device_id")?.Value.ToUpper();
    }
}
