﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Easynvest.Alcatraz.Api.Helpers
{
    public static class JwtBearerAuthenticationHelper
    {
        public static IServiceCollection AddJwtBearer(this IServiceCollection services, IConfiguration config)
        {
            string issuer = config["JWTAuthentication:Issuer"];
            string audience = config["JWTAuthentication:Audience"];
            string secret = config["JWTAuthentication:Secret"];

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateActor = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = issuer,
                    ValidAudience = audience,
                    IssuerSigningKey = new SymmetricSecurityKey(WebEncoders.Base64UrlDecode(secret)),
                };
            });

            return services;
        }
    }
}
