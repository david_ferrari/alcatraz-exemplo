﻿using Easynvest.Alcatraz.Api.Constants;
using Easynvest.Alcatraz.Api.Extensions;
using Easynvest.Alcatraz.Api.Filters;
using Easynvest.Alcatraz.Api.Helpers;
using Easynvest.Alcatraz.CrossCutting.Service;
using Easynvest.Alcatraz.Infra.IoC;
using Easynvest.KingsCross.Extensions;
using Easynvest.PushNotification.FirebaseCloudMessage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO.Compression;

namespace Easynvest.Alcatraz.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
              .AddOptions(Configuration)
              .AddSingleton(Configuration)
              .AddKingsCross(Configuration)
              .AddJwtBearer(Configuration)
              .AddAuthorization(options =>
              {
                  options.AddPolicy(AuthorizationRoles.REQUIRED_INTERNAL_RECOVERY_AUTHORIZATION,
                      policy => policy.RequireRole(AuthorizationRoles.REQUIRED_INTERNAL_RECOVERY_AUTHORIZATION));
                  options.AddPolicy(AuthorizationRoles.REQUIRED_SECURITY_ROLE,
                      policy => policy.RequireRole(AuthorizationRoles.REQUIRED_SECURITY_ROLE));
              });

            services
                  .AddControllers(configuration => configuration.Filters.Add(typeof(HttpGlobalExceptionFilter)))
                  .AddNewtonsoftJson()
                  .AddJsonOptions(options => options.JsonSerializerOptions.IgnoreNullValues = true);

            services
                .AddApiVersioning(options => options.ReportApiVersions = true);

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);

            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
            });

            services.AddFirebaseCloudMessage(
                Configuration.GetSection("PushNotification:ServerKey").Value,
                Convert.ToBoolean(Configuration.GetSection("PushNotification:IsTeste").Value)
            );

            services
                    .AddSwagger()
                    .AddAlcatrazServices(Configuration)
                    .AddHealthCheck(Configuration)
                    .AddMassTransit();

            services.Configure<KestrelServerOptions>(options => { options.AllowSynchronousIO = true; });
            services.Configure<IISServerOptions>(options => { options.AllowSynchronousIO = true; });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseMiddleware<GCMiddleware>();

            if (Environment.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseMiddleware<RequestLogContextMiddleware>();
            app.UseSerilogRequestLogging(options => options.EnrichDiagnosticContext = (diagnosticContext, httpContext) => SerilogHttpContextEx.CustomEnrichLogic(diagnosticContext, httpContext));

            app
            .UseAuthentication()
            .UseRouting()
            .UseAuthorization()
            .UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.UseHealthChecks();
            });

            if (!Environment.IsProduction())
            {
                app.UseSwagger(c =>
                {
                    c.PreSerializeFilters.Add((apiDoc, httpReq) =>
                    {
                        apiDoc.Servers = new List<OpenApiServer>
                        {
                            new OpenApiServer {Url = $"{httpReq.Scheme}://{httpReq.Host.Value}", Description = "Development"},
                            new OpenApiServer {Url = $"{httpReq.Scheme}://{httpReq.Host.Value}/alcatraz", Description = "Kubernetes"}
                        };
                    });
                })
                .UseSwaggerUI(c => c.SwaggerEndpoint("v1/swagger.json", "v1"));
            }

            app.UseResponseCompression();
            ServiceActivator.Configure(app.ApplicationServices);
        }
    }
}
