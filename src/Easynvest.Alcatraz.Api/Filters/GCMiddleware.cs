﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Filters
{
    public class GCMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public GCMiddleware(RequestDelegate next, ILogger<GCMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S1215:\"GC.Collect\" should not be called", Justification = "Serilog Clean")]
        public async Task Invoke(HttpContext httpContext)
        {
            await _next(httpContext);

            try
            {
                GC.Collect(2, GCCollectionMode.Forced, true);
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Erro ao limpar memoria.");                
            }
        }
    }
}
