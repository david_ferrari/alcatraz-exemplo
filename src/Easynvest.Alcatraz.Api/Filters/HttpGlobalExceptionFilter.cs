﻿using Easynvest.Alcatraz.CrossCutting.Exceptions;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Easynvest.Alcatraz.Api.Filters
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        private readonly ILogger _logger;

        public HttpGlobalExceptionFilter(ILogger<HttpGlobalExceptionFilter> logger)
        {
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            switch (context.Exception)
            {
                case ServiceUnavailableException ex:
                    ManageException(context, HttpStatusCode.ServiceUnavailable);
                    break;
                case ForbiddenException ex:
                    ManageException(context, HttpStatusCode.Forbidden);
                    break;
                default:
                    ManageException(context, HttpStatusCode.InternalServerError);
                    break;
            }
        }

        private void ManageException(ExceptionContext context, HttpStatusCode httpStatusCode)
        {
            _logger.LogExError(context.Exception, context.Exception.Message);

            context.HttpContext.Response.StatusCode = (int)httpStatusCode;
            context.ExceptionHandled = true;
        }
    }
}
