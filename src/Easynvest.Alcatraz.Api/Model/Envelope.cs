﻿using System;
using System.Collections.Generic;

namespace Easynvest.Alcatraz.Api.Model
{
    public class Envelope<T>
    {
        public T Result { get; }
        public IEnumerable<string> ErrorMessages { get; }
        public DateTime TimeGenerated { get; }

        protected internal Envelope(T result) : this()
        {
            Result = result;
        }

        protected internal Envelope(T result, IEnumerable<string> errorMessages) : this()
        {
            Result = result;
            ErrorMessages = errorMessages;
        }

        protected internal Envelope(IEnumerable<string> errorMessages):this()
        {
            ErrorMessages = errorMessages;
        }

        protected internal Envelope(string errorMessage):this()
        {
            ErrorMessages = new HashSet<string> { errorMessage };
        }

        protected internal Envelope()
        {
            TimeGenerated = DateTime.UtcNow;
        }
    }
}
