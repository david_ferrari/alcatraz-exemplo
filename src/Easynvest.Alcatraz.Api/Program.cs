﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Exceptions.Core;
#pragma warning disable S1128 // Unused "using" should be removed
using Serilog.Formatting.Elasticsearch;
#pragma warning restore S1128 // Unused "using" should be removed
using System;
using System.IO;
using System.Reflection;

namespace Easynvest.Alcatraz.Api
{
    public class Program
    {
        protected Program()
        { }

        public static IConfiguration Configuration { get; } = BuildConfiguration();

        public static void Main(string[] args)
        {
            try
            {
                BuildWebHost(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Aplicação terminou inesperadamente");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                   .UseStartup<Startup>()
                   .UseConfiguration(Configuration)
                   .UseSerilog((host, loggerConfiguration) => CreateLoggerConfiguration(loggerConfiguration));

        private static void CreateLoggerConfiguration(LoggerConfiguration loggerConfiguration)
        {
            var assembly = Assembly.GetExecutingAssembly().GetName();

            loggerConfiguration
                .ReadFrom.Configuration(Configuration)
                .Enrich.FromLogContext()
                .Enrich.WithProperty("Assembly", $"{assembly.Name}")
                .Enrich.WithProperty("Version", $"{assembly.Version}")
                .Enrich.WithProperty("Jornada", "login")
                .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder().WithDefaultDestructurers().WithRootName("Exception"))
            #if DEBUG
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {Properties:j}{NewLine}{Exception}", restrictedToMinimumLevel: LogEventLevel.Debug);
            #else
                .WriteTo.Console(new ElasticsearchJsonFormatter(inlineFields: true));
            #endif
        }

        private static IConfiguration BuildConfiguration()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production";
            var appSettingsJson = $"appsettings.{env}.json";

            return new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true)
                        .AddJsonFile(appSettingsJson, optional: true, reloadOnChange: true)
                        .AddEnvironmentVariables()
                        .Build();
        }
    }
}