﻿namespace Easynvest.Alcatraz.Api.Constants
{
    internal static class AuthorizationRoles
    {
        internal const string REQUIRED_INTERNAL_RECOVERY_AUTHORIZATION = "GRP_EASYNVEST_API_ALCATRAZ_INTERNAL_RECOVERY";

        internal const string REQUIRED_SECURITY_ROLE = "GRP_EASYNVEST_API_ALCATRAZ_BLOCK_USER";
    }
}
