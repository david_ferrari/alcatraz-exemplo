﻿using Easynvest.Alcatraz.Api.Constants;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Domain.Enumerators;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/internal/userBlocks")]
    [Produces("application/json")]
    public class UserBlockController : Controller
    {
        private readonly IMediator _mediator;

        public UserBlockController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("{type}")]
        [Authorize(Policy = AuthorizationRoles.REQUIRED_SECURITY_ROLE)]
        public async Task<IActionResult> Get([FromQuery]BlockType type)
        {
            var response = await _mediator.Send(new GetBlockedUsersQuery(type));

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response.Value);
        }

        [HttpPost]
        [Route("{customerId}/{blockType}")]
        [Authorize(Policy = AuthorizationRoles.REQUIRED_SECURITY_ROLE)]
        public async Task<IActionResult> Post(string customerId, BlockType blockType)
        {
            var response = await _mediator.Send(new BlockUserCommand(customerId, blockType));

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response.Value);
        }

        [HttpDelete]
        [Route("{customerId}/{blockType}")]
        [Authorize(Policy = AuthorizationRoles.REQUIRED_SECURITY_ROLE)]
        public async Task<IActionResult> Delete(string customerId, BlockType blockType)
        {
            var response = await _mediator.Send(new UnblockUserCommand(customerId, blockType));

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response.Value);
        }

        [HttpPost]
        [Route("refresh/{blockType}")]
        public async Task<IActionResult> Refresh(BlockType blockType)
        {
            var response = await _mediator.Send(new RefreshBlocksCommand(blockType));

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }
    }
}
