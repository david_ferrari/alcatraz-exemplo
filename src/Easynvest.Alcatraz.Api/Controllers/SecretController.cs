﻿using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Produces("application/json")]
    [Route("apis/internal/{controller}")]
    public class SecretController : Controller
    {
        private readonly IMediator _mediator;

        public SecretController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost, AllowAnonymous]
        [ProducesResponseType(typeof(Response), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Post([FromBody] CreateSecretCommand command)
        {
            if (command is null)
                return BadRequest();

            var response = await _mediator.Send(command);
            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }
    }
}
