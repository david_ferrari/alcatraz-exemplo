﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Requests;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/v{api-version:apiVersion}/customer-authentication-token")]
    [ApiController]
    [ApiVersion("1.0")]
    public class CustomerAuthenticationEasyTokenController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CustomerAuthenticationEasyTokenController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Response<ValidateAuthenticatedCustomerTokenResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("authenticate")]
        public async Task<IActionResult> Authenticate(ValidateAuthenticatedCustomerTokenRequest request)
        {
            if (request == null)
                return BadRequest();

            var query = new ValidateAuthenticatedCustomerTokenQuery(request.Username, request.Password, request.FcmToken, request.DeviceUid, request.OtpCode, HttpContext.Connection.RemoteIpAddress.ToString(), request.TrustedDevice);

            var response = await _mediator.Send(query);
            if (response.IsFailure)
            {
                var errorStatusCode = StatusCodes.Status400BadRequest;
                if (response.ErrorResponse.Error != null && response.ErrorResponse.Error.StatusCode != 0)
                    errorStatusCode = response.ErrorResponse.Error.StatusCode;

                return StatusCode(errorStatusCode, response.ErrorResponse);
            }

            return Ok(response);
        }
    }
}