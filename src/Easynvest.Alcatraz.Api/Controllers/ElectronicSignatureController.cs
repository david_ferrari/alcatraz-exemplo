﻿using Easynvest.Alcatraz.Api.Constants;
using Easynvest.Alcatraz.Api.Helpers;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Commands.Request;
using Easynvest.Alcatraz.Application.Responses;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/electronicsignature")]
    [Produces("application/json")]
    public class ElectronicSignatureController : Controller
    {
        private readonly IMediator _mediator;

        public ElectronicSignatureController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet, Authorize]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("validate")]
        public async Task<IActionResult> ValidateElectronicSignature([FromQuery] string electronicSignature)
        {
            var command = new ValidateElectronicSignatureCommand(electronicSignature, User.GetUsername());

            var response = await _mediator.Send(command);

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost, Authorize]
        [ProducesResponseType(typeof(Response<ValidateElectronicSignatureResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("validate")]
        public async Task<IActionResult> ValidateElectronicSignature([FromBody] ValidateElectronicSignatureRequest request)
        {
            var command = new ValidateElectronicSignatureCommand(request?.ElectronicSignature, User.GetUsername());

            var response = await _mediator.Send(command);

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost, Authorize]
        [ProducesResponseType(typeof(ValidateElectronicSignatureAndDeviceResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("validate/device")]
        public async Task<IActionResult> ValidateElectronicSignatureAndDevice([FromBody] ValidateElectronicSignatureRequest request)
        {
            var command = new ValidateElectronicSignatureAndDeviceCommand(request?.ElectronicSignature, User.GetUsername(), User.GetDeviceId());

            var response = await _mediator.Send(command);

            if (response.IsFailure)
                return StatusCode(response.ErrorResponse.Error.StatusCode, response.ErrorResponse);


            return Ok(response);
        }

        //Serviço utilizado pelo ws titulo (não tem token)
        [HttpPost, AllowAnonymous]
        [ProducesResponseType(typeof(Response<ValidateElectronicSignatureResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("internal/validate")]
        public async Task<IActionResult> InternalValidateElectronicSignature([FromBody] ValidateElectronicSignatureCommand command)
        {
            var response = await _mediator.Send(command);

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPatch, Authorize]
        [ProducesResponseType(typeof(Response<ChangeLoggedPasswordResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<ChangeLoggedPasswordResponse>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Patch([FromBody] ChangeElectronicSignatureRequest request)
        {
            var command = new ChangeElectronicSignatureCommand
                (
                    request,
                    User.GetUsername()
                );

            var response = await _mediator.Send(command);

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost, Authorize]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("recovery")]
        public async Task<IActionResult> Recovery()
        {
            var accountEmail = User.GetAccountEmail();
            var userName = User.GetUsername();

            if (string.IsNullOrEmpty(accountEmail) || string.IsNullOrEmpty(userName))
                return Unauthorized();

            var command = new RequestHashElectronicSignatureRecoveryCommand(accountEmail, userName);

            var response = await _mediator.Send(command);

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost]
        [Authorize(Policy = AuthorizationRoles.REQUIRED_INTERNAL_RECOVERY_AUTHORIZATION)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("internalrecovery")]
        public async Task<IActionResult> Recovery([FromBody] RecoveryForgottenElectronicSignatureRequest request)
        {
            var command = new RequestHashElectronicSignatureRecoveryCommand(request.AccountEmail, request.UserName);

            var response = await _mediator.Send(command);
            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ChangeForgottenElectronicSignatureCommand command)
        {
            if (command is null)
                return BadRequest();

            var response = await _mediator.Send(command);
            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateElectronicSignatureCommand command)
        {
            if (command is null)
                return BadRequest();

            var response = await _mediator.Send(command);
            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }
    }
}
