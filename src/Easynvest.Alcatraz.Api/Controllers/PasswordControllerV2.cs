﻿using Easynvest.Alcatraz.Application.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Authorize]
    [ApiVersion("2.0"), Route("apis/v{api-version:apiVersion}/password")]
    public class PasswordControllerV2 : Controller
    {
        private readonly IMediator _mediator;

        public PasswordControllerV2(IMediator mediator) => _mediator = mediator;

        [HttpPut]
        [AllowAnonymous]
        public async Task<IActionResult> Put([FromBody] ChangeForgottenPasswordCommand command)
        {
            var response = await _mediator.Send(command);
            if (response.IsFailure)
                return BadRequest(response);

            var authenticationResponse = await _mediator.Send(new AuthenticateWithChangedPasswordCommand(command.CustomerId, command.SecretCandidate, command.ClientId));

            if (response.IsFailure)
                return BadRequest(authenticationResponse);

            return Ok(authenticationResponse.Value);
        }
    }
}
