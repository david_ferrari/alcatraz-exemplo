﻿using Easynvest.Alcatraz.Api.Helpers;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Requests;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Responses;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/customer-authentication")]
    public class CustomerAuthenticationController : Controller
    {
        private readonly IMediator _mediator;

        public CustomerAuthenticationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticatedCustomerRequest request)
        {
            if (request == null)
                return BadRequest();

            var query = new ValidateAuthenticatedCustomerQuery(request.UserName, request.Password, request.FcmToken, HttpContext.Connection.RemoteIpAddress.ToString());

            var response = await _mediator.Send(query);

            if (!response.Authenticated
                && new string[] {
                    ValidateAuthenticateType.InvalidCredential.Value,
                    ValidateAuthenticateType.BlockedUser.Value,
                    ValidateAuthenticateType.DisabledUser.Value,
                    ValidateAuthenticateType.Err_AuthenticateMode_Not_Found.Value,
                    ValidateAuthenticateType.Err_Authenticate_Fail.Value,
                    "ERROR"
                }.Any(response.AuthenticatedMessages.ContainsKey))
            {
                return BadRequest(Response<ValidateAuthenticatedCustomerResponse>.Fail(response, response.AuthenticatedMessages.Values));
            }

            return Ok(Response<ValidateAuthenticatedCustomerResponse>.Ok(response));
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("refresh")]
        public async Task<IActionResult> Refresh()
        {
            var query = new RefreshAuthenticatedCustomerQuery(User.GetUsername());

            var response = await _mediator.Send(query);

            if (!response.Authenticated)
                return BadRequest(Response<ValidateAuthenticatedCustomerResponse>.Fail(response, response.AuthenticatedMessages.Values));

            return Ok(Response<ValidateAuthenticatedCustomerResponse>.Ok(response));
        }
    }
}
