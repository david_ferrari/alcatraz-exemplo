﻿using System.Net;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Api.Helpers;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Commands.Request;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Easynvest.Alcatraz.Api.Controllers
{
	[Authorize]
	[Route("apis/password")]
	public class PasswordController : Controller
	{
		private readonly IMediator _mediator;

		public PasswordController(IMediator mediator)
		{
			_mediator = mediator;
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> Post([FromBody] CreatePasswordCommand command)
		{
			var response = await _mediator.Send(command);
			if (response.IsFailure)
				return BadRequest(response);

			return Ok(response);
		}

		[HttpPut]
		[AllowAnonymous]
		public async Task<IActionResult> Put([FromBody] ChangeForgottenPasswordCommand command)
		{
			var response = await _mediator.Send(command);
			if (response.IsFailure)
				return BadRequest(response);

			return Ok(response);
		}

		[HttpPatch, Authorize]
		[ProducesResponseType(typeof(string[]), (int)HttpStatusCode.OK)]
		[ProducesResponseType(typeof(string[]), (int)HttpStatusCode.BadRequest)]
		[ProducesResponseType(typeof(void), (int)HttpStatusCode.Unauthorized)]
		[ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
		public async Task<IActionResult> Patch([FromBody] ChangeLoggedPasswordRequest request)
		{
			if (request == null)
				return BadRequest();

			var customerId = User.GetUsername();

			var command = new ChangeLoggedPasswordCommand
				(
					request,
					customerId
				);

			var response = await _mediator
				.Send(command);

			if (response.IsFailure)
				return BadRequest(response);

			return Ok(response);
		}
	}
}
