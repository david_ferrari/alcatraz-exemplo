﻿using Easynvest.Alcatraz.Api.Helpers;
using Easynvest.Alcatraz.Application.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/trustedDevice")]
    [Produces("application/json")]
    public class TrustedDeviceController : Controller
    {
        private readonly IMediator _mediator;

        public TrustedDeviceController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddTrustedDeviceCommand command)
        {
            command.CustomerId = User.GetUsername();
            var result = await _mediator.Send(command);
            if (result.IsFailure)
                return BadRequest(result.ErrorResponse);

            return Ok();
        }
    }
}
