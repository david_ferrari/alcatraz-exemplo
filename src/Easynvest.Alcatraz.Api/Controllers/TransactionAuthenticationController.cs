﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Commands.Request;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/v{api-version:apiVersion}/authenticate-transaction")]
    [ApiController]
    [ApiVersion("1.0")]
    public class TransactionAuthenticationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TransactionAuthenticationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Authenticate([FromBody]TransactionAuthenticationRequest request)
        {
            var requestId = GetRequestId();
            var accountId = User.Identity.Name;
            var command = new AuthenticateTransactionCommand(requestId, accountId, request.OtpCode, request.DeviceUid, request.Integration);
            var response = await _mediator.Send(command);

            if (response.IsFailure)
                return BadRequest(response.Messages);

            return Ok();
        }
        private string GetRequestId()
        {
            var headerRequestId = Request
                .Headers
                .Keys
                .FirstOrDefault(it =>
                    it.Equals("x-requestid", StringComparison.InvariantCultureIgnoreCase) ||
                    it.Equals("request-id", StringComparison.InvariantCultureIgnoreCase));

            return headerRequestId ?? Guid.NewGuid().ToString();
        }
    }
}