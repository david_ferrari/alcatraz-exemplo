﻿using System.Net;
using System.Threading.Tasks;
using Easynvest.Alcatraz.Application.Commands;
using Easynvest.Alcatraz.Application.Query.Queries;
using Easynvest.Alcatraz.Application.Query.Responses;
using Easynvest.Alcatraz.Application.Requests;
using Easynvest.Alcatraz.Application.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/v{api-version:apiVersion}/code-authentication")]
    [ApiController]
    [ApiVersion("1.0")]
    public class CodeAuthenticationController : Controller
    {
        private readonly IMediator _mediator;

        public CodeAuthenticationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(SendAuthenticationCodeResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(SendAuthenticationCodeResponse), (int)HttpStatusCode.OK)]
        [Route("send")]
        public async Task<IActionResult> SendCode([FromBody] SendAuthenticationCodeRequest request)
        {
            var response = await _mediator.Send(new SendAuthenticationCodeCommand(request.Username, request.Password, request.FcmToken, HttpContext.Connection.RemoteIpAddress.ToString()));
            if (response.IsSuccess)
                return Ok();

            return BadRequest(response.ErrorResponse);
        }

        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(ValidateAuthenticationCodeResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ValidateAuthenticationCodeResponse), (int)HttpStatusCode.OK)]
        [Route("validate")]
        public async Task<IActionResult> Validate([FromBody] ValidateAuthenticationCodeQuery request)
        {
            request = new ValidateAuthenticationCodeQuery(request.Username, request.Password, request.TemporaryCode, request.FcmToken, HttpContext.Connection.RemoteIpAddress.ToString(), request.DeviceUid);
            var response = await _mediator.Send(request);
            if (response.IsSuccess)
                return Ok(response);

            return BadRequest(response.ErrorResponse);
        }
    }
}