﻿using Easynvest.Alcatraz.Application.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Produces("application/json")]
    [Route("apis/hash-password")]
    public class PasswordRecoveryController : Controller
    {
        private readonly IMediator _mediator;

        public PasswordRecoveryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("recovery")]
        public async Task<IActionResult> Recovery([FromBody] RequestHashPasswordRecoveryCommand command)
        {
            if (command == null)
                return BadRequest();

            var response = await _mediator.Send(command).ConfigureAwait(false);
            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }

        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody] RequestHashPasswordCreateCommand command)
        {
            if (command == null)
                return BadRequest();

            var response = await _mediator.Send(command).ConfigureAwait(false);
            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response);
        }
    }
}
