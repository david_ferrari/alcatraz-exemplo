﻿using Easynvest.Alcatraz.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Api.Controllers
{
    [Route("apis/[controller]")]
    public class HashController : Controller
    {

        private readonly IMediator _mediator;

        public HashController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromQuery]string value)
        {
            var response = await _mediator.Send(new ValidHashQuery(value));

            if (response.IsFailure)
                return BadRequest(response);
            
            return Ok();
        }
    }
}