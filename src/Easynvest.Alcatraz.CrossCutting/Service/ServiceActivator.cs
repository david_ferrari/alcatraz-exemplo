﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Easynvest.Alcatraz.CrossCutting.Service
{
    public class ServiceActivator
    {
        protected ServiceActivator()
        {

        }

        internal static IServiceProvider _serviceProvider { get; private set; }

        public static void Configure(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public static IService GetService<IService>(IServiceProvider serviceProvider = null)
        {
            var provider = serviceProvider ?? _serviceProvider;
            return provider.GetService<IService>();
        }
    }
}
