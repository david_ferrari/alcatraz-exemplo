﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.CrossCutting.Extensions
{
    public static class SerilogExtension
    {
        public static void LogExDebug(this ILogger logger, string message, params object[] args) =>
            Task.Run(async () => await LogEx(logger, LogLevel.Debug, message, null, args));

        public static void LogExInformation(this ILogger logger, string message, params object[] args) =>
            Task.Run(async () => await LogEx(logger, LogLevel.Information, message, null, args).ConfigureAwait(false));

        public static void LogExWarning(this ILogger logger, string message, params object[] args) =>
            Task.Run(async () => await LogEx(logger, LogLevel.Warning, message, null, args));

        public static void LogExWarning(this ILogger logger, Exception ex, string message, params object[] args) =>
            Task.Run(async () => await LogEx(logger, LogLevel.Warning, message, ex, args));

        public static void LogExError(this ILogger logger, string message, params object[] args) =>
            Task.Run(async () => await LogEx(logger, LogLevel.Error, message, null, args));

        public static void LogExError(this ILogger logger, Exception ex, string message, params object[] args) =>
            Task.Run(async () => await LogEx(logger, LogLevel.Error, message, ex, args));

        public static async Task LogEx(this ILogger logger, LogLevel level, string message, Exception ex, params object[] args)
        {
            await Task.Run(() =>
            {
                if (level.Equals(LogLevel.Error) || ex != null)
                    logger.Log(level, ex, message, args);
                else
                    logger.Log(level, message, args);
            });
        }
    }
}
