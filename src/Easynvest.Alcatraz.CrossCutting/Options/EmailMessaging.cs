﻿namespace Easynvest.Alcatraz.CrossCutting.Options
{
    public class EmailMessaging
    {
        public readonly string RoutingKey = "Internal";

        public string Exchange { get; set; }
        public string Queue { get; set; }
    }
}
