﻿namespace Easynvest.Alcatraz.CrossCutting.Options
{
    public class EmailTemplateOption
    {
        public int ChangedLoggedElectronicSignature { get; set; }
        public int ChangedLoggedPassword { get; set; }
    }
}
