﻿namespace Easynvest.Alcatraz.CrossCutting
{
    public class AuthOptions
    {
        public string BaseEndpoint { get; set; }

        public string AuthenticateEndpoint { get; set; }
    }
}
