﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.CrossCutting.Options
{
    [ExcludeFromCodeCoverage]
    public class AuthCodeOptions
    {
        public int EmailId { get; set; }

        public int ExpirationInMinutes { get; set; }
    }
}
