﻿namespace Easynvest.Alcatraz.CrossCutting.Options
{
    public class AuthenticationOption
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string Secret { get; set; }
    }
}
