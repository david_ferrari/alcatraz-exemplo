﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.CrossCutting.Options
{
    [ExcludeFromCodeCoverage]
    public class HttpPolicySettingsOptions
    {
        public int NumberOfExceptionsAllowedBeforeBreaking { get; set; }
        public int DurationOfBreakInSeconds { get; set; }
        public int TotalResponseTimeoutInSeconds { get; set; }
        public int HttpRequestTimeoutInSeconds { get; set; }
        public int RetryCount { get; set; }
        public int DelayBetweenRetries { get; set; }
    }
}
