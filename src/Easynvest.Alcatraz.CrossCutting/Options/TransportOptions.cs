﻿namespace Easynvest.Alcatraz.CrossCutting.Options
{
    public class TransportOptions
    {
        public string Host { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
