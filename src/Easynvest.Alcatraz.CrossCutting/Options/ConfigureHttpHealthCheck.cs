﻿using System;

namespace Easynvest.Alcatraz.CrossCutting.Options
{
    public class ConfigureHttpHealthCheck
    {
        public int TimeoutPerRequesttrarFromMilliseconds { get; set; }

        public TimeSpan TimeoutPerRequest => TimeSpan.FromMilliseconds(TimeoutPerRequesttrarFromMilliseconds);
    }
}
