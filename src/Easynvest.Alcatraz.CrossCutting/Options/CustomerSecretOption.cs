﻿namespace Easynvest.Alcatraz.CrossCutting
{
    public class CustomerSecretOption
    {
        public int MaxInvalidLoginAttempts { get; set; }

        public int ExpirationTimeInDay { get; set; }
    }
}
