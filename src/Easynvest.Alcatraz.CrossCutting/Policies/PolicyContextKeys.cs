﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.CrossCutting.Policies
{
    [ExcludeFromCodeCoverage]
    public static class PolicyContextKeys
    {
        public const string LOGGER_KEY = "logger";
        public const string CUSTOME_ID_KEY = "customerId";
    }
}
