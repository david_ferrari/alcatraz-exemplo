﻿using Easynvest.Alcatraz.CrossCutting.Extensions;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using Polly;
using Polly.Timeout;
using System;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.CrossCutting.Policies
{
    public class PolicyRulesService: IPolicyRulesService
    {
        public IAsyncPolicy GetPolicesOracle()
        {            
            var timeoutTryPolicy = Policy.TimeoutAsync(30,
                TimeoutStrategy.Pessimistic,
                OnTimeoutAsync);

            var circuitBreakerPolicy = Policy.Handle<OracleException>()
                .CircuitBreakerAsync(
                    3,
                    TimeSpan.FromSeconds(5),
                    OnBreak,
                    OnReset
                )
                .WrapAsync(timeoutTryPolicy);

            var waitAndRetryPolicy = Policy.Handle<OracleException>()
                .WaitAndRetryAsync(
                    3,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    onRetry: OnRetry
                )
                .WrapAsync(circuitBreakerPolicy);

            return waitAndRetryPolicy;
        }

        public IAsyncPolicy GetPolicesOracleWithoutContext(string query)
        {
            var timeoutTryPolicy = Policy.TimeoutAsync(30,
               TimeoutStrategy.Pessimistic,
               (context, timeSpan, task) =>
               {
                   Serilog.Log.Warning("TimeoutAsync acionado na query {Query} com oracle: Espera: {WaitTime} seg.", query, timeSpan.TotalSeconds);
                   return Task.CompletedTask;
               });

            var circuitBreakerPolicy = Policy.Handle<OracleException>()
                .CircuitBreakerAsync(
                    3,
                    TimeSpan.FromSeconds(5),
                    (exception, timespan, context)
                    =>
                    {
                        Serilog.Log.Warning(exception, "CircuitBreaker: o oracle está fora!");
                    },
                    (context) => Serilog.Log.Warning("CircuitBreaker: o repositório está de volta!")
                )
                .WrapAsync(timeoutTryPolicy);

            var waitAndRetryPolicy = Policy.Handle<OracleException>()
                .WaitAndRetryAsync(
                    3,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    onRetry: (exception, timeSpan, retries, context)
                    =>
                    {
                        Serilog.Log.Warning(exception, "WaitAndRetry on Oracle: query {Query} Tentativa {RetryCount}, Espera: {WaitTime} seg.", query, retries, timeSpan.TotalSeconds);
                    }
                )
                .WrapAsync(circuitBreakerPolicy);

            return waitAndRetryPolicy;
        }

        private void OnBreak(Exception exception, TimeSpan timespan, Context context)
        {
            var logger = context[PolicyContextKeys.LOGGER_KEY] as ILogger;
            logger.LogExWarning(exception, "CircuitBreaker: o repositório está fora!");
        }

        private void OnReset(Context context)
        {
            var logger = context[PolicyContextKeys.LOGGER_KEY] as ILogger;
            logger.LogExWarning("CircuitBreaker: o repositório está de volta!");
        }

        private void OnRetry(Exception exception, TimeSpan timeSpan, int retries, Context context)
        {
            string customerId = string.Empty;
            var logger = context[PolicyContextKeys.LOGGER_KEY] as ILogger;

            if (context.ContainsKey(PolicyContextKeys.CUSTOME_ID_KEY))
                customerId = context[PolicyContextKeys.CUSTOME_ID_KEY] as string;

            if (string.IsNullOrEmpty(customerId))
                logger.LogExWarning(exception, "WaitAndRetry: Tentativa {RetryCount}, Espera: {WaitTime} seg.", retries, timeSpan.TotalSeconds);
            else
                logger.LogExWarning(exception, "WaitAndRetry: Tentativa {RetryCount}, Espera: {WaitTime} seg., cliente {CustomerId}", retries, timeSpan.TotalSeconds, customerId);
        }

        private async Task OnTimeoutAsync(Context context, TimeSpan timeSpan, Task task)
        {
            await task.ContinueWith(t =>
            {
                string customerId = string.Empty;

                var logger = context[PolicyContextKeys.LOGGER_KEY] as ILogger;

                if (context.ContainsKey(PolicyContextKeys.CUSTOME_ID_KEY))
                    customerId = context[PolicyContextKeys.CUSTOME_ID_KEY] as string;

                if (string.IsNullOrEmpty(customerId))
                    logger.LogExWarning("TimeoutAsync acionado: Espera: {WaitTime} seg.", timeSpan.TotalSeconds);
                else
                    logger.LogExWarning("TimeoutAsync acionado: Espera: {WaitTime} seg., cliente {CustomerId}", timeSpan.TotalSeconds, customerId);
            });
        }
    }

    public interface IPolicyRulesService
    {
        IAsyncPolicy GetPolicesOracle();
        IAsyncPolicy GetPolicesOracleWithoutContext(string query);
    }
}
