﻿using System;

namespace Easynvest.Alcatraz.CrossCutting.Exceptions
{
    public class ServiceUnavailableException : Exception
    {
        public ServiceUnavailableException(string message)
            : base(message)
        {

        }

        public ServiceUnavailableException(string message, Exception ex)
            : base(message, ex)
        {

        }

        public ServiceUnavailableException() : base()
        {
        }

        protected ServiceUnavailableException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }
    }
}
