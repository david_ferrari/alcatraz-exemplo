﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easynvest.Alcatraz.CrossCutting.Exceptions
{
    public class BopeApiCallException : Exception
    {
        public BopeApiCallException()
        { }

        public BopeApiCallException(string message): base(message)
        { }

        public BopeApiCallException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
