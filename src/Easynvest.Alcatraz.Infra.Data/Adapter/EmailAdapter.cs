﻿using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Models.Entities;
using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.Adapter
{
    [ExcludeFromCodeCoverage]
    internal static class EmailAdapter
    {
        internal static MailData ToMailData(this TemplateEmail email)
        {
            return new MailData
            {
                Content = email.Content,
                From = email.From,
                Subject = email.Subject,
                To = email.To
            };

        }
    }
}
