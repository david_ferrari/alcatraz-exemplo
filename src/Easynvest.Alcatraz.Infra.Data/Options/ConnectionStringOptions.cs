﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.Options
{
    [ExcludeFromCodeCoverage]
    public class ConnectionStringOptions
    {
        public string ConnectionString { get; set; }
    }
}
