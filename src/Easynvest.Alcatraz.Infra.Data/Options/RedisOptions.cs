﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Caching.Redis;

namespace Easynvest.Alcatraz.Infra.Data.Options
{
    [ExcludeFromCodeCoverage]
    public class RedisOptions
    {
        public string ConnectionString { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class WriteRedisOptions : RedisCacheOptions
    {
    }

    [ExcludeFromCodeCoverage]
    public class ReadRedisOptions : RedisCacheOptions
    {
    }
}