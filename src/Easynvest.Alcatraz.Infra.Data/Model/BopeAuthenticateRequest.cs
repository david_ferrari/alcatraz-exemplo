﻿using Easynvest.Alcatraz.Domain.Models.ValueObjects;

namespace Easynvest.Alcatraz.Infra.Data.Model
{
    internal class BopeAuthenticateRequest
    {
        public BopeAuthenticateRequest(string accountId, string otpCode, string deviceUid, BopeIntegrationType integration)
        {
            AccountId = accountId;
            OtpCode = otpCode;
            DeviceUid = deviceUid;
            Integration = integration;
        }

        public string AccountId { get; }
        public string OtpCode { get; }
        public string DeviceUid { get; }
        public BopeIntegrationType Integration { get; }
    }
}
