﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Major Code Smell", "S2436:Types and methods should not have too many generic parameters", Justification = "<Pending>", Scope = "member", Target = "~M:Easynvest.Alcatraz.Infra.Data.Repositories.SqlMapperQueryPolly.QueryAsync``4(System.Data.IDbConnection,System.Boolean,System.String,System.Func{``0,``1,``2,``3},System.Object,System.Data.IDbTransaction,System.Boolean,System.String,System.Nullable{System.Int32},System.Nullable{System.Data.CommandType})~System.Threading.Tasks.Task{System.Collections.Generic.IEnumerable{``3}}")]
