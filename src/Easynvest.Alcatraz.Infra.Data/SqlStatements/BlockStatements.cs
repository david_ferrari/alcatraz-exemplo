﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
    [ExcludeFromCodeCoverage]
    internal static class BlockStatements
    {
        internal const string InsertUserBlock = @"
			INSERT INTO 
				AUTH_DATA.USER_BLOCKS
				(
					CUSTOMERID,
					BLOCK_DATE,
					BLOCK_TYPE
				)
			VALUES
				(
					:CustomerId,
					:BlockDate,
					:BlockType
				)
		";

        internal const string RemoveUserBlock = @"
			DELETE FROM
				AUTH_DATA.USER_BLOCKS
				WHERE CUSTOMERID = :customerId
				AND BLOCK_TYPE = :blockType
		";

        internal const string GetBlocksByType = @"
			SELECT CUSTOMERID as CustomerId, BLOCK_TYPE as BlockType, BLOCK_DATE as BlockDate FROM
				AUTH_DATA.USER_BLOCKS
				WHERE BLOCK_TYPE = :blockType
		";

        internal const string GetUserBlocks = @"
			SELECT CUSTOMERID as CustomerId, BLOCK_TYPE as BlockType, BLOCK_DATE as BlockDate FROM
				AUTH_DATA.USER_BLOCKS WHERE CUSTOMERID = :CustomerId
		";
    }
}
