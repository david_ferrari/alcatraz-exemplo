﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
    [ExcludeFromCodeCoverage]
    public static class SqlAuthCustomerUsers
    {
        /// <summary>
        /// Autentica usuário na plataforma através do CPF
        /// </summary>
        public const string AuthenticateLoginWithCustomizableUsername = @"
            SELECT
                VW_CUSTOMER_USERS.ID
                ,VW_CUSTOMER_USERS.CUSTOMERID
                ,VW_CUSTOMER_USERS.USERNAME
                ,VW_CUSTOMER_USERS.NAME
                ,VW_CUSTOMER_USERS.BIRTHDATE
                ,VW_CUSTOMER_USERS.EMAIL
                ,VW_CUSTOMER_USERS.ACCOUNTNUMBER
                ,PASSWORDS.ELETRONICSIGNATURE
                ,PASSWORDS.INVALIDLOGINATTEMPTS
                ,PASSWORDS.PASSWORD ENCODEDPASSWORD
                ,UPPER(VW_CUSTOMER_USERS.STATUS) AS STATUS
                ,VW_CUSTOMER_USERS.CREATIONDATE
                ,VW_CUSTOMER_USERS.LASTMODIFICATIONDATE
                ,VW_CUSTOMER_USERS.ADVISORCODE
                ,FCM_TOKEN.FCM_TOKEN AS FCMTOKEN
                ,FCM_TOKEN.LAST_ACCESS_DATE AS LASTACCESSDATE
                ,VW_CUSTOMER_USERS.PUBLICID
                ,VW_CUSTOMER_USERS.ACCESSVERSION
                ,VW_CUSTOMER_USERS.ACCOUNTNUMBER AS MAINACCOUNTNUMBER
            FROM AUTH_DATA.VW_CUSTOMER_USERS VW_CUSTOMER_USERS
            INNER JOIN AUTH_DATA.PASSWORDS PASSWORDS ON PASSWORDS.CUSTOMERID = VW_CUSTOMER_USERS.CUSTOMERID
            LEFT JOIN AUTH.ACCESS_TOKEN FCM_TOKEN ON FCM_TOKEN.CUSTOMER_ID = VW_CUSTOMER_USERS.CUSTOMERID
            WHERE VW_CUSTOMER_USERS.USERNAME = :username
        ";


        /// <summary>
        /// Autentica usuário na plataforma através do CPF
        /// </summary>
        public const string AuthenticateLoginWithCpf = @"
            SELECT
                 VW_CUSTOMER_USERS.ID
                ,VW_CUSTOMER_USERS.CUSTOMERID
                ,VW_CUSTOMER_USERS.USERNAME
                ,VW_CUSTOMER_USERS.NAME
                ,VW_CUSTOMER_USERS.BIRTHDATE
                ,VW_CUSTOMER_USERS.EMAIL
                ,VW_CUSTOMER_USERS.ACCOUNTNUMBER
                ,PASSWORDS.ELETRONICSIGNATURE
                ,PASSWORDS.INVALIDLOGINATTEMPTS
                ,PASSWORDS.PASSWORD ENCODEDPASSWORD
                ,UPPER(VW_CUSTOMER_USERS.STATUS) AS STATUS
                ,VW_CUSTOMER_USERS.CREATIONDATE
                ,VW_CUSTOMER_USERS.LASTMODIFICATIONDATE
                ,VW_CUSTOMER_USERS.ADVISORCODE
                ,VW_CUSTOMER_USERS.ACCESSVERSION
                ,FCM_TOKEN.FCM_TOKEN AS FCMTOKEN
                ,FCM_TOKEN.LAST_ACCESS_DATE AS LASTACCESSDATE
                ,VW_CUSTOMER_USERS.PUBLICID
                ,VW_CUSTOMER_USERS.ACCOUNTNUMBER AS MAINACCOUNTNUMBER
            FROM AUTH_DATA.VW_CUSTOMER_USERS VW_CUSTOMER_USERS
            INNER JOIN AUTH_DATA.PASSWORDS PASSWORDS ON PASSWORDS.CUSTOMERID = VW_CUSTOMER_USERS.CUSTOMERID
            LEFT JOIN AUTH.ACCESS_TOKEN FCM_TOKEN ON FCM_TOKEN.CUSTOMER_ID = VW_CUSTOMER_USERS.CUSTOMERID
            WHERE VW_CUSTOMER_USERS.CUSTOMERID = :value
        ";

        /// <summary>
        /// Autentica usuário na plataforma através do EMAIL
        /// </summary>
        public const string AuthenticateLoginWithEmail = @"
            SELECT
                VW_CUSTOMER_USERS.ID
                ,VW_CUSTOMER_USERS.CUSTOMERID
                ,VW_CUSTOMER_USERS.USERNAME
                ,VW_CUSTOMER_USERS.NAME
                ,VW_CUSTOMER_USERS.BIRTHDATE
                ,VW_CUSTOMER_USERS.EMAIL
                ,VW_CUSTOMER_USERS.ACCOUNTNUMBER
                ,PASSWORDS.ELETRONICSIGNATURE
                ,PASSWORDS.INVALIDLOGINATTEMPTS
                ,PASSWORDS.PASSWORD ENCODEDPASSWORD
                ,UPPER(VW_CUSTOMER_USERS.STATUS) AS STATUS
                ,VW_CUSTOMER_USERS.CREATIONDATE
                ,VW_CUSTOMER_USERS.LASTMODIFICATIONDATE
                ,VW_CUSTOMER_USERS.ADVISORCODE
                ,FCM_TOKEN.FCM_TOKEN AS FCMTOKEN
                ,FCM_TOKEN.LAST_ACCESS_DATE AS LASTACCESSDATE
                ,VW_CUSTOMER_USERS.PUBLICID
                ,VW_CUSTOMER_USERS.ACCESSVERSION
                ,VW_CUSTOMER_USERS.ACCOUNTNUMBER AS MAINACCOUNTNUMBER
            FROM AUTH_DATA.VW_CUSTOMER_USERS VW_CUSTOMER_USERS
            INNER JOIN AUTH_DATA.PASSWORDS PASSWORDS ON PASSWORDS.CUSTOMERID = VW_CUSTOMER_USERS.CUSTOMERID
            LEFT JOIN AUTH.ACCESS_TOKEN FCM_TOKEN ON FCM_TOKEN.CUSTOMER_ID = VW_CUSTOMER_USERS.CUSTOMERID
            WHERE UPPER(VW_CUSTOMER_USERS.EMAIL) = UPPER(:value)
        ";

        /// <summary>
        /// Autentica usuário na plataforma através do numero da Conta
        /// </summary>
        public const string AuthenticateLoginWithAccountNumber = @"
            SELECT
                 U.ID
                ,U.CUSTOMERID
                ,U.USERNAME
                ,U.NAME
                ,U.BIRTHDATE
                ,U.EMAIL
                ,U.ACCOUNTNUMBER
                ,U.ELETRONICSIGNATURE
                ,U.INVALIDLOGINATTEMPTS
                ,U.ENCODEDPASSWORD
                ,UPPER(U.STATUS) AS STATUS
                ,U.CREATIONDATE
                ,U.LASTMODIFICATIONDATE
                ,U.ADVISORCODE
                ,FCM_TOKEN.FCM_TOKEN AS FCMTOKEN
                ,FCM_TOKEN.LAST_ACCESS_DATE AS LASTACCESSDATE
                ,U.PUBLICID
                ,U.ACCESSVERSION
                ,C.ACCOUNTNUMBER AS MAINACCOUNTNUMBER
            FROM
                AUTH_DATA.VW_CUSTOMER_ACCOUNT U 
                LEFT JOIN AUTH.ACCESS_TOKEN FCM_TOKEN ON FCM_TOKEN.CUSTOMER_ID = U.CUSTOMERID 
                JOIN AUTH.VW_CUSTOMER_USERS C ON C.CUSTOMERID = U.CUSTOMERID
            WHERE 
                U.ACCOUNTNUMBER = :value AND U.ISACTIVE = 1";
    }
}