﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
    [ExcludeFromCodeCoverage]
    internal static class PasswordRecoveryStatement
    {
        internal const string GetByHash =
            @"SELECT 
	PR.HASH AS Hash, 
	PR.CREATION_DATE AS CreationDate, 
	PR.EXPIRATION_DATE AS ExpirationDate, 
	PR.CUSTOMERID AS CustomerId 
FROM 
	AUTH.PASSWORD_RESET PR
WHERE PR.HASH = :hash";

        internal const string GetCustomerInfoByCustomerId = @"
            SELECT 
	            NAME,
	            BIRTHDATE,
	            EMAIL,
	            CUSTOMERID
            FROM 
	            AUTH.VW_CUSTOMER_USERS 
            WHERE 
	            CUSTOMERID = :customerId

        ";

        internal const string UpdateExpirationDate = @"
            UPDATE AUTH.PASSWORD_RESET
             SET EXPIRATION_DATE = :ExpirationDate
            WHERE HASH = :Hash
        ";
    }
}
