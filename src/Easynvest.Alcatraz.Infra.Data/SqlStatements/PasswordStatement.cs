﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
    [ExcludeFromCodeCoverage]
    internal static class PasswordStatement
    {
        internal const string GetClientPassword = @"
			SELECT 
				PASSWORDS.CUSTOMERID AS CustomerId, 
				PASSWORD AS EncodedPassword, 
				INVALIDLOGINATTEMPTS AS LoginAttempts,
				INVALIDLOGINATTEMPTS AS CurrentLoginAttempts,
				PASSWORDVERSION as Version,
				ACCESS_TOKEN.FCM_TOKEN AS FcmToken,
				ACCESS_TOKEN.LAST_ACCESS_DATE AS LastAccessData,
				SALT.SALT as Salt,
				SALT.CREATIONDATE as CreationDate
			FROM 
				AUTH.PASSWORDS PASSWORDS 
			LEFT JOIN
				AUTH.ACCESS_TOKEN ACCESS_TOKEN 
					ON ACCESS_TOKEN.CUSTOMER_ID = PASSWORDS.CUSTOMERID
			LEFT JOIN 
				AUTH.SECRET_SALTS SALT
					ON SALT.CUSTOMERID = PASSWORDS.CUSTOMERID AND SALT.SECRETTYPE = :secretType
			WHERE 
				PASSWORDS.CUSTOMERID = :customerId
    ";

        internal const string GetClientEletronicSignature = @"
			SELECT 
				PASSWORDS.CUSTOMERID AS CustomerId, 
				ELETRONICSIGNATURE AS EncodedPassword, 
				INVALIDSIGNATUREATTEMPTS AS LoginAttempts,
				INVALIDSIGNATUREATTEMPTS AS CurrentLoginAttempts,
				ELETRONICSIGNATUREVERSION as Version,
				ACCESS_TOKEN.FCM_TOKEN AS FcmToken,
				ACCESS_TOKEN.LAST_ACCESS_DATE AS LastAccessData,
				SALT.SALT as Salt,
				SALT.CREATIONDATE as CreationDate
			FROM
				AUTH.PASSWORDS PASSWORDS 
			LEFT JOIN AUTH.ACCESS_TOKEN ACCESS_TOKEN
				ON ACCESS_TOKEN.CUSTOMER_ID = PASSWORDS.CUSTOMERID 
			LEFT JOIN 
				AUTH.SECRET_SALTS SALT
					ON SALT.CUSTOMERID = PASSWORDS.CUSTOMERID AND SALT.SECRETTYPE = :secretType
			WHERE 
					PASSWORDS.CUSTOMERID = :customerId
		";

        internal const string UpdatePasswordCustomerSecret = @"
			UPDATE
				AUTH.PASSWORDS 
			SET
				PASSWORD = :EncodedPassword, 
				INVALIDLOGINATTEMPTS = :LoginAttempts 
			WHERE
				CUSTOMERID = :customerId
		";

        internal const string UpdateEletronicSignatureCustomerSecret = @"
			UPDATE
				AUTH.PASSWORDS
			SET
				ELETRONICSIGNATURE = :EncodedPassword, 
				INVALIDSIGNATUREATTEMPTS = :LoginAttempts 
			WHERE 
				CUSTOMERID = :customerId
		";

        internal const string DeletePasswordHistory = @"
			DELETE FROM 
				AUTH.PASSWORD_HISTORY 
			WHERE 
				CUSTOMERID = :CUSTOMERID
		";

        internal const string GetPasswordsClientsByCustomerId = @"
			SELECT
				PASS.PASSWORD AS ENCODEDPASSWORD,
				PASS.ELETRONICSIGNATURE AS ENCODEDELETRONICSIGNATURE,
				PASS.INVALIDLOGINATTEMPTS AS INVALIDLOGINATTEMPTS,
				PASS.INVALIDSIGNATUREATTEMPTS AS INVALIDSIGNATUREATTEMPTS,
				PASS.PASSWORDVERSION,
				PASS.ELETRONICSIGNATUREVERSION
			FROM
				AUTH.PASSWORDS PASS
			WHERE
				PASS.CUSTOMERID = :CUSTOMERID
		";

        internal const string GetSalt = @"
			SELECT SECRETTYPE,
				SALT,
				CREATIONDATE FROM 
				AUTH.SECRET_SALTS 
			WHERE 
				CUSTOMERID = :CUSTOMERID
				AND SECRETTYPE = :SECRETTYPE
		";

        internal const string GetPasswordHistoryByCustomerId = @"
			SELECT
				HIST.PASSWORD AS ENCODEDPASSWORD,
				HIST.CREATIONDATE AS CREATIONDATE,
				HIST.TYPE
			FROM
				AUTH.PASSWORD_HISTORY HIST        
			WHERE
				HIST.CUSTOMERID = :CUSTOMERID 
			AND
				HIST.TYPE = :TYPE
		";

        internal const string GetCustomerDataByCustomerId = @"
			SELECT 
				USR.NAME AS NAME,
				USR.BIRTHDATE AS BIRTHDATE,
				USR.CUSTOMERID AS CUSTOMERID,
				USR.EMAIL AS EMAIL,
				UPPER(USR.STATUS) AS STATUS,
				TOKEN.FCM_TOKEN AS FCMTOKEN
			FROM 
				AUTH.VW_CUSTOMER_USERS USR 
			LEFT JOIN AUTH.ACCESS_TOKEN TOKEN 
				ON USR.CUSTOMERID = TOKEN.CUSTOMER_ID        
			WHERE 
				USR.CUSTOMERID = :CUSTOMERID
		";

        internal const string UpdateCustomerSecret = @"
			UPDATE
				AUTH.PASSWORDS
			SET
				PASSWORD = :PASSWORD,
				ELETRONICSIGNATURE = :ELETRONICSIGNATURE, 
				INVALIDLOGINATTEMPTS = :INVALIDLOGINATTEMPTS,
				INVALIDSIGNATUREATTEMPTS = :INVALIDSIGNATUREATTEMPTS,
				PASSWORDVERSION = :PASSWORDVERSION,
				ELETRONICSIGNATUREVERSION = :ELETRONICSIGNATUREVERSION
			WHERE
				CUSTOMERID = :CUSTOMERID
		";

        internal const string InsertCustomerPassword = @"
			INSERT INTO 
				AUTH_DATA.PASSWORDS
				(
					CUSTOMERID,
					PASSWORD,
					PASSWORDVERSION,
					INVALIDLOGINATTEMPTS
				)
			VALUES
				(
					:CUSTOMERID,
					:PASSWORD,
					:PASSWORDVERSION,
					:INVALIDLOGINATTEMPTS
				)";

        internal const string DeleteCustomerPassword = @"
			DELETE FROM
				AUTH_DATA.PASSWORDS
				WHERE CUSTOMERID = :CUSTOMERID";

        internal const string UpdateOrInsertSalt = @"
            MERGE INTO
	            AUTH.SECRET_SALTS T1
            USING (
                	SELECT
		                :customerId CUSTOMERID,
		                :secretType SECRETTYPE
	                FROM
		                DUAL
                    ) T2 
            ON
	                (
                        T1.CUSTOMERID = T2.CUSTOMERID AND
                        T1.SECRETTYPE = T2.SECRETTYPE
                    )
	        WHEN MATCHED THEN
                UPDATE SET
                    T1.SALT = :salt,
	                T1.CREATIONDATE = :creationDate
                WHERE
	                T1.CUSTOMERID = :customerId AND
                    T1.SECRETTYPE = :secretType
	        WHEN NOT MATCHED THEN
                INSERT
	                (
                        T1.CUSTOMERID, 
                        T1.SECRETTYPE, 
                        T1.SALT, 
                        T1.CREATIONDATE
                    )
                VALUES 
                    (
                        :customerId,
                        :secretType,
                        :salt,
                        :creationDate
                    )
        ";

        internal const string InsertCustomerHistoryPassword = @"
			INSERT INTO
				AUTH.PASSWORD_HISTORY
				(
					ID,
					CUSTOMERID,
					PASSWORD,
					CREATIONDATE,
					TYPE,	
					SALT
				)
			VALUES
				(
					AUTH_DATA.SEQ_PASSWORD_HISTORY.NEXTVAL,
					:CUSTOMERID,
					:PASSWORD,
					:CREATIONDATE,
					:TYPE,
					:SALT
				)
		";

        internal const string InsertResetSecretRecovery = @"
			INSERT INTO
				AUTH.PASSWORD_RESET
				(
					HASH,
					CUSTOMERID,
					CREATION_DATE,
					EXPIRATION_DATE
				)
			VALUES
			(
				:hash,
				:customerId,
				:creationDate,
				:expirationDate
			)
		";

        internal const string UpdateFcmTokenCustomerSecret = @"
			MERGE INTO 
				AUTH.ACCESS_TOKEN T1
			USING
				(
					SELECT
						:customerId CUSTOMER_ID 
					FROM
						DUAL
				) T2
			ON 
				(T1.CUSTOMER_ID = T2.CUSTOMER_ID)
			WHEN MATCHED THEN
				UPDATE SET 
					T1.FCM_TOKEN = :fcmToken, 
					T1.LAST_ACCESS_DATE = :lastAccessDate
				WHERE
					T1.CUSTOMER_ID = :customerId
			WHEN NOT MATCHED THEN
				INSERT
					(
						T1.CUSTOMER_ID,
						T1.FCM_TOKEN,
						T1.LAST_ACCESS_DATE
					)
				VALUES
					(
						:customerId,
						:fcmToken,
						:lastAccessDate
					)
		";


        internal const string GetPasswordBlacklist = @"
			SELECT
				PASSWORD
			FROM
				AUTH.PASSWORD_BLACKLIST
		";
    }
}
