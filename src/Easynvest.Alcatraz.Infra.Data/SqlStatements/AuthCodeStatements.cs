﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
	[ExcludeFromCodeCoverage]
    internal static class AuthCodeStatements
    {
        internal const string InsertCustomerPassword = @"
			INSERT INTO
				AUTH.AUTH_CODES
				(
					CUSTOMERID,
					CODE,
					CREATIONDATE,
					EXPIRATIONDATE
				)
			VALUES
				(
					:CUSTOMERID,
					:CODE,
					:CREATIONDATE,
					:EXPIRATIONDATE
				)
		";

        internal const string GetLatestCode = @"
			SELECT CUSTOMERID, CODE, CREATIONDATE, EXPIRATIONDATE FROM
			(
				SELECT
					CUSTOMERID,
					CODE,
					CREATIONDATE,
					EXPIRATIONDATE
				FROM
					AUTH.AUTH_CODES
				WHERE
					CUSTOMERID = :CUSTOMERID
				ORDER BY CREATIONDATE DESC
			)
			WHERE ROWNUM = 1
		";
    }
}