﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
	[ExcludeFromCodeCoverage]
	internal static class LegacyStatements
	{
		internal const string UpdateSignatureLegacy = @"
			UPDATE
				REGS.CUSTOMER_USERS
			SET 
				ELETRONICSIGNATURE = :ELETRONICSIGNATURE,
				INVALIDLOGINATTEMPTS = 0
			WHERE
				CUSTOMERID = :CUSTOMERID
		";

		internal const string UpdatePasswordLegacy = @"
			UPDATE
				REGS.CUSTOMER_USERS
			SET 
				PASSWORD = :PASSWORD,
				INVALIDLOGINATTEMPTS = 0
			WHERE
				CUSTOMERID = :CUSTOMERID
		";
	}
}
