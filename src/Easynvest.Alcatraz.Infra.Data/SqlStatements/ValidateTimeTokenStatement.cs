﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
    [ExcludeFromCodeCoverage]
    internal static class ValidateTimeTokenStatement
    {
        private const string DefaultQuery = "SELECT CUSTOMER_ID AS CustomerId, DEVICE_UID AS DeviceUid, EXPIRY_TIME AS ExpiryTime, CONFIRMED_TRUSTED_DEVICE AS ConfirmedTrustedDevice FROM AUTH.VALIDATE_TIME_TOKEN";

        internal static readonly string GetByDeviceUid = $"{DefaultQuery} WHERE DEVICE_UID = :DeviceUid AND CUSTOMER_ID = :CustomerId";

        internal static readonly string RegisterDevice = "INSERT INTO AUTH.VALIDATE_TIME_TOKEN (CUSTOMER_ID, DEVICE_UID, EXPIRY_TIME, CONFIRMED_TRUSTED_DEVICE) VALUES (:CustomerId, :DeviceUid, :ExpiryTime, :ConfirmedTrustedDevice)";

        internal static readonly string UpdateDevice = @"UPDATE AUTH.VALIDATE_TIME_TOKEN SET 
            EXPIRY_TIME = :ExpiryTime,
            CONFIRMED_TRUSTED_DEVICE = :ConfirmedTrustedDevice
            WHERE
                CUSTOMER_ID = :CustomerId AND DEVICE_UID = :DeviceUid";
    }
}