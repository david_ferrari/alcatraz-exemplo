﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.SqlStatements
{
    [ExcludeFromCodeCoverage]
    internal static class EmailStetament
    {

        internal const string GetEmailById = @"
            SELECT 
                ID,
                CONTENT, 
                ""FROM"", 
                SUBJECT 
            FROM 
                AUTH.VW_TCV_EMAILS
            WHERE 
                ID = :emailId
                AND IC_ATIVO = 1
        ";
    }
}
