﻿namespace Easynvest.Alcatraz.Infra.Data.Enumerators
{
    public enum QueueMessageType : byte
    {
        PlainText = 1,
        Object = 2
    }

    public enum QueueMessageFormatter : byte
    {
        Xml = 1,
        Binary = 2
    }
}
