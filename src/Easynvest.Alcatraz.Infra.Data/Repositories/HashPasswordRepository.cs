﻿using Dapper;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    [ExcludeFromCodeCoverage]
    public class HashPasswordRepository : IHashPasswordRepository
    {
        private readonly ILogger<HashPasswordRepository> _logger;
        private readonly IRepository _repository;

        public HashPasswordRepository(IRepository repository, ILogger<HashPasswordRepository> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task Create(PasswordRecoveryRequest passwordRecoveryRequest)
        {
            try
            {
                using var conn = _repository.CreateOracleConnection();
                await conn.ExecuteAsync(PasswordStatement.InsertResetSecretRecovery, new
                {
                    hash = passwordRecoveryRequest.Hash,
                    customerId = passwordRecoveryRequest.Customer.CustomerId,
                    creationDate = passwordRecoveryRequest.CreationDate,
                    expirationDate = passwordRecoveryRequest.ExpirationDate.Value
                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao criar a recuperação de senha/assinatura eletrônica do usuário: {passwordRecoveryRequest.Customer.CustomerId}");
                throw new InvalidOperationException("Erro ao criar a recuperação de senha/assinatura eletrônica do usuário", ex);
            }
        }

        public async Task<PasswordRecoveryRequest> GetByHash(string hash)
        {
            using var conn = _repository.CreateOracleConnection();
            var resultGet = await conn.QueryAsync<dynamic>(PasswordRecoveryStatement.GetByHash, new { hash });

            if (resultGet == null || !resultGet.Any())
                return null;

            var pwdRecovery = resultGet.First();

            return PasswordRecoveryRequest.Create(new Customer((string)pwdRecovery.CUSTOMERID, null, string.Empty, string.Empty),
                                                 new ExpirationDate((DateTime)pwdRecovery.EXPIRATIONDATE),
                                                 (DateTime)pwdRecovery.CREATIONDATE,
                                                 (string)pwdRecovery.HASH);
        }

        public async Task UpdateExpirationDate(PasswordRecoveryRequest passwordRecoveryRequest)
        {
            using var connection = _repository.CreateOracleConnection();
            await connection.ExecuteAsync(PasswordRecoveryStatement.UpdateExpirationDate,
                    new
                    {
                        Hash = passwordRecoveryRequest.Hash,
                        ExpirationDate = passwordRecoveryRequest.ExpirationDate.Value,
                    }).ConfigureAwait(false);
        }
    }
}