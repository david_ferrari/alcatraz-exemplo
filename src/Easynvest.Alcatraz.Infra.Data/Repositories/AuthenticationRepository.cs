﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Easynvest.Alcatraz.CrossCutting;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly AuthOptions _authOptions;
        private readonly ILogger<AuthenticationRepository> _logger;
        private readonly Stopwatch _stopWatch;
        protected Dictionary<string, string> _contentHeader = new Dictionary<string, string>() { { "Content-Type", "application/x-www-form-urlencoded" } };

        public AuthenticationRepository(IHttpClientFactory clientFactory, 
            ILogger<AuthenticationRepository> logger, 
            IOptions<AuthOptions> authOptions)
        {
            _clientFactory = clientFactory;
            _authOptions = authOptions.Value;
            _logger = logger;
            _stopWatch = new Stopwatch();
        }

        public async Task<AuthenticationDto> Authenticate(string userName, string password, string clientId)
        {
            try
            {
                _logger.LogExInformation($"[Alcatraz][AuthenticationRepository] - Iniciando autenticação do usuário. [UserName: {userName} - ClientId: {clientId}]");

                var endpoint = $"{_authOptions.AuthenticateEndpoint}";

                using var _client = _clientFactory.CreateClient("auth");
                using var request = new HttpRequestMessage(HttpMethod.Post, endpoint)
                {
                    Content = new FormUrlEncodedContent(GetBodyCustomerById(userName, password, clientId))
                };                
                _logger.LogExDebug($"EndPoint Auth Post: ({_client.BaseAddress}{endpoint}), Cliente: ({clientId})");

                _stopWatch.Start();
                var response = await _client.SendAsync(request);
                _stopWatch.Stop();

                _logger.LogExDebug($"End of (POST -> {_client.BaseAddress}{endpoint}) request");

                if (!(response?.IsSuccessStatusCode ?? false))
                {
                    _logger.LogExWarning($"[Alcatraz][AuthenticationRepository] - Não foi possível realizar a autenticação com o usuario { userName }. Url: {_client.BaseAddress}{endpoint} HttpStatusCode: { response?.StatusCode }");
                    return default(AuthenticationDto);
                }

                if (response.IsSuccessStatusCode)
                {
                    using (var contentResponse = await response.Content.ReadAsStreamAsync())
                    {
                        var serialize = Newtonsoft.Json.JsonSerializer.CreateDefault();
                        using var streamReader = new StreamReader(contentResponse, Encoding.UTF8);
                        using var jsonReader = new Newtonsoft.Json.JsonTextReader(streamReader);

                        AuthenticationDto authenticationDto = serialize.Deserialize<AuthenticationDto>(jsonReader);
                        return authenticationDto;
                    }
                }

                return default(AuthenticationDto);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Ocorreu um erro ao se comunicar com a API auth");
                throw;
            }
        }

        private List<KeyValuePair<string, string>> GetBodyCustomerById(string userName, string password, string clientId)
        {
            return new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("userName", userName),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("client_id", clientId),
                new KeyValuePair<string, string>("grant_type", "password")
            };
        }
    }
}
