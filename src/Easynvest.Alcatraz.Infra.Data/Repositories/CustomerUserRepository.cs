﻿using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    [ExcludeFromCodeCoverage]
    public class CustomerUserRepository : IAuthenticateCustomerRepository
    {
        private readonly ILogger<CustomerRepository> _logger;
        private readonly IRepository _repository;

        public CustomerUserRepository(IRepository repository, ILogger<CustomerRepository> logger)            
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<IEnumerable<CustomerUserDto>> GetAuthenticatedCustomer(string value, CustomerLoginType customerLoginType)
        {
            var customerReturn = new List<CustomerUserDto>();

            try
            {
                var sql = GetCustomerLoginSqlStatementByUsername(customerLoginType);

                using var conn = _repository.CreateOracleConnection();
                var ret = await conn.QueryAsync<CustomerUserDto>(true, sql, new { value });

                if (ret.Any())
                    customerReturn = ret.ToList();
            }
            catch (OracleException oex)
            {
                _logger.LogExError(oex, "Erro ao processar operação no banco de dados. {CustomerId} {LoginType}", value, customerLoginType);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Erro ao processar operação no banco de dados. {CustomerId} {LoginType}", value, customerLoginType);
                throw;
            }

            return customerReturn;
        }

        private static string GetCustomerLoginSqlStatementByUsername(CustomerLoginType loginType)
        {
            var sqlStatement = SqlAuthCustomerUsers.AuthenticateLoginWithCustomizableUsername;

            if (CustomerLoginType.CustomerLoginDocument.Equals(loginType))
                sqlStatement = SqlAuthCustomerUsers.AuthenticateLoginWithCpf;

            if (CustomerLoginType.CustomerLoginEmail.Equals(loginType))
                sqlStatement = SqlAuthCustomerUsers.AuthenticateLoginWithEmail;

            if (CustomerLoginType.CustomerLoginAccountNumber.Equals(loginType))
                sqlStatement = SqlAuthCustomerUsers.AuthenticateLoginWithAccountNumber;

            return sqlStatement;
        }
    }
}
