﻿using Dapper;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    [ExcludeFromCodeCoverage]
    public class ValidateTimeTokenRepository : IValidateTimeTokenRepository
    {
        private readonly ILogger<PasswordRepository> _logger;
        private readonly IRepository _repository;

        public ValidateTimeTokenRepository(IRepository repository, ILogger<PasswordRepository> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<ValidateTimeToken> GetValidateTimeTokenByDeviceUid(string deviceUid, string customerId)
        {
            try
            {
                using var connection = _repository.CreateOracleConnection();
                var validateTimeToken = await connection
                    .QueryFirstOrDefaultAsync<ValidateTimeTokenDto>
                    (
                        true,
                        ValidateTimeTokenStatement.GetByDeviceUid,
                        new
                        {
                            DeviceUid = deviceUid,
                            CustomerId = customerId
                        }
                    );

                return validateTimeToken == null ? default(ValidateTimeToken) : new ValidateTimeToken(validateTimeToken.CustomerId, validateTimeToken.DeviceUid, validateTimeToken.ExpiryTime, validateTimeToken.ConfirmedTrustedDevice);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao recuperar os dados para o deviceUid {deviceUid}");
                throw;
            }
        }

        public async Task RegisterTimeToken(ValidateTimeToken validateTimeToken)
        {
            try
            {
                using var connection = _repository.CreateOracleConnection();
                await connection
                    .ExecuteAsync
                    (
                        ValidateTimeTokenStatement.RegisterDevice,
                        new
                        {
                            validateTimeToken.CustomerId,
                            validateTimeToken.DeviceUid,
                            validateTimeToken.ExpiryTime,
                            ConfirmedTrustedDevice = (int)validateTimeToken.ConfirmedTrustedDevice
                        }
                    );
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao inserir os dados do device {validateTimeToken.DeviceUid} para o cliente {validateTimeToken.CustomerId}");
                throw;
            }
        }

        public async Task UpdateTimeToken(ValidateTimeToken validateTimeToken)
        {
            try
            {
                using var connection = _repository.CreateOracleConnection();
                await connection
                    .ExecuteAsync
                    (
                        ValidateTimeTokenStatement.UpdateDevice,
                        new
                        {
                            validateTimeToken.CustomerId,
                            validateTimeToken.DeviceUid,
                            validateTimeToken.ExpiryTime,
                            ConfirmedTrustedDevice = (int)validateTimeToken.ConfirmedTrustedDevice
                        }
                    );
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao atualizar os dados do device {validateTimeToken.DeviceUid} para o cliente {validateTimeToken.CustomerId}");
                throw;
            }
        }
    }
}