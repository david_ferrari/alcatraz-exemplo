﻿using Easynvest.Alcatraz.CrossCutting.Policies;
using Easynvest.Alcatraz.CrossCutting.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    public static class SqlMapperQueryPolly
    {
        private static IPolicyRulesService _policyRulesService = ServiceActivator.GetService<IPolicyRulesService>();

        public static async Task<IEnumerable<T>> QueryAsync<T>(this IDbConnection cnn, bool withPolly, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            var taskQuery = Dapper.SqlMapper.QueryAsync<T>(cnn, sql, param, transaction, commandTimeout, commandType);

            if (!withPolly)
                return await taskQuery;

            var policy = _policyRulesService.GetPolicesOracleWithoutContext($"{sql.Substring(0, 15)} with params {System.Text.Json.JsonSerializer.Serialize(param)}");
            return await policy.ExecuteAsync(() => taskQuery);
        }

        public static async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(this IDbConnection cnn, bool withPolly, string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        {
            var taskQuery = Dapper.SqlMapper.QueryAsync<TFirst, TSecond, TThird, TReturn>(cnn, sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

            if (!withPolly)
                return await taskQuery;

            var policy = _policyRulesService.GetPolicesOracleWithoutContext($"{sql.Substring(0, 15)} with params {System.Text.Json.JsonSerializer.Serialize(param)}");
            return await policy.ExecuteAsync(() => taskQuery);
        }

        public static async Task<T> QueryFirstOrDefaultAsync<T>(this IDbConnection cnn, bool withPolly, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            var taskQuery = Dapper.SqlMapper.QueryFirstOrDefaultAsync<T>(cnn, sql, param, transaction, commandTimeout, commandType);
            if (!withPolly)
                return await taskQuery;

            var policy = _policyRulesService.GetPolicesOracleWithoutContext($"{sql.Substring(0, 15)} with params {System.Text.Json.JsonSerializer.Serialize(param)}");            
            return await policy.ExecuteAsync(() => taskQuery);
        }
    }
}
