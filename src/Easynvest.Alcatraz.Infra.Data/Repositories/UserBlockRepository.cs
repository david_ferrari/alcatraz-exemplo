﻿using Dapper;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    [ExcludeFromCodeCoverage]
    public class UserBlockRepository : IUserBlockRepository
    {
        private readonly IRepository _repository;

        public UserBlockRepository(IRepository repository)               
        {
            _repository = repository;
        }

        public async Task<IReadOnlyCollection<UserBlock>> Get(string customerId)
        {
            using var connection = _repository.CreateOracleConnection();
            var result = await connection.QueryAsync<UserBlock>(true, BlockStatements.GetUserBlocks, new { customerId });
            return result.ToList().AsReadOnly();
        }

        public async Task<IReadOnlyCollection<UserBlock>> Get(BlockType blockType)
        {
            using var connection = _repository.CreateOracleConnection();
            var result = await connection.QueryAsync<UserBlock>(true, BlockStatements.GetBlocksByType, new { blockType });
            return result.ToList().AsReadOnly();
        }

        public async Task Add(UserBlock user)
        {
            using var connection = _repository.CreateOracleConnection();
            await connection.ExecuteAsync(BlockStatements.InsertUserBlock, new { user.CustomerId, user.BlockDate, user.BlockType });
        }

        public async Task Remove(string customerId, BlockType blockType)
        {
            using var connection = _repository.CreateOracleConnection();
            await connection.ExecuteAsync(BlockStatements.RemoveUserBlock, new { customerId, blockType });
        }
    }
}
