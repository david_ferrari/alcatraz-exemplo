﻿using Easynvest.Alcatraz.Infra.Data.Options;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using Polly;
using Serilog;
using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    [ExcludeFromCodeCoverage]

    public class Repository: IRepository
    {        
        private static Lazy<IOptions<ConnectionStringOptions>> ConnectionStringOptions { get; set; }

        public Repository(IOptions<ConnectionStringOptions> connectionString)
        {
            if(ConnectionStringOptions == null || !ConnectionStringOptions.IsValueCreated)
                ConnectionStringOptions = new Lazy<IOptions<ConnectionStringOptions>>(connectionString);
        }

        public IDbConnection CreateOracleConnection()
            => 
            Policy
            .Handle<OracleException>()
            .WaitAndRetry(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(2),
                TimeSpan.FromSeconds(5)
            }, (result, timeSpan, retryCount, context) =>
            {
                Log.Warning("Tentanto reconectar ao banco {TimeSpan}, tentativa {RetryCount}", timeSpan, retryCount);
            })
            .Wrap(
                Policy.Handle<OracleException>()
                .CircuitBreaker(
                    3,
                    TimeSpan.FromSeconds(5),
                    (exception, timespan, context) =>
                    {
                        Log.Warning(exception, "CircuitBreaker: o banco está fora!");
                    },
                    (context) =>
                    {
                        Log.Warning("CircuitBreaker: o banco está de volta!");
                    }
                ))           
            .Execute(() => new OracleConnection(ConnectionStringOptions?.Value?.Value?.ConnectionString ?? string.Empty));       
    }
}