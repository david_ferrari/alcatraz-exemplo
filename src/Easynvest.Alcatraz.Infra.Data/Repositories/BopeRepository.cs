﻿using Easynvest.Alcatraz.CrossCutting.Exceptions;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Dtos;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.ValueObjects;
using Easynvest.Alcatraz.Infra.Data.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    public class BopeRepository : IBopeRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ILogger<BopeRepository> _logger;
        private readonly Stopwatch _stopWatch;

        public BopeRepository(IHttpClientFactory clientFactory,
            ILogger<BopeRepository> logger)
        {
            _clientFactory = clientFactory;
            _logger = logger;
            _stopWatch = new Stopwatch();
        }

        public async Task<bool> Authenticate(string requestId, string accountId, string otpCode, string deviceUid, BopeIntegrationType integration)
        {
            try
            {
                var endpoint = "v1/authenticate";
              
                using var _client = _clientFactory.CreateClient("bope");                
                using var request = new HttpRequestMessage(HttpMethod.Post, endpoint)
                {
                    Content = new StringContent(
                     JsonSerializer.Serialize(new BopeAuthenticateRequest(accountId, otpCode, deviceUid, integration)),
                     Encoding.UTF8, "application/json")
                };
                request.Headers.Add("x-requestid", requestId);

                _logger.LogExDebug($"[{requestId} {accountId}] Iniciando requisição para {_client.BaseAddress}{endpoint}.");

                _stopWatch.Start();
                var result = await _client.SendAsync(request);
                _stopWatch.Stop();

                _logger.LogExDebug("[{requestId} {accountId}] End of (POST -> {baseAddress}{endpoint}) request in {totalMilliseconds}ms. StatusCode: {statusCode}.",
                                    requestId, accountId, _client.BaseAddress, 
                                    endpoint, _stopWatch.Elapsed.TotalMilliseconds, (int)result.StatusCode);

                return result.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"[{requestId} {accountId}] Ocorreu um erro ao se comunicar com a API Bope");
                throw;
            }
        }

        public async Task<DeviceRegisteredDto> GetTokenInfo(string requestId, string accountId)
        {
            var endpoint = $"v1/devices/{accountId}";
            try
            {
                using var _client = _clientFactory.CreateClient("bope");
                using var request = new HttpRequestMessage(HttpMethod.Get, endpoint);
                request.Headers.Add("x-requestid", requestId);

                _logger.LogExDebug($"[{{RequestId}} {{CustomerId}}] Iniciando requisição para {_client.BaseAddress}{endpoint}.", requestId, accountId);

                _stopWatch.Start();
                var result = await _client.SendAsync(request);
                _stopWatch.Stop();
                _logger.LogExDebug($"[{{RequestId}} {{CustomerId}}] End of (GET -> {_client.BaseAddress}{endpoint}) request in {_stopWatch.Elapsed.TotalMilliseconds}ms. StatusCode: {{StatusCode}}.",
                                    requestId, accountId, (int)result.StatusCode);

                if (result.IsSuccessStatusCode)
                {
                    using (var contentResponse = await result.Content.ReadAsStreamAsync())
                    {
                        var serialize = Newtonsoft.Json.JsonSerializer.CreateDefault();
                        using var streamReader = new StreamReader(contentResponse, Encoding.UTF8);
                        using var jsonReader = new Newtonsoft.Json.JsonTextReader(streamReader);

                        ResultDeviceDto authenticateCustomerResponse = serialize.Deserialize<ResultDeviceDto>(jsonReader);

                        return authenticateCustomerResponse?.Value;
                    }
                }
                else if (result.StatusCode >= HttpStatusCode.InternalServerError)
                {
                    throw new BopeApiCallException();
                }

                _logger.LogExInformation("[{RequestId}] Dispositivo não encontrado para o usuário: {CustomerId}", requestId, accountId);

                return null;
            }
            catch (Exception ex) when (!(ex is BopeApiCallException))
            {
                _logger.LogExError(ex, "[{RequestId} {CustomerId}] Ocorreu um erro ao se comunicar com a API Bope, login será realizado sem a validação do MFA.", requestId, accountId);
                return null;
            }
        }
    }

    internal class ResultDeviceDto
    {
        public DeviceRegisteredDto Value { get; set; }
    }
}