﻿using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    [ExcludeFromCodeCoverage]
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IRepository _repository;

        public CustomerRepository(IRepository repository)             
        {
            _repository = repository;
        }

        public async Task<Customer> GetCustomerInfoByCustomerId(string customerId)
        {
            using (var conn = _repository.CreateOracleConnection())
            {
                return await conn.QueryFirstOrDefaultAsync<Customer>(true, PasswordStatement.GetCustomerDataByCustomerId,
                        new { customerId }).ConfigureAwait(false);
            }
        }
    }
}
