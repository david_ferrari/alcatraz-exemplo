﻿using Dapper;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Domain.Services;
using Easynvest.Alcatraz.Infra.Data.Repositories;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data
{
    [ExcludeFromCodeCoverage]
    public class PasswordRepository : IPasswordRepository
    {
        private const int INITIAL_INVALID_LOGIN_ATTEMPTS = 0;
        private readonly ILogger<PasswordRepository> _logger;
        private readonly IRepository _repository;

        public PasswordRepository(IRepository repository, ILogger<PasswordRepository> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<Password> GetByCustomerId(string customerId, string password)
        {
            try
            {
                var result = default(Password);
                var customer = default(Customer);

                using var connection = _repository.CreateOracleConnection();
                customer = await connection
                    .QueryFirstOrDefaultAsync<Customer>
                    (
                        true,
                        PasswordStatement.GetCustomerDataByCustomerId,
                        new { customerId }
                    );

                var encodedPassword = EncodePasswords.EncodeString(password);
                var passwordHistory = new List<PasswordHistory>();
                var electronicSignatureHistory = new List<PasswordHistory>();
                var passwordDetails = new PasswordDetails
                    (
                        encodedPassword,
                        string.Empty,
                        INITIAL_INVALID_LOGIN_ATTEMPTS,
                        INITIAL_INVALID_LOGIN_ATTEMPTS,
                        null,
                        null
                    );

                result = new Password(customer, passwordHistory, electronicSignatureHistory, passwordDetails);
                result.Blacklist = await GetBlacklist();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao recuperar os dados para criação da senha do cliente {customerId}");
                throw;
            }
        }

        public async Task<Password> GetByCustomerId(string customerId)
        {
            try
            {
                var result = default(Password);

                using var connection = _repository.CreateOracleConnection();
                var passwordDetails = connection
                    .QueryFirstOrDefaultAsync<PasswordDetails>
                    (
                        true,
                        PasswordStatement.GetPasswordsClientsByCustomerId,
                        new { customerId }
                    );

                var salt = connection.QueryFirstOrDefaultAsync<SaltData>(true, PasswordStatement.GetSalt, new
                {
                    customerId,
                    secretType = SecretType.Password
                });
                var eletronicSalt = connection.QueryFirstOrDefaultAsync<SaltData>(true, PasswordStatement.GetSalt, new
                {
                    customerId,
                    secretType = SecretType.EletronicSignature
                });

                var passwordHistory = connection
                    .QueryAsync<PasswordHistory>
                    (
                        PasswordStatement.GetPasswordHistoryByCustomerId,
                        new
                        {
                            customerId,
                            Type = (int)SecretType.Password
                        }
                    );

                var electronicSignatureHistory = connection
                    .QueryAsync<PasswordHistory>
                    (
                        PasswordStatement.GetPasswordHistoryByCustomerId,
                        new
                        {
                            customerId,
                            Type = (int)SecretType.EletronicSignature
                        }
                    );

                var customer = connection
                    .QueryFirstOrDefaultAsync<Customer>
                    (
                        true,
                        PasswordStatement.GetCustomerDataByCustomerId,
                        new { customerId }
                    );

                await Task.WhenAll
                    (
                        passwordDetails,
                        salt,
                        eletronicSalt,
                        passwordHistory,
                        electronicSignatureHistory,
                        customer
                    );

                result = new Password
                    (
                        await customer,
                        await passwordHistory,
                        await electronicSignatureHistory,
                        await passwordDetails
                    );

                if (result.PasswordDetails != null)
                {
                    result.PasswordDetails.PasswordSalt = await salt;
                    result.PasswordDetails.EletronicSignatureSalt = await eletronicSalt;
                }

                result.Blacklist = await GetBlacklist();

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao recuperar os dados de senha do cliente {customerId}");
                throw;
            }
        }

        private async Task<PasswordBlacklist> GetBlacklist()
        {
            using var connection = _repository.CreateOracleConnection();
            var result = await connection.QueryAsync<string>(PasswordStatement.GetPasswordBlacklist);
            return new PasswordBlacklist(result.ToList());
        }
        public async Task InsertPassword(Password password)
        {
            try
            {
                using var connection = _repository.CreateOracleConnection();
                await connection.ExecuteAsync(PasswordStatement.DeleteCustomerPassword, new { password.Customer.CustomerId });
                await connection
                    .ExecuteAsync
                    (
                        PasswordStatement.InsertCustomerPassword,
                        new
                        {
                            password = password.PasswordDetails.EncodedPassword,
                            invalidLoginAttempts = password.PasswordDetails.InvalidLoginAttempts,
                            customerId = password.Customer.CustomerId,
                            passwordVersion = password.PasswordDetails.PasswordVersion
                        }
                    )
                    .ConfigureAwait(false);

                await UpsertSalt(password.Customer.CustomerId, password.PasswordDetails.PasswordSalt, SecretType.Password);
                await HandlerPasswordHistory(password, connection).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao inserir os dados de nova senha do cliente {password.ToString()}. Exception: {ex.Message}");
                throw;
            }
        }

        private async Task UpsertSalt(string customerId, SaltData data, SecretType secretType)
        {
            try
            {
                if (data == null)
                    return;

                using var connection = _repository.CreateOracleConnection();
                await connection.ExecuteAsync(PasswordStatement.UpdateOrInsertSalt, new
                {
                    customerId,
                    secretType,
                    salt = data.Salt,
                    creationDate = data.CreationDate
                });
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"Erro ao inserir os dados de nova senha do cliente {customerId}");
                throw;
            }
        }

        public async Task UpdateSecret(Password password)
        {
            try
            {
                using var connection = _repository.CreateOracleConnection();
                await connection.ExecuteAsync(PasswordStatement.UpdateCustomerSecret, new
                {
                    password = password.PasswordDetails.EncodedPassword,
                    eletronicSignature = password.PasswordDetails.EncodedEletronicSignature,
                    invalidLoginAttempts = password.PasswordDetails.InvalidLoginAttempts,
                    invalidSignatureAttempts = password.PasswordDetails.InvalidSignatureAttempts,
                    customerId = password.Customer.CustomerId,
                    passwordVersion = password.PasswordDetails.PasswordVersion,
                    eletronicSignatureVersion = password.PasswordDetails.EletronicSignatureVersion,
                });

                await UpsertSalt(password.Customer.CustomerId, password.PasswordDetails.PasswordSalt, SecretType.Password);
                await UpsertSalt(password.Customer.CustomerId, password.PasswordDetails.EletronicSignatureSalt, SecretType.EletronicSignature);
                await HandlerPasswordHistory(password, connection).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, $"[{password.Customer.CustomerId}] Erro ao atualizar os dados de senha do cliente {password.ToString()}. Exception: {ex.Message}");
                throw;
            }
        }

        private static async Task HandlerPasswordHistory(Password password, IDbConnection connection)
        {
            await connection
                .ExecuteAsync
                (
                    PasswordStatement.DeletePasswordHistory,
                    new
                    {
                        customerId = password.Customer.CustomerId
                    }
                );

            var lastSecrets = password.LastSixPasswords.Union(password.LastSixElectronicSignatures);

            foreach (var pwd in lastSecrets)
            {
                await connection
                    .ExecuteAsync
                    (
                        PasswordStatement.InsertCustomerHistoryPassword,
                        new
                        {
                            customerId = password.Customer.CustomerId,
                            password = pwd.EncodedPassword,
                            creationDate = pwd.CreationDate,
                            type = pwd.Type,
                            salt = pwd.Salt
                        }
                    );
            }
        }
    }
}
