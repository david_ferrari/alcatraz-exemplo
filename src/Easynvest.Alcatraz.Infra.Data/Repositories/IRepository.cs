﻿using System.Data;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    public interface IRepository
    {
        IDbConnection CreateOracleConnection();
    }
}
