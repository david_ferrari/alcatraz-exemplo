﻿using Dapper;
using Easynvest.Alcatraz.CrossCutting.Extensions;
using Easynvest.Alcatraz.Domain.Entities;
using Easynvest.Alcatraz.Domain.Enumerators;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    [ExcludeFromCodeCoverage]
    public class CustomerSecretRepository : ICustomerSecretRepository
    {
        private readonly ILogger<CustomerSecretRepository> _logger;
        private readonly IRepository _repository;

        public CustomerSecretRepository(ILogger<CustomerSecretRepository> logger, IRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<CustomerSecret> Get(string customerId, SecretType passwordType)
        {
            try
            {
                var result = default(CustomerSecret);
                var sql = GetStatement(passwordType);

                using (var connection = _repository.CreateOracleConnection())
                {
                    var parameter = new { customerId, secretType = passwordType };

                    var ret = await connection.QueryAsync<CustomerSecret, AccessToken, SaltData, CustomerSecret>(true, sql,
                        (customerSecret, accessToken, salt) =>
                        {
                            customerSecret.AccessToken = accessToken;
                            customerSecret.SaltData = salt;
                            return customerSecret;
                        },
                        parameter,
                        splitOn: "FcmToken, Salt");

                    result = ret.FirstOrDefault();
                }

                return result;
            }
            catch (OracleException oex)
            {
                _logger.LogExError(oex, "Erro ao recuperar os dados de senha do cliente {CustomerId}", customerId);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Erro ao recuperar os dados de senha do cliente {CustomerId}", customerId);
                throw;
            }
        }

        private static string GetStatement(SecretType passwordType)
            => passwordType.Equals(SecretType.EletronicSignature)
                ? PasswordStatement.GetClientEletronicSignature
                : PasswordStatement.GetClientPassword;

        private static string GetUpdateStatement(SecretType passwordType) =>
            passwordType.Equals(SecretType.EletronicSignature)
                ? PasswordStatement.UpdateEletronicSignatureCustomerSecret
                : PasswordStatement.UpdatePasswordCustomerSecret;

        public async Task UpdateCustomerSecret(CustomerSecret customerSecret, SecretType type)
        {
            try
            {
                var sql = GetUpdateStatement(type);

                using var connection = _repository.CreateOracleConnection();
                await connection.ExecuteAsync(sql, customerSecret).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Erro ao atualizar os dados de senha do cliente {CustomerId}", customerSecret.CustomerId);

                throw;
            }
        }

        public async Task UpdateFcmToken(CustomerSecret customerSecret)
        {
            try
            {
                var sql = PasswordStatement.UpdateFcmTokenCustomerSecret;
                var parameters = new
                {
                    fcmToken = customerSecret.AccessToken.FcmToken,
                    lastAccessDate = customerSecret.AccessToken.LastAccessData,
                    customerId = customerSecret.CustomerId
                };

                using (var connection = _repository.CreateOracleConnection())
                {
                    await connection.ExecuteAsync(sql, parameters).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                _logger.LogExError(ex, "Erro ao atualizar os dados de fcmToken do cliente {CustomerId}", customerSecret.CustomerId);
                throw;
            }
        }
    }
}
