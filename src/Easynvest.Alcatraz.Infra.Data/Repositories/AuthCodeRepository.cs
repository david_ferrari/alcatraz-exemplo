﻿using Dapper;
using Easynvest.Alcatraz.Domain.Interfaces.Repositories;
using Easynvest.Alcatraz.Domain.Models.Entities;
using Easynvest.Alcatraz.Infra.Data.SqlStatements;
using System.Threading.Tasks;

namespace Easynvest.Alcatraz.Infra.Data.Repositories
{
    public class AuthCodeRepository: IAuthCodeRepository
    {
        private readonly IRepository _repository;

        public AuthCodeRepository(IRepository repository)            
        {
            _repository = repository;
        }

        public async Task InsertCode(AuthCode code)
        {
            using var conn = _repository.CreateOracleConnection();
            await conn.ExecuteAsync(AuthCodeStatements.InsertCustomerPassword, new
            {
                code.CustomerId,
                code.Code,
                code.CreationDate,
                code.ExpirationDate
            });
        }

        public async Task<AuthCode> GetLatestCode(string customerId)
        {
            using var conn = _repository.CreateOracleConnection();
            return await conn.QuerySingleOrDefaultAsync<AuthCode>(AuthCodeStatements.GetLatestCode, new { customerId });
        }
    }
}
