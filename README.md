# Easynvest Alcatraz

Objetivo: Aplicação para gerenciamento de acesso dos usuários.

## Produção
* [Kibana](http://kibana.easynvest.com.br/_plugin/kibana/goto/d476118e2a91b00128e6660d5de68058)
* [Health Check](http://api.easynvest.com.br/alcatraz/health)

## Homologação
* [Kibana](http://kibana-homolog.easynvest.com.br/_plugin/kibana/goto/ff71cff8fbee76cabf0128592cc8d08f)
* [Health Check](http://apis-internal.hom.easynvest.io/alcatraz/apis/health)

### Dependências da aplicação

 - Base de Dados **_Oracle_**
 - API Bope